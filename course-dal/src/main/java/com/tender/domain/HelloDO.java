package com.tender.domain;

import lombok.Data;

@Data
public class HelloDO {

    private String name;

    private Integer age;

    private boolean sex;

}
