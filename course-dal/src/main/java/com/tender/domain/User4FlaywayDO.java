package com.tender.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class User4FlaywayDO implements Serializable {

    private Long id;

    private String name;

    private static final long serialVersionUID = 1L;

}