package com.tender.mapper;

import com.tender.domain.User4FlaywayDO;
import com.tender.domain.User4FlaywayDOExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface User4FlaywayDOMapper {
    int countByExample(User4FlaywayDOExample example);

    int deleteByExample(User4FlaywayDOExample example);

    int deleteByPrimaryKey(Long id);

    int insert(User4FlaywayDO record);

    int insertSelective(User4FlaywayDO record);

    List<User4FlaywayDO> selectByExample(User4FlaywayDOExample example);

    User4FlaywayDO selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") User4FlaywayDO record, @Param("example") User4FlaywayDOExample example);

    int updateByExample(@Param("record") User4FlaywayDO record, @Param("example") User4FlaywayDOExample example);

    int updateByPrimaryKeySelective(User4FlaywayDO record);

    int updateByPrimaryKey(User4FlaywayDO record);
}