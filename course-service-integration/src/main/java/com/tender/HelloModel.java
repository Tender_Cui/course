package com.tender;

import lombok.Data;

@Data
public class HelloModel {

    private String name;

    private Integer age;

    private boolean sex;
}
