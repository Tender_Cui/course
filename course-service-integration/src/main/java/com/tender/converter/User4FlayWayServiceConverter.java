package com.tender.converter;

import com.tender.domain.User4FlaywayDO;
import com.tender.domain.User4FlaywayDOCustomParam;
import com.tender.domain.User4FlaywayDOExample;
import com.tender.service.model.User4FlayWayModel;
import com.tender.utils.DataUtils;

public class User4FlayWayServiceConverter {

    public static User4FlaywayDOExample param2Example(User4FlaywayDOCustomParam param) {
        User4FlaywayDOExample example = new User4FlaywayDOExample();
        User4FlaywayDOExample.Criteria criteria = example.createCriteria();
        DataUtils.setLongValueIfNotNull(param.getId(), criteria::andIdEqualTo);

        return example;
    }

    public static User4FlayWayModel do2Model(User4FlaywayDO user4FlayWayDO) {
        return DataUtils.copyProperties(user4FlayWayDO, User4FlayWayModel.class);
    }

}
