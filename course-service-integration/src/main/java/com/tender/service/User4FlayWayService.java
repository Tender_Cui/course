package com.tender.service;

import com.tender.domain.User4FlaywayDO;
import com.tender.domain.User4FlaywayDOCustomParam;
import com.tender.service.model.User4FlayWayModel;

import java.util.List;

public interface User4FlayWayService {

    User4FlayWayModel selectOne(User4FlaywayDOCustomParam param);

    List<User4FlayWayModel> selectList(User4FlaywayDOCustomParam param);

    int count(User4FlaywayDOCustomParam param);

    int removeByCondition(User4FlaywayDOCustomParam param);

    int saveInfo(User4FlaywayDO param);

    int updateByConditionSelective(User4FlaywayDOCustomParam param);

    int updateByIdSelective(User4FlaywayDOCustomParam param);

}
