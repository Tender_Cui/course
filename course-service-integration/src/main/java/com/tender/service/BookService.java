package com.tender.service;

import com.tender.service.model.BookModel;

/**
 * 模拟一个 第三方 的接口
 */
public interface BookService {

    /**
     * 根据书名来预定书本
     * @param bookName
     * @return
     */
    BookModel orderBook(String bookName);
}
