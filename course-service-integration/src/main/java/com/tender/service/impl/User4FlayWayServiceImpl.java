package com.tender.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.tender.converter.User4FlayWayServiceConverter;
import com.tender.domain.User4FlaywayDO;
import com.tender.domain.User4FlaywayDOCustomParam;
import com.tender.domain.User4FlaywayDOExample;
import com.tender.mapper.User4FlaywayDOMapper;
import com.tender.service.User4FlayWayService;
import com.tender.service.model.User4FlayWayModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class User4FlayWayServiceImpl implements User4FlayWayService {

    @Autowired
    private User4FlaywayDOMapper user4FlaywayDOMapper;

    @Override
    public User4FlayWayModel selectOne(User4FlaywayDOCustomParam param) {
        User4FlaywayDOExample example = User4FlayWayServiceConverter.param2Example(param);
        List<User4FlaywayDO> user4FlaywayDOS = user4FlaywayDOMapper.selectByExample(example);

        return CollectionUtil.isNotEmpty(user4FlaywayDOS) ? User4FlayWayServiceConverter.do2Model(user4FlaywayDOS.get(0)) : null;
    }

    @Override
    public List<User4FlayWayModel> selectList(User4FlaywayDOCustomParam param) {
        return null;
    }

    @Override
    public int count(User4FlaywayDOCustomParam param) {
        return 0;
    }

    @Override
    public int removeByCondition(User4FlaywayDOCustomParam param) {
        return 0;
    }

    @Override
    public int saveInfo(User4FlaywayDO param) {
        return 0;
    }

    @Override
    public int updateByConditionSelective(User4FlaywayDOCustomParam param) {
        return 0;
    }

    @Override
    public int updateByIdSelective(User4FlaywayDOCustomParam param) {
        return 0;
    }
}
