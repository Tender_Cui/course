package com.tender.service.impl;

import com.tender.service.BookService;
import com.tender.service.StudentService;
import com.tender.service.model.BookModel;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
@Data
public class StudentServiceImpl implements StudentService {

    private BookService bookService;

    @Override
    public BookModel orderBook(String bookName) {
        return bookService.orderBook(bookName);
    }

}
