package com.tender.service.impl;

import com.tender.HelloModel;
import com.tender.domain.HelloDO;
import com.tender.service.HelloService;
import com.tender.utils.DataUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HelloServiceImpl implements HelloService {

    @Override
    public List<HelloModel> list() {

        // 模仿数据库查询 db
        List<HelloDO> list = new ArrayList<>();
        HelloDO helloDO = new HelloDO();
        helloDO.setSex(true);
        helloDO.setName("lucy");
        helloDO.setAge(18);
        list.add(helloDO);

        helloDO = new HelloDO();
        helloDO.setSex(false);
        helloDO.setName("jack");
        helloDO.setAge(20);
        list.add(helloDO);

        return DataUtils.transform(list, item -> DataUtils.copyProperties(item, HelloModel.class));
    }
}
