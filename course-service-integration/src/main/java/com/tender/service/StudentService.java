package com.tender.service;

import com.tender.service.model.BookModel;

public interface StudentService {

    /**
     * 根据书名来预定书本
     * @param bookName
     * @return
     */
    BookModel orderBook(String bookName);
}
