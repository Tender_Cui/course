package com.tender.service;

import com.tender.HelloModel;

import java.util.List;

public interface HelloService {

    List<HelloModel> list();

}
