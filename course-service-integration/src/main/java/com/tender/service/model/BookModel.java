package com.tender.service.model;

import lombok.Data;

@Data
public class BookModel {

    private String id;

    private String bookName;

    private String location;

}
