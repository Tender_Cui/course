package com.tender.service.model;

import lombok.Data;

@Data
public class HelloModel {

    private String name;

    private Integer age;

    private boolean sex;

}
