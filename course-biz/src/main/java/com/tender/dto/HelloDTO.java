package com.tender.dto;

import lombok.Data;

@Data
public class HelloDTO {

    private String name;

    private Integer age;

    private boolean sex;

    private String branch;
}
