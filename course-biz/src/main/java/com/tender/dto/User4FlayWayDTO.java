package com.tender.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class User4FlayWayDTO implements Serializable {

    private Long id;

    private String name;

}