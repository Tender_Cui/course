package com.tender.converter;

import com.tender.domain.User4FlaywayDOCustomParam;
import com.tender.request.User4FlaywayQueryRequest;
import com.tender.utils.DataUtils;

public class User4FlayWayBizConverter {

    public static User4FlaywayDOCustomParam request2Param(User4FlaywayQueryRequest param) {
        return DataUtils.copyProperties(param, User4FlaywayDOCustomParam.class);
    }

}
