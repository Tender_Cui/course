package com.tender.impl;

import com.tender.User4FlayWayBizService;
import com.tender.converter.User4FlayWayBizConverter;
import com.tender.domain.User4FlaywayDOCustomParam;
import com.tender.dto.User4FlayWayDTO;
import com.tender.request.User4FlaywayQueryRequest;
import com.tender.service.User4FlayWayService;
import com.tender.service.model.User4FlayWayModel;
import com.tender.utils.DataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class User4FlayWayBizServiceImpl implements User4FlayWayBizService {

    @Autowired
    private User4FlayWayService user4FlayWayService;

    @Override
    public User4FlayWayDTO selectOne(User4FlaywayQueryRequest param) {
        User4FlaywayDOCustomParam oneParam = User4FlayWayBizConverter.request2Param(param);
        User4FlayWayModel user4FlayWayModel = user4FlayWayService.selectOne(oneParam);
        return DataUtils.copyProperties(user4FlayWayModel, User4FlayWayDTO.class);
    }
}
