package com.tender.impl;

import com.google.common.collect.Lists;
import com.tender.HelloBizService;
import com.tender.*;
import com.tender.dto.HelloDTO;
import com.tender.service.HelloService;
import com.tender.utils.DataUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class HelloBizServiceImpl implements HelloBizService {

    @Autowired
    private HelloService helloService;

    @Override
    public List<HelloDTO> list() {

        List<HelloModel> helloModelList = helloService.list();
        return CollectionUtils.isEmpty(helloModelList) ? Lists.newArrayList() :
                DataUtils.transform(helloModelList, item -> DataUtils.copyProperties(item, HelloDTO.class));

    }
}
