package com.tender;

import com.tender.dto.HelloDTO;

import java.util.List;

public interface HelloBizService {

    List<HelloDTO> list();

}
