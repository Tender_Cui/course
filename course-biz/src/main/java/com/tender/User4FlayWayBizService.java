package com.tender;

import com.tender.dto.User4FlayWayDTO;
import com.tender.request.User4FlaywayQueryRequest;

public interface User4FlayWayBizService {

    User4FlayWayDTO selectOne(User4FlaywayQueryRequest param);

}
