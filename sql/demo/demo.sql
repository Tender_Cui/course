CREATE TABLE `user_4_flayway` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT "id",
  `name` varchar(32) DEFAULT NULL COMMENT "名称",
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `user_4_flayway` (`id`, `name`) VALUES (1, 'tender');
INSERT INTO `user_4_flayway` (`id`, `name`) VALUES (2, 'lisi');
INSERT INTO `user_4_flayway` (`id`, `name`) VALUES (3, 'wanghong');
INSERT INTO `user_4_flayway` (`id`, `name`) VALUES (4, 'lucy');
