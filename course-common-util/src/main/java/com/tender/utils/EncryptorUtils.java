package com.tender.utils;

import org.jasypt.util.text.BasicTextEncryptor;

public class EncryptorUtils {

    public static void main(String[] args) {
        // BasicTextEncryptor 的默认加密算法为： PBEWithMD5AndDES
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        // 这个是加密的盐
        textEncryptor.setPassword("wb-csd876509");
        String userName = textEncryptor.encrypt("root");
        String passWord = textEncryptor.encrypt("88888888");

        System.out.println("------------------ 加密操作 ------------------");
        System.out.println("userName加密后 = " + userName);
        System.out.println("passWord加密后 = " + passWord);

        System.out.println("------------------ 解密操作 ------------------");
        System.out.println("userName解密后 = " + textEncryptor.decrypt(userName));
        System.out.println("passWord解密后 = " + textEncryptor.decrypt(passWord));
    }
}
