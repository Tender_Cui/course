package com.tender.enums;

import lombok.Getter;

@Getter
public enum ErrorTypeEnum {

    DATA_NOT_EMPTY("SC_100000000", "data not empty", "%s数据不能为空"),
    RECORD_HAS_EXISTS("SC_100000001", "record has exists", "%s数据已经存在")

    ;
    private String errorCode;

    // 异常描述英文
    private String errorMsgEn;

    // 异常描述中文
    private String errorMsgCn;

    ErrorTypeEnum(String errorCode, String errorMsgEn, String errorMsgCn) {
        this.errorCode = errorCode;
        this.errorMsgEn = errorMsgEn;
        this.errorMsgCn = errorMsgCn;
    }
}
