# 重要提示
    ./references 里面都是参考手册，很重要，勿删!

# 特性支持列表
* 数据库用户名、密码支持加解密
* mybatis逆向工程支持
* thymeleaf模板引擎支持
* 支持docker打镜像&&推送到Harbor
* 支持Jenkins流水线Pipeline文件
* 