package com.tender.controller;

import com.tender.User4FlayWayBizService;
import com.tender.dto.User4FlayWayDTO;
import com.tender.request.User4FlaywayQueryRequest;
import com.tender.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class User4FlayWayController {

    @Autowired
    private User4FlayWayBizService user4FlayWayBizService;

    @PostMapping("/selectOne")
    public Result<User4FlayWayDTO> selectOne(@RequestBody User4FlaywayQueryRequest request) {
        return Result.of(user4FlayWayBizService.selectOne(request));
    }

}

