package com.tender.controller.templates;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

// 此处不能使用 @RestController
// @RestController
@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(Model model) {
        // 业务逻辑
        model.addAttribute("username", "lucy");

        return "index";
    }

}
