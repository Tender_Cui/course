package com.tender.controller;

import com.tender.HelloBizService;
import com.tender.dto.HelloDTO;
import com.tender.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class HelloController {

    @Autowired
    private HelloBizService helloBizService;

    @GetMapping("/hello")
    public Result<HelloDTO> Hello() {
        HelloDTO helloDTO = new HelloDTO();
        helloDTO.setAge(30);
        helloDTO.setName("Tender");
        helloDTO.setSex(true);
        helloDTO.setBranch("feature-dev");
        return Result.of(helloDTO);
    }

    @GetMapping("list")
    public Result<List<HelloDTO>> list() {
        return Result.of(helloBizService.list());
    }

}
