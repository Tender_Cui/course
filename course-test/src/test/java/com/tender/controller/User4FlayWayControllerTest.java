package com.tender.controller;

import com.tender.CourseApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlayWayControllerTest {

    // 注入Spring容器
    @Autowired
    private WebApplicationContext applicationContext;

//    @Autowired
//    private User4FlayWayController user4FlayWayController;

    // 模拟Http请求
    private MockMvc mockMvc;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Before
    public void setupMockMvc() {
        // 初始化MockMvc对象
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

//    @Before
//    public void setupMockMvc() {
//        // 初始化MockMvc对象
//        mockMvc = MockMvcBuilders.standaloneSetup(user4FlayWayController).build();
//    }

    /**
     * 查询单个测试
     * @throws Exception
     */
    @Test
    public void selectOneTest() throws Exception {
        String json = "{\"id\": 2}";
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/selectOne")    //构造一个post请求
                        // 发送端和接收端数据格式
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(json.getBytes()))
                // 断言校验返回的code编码
                .andExpect(MockMvcResultMatchers.status().isOk())
                // 添加处理器打印返回结果
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
        System.out.println(mvcResult);
    }
}

