package com.tender.bizservice;

import com.alibaba.fastjson.JSON;
import com.tender.CourseApplication;
import com.tender.User4FlayWayBizService;
import com.tender.dto.User4FlayWayDTO;
import com.tender.request.User4FlaywayQueryRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlayWayBizServiceTest {

    @Autowired
    private User4FlayWayBizService user4FlayWayBizService;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Test
    public void  selectOneTest() {
        User4FlaywayQueryRequest param = new User4FlaywayQueryRequest();
        param.setId(2L);
        User4FlayWayDTO user4FlayWayDTO = user4FlayWayBizService.selectOne(param);
        System.out.println(JSON.toJSONString(user4FlayWayDTO));
    }

}
