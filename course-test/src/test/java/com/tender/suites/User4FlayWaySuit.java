package com.tender.suites;

import com.tender.bizservice.User4FlayWayBizServiceTest;
import com.tender.controller.User4FlayWayControllerTest;
import com.tender.dao.User4FlaywayDOMapperTest;
import com.tender.service.User4FlayWayServiceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 统一执行User4FlayWay 相关的单元测试
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        User4FlayWayBizServiceTest.class,
        User4FlayWayControllerTest.class,
        User4FlaywayDOMapperTest.class,
        User4FlayWayServiceTest.class
})
public class User4FlayWaySuit {

}
