package com.tender.service;

import com.alibaba.fastjson.JSON;
import com.tender.CourseApplication;
import com.tender.service.impl.StudentServiceImpl;
import com.tender.service.model.BookModel;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.anyString;

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class MockInjectInterfaceTest {

    // 模拟jvm 参数
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Mock
    private BookService bookService;

    @InjectMocks // 注意不许是一个类，不能是接口
    private StudentServiceImpl studentService;

    @Test
    public void orderBookTest() {
        BookModel bookModel = new BookModel();
        bookModel.setBookName("西游记");
        bookModel.setId("001");
        bookModel.setLocation("图书馆1好楼-四大名著-15排");

        Mockito.when(bookService.orderBook(anyString())).thenReturn(bookModel);
        BookModel result = studentService.orderBook("");
        System.out.println("测试结果：" + JSON.toJSONString(result));

        Assert.assertTrue("预定数据不符合", result.equals(bookModel));
    }

}
