package com.tender.service;

import com.alibaba.fastjson.JSON;
import com.tender.CourseApplication;
import com.tender.domain.User4FlaywayDOCustomParam;
import com.tender.service.model.User4FlayWayModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlayWayServiceTest {

    @Autowired
    private User4FlayWayService user4FlayWayService;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Test
    public void selectOneTest() {
        User4FlaywayDOCustomParam param = new User4FlaywayDOCustomParam();
        param.setId(2L);
        User4FlayWayModel user4FlayWayModel = user4FlayWayService.selectOne(param);
        System.out.println(JSON.toJSONString(user4FlayWayModel));
    }


    @Test(expected = ArithmeticException.class)
    public void exceptionTest() {
        int i = 0;
        int j = 9;
        int result = j / i;
        System.out.println(result);
    }
}
