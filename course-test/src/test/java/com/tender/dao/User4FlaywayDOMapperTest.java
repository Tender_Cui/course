package com.tender.dao;

import com.tender.CourseApplication;
import com.tender.domain.User4FlaywayDO;
import com.tender.domain.User4FlaywayDOExample;
import com.tender.mapper.User4FlaywayDOMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlaywayDOMapperTest {

    @Autowired
    private User4FlaywayDOMapper user4FlaywayDOMapper;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Test
    public void testSelectOne() {
        User4FlaywayDOExample example = new User4FlaywayDOExample();
        User4FlaywayDOExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(1L);
        List<User4FlaywayDO> user4FlaywayDOS = user4FlaywayDOMapper.selectByExample(example);
        System.out.println(user4FlaywayDOS);
    }


    @Test
    @Transactional
    @Rollback(value = true)
    public void testAddOne() {
        List<User4FlaywayDO> user4FlaywayDOS = user4FlaywayDOMapper.selectByExample(new User4FlaywayDOExample());
        System.out.println("*************************************");
        System.out.println("原始数据个数：" + user4FlaywayDOS.size());
        System.out.println("*************************************");

        User4FlaywayDO record = new User4FlaywayDO();
        record.setId(5L);
        record.setName("tom");
        int size = user4FlaywayDOMapper.insertSelective(record);
        System.out.println("*************************************");
        System.out.println("影响的行数：" + size);
        System.out.println("*************************************");
    }

}
