package com.tender;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.edge.EdgeDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.opera.OperaDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class ChromeDriverSelenium {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "E:\\chromedriver-win64\\chromedriver.exe");
        // 谷歌驱动
        ChromeOptions options = new ChromeOptions();
        // 允许所有请求
        options.addArguments("--remote-allow-origins=*");
        WebDriver driver = new ChromeDriver(options); //Chrome浏览器
        //等待加载完成
        // TimeUnit.SECONDS.sleep(5);
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(TimeUnit.SECONDS.toMillis(30)));
        // TimeUnit.SECONDS.sleep(5);
        // WebDriver driver = new FirefoxDriver(); //Firefox浏览器
        // WebDriver driver = new EdgeDriver(); //Edge浏览器
        // WebDriver driver = new InternetExplorerDriver(); // Internet Explorer浏览器
        // WebDriver driver = new OperaDriver(); //Opera浏览器
        // WebDriver driver = new PhantomJSDriver(); //PhantomJS

        driver.get("https://www.baidu.com");
        driver.findElement(By.id("kw")).sendKeys("selenium");
        // 4.获取“百度一下”按钮，进行搜索
        driver.findElement(By.id("su")).click();
        // 5.退出浏览器
        // driver.quit();

    }
}
