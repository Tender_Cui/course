package com.tender;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableEncryptableProperties
@MapperScan("com.tender.mapper")
public class CourseApplication {

    public static void main(String[] args) {
        System.setProperty("jasypt.encryptor.password", "wb-csd876509");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(CourseApplication.class, args);
        int i = 0;
    }
}