# 参考文献

# 一、demo

```shell
location /wlview {
    # 允许iframe嵌入
    add_header X-Frame-Options "ALLOWALL" always;
    # 如果你使用了 CSP，也需要允许 iframe
    add_header Content-Security-Policy "frame-ancestors 'self' *" always;
    # 配置 CORS
    add_header Access-Control-Allow-Origin *;   # 允许所有域访问
    add_header Access-Control-Allow-Methods 'GET, POST, OPTIONS';  # 允许的请求方法
    add_header Access-Control-Allow-Headers 'Content-Type, X-Requested-With, Authorization';  # 允许的请求头
          
    alias /usr/local/nginx/visualization/dist;
    try_files $uri $uri/ /wlview/index.html;
}
```

&emsp;&emsp;<span style="color:red">注意：</span> `/wlview/index.html` 以 <span style="color:yellow">/wlview</span> 开头： 
```shell
try_files $uri $uri/ /wlview/index.html;
```