# 参考文献

* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)
* [Harbor安装部署实战详细手册](https://blog.csdn.net/qq319601040/article/details/129021918)

# 一、Harbor 简介

&emsp;&emsp;Harbor（港口，港湾）是一个用于存储和分发Docker镜像的企业级Registry服务器。

&emsp;&emsp;除了Harbor这个私有镜像仓库之外，还有Docker官方提供的Registry。相对Registry，Harbor具有很多优势：

* **提供分层传输机制，优化网络传输**：Docker镜像是分层的，而如果每次传输都使用全量文件(所以用FTP的方式并不适合)，显然不经济。必须提供识别分层传输的机制，以层的UUID为标识，确定传输的对象。
* **提供WEB界面，优化用户体验**：只用镜像的名字来进行上传下载显然很不方便，需要有一个用户界面可以支持登陆、搜索功能，包括区分公有、私有镜像。
* **支持水平扩展集群**：当有用户对镜像的上传下载操作集中在某服务器，需要对相应的访问压力作分解。
* **良好的安全机制**：企业中的开发团队有很多不同的职位，对于不同的职位人员，分配不同的权限，具有更好的安全性。

# 二、安装

&emsp;&emsp;环境准备：


|       机子	        |           权限说明      |
|:----------------:|:-------------------------:|
| 192.168.211.101  |         制作镜像，推送镜像   |
| 192.168.211.102	 |          Harbor           |
| 192.168.211.103  |           拉取镜像          |

&emsp;&emsp;Harbor 需要安装在 <span style="color:red">192.168.211.102</span> 上面（视各人情况）。



## 2.1、安装docker

&emsp;&emsp;[请参考docker安装手册](./docker_Centos7_快速入门.md ':include')

## 2.2、安装docker-compose

### 2.2.1、下载

```shell
# 我下载的版本是：v2.19.0
# 去这个地址：https://github.com/docker/compose/releases?page=2 去手动下载名称为：docker-compose-linux-x86_64 的文件，
# 将下载的文件放入到：/usr/local/bin/ 目录下，命名为：docker-compose
```

> uname -s：是系统内核名称(Linux)，uname -m：是计算机硬件架构(x86_64)

### 2.2.2、赋权

```shell
chmod +x /usr/local/bin/docker-compose
```

### 2.2.3、测试

```shell
docker-compose --version
```

![查看docker-compose版本](./pictures/harbor/img.png)

## 2.3、安装 harbor

### 2.3.1、下载

```shell
# 我下载的版本是 harbor-offline-installer-v2.10.0.tgz
# 网速慢，可以手动去 https://github.com/goharbor/harbor/releases 下载
wget https://github.com/goharbor/harbor/releases/download/v2.10.0/harbor-offline-installer-v2.10.0.tgz
```

### 2.3.2、解压

```shell
tar -zxvf harbor-offline-installer-v2.10.0.tgz -C ./../intalls/
```

### 2.3.3、修改配置文件

```shell
cd harbor
cp harbor.yml.tmpl harbor.yml
vi harbor.yml
```

```shell
# 修改内容如下：
# 主机ip
hostname: 192.168.255.102
# 自定义访问端口
port: 5000

# 页面登录密码
harbor_admin_password: 88888888
```

![修改配置文件01](./pictures/harbor/img_1.png)

![修改配置文件02](./pictures/harbor/img_2.png)

![修改配置文件03](./pictures/harbor/img_3.png)

### 2.3.4、安装Harbor

```shell
./prepare

./install.sh 
```

![harbor安装成功](./pictures/harbor/img_4.png)

### 2.3.5、启动

```shell
# 在如下目录下：
cd /opt/installs/harbor

# 启动
docker-compose up -d     # 启动
docker-compose stop      # 停止
docker-compose restart   # 重新启动
```

![harbor启动成功](./pictures/harbor/img_5.png)

### 2.3.6、访问Harbor

```shell
# 防火墙放开 1000 端口
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --reload

192.168.211.102:1000
```

![harbor访问](./pictures/harbor/img_6.png)

> 登录进去

![harbor访问02](./pictures/harbor/img_7.png)

# 三、在Harbor创建用户和项目

## 3.1、创建项目

&emsp;&emsp;Harbor的项目分为公开和私有的：

* 公开项目：所有用户都可以访问，通常存放公共的镜像，默认有一个library公开项目。
* 私有项目：只有授权用户才可以访问，通常存放项目本身的镜像。

![创建项目](./pictures/harbor/img_8.png)

![创建项目02](./pictures/harbor/img_9.png)

![创建项目03](./pictures/harbor/img_10.png)

## 3.2、创建用户

![创建用户](./pictures/harbor/img_11.png)

## 3.3、给私有项目分配用户

![给项目关联用户](./pictures/harbor/img_12.png)

|  角色	  |           权限说明           |
|:---------:|:------------------------------:|
|    访客   |         对于指定项目拥有只读权限        |
| 开发人员	 |          对于指定项目拥有读写权限         |
|  维护人员  |      对于指定项目拥有读写权限，创建 Webhooks     |
|   项目管理员   | 除了读写权限，同时拥有用户管理/镜像扫描等管理权限 |

## 3.4、以新用户登录Harbor

![新用户登录](./pictures/harbor/img_13.png)

# 四、把镜像上传到Harbor

> 以 运行nginx 为示例

```shell
# 拉取最新的 nginx
docker search nginx
# 查看镜像
docker images
# 启动 nginx 容器
docker run -d --name nginx -p 80:80 nginx
```

![nginx运行示例结果](./pictures/harbor/img_14.png)

![nginx进程示例结果](./pictures/harbor/img_15.png)

## 4.1、给镜像打上标签

```shell
# 固定格式：
# harbor下的一个项目名称：course_project (必须提前建好)
docker tag nginx:latest 192.168.255.102:5000/course_project/nginx:v1
```

## 4.2、推送镜像

```shell
docker push 192.168.255.102:5000/course_project/nginx:v1
```

&emsp;&emsp;会出现如下报错，是因为 Docker 没有把Harbor加入信任列表中（他信任dockerHub）

```shell
[root@slave-01 ~]# docker push 192.168.255.102:5000/course_project/nginx:v1
The push refers to repository [192.168.255.102:5000/course_project/nginx]
Get "https://192.168.255.102:5000/v2/": http: server gave HTTP response to HTTPS client
```

## 4.3、把Harbor地址加入到Docker信任列表
    
```shell
vi /etc/docker/daemon.json

{ 
    # "registry-mirrors": ["https://zydiol88.mirror.aliyuncs.com"],
    "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"],
    "insecure-registries": ["192.168.255.102:5000"]
}
```
&emsp;&emsp;<span style="color:red">重启docker</span>

## 4.4、再次执行推送命令，会提示权限不足

```shell
docker push 192.168.255.102:5000/course_project/nginx:v1
```

![推送报错：提示权限不足](./pictures/harbor/img_16.png)

&emsp;&emsp;<span style="color:red">需要先登录Harbor，再推送镜像</span>

## 4.5、登录Harbor

```shell
docker login -u user_course -p Course88888888 192.168.255.102:5000
```

![登录harbor&&推送镜像](./pictures/harbor/img_17.png)

![Harbor运营门户查看镜像](./pictures/harbor/img_18.png)


# 五、从Harbor下载镜像

&emsp;&emsp;<span style="color:red">192.168.255.103</span> 机器拉取镜像。

## 5.1、安装docker

&emsp;&emsp;[请参考docker安装手册](./docker_Centos7_快速入门.md ':include')

## 5.2、修改Docker配置

```shell
vi /etc/docker/daemon.json

{ 
    # "registry-mirrors": ["https://zydiol88.mirror.aliyuncs.com"],
    "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn"],
    "insecure-registries": ["192.168.255.102:5000"]
}
```
&emsp;&emsp;<span style="color:red">重启docker</span>

## 5.3、先登录，再从Harbor下载镜像

```shell
# 先登录
docker login -u user_course -p Course88888888 192.168.255.102:5000

# 再拉取
docker pull 192.168.255.102:5000/course_project/nginx:v1
```

![拉取并查看镜像](./pictures/harbor/img_19.png)