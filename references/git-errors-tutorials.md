# 参考文献

# 一、git 安装

> 下载地址

* [git 淘宝镜像地址下载(速度快)](https://registry.npmmirror.com/binary.html?path=git-for-windows/)
* [git 清华镜像地址下载](https://mirrors.tuna.tsinghua.edu.cn/github-release/git-for-windows/git/)

## 1.1、安装

> 下载后无脑安装

# 二、git安装后的问题

## 2.1、问题1
> Error: Could not fork child process: Resource temporarily unavailable (-1)

&emsp;&emsp;这个问题排查了好久，网上的文章千篇一律的都是抄袭的，屁用没用。自己也是安装了卸载，卸载了再安装！最后找了一个链接，找到了
解决方法！

> 禁用系统Windows Defender安全中心中的图像强制随机化(强制ASLR).或者为git可执行文件排除ASLR:

&emsp;&emsp;[原始文献点这里](https://qa.1r1g.com/sf/ask/2447170841/)

> 具体修复步骤如下：

![设置](./pictures/git/img.png)

> 下一步

![更新和安全](./pictures/git/img_1.png)

> 下一步

![Windows 更新](./pictures/git/img_2.png)

> 下一步

![应用和浏览器控制](./pictures/git/img_3.png)

> 下一步

![Exploit Protection设置](./pictures/git/img_4.png)

> 下一步

![Exploit Protection设置](./pictures/git/img_4.png)

> 下一步

![关闭高熵 ASLR](./pictures/git/img_5.png)




