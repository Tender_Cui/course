# 参考文献

* [SpringBoot整合Feign](https://blog.csdn.net/weixin_44183847/article/details/128850109)
* [SpringBoot实战（二十四）集成 LoadBalancer](https://blog.csdn.net/qq_33204709/article/details/132967949)

# SpringBoot 整合 OpenFeign、Eureka集群

# 1、系统模块细分

![系统模块细分](./pictures/springboot-openfeign-eureka/img.png)

* `shiro-eureka-server`: eureka 集群服务
* `shiro-eureka-openfeign-facade-api`: Endpoint/Facade/Controller 接口
* `shiro-eureka-openfeign-provider`: 服务提供者
* `shiro-eureka-openfeign-consumer`: 服务消费者

# 2、eureka 集群服务

> [请参考：eureka 集群服务的搭建](./springboot-eurekaServer-cluster.md 'included')

# 3、模块：`shiro-eureka-openfeign-facade-api`

## 3.1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-eureka-openfeign-facade-api</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!-- 用到了一些 spring 相关的注解 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- 用到了一些 OpenFeign 相关的注解 -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 3.2、接口编写

```java
package com.tender.facade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

// name 大写小写都ok
// @FeignClient(name = "SHIRO-FEIGN-PROVIDER")

/**
 * name = "shiro-feign-provider" 或者，需要和服务提供者的 spring.application.name 一样
 * name = "SHIRO-FEIGN-PROVIDER" 都可以
 * 注解 @FeignClient 这边不允许添加 @RequestMapping 注解
 * contextId = "providerServer" 对应于提供着的name @RestController("providerServer") 加不加都可以
 * 比如：
 * // @FeignClient(name = "SHIRO-FEIGN-PROVIDER",contextId = "providerServer")
 */
@FeignClient(name = "shiro-feign-provider")
public interface ProviderFacade {

    @GetMapping("/provider/data")
    String provideData();
}
```

# 4、模块：`shiro-eureka-openfeign-provider`

## 4.1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-eureka-openfeign-provider</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!-- 引入接口：Facade -->
        <dependency>
            <groupId>com.tender</groupId>
            <artifactId>shiro-eureka-openfeign-facade-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 4.2、application.yaml

1. application-dev1.yaml

```yaml
server:
  port: 9205

spring:
  application:
    # 此实例注册到 eureka 服务端的name
    name: shiro-feign-provider

eureka:
  client:
    serviceUrl:
      # eureka服务端提供的注册地址 参考服务端配置的这个路径
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID
    instance-id: shiro-feign-provider-1
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户需要多长时间发送心跳给eureka服务器，表明它仍然活着,默认为30 秒 (与下面配置的单位都是秒)
    leaseRenewalIntervalInSeconds: 10
    # eureka服务器在接收到实例的最后一次发出的心跳后，需要等待多久才可以将此实例删除，默认为90秒
    leaseExpirationDurationInSeconds: 30
```


2. application-dev2.yaml

```yaml
server:
  port: 9206

spring:
  application:
    # 此实例注册到 eureka 服务端的name
    name: shiro-feign-provider

eureka:
  client:
    serviceUrl:
      # eureka服务端提供的注册地址 参考服务端配置的这个路径
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID
    instance-id: shiro-feign-provider-2
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户需要多长时间发送心跳给eureka服务器，表明它仍然活着,默认为30 秒 (与下面配置的单位都是秒)
    leaseRenewalIntervalInSeconds: 10
    # eureka服务器在接收到实例的最后一次发出的心跳后，需要等待多久才可以将此实例删除，默认为90秒
    leaseExpirationDurationInSeconds: 30
```

## 4.3、启动类配置

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
public class ShiroFeignProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroFeignProviderApplication.class);
    }
}
```

## 4.4、服务提供者

```java
package com.tender.controller;

import com.tender.facade.ProviderFacade;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

// @RestController("providerServer")
@RestController()
public class FeignProviderFacadeImpl implements ProviderFacade {

    @Value("${server.port}")
    private String port;

    public String provideData() {
        return "data from provider, port = " + port;
    }
}
```

# 5、模块：`shiro-eureka-openfeign-consumer`

## 5.1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-eureka-openfeign-consumer</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!-- 引入接口：Facade -->
        <dependency>
            <groupId>com.tender</groupId>
            <artifactId>shiro-eureka-openfeign-facade-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- 从 Spring Cloud 2020.0.0 版本开始，spring-cloud-starter-openfeign 不再包含 Ribbon 和 Hystrix
            替代 Hystrix 的方案是使用 Spring Cloud Circuit Breaker 来实现断路器的功能。
            替代 Ribbon 的方案是使用 Spring Cloud LoadBalancer 来实现负载均衡器的功能。
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 5.2、application.yaml

```yaml
server:
  port: 9207

spring:
  application:
    # 此实例注册到 eureka 服务端的name
    name: shiro-feign-consumer

eureka:
  client:
    serviceUrl:
      # eureka服务端提供的注册地址 参考服务端配置的这个路径
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID
    instance-id: shiro-feign-consumer-1
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户需要多长时间发送心跳给eureka服务器，表明它仍然活着,默认为30 秒 (与下面配置的单位都是秒)
    leaseRenewalIntervalInSeconds: 10
    # eureka服务器在接收到实例的最后一次发出的心跳后，需要等待多久才可以将此实例删除，默认为90秒
    leaseExpirationDurationInSeconds: 30

###### feign全局配置 ######
feign:
  client:
    config:
      # 这里用 default 就是全局配置，如果是写服务名称，则是针对某个服务的配置。
      default:
        # 建立连接所用的时间，适用于网络状况正常的情况下，两端连接所需要的时间，毫秒单位
        ConnectTimeOut: 5000
        # 指建立连接后从服务端读取到可用资源所用的时间，毫秒单位
        ReadTimeOut: 5000
```

## 5.3、启动类配置

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
@EnableFeignClients
public class ShiroFeignConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroFeignConsumerApplication.class);
    }
}
```

## 5.4、Controller 类

```java
package com.tender.controller;

import com.tender.facade.ProviderFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer")
public class FeignConsumerController {

    @Autowired
    private ProviderFacade providerFacade;

    @GetMapping("/data")
    public String getData() {
        return providerFacade.provideData();
    }
}
```










