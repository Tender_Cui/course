# 参考文献

* 直接问 `DeepSeek` 

```shell
  win10 安装 peft 版本号 0.12.0
```

# 一、命令安装 `trl`
```shell
pip install peft==0.12.0
```

# 二、测试

```shell
import peft
print(peft.__version__)
```

&emsp;&emsp;结果如下：

```shell
D:\A_Tender_workspace\install_4_code\python\python_3.10.9\python.exe D:\A_Tender_workspace\A_code\A_code_4_python\LLaMA-Factory\envTest\DatasetsTest.py 
0.12.0

```

