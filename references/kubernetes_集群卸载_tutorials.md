# 参考文献

# k8s 集群卸载手册

# 一、停止所有 Kubernetes 相关的服务

```shell
systemctl stop kubelet
systemctl stop docker
```

> 这两个命令会停止 kubelet 和 docker 服务。

# 二、删除 Kubernetes 集群

&emsp;&emsp;在这一步，我们需要通过 kubeadm 命令来删除 Kubernetes 集群。执行以下命令：

```shell
sudo  kubeadm reset -f 
# 自动卸载ipip模块
modprobe -r ipip
# 查看现有的系统模块
lsmod
```

> 这个命令会将 Kubernetes 集群还原到初始状态。

# 三、卸载 docker

&emsp;&emsp;在 Kubernetes 中，Docker 是容器运行时的选择之一。如果你之前安装了 Docker，你需要卸载它。执行以下命令：

```shell
# 卸载已经安装的docker
yum -y remove docker*

# 删除docker的所有镜像和容器
rm -rf /var/lib/docker 
```

> 第一条命令会卸载 Docker 相关的软件包，第二条命令会删除 Docker 的配置文件和数据。

```shell
sudo rm -rf  /etc/kubernetes
sudo rm -rf  ~/.kube
rm -rf /etc/systemd/system/kubelet.service.d
rm -rf /etc/systemd/system/kubelet.service
rm -rf /usr/bin/kube*
rm -rf /etc/cni
rm -rf /opt/cni
rm -rf /var/lib/etcd
rm -rf /var/etc

yum remove -y kubelet kubeadm kubectl
```

> 第一条命令会删除 Kubernetes 的配置文件，第二条命令会删除当前用户的 Kubernetes 配置。






















