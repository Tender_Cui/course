# 参考文献

* [Linux下安装配置maven](https://blog.csdn.net/qq_40240091/article/details/128443135)

# 一、简介
&emsp;&emsp;[官网地址](https://maven.apache.org/)

## 1.1、下载地址

* [官网历史版本下载地址](https://maven.apache.org/docs/history.html)

## 1.2、环境变量配置

### 1.2.1 Centos7

> 上传压缩包

```shell
# 上传到目录：
/opt/softs

# 解压缩到：/opt/installs/apache-maven-3.9.6
# 解压缩后自带目录：apache-maven-3.9.6
tar -zxvf apache-maven-3.9.6-bin.tar.gz -C /opt/installs/
```

> 配置环境变量

```shell
vim /etc/profile

export MAVEN_HOME=/opt/installs/apache-maven-3.9.6
export PATH=${PATH}:${MAVEN_HOME}/bin
```

> 刷新环境变量

```shell
source /etc/profile
```

> 验证 maven

```shell
# 任意路径下
mvn -v
```

![验证 maven](./pictures/maven/img.png)
