# 参考文献

* [不支持的字符集 (在类路径中添加 orai18n.jar): ZHS16GBK;](https://blog.csdn.net/qq904274014/article/details/90899925/)

# 一、项目可能会涉及到多数据源的问题

&emsp;&emsp;这边记录下烟港项目中，遇到的多数据源的情况：既有 `mysql` 数据源，又有 `oracle` 数据源

&emsp;&emsp;多数据源框架，采用的 `mybatis-plus` 的多数据源框架

# 二、`oracle` 常用驱动

```xml
<dependency>
    <groupId>com.oracle.database.jdbc</groupId>
    <artifactId>ojdbc8</artifactId>
    <version>21.5.0.0</version>
</dependency>

<dependency>
    <groupId>com.oracle.database.nls</groupId>
    <artifactId>orai18n</artifactId>
    <version>19.3.0.0</version>
</dependency>

<dependency>
    <groupId>com.oracle.database.jdbc</groupId>
    <artifactId>ojdbc8</artifactId>
    <version>19.8.0.0</version>
</dependency>

```

> 在这几个 `Oracle JDBC` 驱动版本中，通常使用较多的是 `ojdbc8` 版本，它支持 `Java 8` 及以上版本，并且在多数现代 `Java` 项目中得到广泛使用。

# 三、版本对比

## 3.1、`ojdbc8 21.5.0.0`:

* 版本号较新（2021年发布），支持 Java 8 及以上版本的 JDBC 连接。
* 适用于 Oracle 19c 及更新版本的数据库。
* 在较新的 Oracle 数据库版本中推荐使用。

## 3.2、`ojdbc8 19.8.0.0`:

* 版本稍旧（2019年发布），但同样支持 Java 8。
* 适用于 Oracle 12c 和 18c 版本的数据库。
* 这是 Oracle 数据库 12c 及 18c 的常见 JDBC 驱动版本。

## 3.3、`orai18n 19.3.0.0`:

* 这是 Oracle 数据库的国际化支持库，主要提供字符集转换和 NLS (National Language Support) 支持。
* 这个库通常与 ojdbc8 一起使用，但不是直接的 JDBC 驱动。它用于处理数据库中的多语言支持。
* 如果你在项目中没有涉及到特殊的语言设置和字符集问题，可以不必使用这个库。

# 四、项目踩坑

## 4.1、缺少字符集

> SQL state [99999]; error code [17056]; 不支持的字符集 (在类路径中添加 orai18n.jar): ZHS16GBK;

&emsp;&emsp;解决方案:

```xml
<dependency>
    <groupId>com.oracle.database.jdbc</groupId>
    <artifactId>ojdbc8</artifactId>
    <version>19.8.0.0</version>
</dependency>
<dependency>
    <groupId>cn.easyproject</groupId>
    <artifactId>orai18n</artifactId>
    <version>12.1.0.2.0</version>
    <!-- <scope>provided</scope> -->
</dependency>
```
