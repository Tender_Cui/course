# 参考文献

* [mybatis3 的源码](https://github.com/mybatis/mybatis-3) ： `mybatis3` 的源代码
* [mybatis-spring 的源码](https://github.com/mybatis/spring) ： `mybatis` 和 `spring` 的整合源码
* [mybatis-parent 的源码](https://github.com/mybatis/parent) ： `mybatis` 和 `mybatis-spring` 共同依赖的一个公共 `parent` 项目，只是定义了一些公共的属性及项目依赖的插件信息

# 一、源码导入步骤

1.1. 克隆 `mybatis3` 的源码   
1.2. 克隆 `mybatis` 和 `spring` 的整合源码   
1.3. 克隆 公共 `parent` 源码   

# 二、利用 `idea` 导入项目

&emsp;&emsp;创建一个<span style="color:red;">空</span>项目。

> file -> new -> project -> Empty Project

![新建一个空项目](./pictures/Code-source-learning-4-mybatis/img.png)

## 2.1、导入 `mybatis3` 的源码

> file -> new -> Module from Existing Sources...

&emsp;&emsp;选择 `mybatis3` 的源码

![选择 `mybatis3` 的源码](./pictures/Code-source-learning-4-mybatis/img_1.png)

![选择 maven](./pictures/Code-source-learning-4-mybatis/img_2.png)

## 2.2、导入 `mybatis` 和 `spring` 的整合源码

&emsp;&emsp;同上

## 2.3、导入 `mybatis` 和 `spring` 的整合源码

&emsp;&emsp;同上

# 3、最终项目结构

![最终项目结构](./pictures/Code-source-learning-4-mybatis/img_3.png)