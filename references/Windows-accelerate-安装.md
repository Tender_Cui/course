# 参考文献

* 直接问 `DeepSeek` 

```shell
  win10 安装 accelerate 版本号 1.2.1
```

# 一、命令安装 `accelerate`
```shell
pip install accelerate==1.2.1
```

# 二、测试

```shell
import accelerate
print(accelerate.__version__)
```

&emsp;&emsp;结果如下：

```shell
D:\A_Tender_workspace\install_4_code\python\python_3.10.9\python.exe D:\A_Tender_workspace\A_code\A_code_4_python\LLaMA-Factory\envTest\AccelerateTest.py 
1.2.1

```

