# 参考文献
* [JDK17在Windows安装以及环境变量配置（超详细的教程）](https://blog.csdn.net/tiehou/article/details/129575138)

# 一、下载

&emsp;&emsp;[jdk17的下载地址](https://www.oracle.com/java/technologies/downloads/#jdk17-linux)

![jdk17下载选择](./pictures/jdk17_Win10/img.png)

# 二、安装

&emsp;&emsp;`jdk`选择自定义的安装路径

![选择jdk安装路径](./pictures/jdk17_Win10/img_1.png)

&emsp;&emsp;确保磁盘空间足够，然后我们`下一步`
![下一步](./pictures/jdk17_Win10/img_2.png)

&emsp;&emsp;安装完成

![安装完成](./pictures/jdk17_Win10/img_3.png)


# 三、添加环境变量

&emsp;&emsp;计算机图标->鼠标右键->属性->高级系统设置->环境变量，进入系统变量(s)

## 3.1、添加系统变量(s)

&emsp;&emsp;设置`JAVA_HOE`环境变量

```html
JAVA_HOE = E:\install_code\jdk\jdk17\jdk
```

![安装完成](./pictures/jdk17_Win10/img_4.png)

## 3.2、添加`Path`项

&emsp;&emsp;编辑`Path`项，追加`%JAVA_HOME%\bin`

![添加`Path`项](./pictures/jdk17_Win10/img_5.png)

# 四、测试

```html
java -version
```

![查看版本](./pictures/jdk17_Win10/img_6.png)

# 五、踩坑

&emsp;&emsp;如果非自定义安装目录的话，需要删除如下`Path`项：

> <span style="color:red">由于JDK17在我们安装的时候可能会自动进行环境变量配置，我们需要在环境变量配置PATH中删除如下信息。</span>

![踩坑](./pictures/jdk17_Win10/img_7.png)





