# 参考文献
* [看完这篇大总结，彻底掌握Redis的使用！](https://www.zhihu.com/question/486201255?utm_id=0)
* [Redis和Redis可视化管理工具的下载和安装](https://blog.csdn.net/m0_62617719/article/details/131243067)

# 一、简介
> redis的官网地址，是http://redis.io
> Vmware在资助着redis项目的开发和维护。
> 从2010年3月15日起，Redis的开发工作由VMware主持。
> 从2013年5月开始，Redis的开发由Pivotal赞助。    

## 1.1 redis是什么
&emsp;&emsp;据说有一名意大利程序员，在 2004 年到 2006 年间主要做嵌入式工作，之后接触了 Web，2007 年和朋友共同创建了一个网站，
并为了解决这个网站的负载问题（为了避免 MySQL 的低性能），于是亲自定做一个数据库，并于 2009 年开发完成，这个就是 Redis。
这个意大利程序员就是 Salvatore Sanfilippo 江湖人称 Redis 之父，大家更习惯称呼他 Antirez。

&emsp;&emsp;Redis（Remote Dictionary Server )，即远程字典服务，是一个开源的使用ANSI C语言编写、支持网络、
可基于内存亦可持久化的日志型、Key-Value数据库，并提供多种语言的API。Redis属于非关系型数据库中的一种解决方案，
目前也是业界主流的缓存解决方案组件。

&emsp;&emsp;数百万开发人员在使用Redis用作数据库、缓存、流式处理引擎和消息代理。

## 1.2 为什么用redis
&emsp;&emsp;**Redis**是当前互联网世界最为流行的 NoSQL（Not Only SQL）数据库，它的性能十分优越，其性能远超关系型数据库，
可以**支持每秒十几万此的读/写操作**，并且还支持**集群、分布式、主从同步等**配置，理论上可以无限扩展节点，
它还**支持一定的事务能力**，这也保证了高并发的场景下数据的安全和一致性。

&emsp;&emsp;最重要的一点是：Redis的社区活跃，这个很重要。

&emsp;&emsp;Redis 已经成为 IT 互联网大型系统的标配，熟练掌握 Redis 成为开发、运维人员的必备技能。

## 1.3 优点
* 性能极高  
> 官方的 Benchmark 数据：测试完成了 50 个并发执行 10W 个请求。设置和获取的值是一个 256 字节字符串。

> 测试结果：Redis读的速度是110000次/s,写的速度是81000次/s 

* 数据类型丰富
> 支持 Strings, Lists, Hashes, Sets 及 Ordered Sets 数 据类型操作。

* 原子操作
> 所有的操作都是原子性的，同时Redis还支持对几个操作全并后的原子性执行。

* 功能丰富
> 提供了缓存淘汰策略、发布定义、lua脚本、简单事务控制、管道技术等功能特性支持

# 二、版本管理
&emsp;&emsp;Redis 使用标准的做法进行版本管理: **主版本号**.**副版本号**.**补丁号**。 偶数 **副版本号** 表示一个 **稳定的** 发布，
像 1.2, 2.0, 2.2, 2.4, 2.6, 2.8。**奇数副版本号**表示 **不稳定的** 发布，
例如 2.9.x 发布是一个不稳定版本，下一个稳定版本将会是Redis 3.0。

# 三、安装部署
## 3.1、windows安装
&emsp;&emsp;Redis在Windows上不受官方支持。但是，我们还是可以按照下面的说明在Windows上安装Redis进行开发。

> [官方window安装教程](https://redis.io/docs/getting-started/installation/install-redis-on-windows/)这个照着安装是行不通的！！！！

> 但是有开源爱好者提供了window的免安装方式，操作非常简单，下面进行步骤说明。

### 3.1.1、获取安装包
> 直接下载开源的redis-window版本[安装包](https://github.com/tporadowski/redis/releases)

![redis安装资源列表](./pictures/redis/img_1.png)

> 下载后解压到本地磁盘某个目录，目录结构如下：

![redis目录结构](./pictures/redis/img_2.png)

> redis-server.exe和redis.windows.conf就是我们接下来要用到的重要文件了。

### 3.1.2、启动服务

> 启动服务
&emsp;&emsp;打开一个 cmd 窗口 使用 cd 命令切换目录到 D:\install_4_code\Redis-x64-5.0.14.1 运行如下命令：
```shell
redis-server.exe redis.windows.conf
```

![redis启动结果](./pictures/redis/img_3.png)

> 如果想方便的话，可以把 redis 的路径加到系统的环境变量里，这样就省得再输路径了，
> 后面的那个 redis.windows.conf 可以省略，如果省略，会启用默认的。输入之后，会显示如下界面：

&emsp;&emsp;接下来保持这个cmd界面不要关闭，我们启动一个客户端去连接服务端。

### 3.1.3、客户端连接
&emsp;&emsp;服务端程序启动好之后，我们就可以用客户端去连接使用了，目前已经有很多开源的图形化客户端比如Redis-Desktop-Manager，
redis本身也提供了命令行客户端连接工具，接下来我们直接用命令行工具去连接测试。

另启一个 cmd 窗口，原来的不要关闭，不然就无法访问服务端了（不是后台执行的方式）。切换到目录D:\install_4_code\Redis-x64-5.0.14.1，执行如下命令：

```shell
redis-cli.exe -h 127.0.0.1 -p 6379
```

### 3.1.4、检验
&emsp;&emsp;设置一个key名为"hello"，value是"world"的键值对:

![redis-helloWorld校验](./pictures/redis/img_4.png)

## 3.2、linux安装