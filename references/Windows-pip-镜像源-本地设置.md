# 参考文献

# 永久设置镜像源

&emsp;&emsp;如果你想永久更改 `pip` 镜像源，可以修改 `pip` 的配置文件。

# 一、方法一

&emsp;&emsp;<span style="color:red">找到 或 创建 `pip.ini` 配置文件 </span>

&emsp;&emsp;在 `Windows 10` 上，`pip` 的配置文件通常位于以下位置：

## 1.1、全局配置（对所有用户生效）：

```shell
  C:\ProgramData\pip\pip.ini
```

## 1.2、用户级配置（仅对当前用户生效）：

```shell
  C:\Users\<YourUsername>\AppData\Roaming\pip\pip.ini
```

> 如果 `pip.ini` 文件不存在，你可以手动创建它。


## 1.3、配置镜像源

&emsp;&emsp;编辑 `pip.ini` 文件，添加如下内容：

```shell
[global]
index-url = https://mirrors.aliyun.com/pypi/simple/
```

> 首推 <span style="color:red">阿里云镜像</span>   
> 保存后，所有的 pip 安装命令都会默认使用你配置的镜像源。

&emsp;&emsp;常见的几个镜像源包括:

```shell
# 清华大学
https://pypi.tuna.tsinghua.edu.cn/simple

# 阿里云
https://mirrors.aliyun.com/pypi/simple/

# 中国科技大学
https://pypi.mirrors.ustc.edu.cn/simple/

# 华为云
https://mirrors.huaweicloud.com/repository/pypi/simple/
```

# 二、方法二

&emsp;&emsp;使用环境变量配置镜像源（可选）

<hr/>

## 2.1、在 `环境变量` 中添加新的变量：

* 右键点击“此电脑” -> “属性” -> “高级系统设置” -> “环境变量”。
* 在“用户变量”或“系统变量”中点击“新建”。
* 变量名：`PIP_INDEX_URL`
* 变量值：`https://mirrors.aliyun.com/pypi/simple/`

# 三、验证

&emsp;&emsp;使用以下命令来验证镜像源是否生效：

```shell
pip config list
```

> 结果：

```shell
C:\Users\bruce>pip config list
global.index-url='https://mirrors.aliyun.com/pypi/simple/'

```