# 参考文献
* [跨域问题详解](http://www.itsoku.com/course/6/242)
* [springboot实现跨域的五种方式](https://blog.csdn.net/weixin_48581386/article/details/129062092)
* [SpringBoot 跨域配置](https://blog.csdn.net/feyehong/article/details/126335715)

# 一、背景
> 为什么会出现跨域问题

&emsp;&emsp;出于浏览器的同源策略限制。同源策略（Same origin policy）是一种约定，它是浏览器最核心、最基本的安全功能，
如果缺少了同源策略，则浏览器的正常功能可能都会受到影响。可以说Web是构建在同源策略基础之上的，浏览器只是针对同源策略的一种实现。

> 同源策略

&emsp;&emsp;同源策略会阻止一个域的javascript脚本和另外一个域的内容进行交互。

&emsp;&emsp;所谓同源（即指在同一个域）就是两个页面具有相同的协议（protocol），主机（host）和端口号（port）

> 简单示例：

&emsp;&emsp;下表给出了与 URL http://store.company.com/dir/page.html 的源进行对比的示例:

| ID | URL                                         | 结果  | 原因        |
|----|---------------------------------------------|-------|------------|
| 1  | http://store.company.com/dir2/other.html    | 同源   | 只有路径不同  |
| 2  | https://store.company.com/secure.html		| 非同源	| 协议不同      |
| 3  | http://store.company.com:81/dir/etc.html	| 非同源	| 端口不同      |
| 4  | http://news.company.com/dir/other.html	    | 非同源	| 主机不同      |

&emsp;&emsp;也就是说当在http://store.company.com/dir/page.html这个网站中向 1、2、3 三个地址发起 Ajax 请求都会失败并且会报跨域的错误。
这就是浏览器的同源策略，只能访问同源的数据。

> 事实上，不是所有跨源操作都被禁止。跨源操作主要分为三大类，**跨源写操作**、**跨源资源嵌入**和**跨源读操作**。

* **跨源写操作**一般是被允许的。如超链接、重定向、表单提交。
* **跨源资源嵌入**一般也是被允许的。如嵌入&lt;img/>、&lt;video/>、&lt;audio/>等。
* **跨源读操作**不被允许，要实现跨源读操作，可以使用JSONP或CORS。

# 二、跨源访问（springboot）

> 这边只讨论后端跨域访问

&emsp;&emsp;对于 CORS的跨域请求，主要有以下几种方式可供选择：

* 返回自定义的CorsFilter (全局配置)
* 重写 WebMvcConfigurer (全局配置)
* 使用注解 @CrossOrigin  (局部配置)
* 手动设置响应头 (HttpServletResponse)  (局部配置)
* 自定web filter 实现跨域

> CorFilter、WebMvConfigurer、@CrossOrigin 需要 SpringMVC **4.2**以上版本才支持，
> 对应springBoot **1.3** 版本以上

> 如果使用了局部跨域是会覆盖全局跨域的规则，所以可以通过 @CrossOrigin 注解来进行细粒度更高的跨域资源控制。

> 其实无论哪种方案，最终目的都是修改响应头，向响应头中添加浏览器所要求的数据，进而实现跨域

## 2.1、配置自定义的 CorsFilter Bean

> 在任意配置类，返回一个 新的 CorsFilter Bean ，并添加映射路径和具体的CORS配置路径

```java
@Configuration
public class GlobalCorsConfigConfiguration {

    @Bean
    public CorsFilter corsFilter() {
        //1. 添加 CORS配置信息
        CorsConfiguration config = new CorsConfiguration();
        // 放行哪些原始域
        //config.addAllowedOrigin("*");
        //原本是addAllowedOrigin,改为addAllowedOriginPattern
        config.addAllowedOriginPattern("*");
        // 是否发送 Cookie
        config.setAllowCredentials(true);
        // 放行哪些请求方式
        config.addAllowedMethod("*");
        // 放行哪些原始请求头部信息
        config.addAllowedHeader("*");
        // 暴露哪些头部信息
        config.addExposedHeader("*");
        //2. 添加映射路径
        UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
        corsConfigurationSource.registerCorsConfiguration("/**",config);
        //3. 返回新的CorsFilter
        return new CorsFilter(corsConfigurationSource);
    }
}
```

> 在springboot中配置跨域时，可能出现如下错误：
>> When allowCredentials is true, allowedOrigins cannot contain the special value “*” since that cannot be set on the “Access-Control-Allow-Origin” response header. To allow credentials to a set of origins, list them explicitly or consider using “allowedOriginPatterns” instead.
>>> 有可能是这个原因:
>>>> springboot升级成2.4.0以上时对AllowedOrigin设置发生了改变，不能有”*“

## 2.2、重写 WebMvcConfigurer
```java
@Configuration
public class CorsWebMvcConfigurer implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                //是否发送Cookie
                .allowCredentials(true)
                //放行哪些原始域
                .allowedOrigins("*")
                .allowedMethods("GET", "POST", "PUT", "DELETE")
                .allowedHeaders("*")
                .exposedHeaders("*");
    }
}
```

## 2.3、使用注解

> 在控制器(类上)上使用注解 @CrossOrigin:，表示该类的所有方法允许跨域。

```java
@RestController
@CrossOrigin(origins = "*")
public class HelloController {
    @RequestMapping("/hello")
    public String hello() {
        return "hello world";
    }
}
```

> 在方法上使用注解 @CrossOrigin:

```shell
    @RequestMapping("/hello")
    @CrossOrigin(origins = "*")
     //@CrossOrigin(value = "http://localhost:8081") //指定具体ip允许跨域
    public String hello() {
        return "hello world";
    }
```

## 2.4、手动设置响应头

> 使用 HttpServletResponse 对象添加响应头(Access-Control-Allow-Origin)来授权原始域，这里 Origin的值也可以设置为 “*”,表示全部放行

```shell
@RequestMapping("/index")
public String index(HttpServletResponse response) {
    response.addHeader("Access-Allow-Control-Origin","*");
    return "index";
}
```

## 2.5、使用自定义filter实现跨域

> 先编写一个过滤器，可以起名字为MyCorsFilter.java

```java
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
@Component
public class MyCorsFilter implements Filter {
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    HttpServletResponse response = (HttpServletResponse) res;
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
    response.setHeader("Access-Control-Max-Age", "3600");
    response.setHeader("Access-Control-Allow-Headers", "x-requested-with,content-type");
    chain.doFilter(req, res);
  }
  public void init(FilterConfig filterConfig) {}
  public void destroy() {}
}
```

> 在web.xml中配置这个过滤器，使其生效

```shell
<!-- 跨域访问 START-->
<filter>
    <filter-name>CorsFilter</filter-name>
    <filter-class>com.mesnac.aop.MyCorsFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>CorsFilter</filter-name>
    <url-pattern>/*</url-pattern>
</filter-mapping>
<!-- 跨域访问 END  -->

```

> springboot可以简化以上配置

```java
import org.springframework.context.annotation.Configuration;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebFilter(filterName = "CorsFilter ")
@Configuration
public class CorsFilter implements Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin","*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PATCH, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        chain.doFilter(req, res);
    }
}
```