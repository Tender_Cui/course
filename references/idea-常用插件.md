# 常用插件

# MybatisX

* **作用**：`mapper.java` 到 `mapper.xml` 的跳转！

# Maven Helper

* **作用**：`maven` 坐标引入相关 `jar` 包后，可能会导致版本冲突，利用该插件，可以很方便的解决冲突！



