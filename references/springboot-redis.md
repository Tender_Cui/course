# 参考文献

* [Spring Boot整合Redis](https://zhuanlan.zhihu.com/p/689217297)

# 工程实例简介

> 详见工程：<span style="color:red">shiro</span> 下的模块：shiro-framework-integration/<span style="color:red">shiro-redis</span>

# 1、导入依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-redis</artifactId>
</dependency>
```

2、application.yaml 增加 redis 的配置

```yaml
spring:
  ##### redis 连接信息配置 #####
  redis:
    # Redis服务器地址
    host: 192.168.255.100
    # Redis服务器端口号
    port: 6379
    # 使用的数据库索引，默认是0
    database: 0
    # 连接超时时间
    timeout: 1800000
    # 设置密码 88888888 加密后是 MgNhKm6EEYeXt/cPzQ01lY8YFhOTCSdy
    password: ENC(MgNhKm6EEYeXt/cPzQ01lY8YFhOTCSdy)
```

3、测试

## 3.1、新建测试类：RedisTest

```java
package com.tender.redis;

import com.tender.ShiroApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = {ShiroApplication.class})
@RunWith(SpringRunner.class)
public class RedisTest {

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    // 注意这里，没有用 泛型
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void contextLoads() {
        redisTemplate.opsForValue().set("name", "tender");
        System.out.println("==> 从redis 获取 key = name 对应的值");
        System.out.println(redisTemplate.opsForValue().get("name"));
    }

}

```

## 3.2、测试字符串

![测试结果](./pictures/springboot-redis/img.png)

# 3.3、查看key的序列化字符串

&emsp;&emsp;启动 redis-cli 客户端，查看 keys *

```html
[root@master bin]# redis-cli
127.0.0.1:6379> auth 88888888
OK
127.0.0.1:6379> keys *
1) "\xac\xed\x00\x05t\x00\x04name"
2) "name"
127.0.0.1:6379>
```

&emsp;&emsp;可以看到有乱码，因为保存的key 默认使用的是JDK序列化方式，因此我们需要给 RedisTemplate 来定制化我们自己的序列化方式。

# 3.4、测试对象

```java
package com.tender.domain;

import lombok.Data;
import java.io.Serializable;

@Data
public class ShiroRole {
    private Long id;

    private Long rowId;

    private String roleName;

    private String label;

    private String description;

    private Integer sortNo;

    private String enableFlag;

    private static final long serialVersionUID = 1L;
}
```

&emsp;&emsp;新增测试代码：

```html
@Test
public void testModel() {
    ShiroRole shiroRole = new ShiroRole();
    shiroRole.setRoleName("simple_role");
    redisTemplate.opsForValue().set("shiro_role", shiroRole);
    System.out.println("==> 从redis 获取 key = shiro_role 对应的值");
    System.out.println(redisTemplate.opsForValue().get("shiro_role"));
}
```

# 3.5、测试对象结果

![测试对象结果](./pictures/springboot-redis/img_1.png)

&emsp;&emsp;这是因为对象序列化需要实现 Serializable 接口

```java
package com.tender.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class ShiroRole implements Serializable {
    private Long id;

    private Long rowId;

    private String roleName;

    private String label;

    private String description;

    private Integer sortNo;

    private String enableFlag;

    private static final long serialVersionUID = 1L;
}
```

![测试对象结果02](./pictures/springboot-redis/img_2.png)

# 4、自定义 RedisTemplate

```java
package com.tender.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) throws Exception {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);

        //JSON序列化配置
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        Jackson2JsonRedisSerializer<Object> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        jackson2JsonRedisSerializer.setObjectMapper(objectMapper);

        //String序列化配置
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        //key采用String的序列化方式
        template.setKeySerializer(stringRedisSerializer);
        //hash的key采用String的序列化方式
        template.setHashKeySerializer(stringRedisSerializer);
        //value序列化方式采用jackson
        template.setValueSerializer(jackson2JsonRedisSerializer);
        //hash的value序列化方式采用jackson
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();

        //开启事务
        template.setEnableTransactionSupport(true);

        return template;
    }
}
```

# 5、测试，调整测试类

## 5.1、清空 redis 测试库的 key

&emsp;&emsp;先清空 redis 测试库的所有 key

```html
# 生产禁止使用
127.0.0.1:6379> flushdb
OK
127.0.0.1:6379> 
127.0.0.1:6379> keys *
(empty array)
```

## 5.2、重跑测试案例

![重跑测试案例结果](./pictures/springboot-redis/img_3.png)

&emsp;&emsp;查看 key 列表

```html
127.0.0.1:6379> keys *
1) "name"
2) "shiro_role"
127.0.0.1:6379> 
```

# 6、封装redis 工具类

&emsp;&emsp;我们基本是不会使用原生的方法来存取数据的，形如redisTemplate.opsForValue().set()编写代码比较麻烦。

&emsp;&emsp;通常，我们会自定义工具类，后续操作数据都通过工具类来处理。

```java
package com.tender.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class RedisUtils {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 指定缓存失效时间
     *
     * @param key     键
     * @param timeout 时间(秒)
     * @return
     */
    public boolean expire(String key, long timeout) {
        try {
            if (timeout > 0) {
                redisTemplate.expire(key, timeout, TimeUnit.SECONDS);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key获取过期时间
     *
     * @param key 键 不能为null
     * @return 时间(秒) 0 永久有效
     */
    public long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断key是否存在
     *
     * @param key 键
     * @return true 存在 false 不存在
     */
    public boolean hasKey(String key) {
        if (StringUtils.isEmpty(key)) {
            return false;
        }

        try {
            return Boolean.TRUE.equals(redisTemplate.hasKey(key));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 普通缓存获取
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 普通缓存放入
     *
     * @param key   键
     * @param value 值
     * @return true 成功 false 失败
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 普通缓存放入并设置时间
     *
     * @param key     键
     * @param value   值
     * @param timeout 时间(秒) time要大于0，如果time小于等于0，将设置无限期
     * @return true 成功 false 失败
     */
    public boolean set(String key, Object value, long timeout) {
        try {
            if (timeout <= 0) {
                return set(key, value);
            }

            redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除缓存
     *
     * @param keys 可以传一个值或多个
     */
    public void delete(String... keys) {
        if (keys == null || keys.length <= 0) {
            return;
        }

        if (keys.length == 1) {
            redisTemplate.delete(keys[0]);
        } else {
            redisTemplate.delete((Collection<String>) CollectionUtils.arrayToList(keys));
        }
    }

    /**
     * 递增
     *
     * @param key
     * @param delta
     * @return
     * @throws Exception
     */
    public long increment(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递增因子必须 > 0");
        }

        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 递减
     *
     * @param key
     * @param delta
     * @return
     * @throws Exception
     */
    public long decrement(String key, long delta) {
        if (delta < 0) {
            throw new RuntimeException("递减因子必须 > 0");
        }

        return redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * 判断hash表中是否有该项的值
     *
     * @param key     键 不能为null
     * @param hashKey 项 不能为null
     * @return true 存在 false 不存在
     */
    public boolean hasKeyHash(String key, String hashKey) {
        return Boolean.TRUE.equals(redisTemplate.opsForHash().hasKey(key, hashKey));
    }

    /**
     * 获取缓存
     *
     * @param key     键 不能为null
     * @param hashKey 项 不能为null
     * @return 值
     */
    public Object getHash(String key, String hashKey) {
        return key == null ? null : redisTemplate.opsForHash().get(key, hashKey);
    }

    /**
     * 向一张hash表中放入数据，如果不存在将创建
     *
     * @param key     键
     * @param hashKey 项
     * @param value   值
     * @return true 成功 false失败
     */
    public boolean setHash(String key, String hashKey, Object value) {
        try {
            redisTemplate.opsForHash().put(key, hashKey, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 向一张hash表中放入数据，如果不存在将创建
     *
     * @param key     键
     * @param hashKey 项
     * @param value   值
     * @param timeout 时间(秒) 注意：如果已存在的hash表有时间,这里将会替换原有的时间
     * @return true 成功 false 失败
     */
    public boolean setHash(String key, String hashKey, Object value, long timeout) {
        try {
            redisTemplate.opsForHash().put(key, hashKey, value);

            if (timeout > 0) {
                expire(key, timeout);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除hash表中的值
     *
     * @param key      键 不能为null
     * @param hashKeys 项 可以是多个 不能为null
     */
    public void deleteHash(String key, Object... hashKeys) {
        redisTemplate.opsForHash().delete(key, hashKeys);
    }

    /**
     * 递增
     *
     * @param key
     * @param hashKey
     * @param delta
     * @return
     */
    public long incrementHash(String key, String hashKey, long delta) {
        return redisTemplate.opsForHash().increment(key, hashKey, delta);
    }

    /**
     * 递增
     *
     * @param key
     * @param hashKey
     * @param delta
     * @return
     */
    public double incrementHash(String key, String hashKey, double delta) {
        return redisTemplate.opsForHash().increment(key, hashKey, delta);
    }

    /**
     * 递减
     *
     * @param key
     * @param hashKey
     * @param delta
     * @return
     */
    public long decrementHash(String key, String hashKey, long delta) {
        return redisTemplate.opsForHash().increment(key, hashKey, -delta);
    }

    /**
     * 递减
     *
     * @param key
     * @param hashKey
     * @param delta
     * @return
     */
    public double decrementHash(String key, String hashKey, double delta) {
        return redisTemplate.opsForHash().increment(key, hashKey, -delta);
    }

    /**
     * 获取key对应的所有键值
     *
     * @param key 键
     * @return 对应的多个键值
     */
    public Map<Object, Object> getHash(String key) {
        return key == null ? null : redisTemplate.opsForHash().entries(key);
    }

    /**
     * 缓存
     *
     * @param key 键
     * @param map 对应多个键值
     * @return true 成功 false 失败
     */
    public boolean setHash(String key, Map<String, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 缓存并设置超时时间
     *
     * @param key     键
     * @param map     对应多个键值
     * @param timeout 时间(秒)
     * @return true 成功 false 失败
     */
    public boolean setHash(String key, Map<String, Object> map, long timeout) {
        try {
            redisTemplate.opsForHash().putAll(key, map);

            if (timeout > 0) {
                expire(key, timeout);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据key获取Set中的所有值
     *
     * @param key 键
     * @return
     */
    public Set<Object> membersSet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据value从一个set中查询，是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false 不存在
     */
    public boolean isMemberSet(String key, Object value) {
        try {
            return Boolean.TRUE.equals(redisTemplate.opsForSet().isMember(key, value));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将数据放入set缓存
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 成功个数
     */
    public long addSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().add(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 将set数据放入缓存
     *
     * @param key     键
     * @param timeout 时间(秒)
     * @param values  值 可以是多个
     * @return 成功个数
     */
    public long addSet(String key, long timeout, Object... values) {
        try {
            Long count = redisTemplate.opsForSet().add(key, values);
            if (timeout > 0) {
                expire(key, timeout);
            }
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取set缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sizeSet(String key) {
        try {
            return redisTemplate.opsForSet().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 移除值为value的
     *
     * @param key    键
     * @param values 值 可以是多个
     * @return 移除的个数
     */
    public long removeSet(String key, Object... values) {
        try {
            return redisTemplate.opsForSet().remove(key, values);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取list缓存的内容
     *
     * @param key   键
     * @param start 开始
     * @param end   结束  0到-1代表所有值
     * @return
     */
    public List<Object> rangeList(String key, long start, long end) {
        try {
            return redisTemplate.opsForList().range(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取list缓存的长度
     *
     * @param key 键
     * @return
     */
    public long sizeList(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 通过索引获取list中的值
     *
     * @param key   键
     * @param index 索引  index>=0时，0 表头，1 第二个元素，依次类推；index<0时，-1，表尾，-2倒数第二个元素，依次类推
     * @return
     */
    public Object indexList(String key, long index) {
        try {
            return redisTemplate.opsForList().index(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean rightPushList(String key, Object value) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key     键
     * @param value   值
     * @param timeout 时间(秒)
     * @return
     */
    public boolean rightPushList(String key, Object value, long timeout) {
        try {
            redisTemplate.opsForList().rightPush(key, value);
            if (timeout > 0) {
                expire(key, timeout);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key   键
     * @param value 值
     * @return
     */
    public boolean rightPushList(String key, List<Object> value) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 将list放入缓存
     *
     * @param key     键
     * @param value   值
     * @param timeout 时间(秒)
     * @return
     */
    public boolean rightPushList(String key, List<Object> value, long timeout) {
        try {
            redisTemplate.opsForList().rightPushAll(key, value);
            if (timeout > 0) {
                expire(key, timeout);
            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 根据索引修改list中的某条数据
     *
     * @param key   键
     * @param index 索引
     * @param value 值
     * @return
     */
    public boolean setList(String key, long index, Object value) {
        try {
            redisTemplate.opsForList().set(key, index, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 移除N个值为value的数据
     *
     * @param key   键
     * @param count 移除多少个
     * @param value 值
     * @return 移除的个数
     */
    public long removeList(String key, long count, Object value) {
        try {
            return redisTemplate.opsForList().remove(key, count, value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
```

## 6.1、调整测试类

```html
    @Test
    public void testString02() {
        redisUtils.set("name02", "tender02");
        System.out.println("==> 从redis 获取 key = name02 对应的值");
        System.out.println(redisTemplate.opsForValue().get("name02"));
    }
```

## 6.2、测试结果

![测试结果](./pictures/springboot-redis/img_4.png)