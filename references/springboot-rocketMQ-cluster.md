# 参考文献

* [Java教程：RocketMq集群消息核心知识与SpringBoot整合并实现生产者与消费者](https://blog.csdn.net/wfeil211/article/details/127088257?spm=1001.2014.3001.5502)
* [【SpringBoot高级篇】SpringBoot集成RocketMQ消息队列](https://blog.csdn.net/qq_45297578/article/details/128728991)
* [Spring boot 3.0整合RocketMQ及不兼容的问题](https://blog.csdn.net/zhenweiyi/article/details/130722046)

# 1、消息生产者

## 1.1、pom.xl

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-rocketmq-provider</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.apache.rocketmq</groupId>
            <artifactId>rocketmq-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 1.2、配置类：RocketMqConfig

```java
package com.tender.config;

import org.apache.rocketmq.spring.autoconfigure.RocketMQAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({RocketMQAutoConfiguration.class})
public class RocketMqConfig {
    
}
```

## 1.3、生产者服务

```java
package com.tender.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 普通消息发送：
 * 1、单向发送：单向发送是指发送方只负责发送消息，不等待服务器回应，且没有回调函数触发。即只发送请求而不管响应。
 * 2、同步发送：同步发送是指消息发送方发出数据后，会在收到接收方发回响应之后才会发送下一个数据包的通讯方式。
 * 3、异步发送：异步发送是指发送方发出数据后，不等接收方发回响应，接着发送下一个数据包的通讯方式。发送方通过回调接口接收服务器响应，并对响应结果进行处理。
 */
@Slf4j
@Service
public class MqOrdinaryProducer {

    @Autowired
    private RocketMQTemplate rocketMqTemplate;

    /**
     * 单向消息
     */
    public void sendMq() {
        rocketMqTemplate.sendOneWay("topic-test", "测试发送单向消息》》》》》》》》》");
    }

    /**
     * 同步发送
     */
    public void sync() {
        SendResult sendResult = rocketMqTemplate.syncSend("topic-test", "sync发送消息。。。。。。。。。。");
        log.info("发送结果{}", sendResult);
    }

    /**
     * 异步发送
     */
    public void async() {
        String msg = "异步发送消息。。。。。。。。。。";
        log.info(">msg:<<" + msg);
        rocketMqTemplate.asyncSend("topic-test", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult var1) {
                log.info("异步发送成功: {}", var1);
            }

            @Override
            public void onException(Throwable var1) {
                //发送失败可以执行重试
                log.info("异步发送失败: ", var1);
            }
        });
    }
}
```

## 1.4、controller

```java
package com.tender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tender.service.MqOrdinaryProducer;

@RestController
@RequestMapping("/ordinary")
public class MqOrdinaryProducerController {

    @Autowired
    private MqOrdinaryProducer mqOrdinaryProducer;

    @GetMapping("/sendMq")
    public String sendMq() {
        mqOrdinaryProducer.sendMq();

        return "success 4 sendMq";
    }
    @GetMapping("/sync")
    public String sync() {
        mqOrdinaryProducer.sync();

        return "success 4 sync";
    }

    @GetMapping("/async")
    public String async() {
        mqOrdinaryProducer.async();

        return "success 4 async";
    }

}
```

## 1.5、启动报错问题

&emsp;&emsp;如果遇到如下问题：一个你没有配置过的ip + 端口

```shell
org.apache.rocketmq.remoting.exception.RemotingConnectException: connect to 10.244.235.192:10911 failed
	at org.apache.rocketmq.remoting.netty.NettyRemotingClient.invokeSync(NettyRemotingClient.java:407) ~[rocketmq-remoting-4.9.3.jar:4.9.3]
	at org.apache.rocketmq.client.impl.MQClientAPIImpl.sendHearbeat(MQClientAPIImpl.java:1033) ~[rocketmq-client-4.9.3.jar:4.9.3]
	at org.apache.rocketmq.client.impl.factory.MQClientInstance.sendHeartbeatToAllBroker(MQClientInstance.java:558) [rocketmq-client-4.9.3.jar:4.9.3]
	at org.apache.rocketmq.client.impl.factory.MQClientInstance.sendHeartbeatToAllBrokerWithLock(MQClientInstance.java:471) [rocketmq-client-4.9.3.jar:4.9.3]
	at org.apache.rocketmq.client.impl.factory.MQClientInstance$4.run(MQClientInstance.java:289) [rocketmq-client-4.9.3.jar:4.9.3]
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [na:1.8.0_351]
	at java.util.concurrent.FutureTask.runAndReset$$$capture(FutureTask.java:308) [na:1.8.0_351]
	at java.util.concurrent.FutureTask.runAndReset(FutureTask.java) [na:1.8.0_351]
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$301(ScheduledThreadPoolExecutor.java:180) [na:1.8.0_351]
	at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:294) [na:1.8.0_351]
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_351]
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_351]
	at java.lang.Thread.run(Thread.java:750) [na:1.8.0_351]
```

&emsp;&emsp;解决办法如下：

&emsp;&emsp;[请参考rocketMQ集群搭建手册](./Centos7-RocketMQ-cluster.md 'included')

```shell
#设置broker节点所在服务器的ip地址
# **这个非常重要**,主从模式下，从节点会根据主节点的brokerIP2来同步数据，如果不配置，主从无法同步
# brokerIP1设置为自己外网能访问的ip，服务器双网卡情况下必须配置，比如阿里云这种，
# 主节点需要配置ip1和ip2，
# 从节点只需要配置ip1即可
brokerIP1=192.168.255.101
brokerIP2=192.168.255.101
```

# 2、消息消费者

## 2.1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-rocketmq-consumer</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.apache.rocketmq</groupId>
            <artifactId>rocketmq-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 2.2、配置类：RocketMqConfig

```java
package com.tender.config;

import org.apache.rocketmq.spring.autoconfigure.RocketMQAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({RocketMQAutoConfiguration.class})
public class RocketMqConfig {
    
}
```

## 2.3、消费者监听服务

```java
package com.tender.listener;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RocketMQMessageListener(consumerGroup = "shiro-rocketmq-consumer", topic = "topic-test")
public class MqOrdinaryConsumer implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        log.info("接收到的数据是：{}", message);
    }
}

```

## 2.4、消费日志

```shell

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::               (v2.7.17)

2024-04-15 10:46:11.478  INFO 11476 --- [           main] c.t.ShiroRocketMqConsumerApplication     : Starting ShiroRocketMqConsumerApplication using Java 1.8.0_351 on DESKTOP-7MEKQSU with PID 11476 (E:\java_code_4_learning\shiro\shiro-framework-integration\shiro-rocketmq-consumer\target\classes started by Administrator in E:\java_code_4_learning\shiro)
2024-04-15 10:46:11.484  INFO 11476 --- [           main] c.t.ShiroRocketMqConsumerApplication     : No active profile set, falling back to 1 default profile: "default"
2024-04-15 10:46:14.749  INFO 11476 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 9097 (http)
2024-04-15 10:46:14.776  INFO 11476 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2024-04-15 10:46:14.776  INFO 11476 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.82]
2024-04-15 10:46:15.188  INFO 11476 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2024-04-15 10:46:15.188  INFO 11476 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 3570 ms
RocketMQLog:WARN No appenders could be found for logger (io.netty.util.concurrent.GlobalEventExecutor).
RocketMQLog:WARN Please initialize the logger system properly.
2024-04-15 10:46:21.491  INFO 11476 --- [           main] a.r.s.s.DefaultRocketMQListenerContainer : running container: DefaultRocketMQListenerContainer{consumerGroup='shiro-rocketmq-consumer', namespace='', nameServer='192.168.255.100:9876;192.168.255.101:9876', topic='topic-test', consumeMode=CONCURRENTLY, selectorType=TAG, selectorExpression='*', messageModel=CLUSTERING', tlsEnable=false}
2024-04-15 10:46:21.493  INFO 11476 --- [           main] o.a.r.s.a.ListenerContainerConfiguration : Register the listener to container, listenerBeanName:mqOrdinaryConsumer, containerBeanName:org.apache.rocketmq.spring.support.DefaultRocketMQListenerContainer_1
2024-04-15 10:46:21.537  INFO 11476 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 9097 (http) with context path ''
2024-04-15 10:46:21.547  INFO 11476 --- [           main] c.t.ShiroRocketMqConsumerApplication     : Started ShiroRocketMqConsumerApplication in 11.541 seconds (JVM running for 12.856)
2024-04-15 10:46:22.501  INFO 11476 --- [etmq-consumer_2] com.tender.listener.MqOrdinaryConsumer   : 接收到的数据是：测试发送单向消息》》》》》》》》》
2024-04-15 10:46:22.501  INFO 11476 --- [etmq-consumer_3] com.tender.listener.MqOrdinaryConsumer   : 接收到的数据是：异步发送消息。。。。。。。。。。
2024-04-15 10:46:22.501  INFO 11476 --- [etmq-consumer_1] com.tender.listener.MqOrdinaryConsumer   : 接收到的数据是：sync发送消息。。。。。。。。。。
```