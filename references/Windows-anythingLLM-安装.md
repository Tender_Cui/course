# 参考文献

* [anything-lLM官网](https://anythingllm.com/)

# 一、下载

* [anything-lLM官网下载](https://anythingllm.com/)

# 二、安装

&emsp;&emsp;无脑安装

# 三、使用

## 3.1、`lLM` 偏好设置

![偏好设置](./pictures/Windows-anythingLLM-安装/img.png)

## 3.2、数据处理与隐私

![数据处理与隐私](./pictures/Windows-anythingLLM-安装/img_1.png)

## 3.3、创建工作区

![创建工作区](./pictures/Windows-anythingLLM-安装/img_2.png)

