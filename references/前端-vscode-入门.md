# 参考文献

* [vscode 官网](https://code.visualstudio.com/)

# 一、使用入门

## 1.1、打开项目【推荐打开文件夹】

&emsp;&emsp;有<span style="color:red"> 三种 </span>打开文件项目的方式:

### 1.1.1、方式一
![打开文件夹](./pictures/前端-vscode-入门/img.png)

<hr>

### 1.1.2、方式二
![打开文件夹2](./pictures/前端-vscode-入门/img_1.png)

<hr>

### 1.1.3、方式三

![打开文件夹3](./pictures/前端-vscode-入门/img_2.png)

## 1.2、OUTLINE【大纲】

&emsp;&emsp;比如咱们打开一个 `html` 页面，点击 `OUTLINE` 就可以看到网页结构

![大纲](./pictures/前端-vscode-入门/img_3.png)

## 1.3、TIMELINE【时间线】

&emsp;&emsp;`TIMELINE` 类似于 `git` 版本管理工具，比如对同一个文件的多次修改与保存，可以通过
`TIMELINE` 看到它的修改记录。

> <span style="color:red">如何管理 OutLine 和 TimeLine </span> ?

![如何管理](./pictures/前端-vscode-入门/img_4.png)

# 二、常用设置

## 2.1、设置字体大小

&emsp;&emsp;**搜索** `font`

![设置字体大小](./pictures/前端-vscode-入门/img_5.png)

## 2.2、颜色主题

![颜色主题](./pictures/前端-vscode-入门/img_6.png)

## 2.3、文件图标主题

![文件图标主题](./pictures/前端-vscode-入门/img_7.png)

<hr>

![选择文件图标主题](./pictures/前端-vscode-入门/img_8.png)

&emsp;&emsp;如果文件图标主题不合适，可以下载插件：`vscode-icons`

# 三、自动保存

![自动保存](./pictures/前端-vscode-入门/img_9.png)

<hr>

&emsp;&emsp;有如下几个选项：

![有如下几个选项](./pictures/前端-vscode-入门/img_10.png)

> 注意点：

* `afterDelay` 需要和 `Auto Save Delay` 配合使用。【单位是ms】

# 四、快速生成 `html` 标准结构

&emsp;&emsp;只需要<span style="color:red">一个感叹号 ! </span>

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

&emsp;&emsp;如果需要修改 `lang="zh-CN"` 怎么修改？

![自定义我们的html页面结构](./pictures/前端-vscode-入门/img_11.png)

# 五、给某个元素快速关联`id`或者`class`

&emsp;&emsp;举例：

* `div.container` 回车 == `<div class="container"></div>`
* `div#container` 回车 == `<div id="container"></div>`