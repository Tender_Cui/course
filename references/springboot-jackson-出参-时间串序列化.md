# 背景

&emsp;&emsp;在 `Spring Boot` 中，利用 `Jackson` 序列化 `Date` 类型的字段时，可以通过配置 `@JsonFormat` 注解来指定格式。
具体来说，你可以使用 `@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")` 来定义如何将 <span style="color:red">Date</span> 类型的字段格式化为你想要的时间字符串格式。

# 一、步骤

## 1.1、使用 `@JsonFormat` 注解格式化日期字段

&emsp;&emsp;`@JsonFormat` 注解允许你指定字段在序列化时的格式。

&emsp;&emsp;假设你有一个 `ResDTO` 类，其中有一个 `Date` 类型的字段，你希望将该字段格式化为 `yyyy-MM-dd HH:mm:ss`。

```java
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

@Data
public class ResDTO {

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date eventDate;
}
```

## 1.2、使用 `Jackson` 进行序列化

&emsp;&emsp;在 `Spring Boot` 中，使用 `ObjectMapper` 可以方便地进行对象到 `JSON` 的转换。以下是一个简单的序列化示例：

```java
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {
    public static void main(String[] args) throws Exception {
        ResDTO dto = new ResDTO();
        dto.setName("Sample Event");
        dto.setEventDate(new Date()); // 当前时间

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(dto);

        System.out.println(json); // 打印 JSON 字符串
    }
}
```

> 输出示例：

```json
{
  "name": "Sample Event",
  "eventDate": "2025-01-03 14:30:00"
}
```

> 在这个例子中，eventDate 被格式化为 yyyy-MM-dd HH:mm:ss。




## 1.3、全局配置日期格式（可选）

&emsp;&emsp;如果你希望全局统一日期格式，而不仅仅是为某个字段添加注解，你可以通过配置 `Jackson2ObjectMapperBuilder` 来设置全局的日期格式。

&emsp;&emsp;在 `Spring Boot` 的 `application.properties` 或 `application.yml` 中，你可以添加以下配置：

* `application.properties` 配置：

```properties
spring.jackson.date-format=yyyy-MM-dd HH:mm:ss
```

* `application.yml` 配置：

```yaml
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
```

> 这样，所有的 `Date` 类型字段都会被格式化成 `yyyy-MM-dd HH:mm:ss`。

# 二、序列化自定义操作【高级】

&emsp;&emsp;如果你需要更多的自定义逻辑（例如，不仅仅是格式化，还要做一些额外的处理），你可以实现自定义的 `JsonSerializer`。

```java
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateSerializer extends JsonSerializer<Date> {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        String formattedDate = dateFormat.format(date);
        jsonGenerator.writeString(formattedDate);
    }
}
```

&emsp;&emsp;然后，通过 `@JsonSerialize` 注解使用这个自定义的序列化器：

```java
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import java.util.Date;

@Data
public class Event {

    private String name;

    @JsonSerialize(using = CustomDateSerializer.class)
    private Date eventDate;
}
```

# 三、总结

* 使用 <span style="color:red">@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")</span> 注解，可以在字段级别指定日期格式。
* 全局配置：通过 <span style="color:green">spring.jackson.date-format </span>属性，你可以全局配置 `Jackson` 处理所有 `Date` 字段时的格式。
* 自定义序列化器：如果需要更复杂的格式化或逻辑，可以实现自定义的 `JsonSerializer`。