# 参考文献

* [Harbor安装部署实战详细手册](https://blog.csdn.net/qq359605040/article/details/129025958)

# 一、简介

&emsp;&emsp;Docker 是一个开源的应用容器引擎，基于 Go 语言 并遵从 Apache2.0 协议开源。

&emsp;&emsp;Docker 可以让开发者打包他们的应用以及依赖包到一个轻量级、可移植的容器中，然后发布到任何流行的 Linux 机器上，也可以实现虚拟化。   

&emsp;&emsp;容器是完全使用沙箱机制，相互之间不会有任何接口（类似 iPhone 的 app）,更重要的是容器性能开销极低。

> Docker容器技术 vs 传统虚拟机技术

![Docker容器技术 vs 传统虚拟机技术](./pictures/docker_Centos7_快速入门/img.png)


|    对比维度     |           虚拟机           |       容器         |
|:-------------:|:--------------------------:|:------------------:|
|   占用磁盘空间  |         非常大，GB级        |   小，MB甚至KB级     |
|     启动速度   |          慢，分钟级         |       快，秒级       |
|    运行形态    |      运行于Hypervisor上     | 直接运行在宿主机内核上 |
|     并发性     | 一台宿主机上十几个，最多几十个 | 上百个，甚至数百上千个 |
|      性能      |         逊于宿主机         |   接近宿主机本地进程   |
|    资源利用率   |             低            |          高         |


> 简单一句话总结：Docker技术就是让我们更加高效轻松地将任何应用在Linux服务器部署和使用。

# 二、安装

&emsp;&emsp;安装之前先把 <span style="color:red">SELINUX</span> 关闭，否则安装了 docker 之后可能不能联网

![关闭SELINUX](./pictures/docker_Centos7_快速入门/img_1.png)

```shell
/etc/selinux/config

# 将 SELINUX=enforcing 改为 SELINUX=disabled 
# 重启机子
```

## 2.1、卸载旧版本

```shell
# 列出已经安装的docker包
yum list installed | grep docker

# 卸载已经安装的docker
yum -y remove docker*

# 删除docker的所有镜像和容器
rm -rf /var/lib/docker 
```

## 2.2、安装必要的工具

```shell
sudo yum install -y yum-utils device-mapper-persistent-data lvm2 
```

## 2.3、设置下载的镜像仓库

```shell
yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

## 2.4、列出需要安装的版本列表

```shell
yum list docker-ce --showduplicates | sort -r
```

```shell
docker-ce.x86_64            3:24.0.7-1.el7                      docker-ce-stable
docker-ce.x86_64            3:24.0.6-1.el7                      docker-ce-stable
docker-ce.x86_64            3:24.0.5-1.el7                      docker-ce-stable
docker-ce.x86_64            3:24.0.4-1.el7                      docker-ce-stable
docker-ce.x86_64            3:24.0.3-1.el7                      docker-ce-stable
docker-ce.x86_64            3:24.0.2-1.el7                      docker-ce-stable
docker-ce.x86_64            3:24.0.1-1.el7                      docker-ce-stable
...省略...
docker-ce.x86_64            3:18.09.0-3.el7                     docker-ce-stable
docker-ce.x86_64            18.06.3.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.2.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.1.ce-3.el7                    docker-ce-stable
docker-ce.x86_64            18.06.0.ce-3.el7                    docker-ce-stable
...省略...
```

## 2.5、安装docker

```shell
#安装 docker-ce 社区版
yum -y install docker-ce-20.10.7 docker-ce-cli-20.10.7 containerd.io-1.4.6
```

## 2.6、查看版本

```shell
docker -v
```

![docker版本号](./pictures/docker_Centos7_快速入门/img_2.png)

## 2.7、启动Docker

```shell
# 启动和自启动
sudo systemctl start docker
sudo systemctl enable docker
```

## 2.8、添加阿里云镜像下载地址

```shell
# 添加国内镜像，如果没有这个文件，就新建
vim /etc/docker/daemon.json
```

> 文件内容如下：

```shell
# 雷神的加速地址：https://82m9ar63.mirror.aliyuncs.com
# 连接 harbor的时候设置的
# "insecure-registries": ["192.168.255.102:5000"]

{
  "registry-mirrors": ["https://82m9ar63.mirror.aliyuncs.com"],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "insecure-registries": ["192.168.255.102:5000"]
}
```

## 2.9、重启docker

```shell
sudo systemctl restart docker
```

# 三、Docker基本命令

## 3.1、镜像命令

&emsp;&emsp;**镜像**：相当于应用的安装包，在Docker部署的任何应用都需要先构建成为镜像

```shell
docker search            # 镜像名称 搜索镜像 
docker pull 镜像名称      # 拉取镜像 
docker pull 镜像名称:版本  # 拉取镜像:对应的版本 
docker images            # 查看本地所有镜像
docker rmi -f 镜像名称    #删除镜像
# 例子
docker pull openjdk:8-jdk-alpine
```

## 3.2、容器命令

&emsp;&emsp;**容器**：容器是由镜像创建而来。容器是Docker运行应用的载体，每个应用都分别运行在Docker的每个容器中。

```text
docker run -i 镜像名称:标签  # 运行容器（默认是前台运行）
docker run -id 镜像名称:标签 # 运行容器（-d 后台运行）

eg: 
docker run -d --name nginx -p 80:80 nginx
docker run -d --name myCourse -p 8080:8080 course:v1

docker ps                  # 查看运行的容器
docker ps -a               # 查看所有的容器

# 查询所有容器常用的参数：
-i：     运行容器
-d：     后台守方式运行（守护式）
--name： 给容器添加名称
-p：     公开容器端口给当前宿主机            # 常用  外部:内部
-v：     挂载目录

docker exec -it 容器ID/容器名称 /bin/bash  # 进入容器内部 
docker start/stop/restart  容器名称/ID    # 启动/停止/重启容器 
docker rm -f 容器名称/ID                  # 强制删除容器（无论容器是否正在运行）
docker logs -f 容器名称/ID                # 查看容器日志

```

# 四、Dockerfile

## 4.1、Dockerfile简介

&emsp;&emsp;Dockerfile 其实就是我们用来构建Docker镜像的源码，当然这不是所谓的编程源码，而是一些命令的组合，只要理解它的逻辑和语法格式，就可以编写Dockerﬁle了。

&emsp;&emsp;简单点说，Dockerfile的作用：它可以让用户个性化定制Docker镜像。因为工作环境中的需求各式各样，网络上的镜像很难满足实际的需求。

> 镜像构建示意图:

![镜像构建示意图](./pictures/docker_Centos7_快速入门/img_3.png)

&emsp;&emsp;可以看到，新镜像是从基础镜像一层一层叠加生成的。每安装一个软件，就在现有镜像的基础上增加一层。

&emsp;&emsp;Dockerfile 常见命令：

|            对比维度                 |              虚拟机                      |
|:----------------------------------:|:------------------------------------|
|        FROM image_name:tag         |                继承基础镜像                 |
|        MAINTAINER user_name        |                声明镜像的作者                |
|           ENV key value            |            设置环境变量 (可以写多条)             |
|            RUN command             |           编译镜像时运行的脚本(可以写多条)           |
|                CMD                 |         设置容器的启动命令（无法在运行中接收参数）         |
|             ENTRYPOINT             |         设置容器的入口程序（可以在运行中接收参数）         |
| ADD source_dir/file dest_dir/file  |  将宿主机的文件复制到容器内，如果是一个压缩文件，将会在复制后**自动解压**   |
| COPY source_dir/file dest_dir/file |         和ADD相似，但是如果有压缩文件并**不能解压**         |
|          WORKDIR path_dir          |                设置工作目录                 |
|                ARG                 | docker build --build-arg 设置编译镜像时加入的参数 |
|               VOLUMN               |              设置容器的挂载卷/目录              |

&emsp;&emsp;<span style="color:red">RUN、CMD、ENTRYPOINT的区别？</span>

* RUN：用于指定 docker build 过程中要运行的命令，即是创建 Docker 镜像（image）的步骤
* CMD：设置容器的启动命令， Dockerfile 中只能有一条 CMD 命令，如果写了多条则最后一条生效，CMD不支持接收docker run的参数。
* ENTRYPOINT：入口程序是容器启动时执行的程序， docker run 中最后的命令将作为参数传递给入口程序 ，ENTRYPOINY类似于 CMD 指令，但可以接收docker run的参数 。

> 以下是mysql官方镜像的Dockerﬁle示例：

```shell
FROM oraclelinux:7-slim
ARG MYSQL_SERVER_PACKAGE=mysql-community-server-minimal-5.7.28
ARG MYSQL_SHELL_PACKAGE=mysql-shell-8.0.18
# Install server
RUN yum install -y https://repo.mysql.com/mysql-community-minimal-releaseel7.rpm \
    https://repo.mysql.com/mysql-community-release-el7.rpm \
    && yum-config-manager --enable mysql57-server-minimal \
    && yum install -y \
    $MYSQL_SERVER_PACKAGE \
    $MYSQL_SHELL_PACKAGE \
    libpwquality \
    && yum clean all \
    && mkdir /docker-entrypoint-initdb.d
VOLUME /var/lib/mysql
COPY docker-entrypoint.sh /entrypoint.sh
COPY healthcheck.sh /healthcheck.sh
ENTRYPOINT ["/entrypoint.sh"]
HEALTHCHECK CMD /healthcheck.sh
EXPOSE 3306 33060
CMD ["mysqld"]
```


> 下面是一个简单的 Dockerfile 示例，展示如何使用 JAR_FILE 构建参数：

```shell
# 基于一个基础镜像
FROM openjdk:8-jdk-alpine

# 变量必须先声明，后面才能使用
ARG JAR_FILE

# 将构建参数中的 JAR_FILE 复制到容器的 /app 目录
COPY $JAR_FILE /opt/application/course/course.jar

# 设置工作目录
WORKDIR /opt/application/course

RUN pwd

# 运行 JAR 文件
CMD ["java", "-jar", "course.jar"]
```

&emsp;&emsp;在这个示例中：
```text
我们首先指定了一个基础镜像 openjdk:8-jdk-alpine。

使用 COPY 指令和 $JAR_FILE 变量将 JAR 文件复制到容器的 /app 目录。

设置工作目录为 /app。

最后，使用 CMD 指令来运行 JAR 文件。

# -t 是为构建的镜像指定一个名字和标签的参数。
当你运行 docker build --build-arg JAR_FILE=xxxx-1.0-SNAPSHOT.jar -t eureka:v1 . 命令时，
Docker 会找到当前目录下的 Dockerfile，并使用指定的构建参数来构建镜像。
```
