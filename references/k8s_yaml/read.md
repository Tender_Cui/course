# 一、问题列表

## 1.1、K8s coredns STATUS 一直是 Pending 


## 1.2、解决办法

&emsp;&emsp;[查看github关于flannel的官网](https://github.com/flannel-io/flannel)，下载最新版本的 kube-flannel.yaml 文件。

```text
搜索如下描述，可以看到 flannel 与 k8s 的版本匹配。
# 
Deploying flannel manually
```

&emsp;&emsp;kube-flannel.yaml 是用来创建 flannel 网络的配置文件。

```shell

# 进入如下目录（视各人情况），将 kube-flannel.yaml 拷贝到该目录
cd /opt/softs/k8s

# 安装
kubectl apply -f kube-flannel.yaml
```

&emsp;&emsp;[kube-flannel.yaml文件的具体内容：请点击!](./kube-flannel_v_0.24.0.yml 'include:')