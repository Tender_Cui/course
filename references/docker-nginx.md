# 参考文献

* [docker hub](https://www.docker.com/products/docker-hub/)
* [docker hub 搜索镜像](https://hub.docker.com/)
* [docker部署nginx](https://blog.csdn.net/qq_39689711/article/details/134931597)

# 1、拉取镜像

&emsp;&emsp;获取`nginx`镜像，镜像搜索 ☞ [请点击这里](https://hub.docker.com/)

## 1.1、搜索nginx

&emsp;&emsp;选择版本，可以去`docker hub`，搜索`nginx`。

![搜索nginx](./pictures/docker-nginx/img.png)

## 1.2、查看nginx版本

&emsp;&emsp;点击 <span style="color:red">tags</span>,可以看到历史版本。

![查看nginx版本](./pictures/docker-nginx/img_1.png)

## 1.3、docker命令查询镜像

&emsp;&emsp;以拉取`nginx`最新版本为示例：

```shell
# 也可以利用命令来搜索
docker search nginx
```

![docker命令查看结果](./pictures/docker-nginx/img_2.png)

## 1.4、docker命令拉取镜像

&emsp;&emsp;使用命令`docker pull nginx`拉取镜像，默认为下载最新版(`latest`)

```shell
# 默认拉取最新版本
docker pull nginx

# 指定下载版本1.20.1
docker pull nginx:1.20.1

# 查看下载到本地的镜像文件
docker images
```

![查看nginx镜像](./pictures/docker-nginx/img_3.png)

# 2、简单部署

## 2.1、启动`nginx`容器（简单版）

&emsp;&emsp;需求：创建一个名为`nginx`的容器，并将容器内的端口`80`映射到主机的端口`8080`。

```shell
docker run --name mynginx -p 8080:80 -d nginx

# 参数说明
# --name nginx 指定容器名称为 mynginx
# -p 8080:80   将外部端口（宿主机）8080 映射到容器内的 80 端口，这样可以通过宿主机的端口访问 Nginx 服务。
# -d           以后台模式运行容器
# nginx        基于 nginx 镜像运行容器。
```

## 2.2、问题分析

&emsp;&emsp;这个命令的缺陷主要有如下几点：

* **缺乏挂载卷**：该命令没有使用 -v 参数来挂载容器内的数据卷或文件。这意味着容器内的数据（例如网站文件、日志文件等）将无法持久化保存。当容器被删除或重新创建时，所有的数据都会丢失。
* **缺少自定义配置**：该命令没有提供自定义的 Nginx 配置文件。默认情况下，Nginx 容器将使用其内部的默认配置。如果你需要自定义 Nginx 的配置，例如修改虚拟主机或代理设置等，你需要编辑容器内的配置文件，或者使用挂载卷将自定义的配置文件传递给容器。
* **缺少容器重启策略**：该命令没有指定容器的重启策略。默认情况下，容器在退出时不会自动重启。如果你希望容器在发生故障或主机重启后自动重启，你可以使用 --restart 参数来设置适当的重启策略。
* **使用最新的 Nginx 镜像** ：该命令使用的是 nginx 镜像，但没有指定具体的版本号。默认情况下，Docker 将使用最新的可用版本。这意味着在将来重新创建容器时，可能会使用一个不同的版本，导致行为和配置的变化。

# 3、<span style="color:red">实际应用</span>部署

## 3.1、创建挂载目录

```shell
mkdir -p /opt/data/docker/nginx/conf
mkdir -p /opt/data/docker/nginx/log
mkdir -p /opt/data/docker/nginx/html
```

## 3.2、关于容器与主机之间互传文件

&emsp;&emsp;将上面创建的，简单版`nginx`容器文件，拷贝到主机

&emsp;&emsp;拷贝语法：`docker cp [容器id/名称]:容器目录 本地目录`

```shell
# 将容器nginx.conf文件复制到宿主机
docker cp mynginx:/etc/nginx/nginx.conf /opt/data/docker/nginx/conf/nginx.conf

# 将容器conf.d文件夹下内容复制到宿主机
docker cp mynginx:/etc/nginx/conf.d /opt/data/docker/nginx/conf/conf.d

# 将容器中的html文件夹复制到宿主机
docker cp mynginx:/usr/share/nginx/html /opt/data/docker/nginx/html
```

> 补充：   
> 将`主机文件`拷贝到`容器目录`，格式：`docker cp 本地目录 [容器id/名]:容器目录`   
> eg: `docker cp  /data/conf/nginx.conf  [容器id/名]:/etc/nginx/nginx.conf`

## 3.3、启动容器命令

```shell
docker run \
-d \
-p 9999:80 \
-v /opt/data/docker/nginx/html:/usr/share/nginx/html:ro \
-v /opt/data/docker/nginx/conf/nginx.conf:/etc/nginx/nginx.conf \
-v /opt/data/docker/nginx/conf/conf.d:/etc/nginx/conf.d \
-v /opt/data/docker/nginx/log:/var/log/nginx \
--restart=always \
--name mynginx \
nginx
```

# 4、前端资源部署

## 4.1、上传前端资源包

&emsp;&emsp;将前端资源包`dist`上传到目录：`/opt/data/docker/nginx/html`

![上传前端包](./pictures/docker-nginx/img_4.png)

## 4.2、查看配置文件`nginx.conf`

&emsp;&emsp;这个文件我们不动。文件所在目录为：`/opt/data/docker/nginx/conf`

```shell

user  root;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;
    
}
```

## 4.3、查看配置文件`nginx.conf`

&emsp;&emsp;修改目录：`/opt/data/docker/nginx/conf/conf.d`下的配置文件：`default.conf`。

```shell
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html/dist; # 修改了这边。
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;  # 增加了这一行
    }

    #error_page  404              /404.html;

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # proxy the PHP scripts to Apache listening on 127.0.0.1:80
    #
    #location ~ \.php$ {
    #    proxy_pass   http://127.0.0.1;
    #}

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    #location ~ \.php$ {
    #    root           html;
    #    fastcgi_pass   127.0.0.1:9000;
    #    fastcgi_index  index.php;
    #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
    #    include        fastcgi_params;
    #}

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
```

## 4.4、进入容器，刷新配置

```shell
docker exec -it mynginx bash

nginx -s reload
```

# 5、测试

&emsp;&emsp;浏览器访问如下地址：

* `http://192.168.255.101:9999/home`
* `http://192.168.255.101:9999`

![访问结果](./pictures/docker-nginx/img_5.png)












