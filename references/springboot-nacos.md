# 参考文献

* [SpringBoot整合Nacos做注册中心](https://blog.csdn.net/weixin_42547791/article/details/136871124)
* [springboot整合nacos](https://blog.csdn.net/weixin_44033066/article/details/129043571)

# 1、nacos 环境准备

## 1.1、创建命名空间

&emsp;&emsp;`一个命名空间`可以被视为`一个项目`，通常一个项目占用一个命名空间。创建 `shiro-integration` 命名空间（个人习惯和spring.application.name 一样，好区分）

![创建命名空间](./pictures/springboot-nacos/img.png)

## 1.2、配置管理 -> 创建配置

&emsp;&emsp;需要先选择命名空间：<span style="color:red">shiro-integration</span>

![需要先选择命名空间](./pictures/springboot-nacos/img_1.png)

&emsp;&emsp; 约定一些规则：

* `Data ID` = spring.application.name-spring.profiles.active.文件后缀

![约定一些规则](./pictures/springboot-nacos/img_2.png)

&emsp;&emsp;具体配置一一比对：

![一一比对](./pictures/springboot-nacos/img_3.png)

&emsp;&emsp;具体的在线配置单内容如下：

![在线配置单内容](./pictures/springboot-nacos/img_4.png)

## 1.3、克隆创建配置

![克隆配置](./pictures/springboot-nacos/img_5.png)

# 2、SpringBoot 集成案例

## 2.1、pom.xml 添加

&emsp;&emsp;引入spring cloud 和 alibaba cloud，这两个的版本要兼容：具体请查看[alibaba版本兼容说明](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

```xml
<dependencyManagement>
    <!-- 版本说明：https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E  -->
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>2021.0.5</version>
        <type>pom</type>
        <scope>import</scope>
    </dependency>
    <!-- https://start.aliyun.com/  利用云原生应用脚手架来选择具体的组件和版本  -->
    <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-alibaba-dependencies</artifactId>
        <version>2021.0.5.0</version>
        <type>pom</type>
        <scope>import</scope>
    </dependency>
</dependencyManagement>
```

&emsp;&emsp;因此决定了需要使用 `nacos-server-2.2.0.tar.gz` 

&emsp;&emsp;nacos 客户端引用 gav，具体参考工程：https://gitee.com/Tender_Cui/shiro 下的模块：`shiro-framework-integration/shiro-nacos`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-nacos</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 2.2、启动类添加注解

```java
package com.tender;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import com.tender.redis.RedisShiroProperties;

@SpringBootApplication
@EnableEncryptableProperties
@EnableConfigurationProperties({RedisShiroProperties.class})
@MapperScan("com.tender.mapper")
@EnableDiscoveryClient
public class ShiroApplication {

    public static void main(String[] args) {
        System.setProperty("jasypt.encryptor.password", "wb-csd876509");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ShiroApplication.class, args);
        int i = 0;
    }
}
```

## 2.3、创建启动类bootstrap.yaml

&emsp;&emsp;将之前的 `application.yaml` 重命名为 `bootstrap.yaml` 或者自己创建`bootstrap.yaml`，
并配置好相应配置

```yaml
spring:
  profiles:
    active: dev

  application:
    name: shiro-integration

  cloud:
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        namespace: 1e3aed37-afbd-405b-b9f1-30fc8e6325ae
        group: DEFAULT_GROUP
        file-extension: yaml
      discovery:
        server-addr: 192.168.255.100:8848
        namespace: 1e3aed37-afbd-405b-b9f1-30fc8e6325ae
        group: DEFAULT_GROUP
```

## 2.4、启动测试

&emsp;&emsp;由于默认端口是 8080 ，如果启动成功的端口是 9999 则说明，动态配置成功。

&emsp;&emsp;<span style="color:red">再次强调</span>，这里的配置：namespace、group、file-extension 都需要和`配置管理`中的配置要一样。

```yaml
spring: 
  cloud:
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        namespace: 76606ed7-e2b9-4d95-a4dd-04669c210464
        group: DEFAULT_GROUP
        file-extension: yaml
      discovery:
        server-addr: 192.168.255.100:8848
        namespace: 76606ed7-e2b9-4d95-a4dd-04669c210464
        group: DEFAULT_GROUP
```

&emsp;&emsp;启动成功！

![启动成功](./pictures/springboot-nacos/img_6.png)

&emsp;&mesp;查看服务列表

![查看服务列表](./pictures/springboot-nacos/img_7.png)

# 3、服务发现与消费

&emsp;&emsp;具体参考工程：https://gitee.com/Tender_Cui/shiro 下的模块：`shiro-framework-integration/shiro-nacos-provider`
&emsp;&emsp;具体参考工程：https://gitee.com/Tender_Cui/shiro 下的模块：`shiro-framework-integration/shiro-nacos-consumer`