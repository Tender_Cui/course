# 参考文献
* [VMware虚拟机安装（非常详细）](https://blog.csdn.net/leah126/article/details/131450225)

# 一、资源下载
> VMware16 下载地址：

    https://www.alipan.com/s/8kJYUCWyeEc

# 二、无脑安装（默认就行）

> 修改安装路径

![vmware安装目录](./pictures/VMware16/img.png)

> 下一步

![不更新](./pictures/VMware16/img_1.png)

> 下一步

![快捷方式](./pictures/VMware16/img_2.png)

# 三、永久激活密钥

* 激活密钥1: ZF3R0-FHED2-M80TY-8QYGC-NPKYF
* 激活密钥2: YF390-0HF8P-M81RQ-2DXQE-M2UT6
* 激活密钥3: ZF71R-DMX85-08DQY-8YMNC-PPHV8