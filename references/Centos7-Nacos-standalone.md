# 参考文献

* [Nacos在Linux（Centos7）环境下部署](https://blog.csdn.net/borners/article/details/102898341)
* [Nacos集群搭建](https://blog.csdn.net/weixin_44033066/article/details/124266381)

# Nacos 单机版安装

# 一、安装包下载

&emsp;&emsp;[历史版本下载](https://github.com/alibaba/nacos/tags)   

&emsp;&emsp;由于我使用的 `spring-cloud-alibaba-dependencies` 版本如下；

```xml
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-alibaba-dependencies</artifactId>
    <version>2021.0.5.0</version>
    <type>pom</type>
    <scope>import</scope>
</dependency>
```

&emsp;&emsp;[<span style="color:red">查看官方版本比对表</span>](https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

&emsp;&emsp;[我需要下载：nacos-server-2.2.0.tar.gz](https://github.com/alibaba/nacos/releases/tag/2.2.0)

![nacos-server-2.2.0.tar.gz](./pictures/centos7-nacos-standalone/img.png)

# 二、安装环境准备

## 2.1、单机部署

&emsp;&emsp;将 nacos-server-2.2.0.tar.gz 上传到 /opt/softs 目录

![上传安装包](./pictures/centos7-nacos-standalone/img_1.png)

&emsp;&emsp;解压到 /opt/installs 目录

```shell
tar -zxvf nacos-server-2.2.0.tar.gz -C ../installs/
```

![解压](./pictures/centos7-nacos-standalone/img_2.png)

![nacos目录](./pictures/centos7-nacos-standalone/img_3.png)

## 2.2、mysql支持

&emsp;&emsp;在0.7版本之前，在单机模式时 nacos 使用嵌入式数据库实现数据的存储，不方便观察数据存储的基本情况。
0.7版本后增加了支持 mysql 数据源的能力。

> mysql 数据库版本要求：5.6.5+

&emsp;&emsp;与 mysql 相关的操作步骤如下：

1. 安装 mysql 数据库，版本要求：5.6.5+

> [Centos7-Mysql5.7安装操作手册](./Centos7-Mysql5.7.md ':included')

2. 初始化 nacos 关于 mysql 的数据库

> mysql 初始化脚本在 nacos 目录下的 conf/mysql-schema.sql 文件

![初始化nacos库](./pictures/centos7-nacos-standalone/img_4.png)

3. 修改 nacos 目录下的 conf/application.properties 文件，增加支持mysql数据源配置（目前只支持mysql）

```shell
cd /opt/installs/nacos/conf
vi application.properties

# 需要放开注释的如容如下：
### If use MySQL as datasource:
spring.datasource.platform=mysql

### Count of DB:
db.num=1

### Connect URL of DB:
db.url.0=jdbc:mysql://192.168.255.101:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true&useUnicode=true&useSSL=false&serverTimezone=UTC
db.user.0=root
db.password.0=88888888
```

![nacos配置文件关于数据源的配置](./pictures/centos7-nacos-standalone/img_5.png)

# 三、启动<span style="color:red">单机版</span> `nacos`

```shell
cd /opt/installs/nacos/bin

# 单机版启动
sh startup.sh -m standalone
```

![nacos 启动成功日志](./pictures/centos7-nacos-standalone/img_6.png)

> 如果有防护墙，则需要给8848 端口放行（nacos默认端口为8848，在application.aproperties中可以配置）

```shell
# 查看防火墙状态
systemctl status firewalld

firewall-cmd --zone=public --add-port=8848/tcp --permanent 
# 防火墙重载刷新
firewall-cmd --reload
```

![防火墙相关](./pictures/centos7-nacos-standalone/img_7.png)

# 四、浏览器访问

```shell
http://192.168.255.100:8848/nacos
```

> <span style="color:red">用户名密码均为: </span> <span style="color:green"> nacos </span> 

![nacos管理界面](./pictures/centos7-nacos-standalone/img_8.png)

# 五、关闭nacos

&emsp;&emsp;nacos/bin/目录下运行:    ./shutdown.sh 或 sh shutdown.sh