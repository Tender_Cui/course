# 参考文献

* [RocketMQ的下载与安装（全网最细保姆级别教学）](https://blog.csdn.net/weixin_50503886/article/details/129680320)
* [Linux教程：RocketMq介绍以及集群服务搭建（双主双从同步双写）并安装可视化平台RocketMq-Dashboard](https://blog.csdn.net/wfeil211/article/details/127071203)
* [万字超详细解析：RocketMQ集群环境搭建](https://blog.csdn.net/wdj_yyds/article/details/130487191)
* [【RocketMQ】 官方运维管理命令mqadmin使用手册（讲解+实操）](https://blog.csdn.net/sinat_14840559/article/details/111934325)
* [RocketMQ双主双从环境搭建](https://blog.csdn.net/weixin_39710170/article/details/130605059)


# 搭建`双主双从` `同步双写`模式

# 1、服务器环境列表


| 序号 |        ip        | 角色                       | 架构模式           |
|----|:----------------:|--------------------------|----------------|
| 1  | 192.168.255.100  | nameserver、brokerserver  | Master1、Slave2 |   
| 2  | 192.168.255.101  | nameserver、brokerserver  | Master2、Slave1 |   

* `192.168.255.100`机器：启动一个`nameserver`，两个`broker`
* `192.168.255.101`机器：启动一个`nameserver`，两个`broker`

# 2、下载、上传、解压安装包

## 2.1、下载

&emsp;&emsp;[rocketMQ的下载地址](https://rocketmq.apache.org/download/)

&emsp;&emsp;下载：rocketmq-all-5.1.4-bin-release.zip

![下载：RocketMQ-5.1.4](./pictures/Centos7-RocketMQ-cluster/img.png)

## 2.2、上传

&emsp;&emsp;上传到目录：`/opt/softs`

![上传到目录](./pictures/Centos7-RocketMQ-cluster/img_1.png)

## 2.3、解压

&emsp;&emsp;解压到目录：`/opt/installs`

```shell
unzip rocketmq-all-5.1.4-bin-release.zip -d ../installs/
```

![解压到目录](./pictures/Centos7-RocketMQ-cluster/img_2.png)

## 2.4、scp 到机器 `192.168.255.101`

```shell
scp -r rocketmq-all-5.1.4-bin-release/ root@192.168.255.101:/opt/installs/
```

## 2.5、配置环境变量（两个机器都执行）

```shell
vim /etc/profile

export ROCKETMQ_HOME=/opt/installs/rocketmq-all-5.1.4-bin-release
export PATH=$PATH:$ROCKETMQ_HOME/bin

# 刷新配置
source /etc/profile
```

# 3、针对机器 `192.168.255.100` 的 `broker` 配置

## 3.1、master1 配置

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/conf
# 进入 2主2丛的目录
cd 2m-2s-sync/

# 修改 master1 的配置
vim broker-a.properties
```

&emsp;&emsp;具体配置如下：

```shell
#所属集群名字
brokerClusterName=rocketmq-cluster
#broker名字，注意此处不同的配置文件填写的不一样
brokerName=broker-a
#0 表示 Master，>0 表示 Slave
brokerId=0
#nameServer地址，分号分割
namesrvAddr=192.168.255.100:9876;192.168.255.101:9876
#设置broker节点所在服务器的ip地址
# **这个非常重要**,主从模式下，从节点会根据主节点的brokerIP2来同步数据，如果不配置，主从无法同步
# brokerIP1设置为自己外网能访问的ip，服务器双网卡情况下必须配置，比如阿里云这种，
# 主节点需要配置ip1和ip2，
# 从节点只需要配置ip1即可
brokerIP1=192.168.255.100
brokerIP2=192.168.255.100
#在发送消息时，自动创建服务器不存在的topic，默认创建的队列数
defaultTopicQueueNums=4
#是否允许 Broker 自动创建Topic，建议线下开启，线上关闭
autoCreateTopicEnable=true
#是否允许 Broker 自动创建订阅组，建议线下开启，线上关闭
autoCreateSubscriptionGroup=true
#Broker 对外服务的监听端口
listenPort=10911
#删除文件时间点，默认凌晨 4点
deleteWhen=04
#文件保留时间，默认 48 小时
fileReservedTime=120
#commitLog每个文件的大小默认1G
mapedFileSizeCommitLog=1073741824
#ConsumeQueue每个文件默认存30W条，根据业务情况调整
mapedFileSizeConsumeQueue=300000
#destroyMapedFileIntervalForcibly=120000
#redeleteHangedFileInterval=120000
#检测物理文件磁盘空间
diskMaxUsedSpaceRatio=88
#存储路径
storePathRootDir=/usr/local/rocketmq/master
#commitLog 存储路径
storePathCommitLog=/usr/local/rocketmq/master/commitlog
#消费队列存储路径存储路径
storePathConsumeQueue=/usr/local/rocketmq/master/consumequeue
#消息索引存储路径
storePathIndex=/usr/local/rocketmq/master/index
#checkpoint 文件存储路径
storeCheckpoint=/usr/local/rocketmq/master/checkpoint
#abort 文件存储路径
abortFile=/usr/local/rocketmq/master/abort
#限制的消息大小
maxMessageSize=65536
#flushCommitLogLeastPages=4
#flushConsumeQueueLeastPages=2
#flushCommitLogThoroughInterval=10000
#flushConsumeQueueThoroughInterval=60000
#Broker 的角色
#- ASYNC_MASTER 异步复制Master
#- SYNC_MASTER 同步双写Master
#- SLAVE
brokerRole=SYNC_MASTER
#刷盘方式
#- ASYNC_FLUSH 异步刷盘
#- SYNC_FLUSH 同步刷盘
flushDiskType=SYNC_FLUSH
#checkTransactionMessageEnable=false
#发消息线程池数量
#sendMessageThreadPoolNums=128
#拉消息线程池数量
#pullMessageThreadPoolNums=128
```

## 3.2、slave2 配置

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/conf
# 进入 2主2丛的目录
cd 2m-2s-sync/

# 修改 slave2 的配置
vim broker-b-s.properties
```

&emsp;&emsp;具体配置如下：

```shell
#所属集群名字
brokerClusterName=rocketmq-cluster
#broker名字，注意此处不同的配置文件填写的不一样
brokerName=broker-b
#0 表示 Master，>0 表示 Slave
brokerId=1
#nameServer地址，分号分割
namesrvAddr=192.168.255.100:9876;192.168.255.101:9876
#设置broker节点所在服务器的ip地址
# **这个非常重要**,主从模式下，从节点会根据主节点的brokerIP2来同步数据，如果不配置，主从无法同步
# brokerIP1设置为自己外网能访问的ip，服务器双网卡情况下必须配置，比如阿里云这种，
# 主节点需要配置ip1和ip2，
# 从节点只需要配置ip1即可
brokerIP1=192.168.255.101
#在发送消息时，自动创建服务器不存在的topic，默认创建的队列数
defaultTopicQueueNums=4
#是否允许 Broker 自动创建Topic，建议线下开启，线上关闭
autoCreateTopicEnable=true
#是否允许 Broker 自动创建订阅组，建议线下开启，线上关闭
autoCreateSubscriptionGroup=true
#Broker 对外服务的监听端口
listenPort=11011
#删除文件时间点，默认凌晨 4点
deleteWhen=04
#文件保留时间，默认 48 小时
fileReservedTime=120
#commitLog每个文件的大小默认1G
mapedFileSizeCommitLog=1073741824
#ConsumeQueue每个文件默认存30W条，根据业务情况调整
mapedFileSizeConsumeQueue=300000
#destroyMapedFileIntervalForcibly=120000
#redeleteHangedFileInterval=120000
#检测物理文件磁盘空间
diskMaxUsedSpaceRatio=88
#存储路径
storePathRootDir=/usr/local/rocketmq/slave
#commitLog 存储路径
storePathCommitLog=/usr/local/rocketmq/slave/commitlog
#消费队列存储路径存储路径
storePathConsumeQueue=/usr/local/rocketmq/slave/consumequeue
#消息索引存储路径
storePathIndex=/usr/local/rocketmq/slave/index
#checkpoint 文件存储路径
storeCheckpoint=/usr/local/rocketmq/slave/checkpoint
#abort 文件存储路径
abortFile=/usr/local/rocketmq/slave/abort
#限制的消息大小
maxMessageSize=65536
#flushCommitLogLeastPages=4
#flushConsumeQueueLeastPages=2
#flushCommitLogThoroughInterval=10000
#flushConsumeQueueThoroughInterval=60000
#Broker 的角色
#- ASYNC_MASTER 异步复制Master
#- SYNC_MASTER 同步双写Master
#- SLAVE
brokerRole=SLAVE
#刷盘方式
#- ASYNC_FLUSH 异步刷盘
#- SYNC_FLUSH 同步刷盘
flushDiskType=ASYNC_FLUSH
#checkTransactionMessageEnable=false
#发消息线程池数量
#sendMessageThreadPoolNums=128
#拉消息线程池数量
#pullMessageThreadPoolNums=128
```

# 4、针对机器 `192.168.255.101` 的 `broker` 配置

## 4.1、master2 配置

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/conf
# 进入 2主2丛的目录
cd 2m-2s-sync/

# 修改 master1 的配置
vim broker-b.properties
```

&emsp;&emsp;具体配置如下：

```shell
#所属集群名字
brokerClusterName=rocketmq-cluster
#broker名字，注意此处不同的配置文件填写的不一样
brokerName=broker-b
#0 表示 Master，>0 表示 Slave
brokerId=0
#nameServer地址，分号分割
namesrvAddr=192.168.255.100:9876;192.168.255.101:9876
#设置broker节点所在服务器的ip地址
# **这个非常重要**,主从模式下，从节点会根据主节点的brokerIP2来同步数据，如果不配置，主从无法同步
# brokerIP1设置为自己外网能访问的ip，服务器双网卡情况下必须配置，比如阿里云这种，
# 主节点需要配置ip1和ip2，
# 从节点只需要配置ip1即可
brokerIP1=192.168.255.101
brokerIP2=192.168.255.101
#在发送消息时，自动创建服务器不存在的topic，默认创建的队列数
defaultTopicQueueNums=4
#是否允许 Broker 自动创建Topic，建议线下开启，线上关闭
autoCreateTopicEnable=true
#是否允许 Broker 自动创建订阅组，建议线下开启，线上关闭
autoCreateSubscriptionGroup=true
#Broker 对外服务的监听端口
listenPort=10911
#删除文件时间点，默认凌晨 4点
deleteWhen=04
#文件保留时间，默认 48 小时
fileReservedTime=120
#commitLog每个文件的大小默认1G
mapedFileSizeCommitLog=1073741824
#ConsumeQueue每个文件默认存30W条，根据业务情况调整
mapedFileSizeConsumeQueue=300000
#destroyMapedFileIntervalForcibly=120000
#redeleteHangedFileInterval=120000
#检测物理文件磁盘空间
diskMaxUsedSpaceRatio=88
#存储路径
storePathRootDir=/usr/local/rocketmq/master
#commitLog 存储路径
storePathCommitLog=/usr/local/rocketmq/master/commitlog
#消费队列存储路径存储路径
storePathConsumeQueue=/usr/local/rocketmq/master/consumequeue
#消息索引存储路径
storePathIndex=/usr/local/rocketmq/master/index
#checkpoint 文件存储路径
storeCheckpoint=/usr/local/rocketmq/master/checkpoint
#abort 文件存储路径
abortFile=/usr/local/rocketmq/master/abort
#限制的消息大小
maxMessageSize=65536
#flushCommitLogLeastPages=4
#flushConsumeQueueLeastPages=2
#flushCommitLogThoroughInterval=10000
#flushConsumeQueueThoroughInterval=60000
#Broker 的角色
#- ASYNC_MASTER 异步复制Master
#- SYNC_MASTER 同步双写Master
#- SLAVE
brokerRole=SYNC_MASTER
#刷盘方式
#- ASYNC_FLUSH 异步刷盘
#- SYNC_FLUSH 同步刷盘
flushDiskType=SYNC_FLUSH
#checkTransactionMessageEnable=false
#发消息线程池数量
#sendMessageThreadPoolNums=128
#拉消息线程池数量
#pullMessageThreadPoolNums=128
```

## 4.2、slave1 配置

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/conf
# 进入 2主2丛的目录
cd 2m-2s-sync/

# 修改 master1 的配置
vim broker-a-s.properties
```

&emsp;&emsp;具体配置如下：

```shell
#所属集群名字
brokerClusterName=rocketmq-cluster
#broker名字，注意此处不同的配置文件填写的不一样
brokerName=broker-a
#0 表示 Master，>0 表示 Slave
brokerId=1
#nameServer地址，分号分割
namesrvAddr=192.168.255.100:9876;192.168.255.101:9876
#设置broker节点所在服务器的ip地址
# **这个非常重要**,主从模式下，从节点会根据主节点的brokerIP2来同步数据，如果不配置，主从无法同步
# brokerIP1设置为自己外网能访问的ip，服务器双网卡情况下必须配置，比如阿里云这种，
# 主节点需要配置ip1和ip2，
# 从节点只需要配置ip1即可
brokerIP1=192.168.255.100
#在发送消息时，自动创建服务器不存在的topic，默认创建的队列数
defaultTopicQueueNums=4
#是否允许 Broker 自动创建Topic，建议线下开启，线上关闭
autoCreateTopicEnable=true
#是否允许 Broker 自动创建订阅组，建议线下开启，线上关闭
autoCreateSubscriptionGroup=true
#Broker 对外服务的监听端口
listenPort=11011
#删除文件时间点，默认凌晨 4点
deleteWhen=04
#文件保留时间，默认 48 小时
fileReservedTime=120
#commitLog每个文件的大小默认1G
mapedFileSizeCommitLog=1073741824
#ConsumeQueue每个文件默认存30W条，根据业务情况调整
mapedFileSizeConsumeQueue=300000
#destroyMapedFileIntervalForcibly=120000
#redeleteHangedFileInterval=120000
#检测物理文件磁盘空间
diskMaxUsedSpaceRatio=88
#存储路径
storePathRootDir=/usr/local/rocketmq/slave
#commitLog 存储路径
storePathCommitLog=/usr/local/rocketmq/slave/commitlog
#消费队列存储路径存储路径
storePathConsumeQueue=/usr/local/rocketmq/slave/consumequeue
#消息索引存储路径
storePathIndex=/usr/local/rocketmq/slave/index
#checkpoint 文件存储路径
storeCheckpoint=/usr/local/rocketmq/slave/checkpoint
#abort 文件存储路径
abortFile=/usr/local/rocketmq/slave/abort
#限制的消息大小
maxMessageSize=65536
#flushCommitLogLeastPages=4
#flushConsumeQueueLeastPages=2
#flushCommitLogThoroughInterval=10000
#flushConsumeQueueThoroughInterval=60000
#Broker 的角色
#- ASYNC_MASTER 异步复制Master
#- SYNC_MASTER 同步双写Master
#- SLAVE
brokerRole=SLAVE
#刷盘方式
#- ASYNC_FLUSH 异步刷盘
#- SYNC_FLUSH 同步刷盘
flushDiskType=ASYNC_FLUSH
#checkTransactionMessageEnable=false
#发消息线程池数量
#sendMessageThreadPoolNums=128
#拉消息线程池数量
#pullMessageThreadPoolNums=128
```

# 5、修改启动脚本（两台机子都执行）

&emsp;&emsp;本地搭建的环境，<span style="color:red">资源有限</span>，因此需要修改 JVM 参数：

## 5.1、修改脚本：runbroker.sh

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/bin

vim runbroker.sh
```

&emsp;&emsp;内容修改如下：

```shell
# 配置修改小一些
# JAVA_OPT="${JAVA_OPT} -server -Xms8g -Xmx8g"

JAVA_OPT="${JAVA_OPT} -server -Xms256m -Xmx256m -Xmn128m"
```

![修改脚本：runbroker.sh](./pictures/Centos7-RocketMQ-cluster/img_3.png)

## 5.2、修改脚本：runserver.sh


```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/bin

vim runserver.sh
```

```shell
# 配置修改小一些
# JAVA_OPT="${JAVA_OPT} -server -Xms4g -Xmx4g -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"

JAVA_OPT="${JAVA_OPT} -server -Xms256m -Xmx256m -Xmn128m -XX:MetaspaceSize=128m -XX:MaxMetaspaceSize=320m"
```

![修改脚本：runserver.sh](./pictures/Centos7-RocketMQ-cluster/img_4.png)

# 6、启动 `nameserver`（两个机器都执行）

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/bin

# 启动命令
nohup sh mqnamesrv &

# 查看日志
[root@master bin]# tail -f nohup.out 
Java HotSpot(TM) 64-Bit Server VM warning: Using the DefNew young collector with the CMS collector is deprecated and will likely be removed in a future release
Java HotSpot(TM) 64-Bit Server VM warning: UseCMSCompactAtFullCollection is deprecated and will likely be removed in a future release.
The Name Server boot success. serializeType=JSON, address 0.0.0.0:9876
```

&emsp;&emsp;可以看到提示词：`The Name Server boot success`。

# 7、启动 `broker`（两个机器都执行）

## 7.1、机器 `192.168.255.100`

### 7.1.1、启动 `master`（名称：`broker-a`）

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/bin

# 启动
# master 启动 broker
nohup mqbroker -c ../conf/2m-2s-sync/broker-a.properties & 
```

![查看master(broker-a)启动日志](./pictures/Centos7-RocketMQ-cluster/img_5.png)

### 7.1.2、启动 `slave`（名称：`broker-b`）

```shell
# 当前目录下

# 启动
# master 启动 broker
nohup mqbroker -c ../conf/2m-2s-sync/broker-b-s.properties & 
```

![查看slave(broker-b)启动日志](./pictures/Centos7-RocketMQ-cluster/img_6.png)

## 7.2、机器 `192.168.255.101`

### 7.2.1、启动 `master`（名称：`broker-b`）

```shell
cd /opt/installs/rocketmq-all-5.1.4-bin-release/bin

# 启动
# master 启动 broker
nohup mqbroker -c ../conf/2m-2s-sync/broker-b.properties & 
```

![查看master(broker-b)启动日志](./pictures/Centos7-RocketMQ-cluster/img_7.png)

### 7.2.2、启动 `slave`（名称：`broker-a`）

```shell
# 当前目录下

# 启动
# master 启动 broker
nohup mqbroker -c ../conf/2m-2s-sync/broker-a-s.properties & 
```

![查看slave(broker-a)启动日志](./pictures/Centos7-RocketMQ-cluster/img_8.png)

# 8、命令行验证

## 8.1、创建 `topic`

TODO

## 8.2、生产消息

TODO

## 8.3、消费消息

TODO

# 9、安装可视化平台 `RocketMq-Dashboard`

&emsp;&emsp;TODO
