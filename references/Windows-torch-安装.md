# 参考文献

* 直接问 `DeepSeek` 

# 一、命令安装 `torch`
```shell
pip install torch==2.4.0
```

# 二、测试

```shell
import torch
print(torch.__version__)  # 输出应为 2.4.0 或对应版本
```

&emsp;&emsp;结果如下：

```shell
D:\A_Tender_workspace\install_4_code\python\python_3.10.9\python.exe D:\A_Tender_workspace\A_code\A_code_4_python\LLaMA-Factory\tenderTest\Test.py 
2.4.0+cpu

```