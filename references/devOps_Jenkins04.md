# 参考文献

* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)
* [解决 Tasks support was removed in SonarQube 7.6. 的问题](https://www.cnblogs.com/qkhh/p/14893147.html)
* [在流水线管道中使用指定的JDK](https://www.cnblogs.com/wangyang0210/p/16773169.html)
* [安装多个java导致sonarqube启动报错解决](https://blog.csdn.net/liusa825983081/article/details/124264244)

# 一、Jenkins + SonarQube 代码审查

> 环境列表:

|    软件    |       服务器      |      版本     |
|:---------:|:----------------:|:-------------:|
|    JDK    | 192.168.255.101  |      1.8      |
|   MySQL   | 192.168.255.101  |      5.7      |
| SonarQube | 192.168.255.101  |      7.9.6    |


## 1.1、SonarQube简介

&emsp;&emsp;SonarQube是一个用于管理代码质量的开放平台，可以快速的定位代码中潜在的或者明显的错误。目前支持java,C#,C/C++,Python,PL/SQL,Cobol,JavaScrip,Groovy等二十几种编程语言的代码质量管理与检测。

&emsp;&emsp;[SonaQube官网](https://www.sonarqube.org/)

## 1.2、安装SonarQube

> <span style="color:red">SonarQube 7.9以上版本已不再支持mysql</span>

&emsp;&emsp;sonar会把一些代码审查的结果保存到数据库

&emsp;&emsp;~~[请查看mysql1.7的安装教程](./Centos7-Mysql5.7.md ':include')~~

&emsp;&emsp;~~在MySQL实例中，创建sonar数据库~~

```shell
create database sonar;
```

![创建数据库sonar](./pictures/devOps_jenkins04/img.png)

> 安装SonarQube

&emsp;&emsp;[下载sonar历史版本压缩包](https://www.sonarsource.com/products/sonarqube/downloads/historical-downloads/)

> 解压sonar，并设置权限

```shell
# 如果没有 unzip 命令，则需要安装
yum install unzip -y

# 解压
unzip sonarqube-7.9.6.zip
# 移走
mv sonarqube-7.9.6 ./../installs/
# 修改名字
mv sonarqube-7.9.6/ sonar7.9.6/

# 创建sonar用户，必须sonar用于启动，否则报错
usseradd sonar
# #更改sonar目录及文件权限
chown -R sonar:sonar /opt/installs

# 修改sonar配置文件
vim /opt/installs/sonar7.9.6/conf/sonar.properties

# ------------ mysql 配置内容 start ---------------
sonar.jdbc.username=root  
sonar.jdbc.password=88888888
sonar.jdbc.url=jdbc:mysql://192.168.255.101:3306/sonar?useUnicode=true&characterEncoding=utf8&rewriteBatchedStatements=true&useConﬁgs= maxPerformance&useSSL=false
# ------------ mysql 配置内容 end ---------------

# 启动命令
# su sonar 切换 sonar 用户
su sonar ./bin/linux-x86-64/sonar.sh start  # 启动
su sonar ./bin/linux-x86-64/sonar.sh status # 查看状态
su sonar ./bin/linux-x86-64/sonar.sh stop   # 停止

# 查看日志
tail -n 300  ./logs/sonar.log 

# sonar 默认是 9000 端口

firewall-cmd --zone=public --add-port=9000/tcp --permanent
firewall-cmd --reload
# 浏览器访问
http://192.168.255.101:9000
```

> 启动 SonarQube 可能会报错

```shell
2024.01.03 03:25:49 INFO  app[][o.s.a.es.EsSettings] Elasticsearch listening on /127.0.0.1:9001
2024.01.03 03:25:49 INFO  app[][o.s.a.ProcessLauncherImpl] Launch process[[key='es', ipcIndex=1, logFilenamePrefix=es]] from [/opt/installs/sonar7.9.6/elasticsearch]: /opt/installs/sonar7.9.6/elasticsearch/bin/elasticsearch
2024.01.03 03:25:49 INFO  app[][o.s.a.SchedulerImpl] Waiting for Elasticsearch to be up and running
2024.01.03 03:25:49 INFO  app[][o.e.p.PluginsService] no modules loaded
2024.01.03 03:25:49 INFO  app[][o.e.p.PluginsService] loaded plugin [org.elasticsearch.transport.Netty4Plugin]
Unrecognized VM option 'UseConcMarkSweepGC'
Error: Could not create the Java Virtual Machine.
INFO  app[][o.e.c.t.TransportClientNodesService] failed to get node info for {#transport#-1}{kGNi65rYR5ixdRsCzivpvw}{127.0.0.1}{127.0.0.1:9001}, disconnecting...
java.lang.IllegalStateException: Future got interrupted
        at org.elasticsearch.common.util.concurrent.FutureUtils.get(FutureUtils.java:60)
        at org.elasticsearch.action.support.AdapterActionFuture.actionGet(AdapterActionFuture.java:34)
        at org.elasticsearch.transport.ConnectionManager.internalOpenConnection(ConnectionManager.java:209)
        at org.elasticsearch.transport.ConnectionManager.openConnection(ConnectionManager.java:80)
        at org.elasticsearch.transport.TransportService.openConnection(TransportService.java:367)
        at org.elasticsearch.client.transport.TransportClientNodesService$SimpleNodeSampler.doSample(TransportClientNodesService.java:411)
        at org.elasticsearch.client.transport.TransportClientNodesService$NodeSampler.sample(TransportClientNodesService.java:362)
        at org.elasticsearch.client.transport.TransportClientNodesService.addTransportAddresses(TransportClientNodesService.java:201)
        at org.elasticsearch.client.transport.TransportClient.addTransportAddress(TransportClient.java:342)
        at org.sonar.application.es.EsConnectorImpl$MinimalTransportClient.<init>(EsConnectorImpl.java:108)
        at org.sonar.application.es.EsConnectorImpl.buildTransportClient(EsConnectorImpl.java:89)
        at org.sonar.application.es.EsConnectorImpl.getTransportClient(EsConnectorImpl.java:74)
        at org.sonar.application.es.EsConnectorImpl.getClusterHealthStatus(EsConnectorImpl.java:61)
        at org.sonar.application.process.EsManagedProcess.checkStatus(EsManagedProcess.java:88)
        at org.sonar.application.process.EsManagedProcess.checkOperational(EsManagedProcess.java:73)
        at org.sonar.application.process.EsManagedProcess.isOperational(EsManagedProcess.java:58)
        at org.sonar.application.process.ManagedProcessHandler.refreshState(ManagedProcessHandler.java:201)
        at org.sonar.application.process.ManagedProcessHandler$EventWatcher.run(ManagedProcessHandler.java:258)
Caused by: java.lang.InterruptedException: null
        at java.base/java.util.concurrent.locks.AbstractQueuedSynchronizer.acquireSharedInterruptibly(AbstractQueuedSynchronizer.java:1048)
        at org.elasticsearch.common.util.concurrent.BaseFuture$Sync.get(BaseFuture.java:251)
        at org.elasticsearch.common.util.concurrent.BaseFuture.get(BaseFuture.java:94)
        at org.elasticsearch.common.util.concurrent.FutureUtils.get(FutureUtils.java:57)
        ... 17 common frames omitted
<-- Wrapper Stopped
```

&emsp;&emsp;报错原因是，本地安装了多个版本的 jdk

&emsp;&emsp;解决方案：手动指定 SonarQube 使用的java版本，配置文件在\conf\wrapper.conf

```shell
cd /opt/installs/sonar7.9.6/conf

vim wrapper.conf

# 更改如下内容：（视各人jdk安装情况）
wrapper.java.command=/opt/installs/jdk-11.0.7/bin/java
```

---

&emsp;&emsp;由于SonarQube 7.9以上版本已不再支持mysql，因此以上关于mysql 的配置，只适合老的版本。sonar 默认有个内存数据库，只适合开发环境使用。

![sonar默认数据库](./pictures/devOps_jenkins04/img_1.png)

> H2是一款内存数据库，适合我们在开发阶段、学习阶段调试代码使用，并不适用于生产阶段

&emsp;&emsp;它支持在内存中创建数据库和表。所以如果我们使用H2数据库的内存模式，那么我们创建的数据库和表都只是保存在内存中，一旦服务器重启，那么内存中的数据库和表就不存在了。

> 访问sonar && 登录

```shell
http://192.168.255.101:9000/
```

![登录sonar](./pictures/devOps_jenkins04/img_2.png)

## 1.3、生成token

![生成token](./pictures/devOps_jenkins04/img_3.png)

> 下一步

![生成token](./pictures/devOps_jenkins04/img_4.png)

    c65bc56183ae0b227572e6e6891a933aaee4caef

> Jenkins 中添加 Secret text 凭证

![Secret text 凭证](./pictures/devOps_jenkins04/img_4.1.png)

## 1.4、Jenkins 整合 sonar

&emsp;&emsp;原理示意图：

![原理示意图](./pictures/devOps_jenkins04/img_5.png)

### 1.4.1、下载插件 SonarQube Scanner

&emsp;&emsp;Jenkins 利用SonarQube Scanner 插件去操作SonarQube Scanner 软件/工具，该软件/工具会和SonarQube 进行交互。

&emsp;&emsp;Manage Jenkins->Plugins->Available plugins

![SonarQube Scanner插件的安装](./pictures/devOps_jenkins04/img_6.png)

### 1.4.2、下载 SonarQube Scanner 软件/工具

> 利用Jenkins 来安装SonarQube Scanner 软件/工具

&emsp;&emsp;Manage Jenkins->System Configuration->Tools

![安装SonarQube Scanner 软件/工具](./pictures/devOps_jenkins04/img_7.png)

### 1.4.3、Jenkins 中配置 SonarQube 的服务地址

&emsp;&emsp;Jenkins 中配置 SonarQube 环境，用来连接 SonarQube。

&emsp;&emsp;Manage Jenkins->System

![配置 SonarQube 的服务地址](./pictures/devOps_jenkins04/img_8.png)

# 二、代码审查测试

## 2.1、非流水线项目

&emsp;&emsp;在Jenkins 中点击之前的freestyle项目->配置->Execute SonarQube Scanner->Analysis properties

![增加构建步骤之执行SonarQube扫描](./pictures/devOps_jenkins04/img_9.png)

> Analysis properties 中的内容（后面会放到代码中统一维护）

```shell
# must be unique in a given SonarQube instance
sonar.projectKey=dev_project_war_package_test
# this is the name and version displayed in the SonarQube UI. Was mandatory prior to SonarQube 6.1.
sonar.projectName=dev_project_war_package_test
sonar.projectVersion=1.0
# Path is relative to the sonar-project.properties file. Replace "\" by "/" on Windows.
# This property is optional if sonar.modules is set.
# 扫描的路径
sonar.sources=.
sonar.exclusions=**/test/**,**/target/**
sonar.java.source=17
sonar.java.target=17
# Encoding of the source code. Default is default system encoding
sonar.sourceEncoding=UTF-8
```

> 扫描配置

![扫描配置](./pictures/devOps_jenkins04/img_10.png)

> 点击构建&&查看结果

&emsp;&emsp;<span style="color:red">有报错(这是由于SonarQube 7.9.6需要使用jdk17了)</span>，具体如下：

```shell
Error: A JNI error has occurred, please check your installation and try again
Exception in thread "main" java.lang.UnsupportedClassVersionError: org/sonarsource/scanner/cli/Main has been compiled by a more recent version of the Java Runtime (class file version 61.0), this version of the Java Runtime only recognizes class file versions up to 52.0
	at java.lang.ClassLoader.defineClass1(Native Method)
	at java.lang.ClassLoader.defineClass(ClassLoader.java:756)
	at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
	at java.net.URLClassLoader.defineClass(URLClassLoader.java:468)
	at java.net.URLClassLoader.access$100(URLClassLoader.java:74)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:369)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:363)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:362)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
	at sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:355)
	at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
	at sun.launcher.LauncherHelper.checkAndLoadMain(LauncherHelper.java:495)
WARN: Unable to locate 'report-task.txt' in the workspace. Did the SonarScanner succeed?
ERROR: SonarQube scanner exited with non-zero code: 1
```

&emsp;&emsp;解决方案：192.168.255.101 机器需要安装jdk17，[请参考jdk17的安装](./Centos7-jdk17.md ':included')，然后
Manage Jenkins->System Configuration->Tools->JDK 安装

![配置上jdk17到jenkins的tools里面](./pictures/devOps_jenkins04/img_11.png)

> 重新配置项目，修改 JDK 为 jdk17

![JDK 为 jdk17](./pictures/devOps_jenkins04/img_12.png)

&emsp;&emsp;<span style="color:red">构建后又报错了。</span>具体错误如下：

![又报错了](./pictures/devOps_jenkins04/img_13.png)

&emsp;&emsp;解决办法：去掉 Task to run

![去掉 Task to run](./pictures/devOps_jenkins04/img_14.png)

&emsp;&emsp;<span style="color:red">构建后又又报错了。</span>具体错误如下：

```shell
ERROR: Error during SonarScanner execution
java.lang.ExceptionInInitializerError
	at com.sonar.sslr.api.typed.ActionParser.<init>(ActionParser.java:59)
	at org.sonar.javascript.parser.JavaScriptParser.<init>(JavaScriptParser.java:33)
	at org.sonar.javascript.parser.JavaScriptParserBuilder.createParser(JavaScriptParserBuilder.java:31)
	at org.sonar.plugins.javascript.JavaScriptSensor.<init>(JavaScriptSensor.java:136)
	at org.sonar.plugins.javascript.JavaScriptSensor.<init>(JavaScriptSensor.java:100)
	... 省略 ...
	at org.sonarsource.scanner.api.EmbeddedScanner.doExecute(EmbeddedScanner.java:189)
	at org.sonarsource.scanner.api.EmbeddedScanner.execute(EmbeddedScanner.java:138)
	at org.sonarsource.scanner.cli.Main.execute(Main.java:126)
	at org.sonarsource.scanner.cli.Main.execute(Main.java:81)
	at org.sonarsource.scanner.cli.Main.main(Main.java:62)
Caused by: net.sf.cglib.core.CodeGenerationException: java.lang.reflect.InaccessibleObjectException-->Unable to make protected final java.lang.Class java.lang.ClassLoader.defineClass(java.lang.String,byte[],int,int,java.security.ProtectionDomain) throws java.lang.ClassFormatError accessible: module java.base does not "opens java.lang" to unnamed module @30e6a763
```

&emsp;&emsp;解决办法：

```shell
--add-opens
java.base/java.lang=ALL-UNNAMED
--add-opens
java.base/sun.net.util=ALL-UNNAMED
--add-opens
java.base/java.util=ALL-UNNAMED
--add-opens
java.base/java.lang.reflect=ALL-UNNAMED
```

> 编辑项目，把 JVM 参数添加到Execute SonarQube Scanner 下

![编辑JVM参数](./pictures/devOps_jenkins04/img_15.png)

&emsp;&emsp;重新构建，查看结果：

![最后构建成功了](./pictures/devOps_jenkins04/img_16.png)

---

![sonar检测结果](./pictures/devOps_jenkins04/img_17.png)

## 2.2、流水线项目

> 在IDEA修改 Jenkinsfile，加入 SonarQube 代码审查阶段

```shell
pipeline {
    agent any

    stages {
        stage('拉取代码') {
            steps {
                checkout scmGit(
                branches: [[name: '*/${branch}']],
//                 branches: [[name: '*/master']],
                extensions: [],
                userRemoteConfigs: [[
                  credentialsId: '6a5f7869-6ebf-4c21-a27a-51f80b9b0e13',
                  url: 'git@192.168.255.100:bruce_tender/project_war_test.git'
                ]])
            }
        }
        stage('SonarQube代码审查'){
            steps{
                tools {
                   jdk "jdk17"
                }
                script{
                    // tool代表要引入Jenkins的一些工具, 'Sonar-Scanner'是之前我们自己起的名字
                    // 自由风格默认会找全局工具
                    scannerHome=tool 'Sonar-Scanner'
                    // JDK 11 需要设置 JVM 启动参数, 否则报错
                    sh '''  
                        export JAVA_OPTS="  
                            --add-opens java.base/java.lang=ALL-UNNAMED  
                            --add-opens java.base/sun.net.util=ALL-UNNAMED  
                            --add-opens java.base/java.util=ALL-UNNAMED  
                            --add-opens java.base/java.lang.reflect=ALL-UNNAMED  
                        "  
                    '''  
                }
                // 引入 SonarQube 的服务器环境
                withSonarQubeEnv('SonarQube-Server'){
                    // 这个配置在系统配置里
                    sh"${scannerHome}/bin/sonar-scanner"
                }
            }
        }
        stage('构建项目') {
            steps {
                sh 'mvn clean package'
            }
        }

        stage('发布项目') {
            steps {
                deploy adapters: [
                  tomcat8(
                    credentialsId: '9fbd4fd5-19a7-45c2-a275-622fa4dfc6d1',
                    path: '',
                    url: 'http://192.168.255.102:8080'
                  )
                ],
                contextPath: null,
                war: 'target/*.war'
            }
        }
    }
}
```

> SonarQube代码审查报错 TODO

