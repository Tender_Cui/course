# 参考文献

* [SpringBoot—集成Kafka详解](https://blog.csdn.net/cold___play/article/details/132398946)
* [学习笔记之Kafka幂等和事务](https://blog.csdn.net/oTengYue/article/details/104727512/)
* [springboot集成Kafka详细入门 基本概念及消费优先级处理](https://blog.csdn.net/weixin_43702295/article/details/135221471)

# 1、pom.xml 引入gav

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-kafka-consumer</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!-- kafka 相关 -->
        <dependency>
            <groupId>org.springframework.kafka</groupId>
            <artifactId>spring-kafka</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

</project>
```

# 2、启动类添加注解

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableKafka
public class ShiroKafkaProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroKafkaProducerApplication.class);
    }
}
```

# 3、bootstrap.yaml 文件配置(生产者、消费者一样)

```yaml
server:
  port: 9090

spring:
  application:
    name: shiro-kafka-producer


#### kafka配置生产者 begin ####
  kafka:
    # 指定kafka server的地址，集群配多个，中间，逗号隔开
    bootstrap-servers: 192.168.255.100:9092,192.168.255.101:9092,192.168.255.102:9092
    # 写入失败时，重试次数。当leader失效，一个 replication 节点会替代成为leader节点，此时可能出现写入失败
    producer:
      retries: 0
      # 每次批量发送消息的数量,produce积累到一定数据，一次发送
      batch-size: 16384
      buffer-memory: 33554432
      # procedure要求leader在考虑 `完成请求` 之前收到的确认数，用于控制发送记录在服务端的持久化，其值可以为如下：
        # acks = 0 如果设置为零，则生产者将不会等待来自服务器的任何确认，该记录将立即添加到套接字缓冲区并视为已发送。在这种情况下，无法保证服务器已收到记录，并且重试配置将不会生效（因为客户端通常不会知道任何故障），为每条记录返回的偏移量始终设置为-1。
        # acks = 1 这意味着leader会将记录写入其本地日志，但无需等待所有副本服务器的完全确认即可做出回应，在这种情况下，如果leader在确认记录后立即失败，但在将数据复制到所有的副本服务器之前，则记录将会丢失。
        # acks = all 这意味着leader将等待完整的同步副本集以确认记录，这保证了只要至少一个同步副本服务器仍然存活，记录就不会丢失，这是最强有力的保证，这相当于acks = -1的设置。
        # 可以设置的值为：all, -1, 0, 1
      acks: 1
      key-serializer: org.apache.kafka.common.serialization.StringSerializer
      value-serializer: org.apache.kafka.common.serialization.StringSerializer

    consumer:
      # 指定默认消费者group id --> 由于在kafka中，同一组中的consumer不会读取到同一个消息，依靠groud.id设置组名
      group-id: DEFAULT_GROUP
      # # smallest和largest才有效，如果smallest重新0开始读取，如果是largest从logfile的offset读取。一般情况下我们都是设置 earliest
      auto-offset-reset: earliest
      # 是否设置自动提交
      enable-auto-commit: false
      # #如果'enable.auto.commit'为true，则消费者偏移自动提交给Kafka的频率（以毫秒为单位），默认值为5000。
      auto-commit-interval: 1000
      key-deserializer: org.apache.kafka.common.serialization.StringDeserializer
      value-deserializer: org.apache.kafka.common.serialization.StringDeserializer
```

# 4、生产者代码、服务者代码

## 4.1、生产者

```java
package com.tender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaProducerController {

    @Autowired
    private KafkaTemplate<String, Object> kafkaTemplate;

    @GetMapping("/send/msg")
    public String sendMsg(@RequestParam String message){
        try {
            // 需要提前建立好 my-topic2
            kafkaTemplate.send("my-topic2", message);
            System.out.println("消息发送成功...");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "success";
    }
}
```

## 4.2、消费者

```java
package com.tender.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerListener {

    @KafkaListener(topics = "my-topic2")
    public void onMessage(String message) {
        System.out.println("被消费的消息: " + message);
    }
}
```

# 5、启动并测试

## 5.1、启动并`发送`消息

![生产者发送消息](./pictures/springboot-kafka-cluster/img.png)

## 5.2、启动并`消费`消息

![生产者消费消息](./pictures/springboot-kafka-cluster/img_1.png)