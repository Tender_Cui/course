# 一、官网
    https://github.com/jasypt/jasypt
# 二、需求
> 在使用SpringBoot开发过程中，会将一些敏感信息配置到SpringBoot项目的配置文件中(不考虑使用配置中心的情况 )，
> 例如数据库的用户名和密码、Redis的密码等。为了保证敏感信息的安全，我们需要将此类数据进行加密配置。

# 三、步骤
## 3.1、添加依赖
> 目前通用的做法是使用 jasypt 对数据库用户名或者密码进行加密，在springboot项目的POM中添加如下依赖，目前最新的版本是3.0.3

```shell
<dependency>
    <groupId>com.github.ulisesbocchio</groupId>
    <artifactId>jasypt-spring-boot-starter</artifactId>
    <version>3.0.3</version>
</dependency>
```

## 3.2、生成密文测试

> 依赖添加之后，会在你本地的maven仓库中下载相关的依赖包，进入到你自己的maven仓库

![jar包路径图](./pictures/jasypt/img_1.png)

> 相关配置，需要查看com.ulisesbocchio.jasyptspringboot.properties.JasyptEncryptorConfigurationProperties

```java
public class EncryptorUtils {

    public static void main(String[] args) {
        // BasicTextEncryptor 的默认加密算法为： PBEWithMD5AndDES
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        // 这个是加密的盐
        textEncryptor.setPassword("wb-csd876509");
        String userName = textEncryptor.encrypt("root");
        String passWord = textEncryptor.encrypt("88888888");

        System.out.println("------------------ 加密操作 ------------------");
        System.out.println("userName加密后 = " + userName);
        System.out.println("passWord加密后 = " + passWord);

        System.out.println("------------------ 解密操作 ------------------");
        System.out.println("userName解密后 = " + textEncryptor.decrypt(userName));
        System.out.println("passWord解密后 = " + textEncryptor.decrypt(passWord));
    }
}
```
```shell
# 结果如下：
------------------ 加密操作 ------------------
userName加密后 = 5ToceETXvdpAv49fgwYmig==
passWord加密后 = jhPEXiRDrxPpO3vM8RLM4zXejbVSKS1m
------------------ 解密操作 ------------------
userName解密后 = root
passWord解密后 = 88888888

Process finished with exit code 0
```

## 3.3、修改Springboot数据库配置和 jasypt配置
```yaml
spring:
  datasource:
    username: ENC(QICgHM+XFyrCJOEd2qoeCQ==)
    password: ENC(MgNhKm6EEYeXt/cPzQ01lY8YFhOTCSdy)
    #serverTimezone=UTC解决时区的报错
    url: jdbc:mysql://localhost:3306/test?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8
    driver-class-name: com.mysql.cj.jdbc.Driver
    type: com.alibaba.druid.pool.DruidDataSource

#####加密解密的配置#####
jasypt:
  encryptor:
    algorithm: PBEWithMD5AndDES
    iv-generator-classname: org.jasypt.iv.NoIvGenerator
    # password: 这个需要通过-D参数来指定 eg: -Djasypt.encryptor.password=xxxxx

```

## 3.4、SpringBoot项目JVM启动参数中配置**加密盐**
```shell
-Djasypt.encryptor.password=wb-csd876509
```

## 3.5、启动类添加注解 `@EnableEncryptableProperties`

```java
@SpringBootApplication
@EnableEncryptableProperties
@EnableConfigurationProperties({RedisShiroProperties.class})
@MapperScan("com.tender.mapper")
@EnableDiscoveryClient
public class ShiroApplication {

    public static void main(String[] args) {
        System.setProperty("jasypt.encryptor.password", "wb-csd876509");
        ConfigurableApplicationContext applicationContext = SpringApplication.run(ShiroApplication.class, args);
        int i = 0;
    }
}
```
