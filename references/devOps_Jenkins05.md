# 参考文献

* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)
* [Jenkins+Docker+SpringCloud 微服务持续集成（微服务+前端+数据库+后端）](https://www.cnblogs.com/darryallen/p/15937121.html)
* [Jenkins连接k8s的多种姿势](https://www.cnblogs.com/ssgeek/p/15101419.html)
* [Jenkins基于https的k8s配置](https://www.cnblogs.com/xiao987334176/p/11338827.html)
* [jenkins连接k8s环境配置](https://blog.csdn.net/ethnicitybeta/article/details/132278378)
* [Jenkins通过kubernetes plugin连接K8s集群★★★★★](https://blog.csdn.net/huangge1199/article/details/126037432)

# 一、Jenkins+Docker+SpringCloud持续集成流程说明

![持续集成流程图片](./pictures/devOps_Jenkins05/img.png)

> 大致流程说明：

* 1、开发人员每天把代码提交到Gitlab代码仓库
* 2、Jenkins从Gitlab中拉取项目源码，编译并打成jar包，然后构建成Docker镜像，将镜像上传到Harbor私有仓库。
* 3、Jenkins发送SSH远程命令，让生产部署服务器到Harbor私有仓库拉取镜像到本地，然后创建容器。
* 4、最后，用户可以访问到容器

## 1.1、环境准备以及服务列表

|    服务器名称    |       服务器     |                             安装的软件                          |
|:---------------:|:---------------:|:-------------------------------------------------------------:|
|   代码托管服务器  | 192.168.255.100 |                            Gitlab-ce-12.4.2                   |
| 持续集成服务器    | 192.168.255.101 | Jenkins，Maven，<span style="color:red">Docker24.0.7-ce</span> |
| Docker仓库服务器 | 192.168.255.102 |  <span style="color:red">Docker24.0.7-ce，Harbor2.10.0</span>  |
|   生产部署服务器  | 192.168.255.103 |        <span style="color:red">Docker24.0.7-ce</span>         |

> 红色的软件为需要安装的软件，黑色代表已经安装

&emsp;&emsp;[docker安装请参考](./docker_Centos7_快速入门.md ':included')   
&emsp;&emsp;[Harbor安装请参考](./devOps_Harbor.md ':included')

## 1.2、微服务的打包&&构建镜像

### 1.2.1、编写 Dockerfile

&emsp;&emsp;<span style="color:red">哪个模块打包可执行jar，Dockerfile 就放在哪个模块下</span>

```dockerfile
# **********************************************************
# *****  哪个模块打包可执行jar，Dockerfile 就放在哪个模块下 *****
# *****  course 项目，Dockerfile 放在 course-test 模块下 *****
# **********************************************************

# 基于一个基础镜像(可以理解为java的 extend 关键字)
FROM openjdk:8-jdk-alpine

# 变量必须先声明，后面才能使用
ARG JAR_FILE

# 将构建参数中的 JAR_FILE 复制到容器的 /app 目录
# docker build --build-arg JAR_FILE=course.jar -t 镜像名称:版本 .
COPY $JAR_FILE /opt/application/course/course.jar

# 设置工作目录
WORKDIR /opt/application/course

# 运行 JAR 文件
CMD ["java", "-jar", "course.jar"]

# 运行容器
# docker run -d --name 取个名字 -p 外部端口:内部端口 镜像名称:版本
```

### 1.2.2、编写 Jenkinsfile

```groovy
// gitlab credential
def git_auth = "848ca7f1-2ba7-4850-b88b-39c72c7fa5df"
// the tag version of image
def tag = "latest"
// harbor server address
def harbor_url = "192.168.255.102:5000/course_project/"

node {
    stage('pull project code 01') {
        checkout scmGit(
            branches: [[name: '*/master']],
            extensions: [],
            userRemoteConfigs: [[
                credentialsId: "${git_auth}",
                url: 'git@192.168.255.100:base_tutorials/course.git'
            ]]
        )
    }

    stage('package && build image 02') {
        // compile，install common jar
        sh "mvn clean -Dmaven.skip.test=true install"
        // more than one dockerfile:build ,this is used to build specified module image
        sh "mvn -f course-test/pom.xml clean package dockerfile:build"
    }
}
```

![jenkins构建结果](./pictures/devOps_Jenkins05/img_1.png)

&emsp;&emsp;查看镜像结果：

![查看镜像结果](./pictures/devOps_Jenkins05/img_2.png)

## 1.3、推送镜像到 Harbor

### 1.3.1、重新打标签

```groovy
// gitlab credential
def git_auth = "848ca7f1-2ba7-4850-b88b-39c72c7fa5df"
// the tag version of image
def tag = "latest"
// harbor server address
def harbor_url = "192.168.255.102:5000/course_project"
// harbor auth
def harbor_auth = ""
def project_name_4_image = "course:v1"


node {
    stage('pull project code 01') {
        checkout scmGit(
            branches: [[name: '*/master']],
            extensions: [],
            userRemoteConfigs: [[
                credentialsId: "${git_auth}",
                url: 'git@192.168.255.100:base_tutorials/course.git'
            ]]
        )
    }
    stage('package build image 02') {
        // compile，install common jar
        sh "mvn clean -Dmaven.skip.test=true install"
        // more than one dockerfile:build ,this is used to build specified module image
        sh "mvn -f course-test/pom.xml clean package dockerfile:build"
        sh "docker tag course-test:0.0.1-SNAPSHOT ${harbor_url}/${project_name_4_harbor}/${project_name_4_image}"
    }
}
```

&emsp;&emsp;查看构建结果

![查看构建结果](./pictures/devOps_Jenkins05/img_3.png)

&emsp;&emsp;查看镜像

![查看打完标签后的镜像](./pictures/devOps_Jenkins05/img_4.png)

### 1.3.2、推送镜像到 Harbor

&emsp;&emsp;由于docker login 是明文的账户和密码，这不安全，因此采用 Jenkins 的凭证。

&emsp;&emsp;Manage Jenkins->Security->Credentials

![新建用户名和密码凭证](./pictures/devOps_Jenkins05/img_5.png)

&emsp;&emsp;获取凭证id，并复制到脚本中

```shell
a8b1d6c0-5578-4c27-a6ce-3ded8a7ab264
```

&emsp;&emsp;利用<span style="color:red">片段生成器</span>来生成代码：

![生成代码](./pictures/devOps_Jenkins05/img_6.png)

&emsp;&emsp;推送镜像

```groovy
// gitlab credential
def git_auth = "848ca7f1-2ba7-4850-b88b-39c72c7fa5df"
// the tag version of image
def tag = "latest"
// harbor server address
def harbor_url = "192.168.255.102:5000"
def project_name_4_harbor = "course_project"
// harbor auth
def harbor_auth = "a8b1d6c0-5578-4c27-a6ce-3ded8a7ab264"
def project_name_4_image = "course:v1"


node {
    stage('pull project code 01') {
        checkout scmGit(
            branches: [[name: '*/master']],
            extensions: [],
            userRemoteConfigs: [[
                credentialsId: "${git_auth}",
                url: 'git@192.168.255.100:base_tutorials/course.git'
            ]]
        )
    }
    stage('package build image 02') {
        // compile，install common jar
        sh "mvn clean -Dmaven.skip.test=true install"
        // more than one dockerfile:build ,this is used to build specified module image
        sh "mvn -f course-test/pom.xml clean package dockerfile:build"
        sh "docker tag course-test:0.0.1-SNAPSHOT ${harbor_url}/${project_name_4_harbor}/${project_name_4_image}"
    }

    stage('push image to Harbor 03') {
        withCredentials([usernamePassword(credentialsId: "${harbor_auth}", passwordVariable: 'password', usernameVariable: 'username')]) {
            // docker login
            sh "docker login -u ${username} -p ${password} ${harbor_url}"
            // push image
            sh "docker push ${harbor_url}/${project_name_4_harbor}/${project_name_4_image}"
        }
    }
}
```

&emsp;&emsp;Jenkins 构建成功&&推送镜像成功

![Jenkins 构建成功](./pictures/devOps_Jenkins05/img_7.png)

&emsp;&emsp;Harbor 查看镜像

![Harbor 查看镜像](./pictures/devOps_Jenkins05/img_8.png)

## 1.4、微服务的<span style="color:red">远程部署</span>与测试

&emsp;&emsp;[容器运行请参考docker的手册文件](./docker_Centos7_快速入门.md ':include')

### 1.4.1、远程操作 192.168.255.103 机器

&emsp;&emsp;为了实现远程发送Shell命令，使得部署服务器执行一些操作，Jenkins 需要安装 <span style="color:red">Publish Over SSH 插件</span>。

&emsp;&emsp;Manage Jenkins->Plugins->Available plugins

![安装Publish Over SSH 插件](./pictures/devOps_Jenkins05/img_9.png)

&emsp;&emsp;配置Publish over SSH，使得 192.168.255.101 可以远程给 192.168.255.103 发送命令

&emsp;&emsp;Manage Jenkins->System->Publish over SSH(最后面)

![配置101机器的私钥文件路径](./pictures/devOps_Jenkins05/img_10.png)

---

![配置103机器的ip和账户名](./pictures/devOps_Jenkins05/img_11.png)

&emsp;&emsp;可以把 192.168.255.101 机子的公钥复制到 192.168.255.103 机子

```shell
# ssh-copy-id 命令 可以把本地主机的公钥复制到远程主机的authorized_keys文件上，
# ssh-copy-id命令也会给远程主机的用户主目录（home）和~/.ssh, 和~/.ssh/authorized_keys设置合适的权限。
ssh-copy-id 192.168.255.103
```

&emsp;&emsp;测试远程连接

![测试远程连接](./pictures/devOps_Jenkins05/img_12.png)

### 1.4.2、生成模板代码

![利用sshPublisher生成模板代码](./pictures/devOps_Jenkins05/img_13.png)

```groovy
sshPublisher(publishers: [
    sshPublisherDesc(
        configName: 'slave-03',
        transfers: [
            sshTransfer(
                cleanRemote: false,
                excludes: '',
                // 这个地方是要执行远程命令的地方，修改这里就可以
                execCommand: "/opt/installs/jenkins_shell/deploy.sh", 
                execTimeout: 120000,
                flatten: false,
                makeEmptyDirs: false,
                noDefaultExcludes: false,
                patternSeparator: '[, ]+',
                remoteDirectory: '',
                remoteDirectorySDF: false,
                removePrefix: '',
                sourceFiles: ''
            )
        ],
        usePromotionTimestamp: false,
        useWorkspaceInPromotion: false,
        verbose: false
    )
])
```

### 1.4.3、192.168.255.103 机器准备脚本

&emsp;&emsp;编写deploy.sh部署脚本

```shell
#!/bin/sh
# 接收外部参数
harbor_url=$1 
harbor_project_name=$2
project_name_4_image=$3
tag=$4
port=$5

imageName=$harbor_url/$harbor_project_name/$project_name_4_image:$tag
echo "$imageName"

# 查询容器是否存在，存在则删除
# grep -w 匹配整个单词
# 特殊处理 tag 为 latest 的情况
container_search_name=""
if [ ${tag} == "latest"]; then
    container_search_name = ${project_name_4_image}
else
    container_search_name = ${project_name_4_image}:${tag}
fi

containerId=echo `docker ps -a | grep -w ${container_search_name} | awk '{print $1}'`
if [ "$containerId" != "" ] ; then
    # 停掉容器
    docker stop $containerId
    # 删除容器
    docker rm $containerId
    echo "成功删除容器"
fi

# 查询镜像是否存在，存在则删除
imageId=`docker images | grep -w $project_name_4_image | awk '{print $3}'`
if [ "$imageId" != "" ] ; then
    #删除镜像
    docker rmi -f $imageId
    echo "成功删除镜像"
fi

# 登录Harbor私服
docker login -u user_course -p Course88888888 $harbor_url
# 下载镜像
docker pull $imageName
# 启动容器
docker run -di -p $port:$port $imageName
echo "容器启动成功"
```

## 1.5、微服务的集群部署与测试（了解即可）

&emsp;&emsp;<span style="color:red"> 推荐通过 K8s 部署</span>，这种通过脚本遍历来构建、发布的方式，了解即可。

## 1.6、jenkins 与 k8s整合

&emsp;&emsp;安装如下插件（安装后重启jenkins）：

> 历史版本插件搜索、下载地址：https://plugins.jenkins.io/ui/search/?query=kubernete

* Kubernetes

&emsp;&emsp;Manage Jenkins->Plugins->Available plugins

![安装Kubernetes 插件](./pictures/devOps_Jenkins05/img_14.png)

### 1.6.1、jenkins 创建 k8s 云

&emsp;&emsp;Manage Jenkins->Clouds->New cloud

![jenkins配置k8s](./pictures/devOps_Jenkins05/img_15.png)

![jenkins配置k8s-02](./pictures/devOps_Jenkins05/img_16.png)

### 1.6.2、生成 jenkins 访问 k8s 的证书

1. 背景

&emsp;&emsp;本文基于Jenkins 和 K8s集群属于<span style="color:red">跨集群</span>的使用场景来进行配置的。

> Jenkins 部署在**A集群**或部署在**传统VM**的环境下，想通过 Jenkins 连接**B集群**，来动态创建pod用以执行构建任务。

2. 端口有什么

&emsp;&emsp;既然是跨集群，那么首先需要考虑的就是网络问题，网络是否可达？需要开通哪些端口的安全组策略？

&emsp;&emsp;在这之前，就需要先了解一下jenkins的运行机制及端口有哪些？

* **http端口**：默认8080，如果在jenkins前面做了反向代理并配置了域名，那么可能是常见的80/443端口
* **Agent Port**：基于JNLP的Jenkins代理通过TCP默认端口50000与Jenkins进行通信
* **SSH port**：jenkins作为ssh服务器，这个一般不会使用，

3. 网络策略打通

&emsp;&emsp;由上面知道了有哪些端口之后，因此需要打通的网络策略包括：

* B集群节点连接jenkins暴露的http port和Agent port
* A集群节点（即jenkins server）连接B集群kube-apiserver暴露的端口

&emsp;&emsp;除网络策略之外，如果jenkins UI的地址，例如通过ingress设置了白名单限制访问，还需要将B集群的相关源ip设置为白名单

4. 查看kube config配置

&emsp;&emsp;由于这里A集群中的jenkins并没有对B集群的操作权限，因此需要配置授权，即发起对B集群的kube apiserver的请求，和kubectl一样利用kube config文件用作请求的鉴权，默认在/root/.kube/config下

```shell
cat /root/.kube/config
```

> 具体内容如下：

```shell
apiVersion: v1
clusters:
- cluster:
    # certificate-authority-data 就是 ca.crt
    certificate-authority-data: xxx
    server: https://192.168.255.101:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    # client-certificate-data 就是 client.crt
    client-certificate-data: xxx
    # client-certificate-data 就是 client.key
    client-key-data: xxx
```

> 其中包含了3段证书相关的内容，也就是我们常见的证书组成格式：ca.crt、client.crt、client.key

5. 生成证书

&emsp;&emsp;在jenkins中能够识别的证书文件为**PKCS#12 certificate**，因此需要先将kube config文件中的证书转换生成**PKCS#12**格式的**pfx**证书文件。

&emsp;&emsp;首先，使用yq命令行工具来解析yaml并通过base 64解码生成各个证书文件

* 服务端证书：

```shell
# 如果 yq 命令不存在
# 访问该网址，手动下载：
https://github.com/mikefarah/yq/releases 
mv /usr/local/bin/yq_linux_amd64 /usr/local/bin/yq
# 或者如下命令
sudo wget -O /usr/local/bin/yq https://github.com/mikefarah/yq/releases/download/v4.40.4/yq_linux_amd64
sudo chmod +x /usr/local/bin/yq

# client-certificate-data —> base 64解码 —> client.crt
yq e '.clusters[0].cluster.certificate-authority-data' /root/.kube/config | base64 -d > ca.crt
```

* 客户端证书 ：

```shell
# client-certificate-data —> base 64解码 —> client.crt
yq e '.users[0].user.client-certificate-data' /root/.kube/config | base64 -d > client.crt

# client-key-data —> base 64解码 —> client.key
yq e '.users[0].user.client-key-data' /root/.kube/config | base64 -d > client.key
```

> 然后，通过openssl进行证书格式的转换，生成Client P12认证文件cert.pfx

```shell
openssl pkcs12 -export -out cert.pfx -inkey client.key -in client.crt -certfile ca.crt
# 88888888
Enter Export Password:  # 输入密码加密
Verifying - Enter Export Password:
```

&emsp;&emsp;查看密钥文件列表：

![查看密钥文件列表](./pictures/devOps_Jenkins05/img_17.png)

&emsp;&emsp;通过踩坑证明，这里<span style="color:red">必须输入密码</span>，不然在后面添加 jenkins 相关配置后验证会报错

### 1.6.3、jenkins 配置 k8s 云

1. Jenkins 配置凭证

![配置jenkins 凭证](./pictures/devOps_Jenkins05/img_18.png)



```shell
kubectl create sa jenkins

kubectl describe sa jenkins

kubectl describe secrets jenkins-token-4kk4q -n default
```











