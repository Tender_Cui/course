# 参考文献

* [`seata`中文官网](https://seata.apache.org/zh-cn/)
* [`seata github`源码](https://github.com/apache/incubator-seata)
* [`seata`部署指南](https://seata.apache.org/zh-cn/docs/ops/deploy-server)

# 机器准备


| 机器ip            | 部署资源  | 端口          |
|-----------------|:-----:|-------------|
| 192.168.255.100 | Seata | 8091（默认）    |  


# 1、下载

&emsp;&emsp;从[`github`官网下载](https://github.com/apache/incubator-seata)，目前最新的版本：`seata-server-2.0.0.tar.gz`

![下载地址](./pictures/Centos7-seata/img.png)

&emsp;&emsp;选择`Assets`，下载资源

![下载资源](./pictures/Centos7-seata/img_1.png)

# 2、上传与解压

## 2.1、上传

![上传](./pictures/Centos7-seata/img_2.png)

## 2.2、解压

```shell
tar -zxvf seata-server-2.0.0.tar.gz -C ../installs/
```

# 3、创建数据库

&emsp;&emsp;`Seata Server`的信息需要持久化到数据库，我们创建一个数据库，用来持久化信息使用。

```sqlite-psql
ceata database seata;

use seata;
```

## 3.1、建表语句

&emsp;&emsp;`github`开源项目上的建表语句位置。

![建表语句](./pictures/Centos7-seata/img_3.png)

&emsp;&emsp;[建表语句的来源地址](https://github.com/apache/incubator-seata/blob/2.x/script/server/db/mysql.sql)

```sql
-- -------------------------------- The script used when storeMode is 'db' --------------------------------
-- the table to store GlobalSession data
CREATE TABLE IF NOT EXISTS `global_table`
(
    `xid`                       VARCHAR(128) NOT NULL,
    `transaction_id`            BIGINT,
    `status`                    TINYINT      NOT NULL,
    `application_id`            VARCHAR(32),
    `transaction_service_group` VARCHAR(32),
    `transaction_name`          VARCHAR(128),
    `timeout`                   INT,
    `begin_time`                BIGINT,
    `application_data`          VARCHAR(2000),
    `gmt_create`                DATETIME,
    `gmt_modified`              DATETIME,
    PRIMARY KEY (`xid`),
    KEY `idx_status_gmt_modified` (`status` , `gmt_modified`),
    KEY `idx_transaction_id` (`transaction_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- the table to store BranchSession data
CREATE TABLE IF NOT EXISTS `branch_table`
(
    `branch_id`         BIGINT       NOT NULL,
    `xid`               VARCHAR(128) NOT NULL,
    `transaction_id`    BIGINT,
    `resource_group_id` VARCHAR(32),
    `resource_id`       VARCHAR(256),
    `branch_type`       VARCHAR(8),
    `status`            TINYINT,
    `client_id`         VARCHAR(64),
    `application_data`  VARCHAR(2000),
    `gmt_create`        DATETIME(6),
    `gmt_modified`      DATETIME(6),
    PRIMARY KEY (`branch_id`),
    KEY `idx_xid` (`xid`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- the table to store lock data
CREATE TABLE IF NOT EXISTS `lock_table`
(
    `row_key`        VARCHAR(128) NOT NULL,
    `xid`            VARCHAR(128),
    `transaction_id` BIGINT,
    `branch_id`      BIGINT       NOT NULL,
    `resource_id`    VARCHAR(256),
    `table_name`     VARCHAR(32),
    `pk`             VARCHAR(36),
    `status`         TINYINT      NOT NULL DEFAULT '0' COMMENT '0:locked ,1:rollbacking',
    `gmt_create`     DATETIME,
    `gmt_modified`   DATETIME,
    PRIMARY KEY (`row_key`),
    KEY `idx_status` (`status`),
    KEY `idx_branch_id` (`branch_id`),
    KEY `idx_xid` (`xid`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `distributed_lock`
(
    `lock_key`       CHAR(20) NOT NULL,
    `lock_value`     VARCHAR(20) NOT NULL,
    `expire`         BIGINT,
    primary key (`lock_key`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

INSERT INTO `distributed_lock` (lock_key, lock_value, expire) VALUES ('AsyncCommitting', ' ', 0);
INSERT INTO `distributed_lock` (lock_key, lock_value, expire) VALUES ('RetryCommitting', ' ', 0);
INSERT INTO `distributed_lock` (lock_key, lock_value, expire) VALUES ('RetryRollbacking', ' ', 0);
INSERT INTO `distributed_lock` (lock_key, lock_value, expire) VALUES ('TxTimeoutCheck', ' ', 0);
```

# 4、修改`Seata`配置文件

&emsp;&emsp;先备份原来的配置文件

```shell
cp application.yml application.yml.back
```

&emsp;&emsp;怎么配置，可以参考配置文件：`application.example.yml`

```yaml
server:
  port: 7091

spring:
  application:
    name: seata-server

logging:
  config: classpath:logback-spring.xml
  file:
    path: ${log.home:${user.home}/logs/seata}
  extend:
    logstash-appender:
      destination: 127.0.0.1:4560
    kafka-appender:
      bootstrap-servers: 127.0.0.1:9092
      topic: logback_to_logstash

console:
  user:
    username: seata
    password: seata

seata:
  config:
    type: nacos
    nacos:
      server-addr: 192.168.255.100:8848
      namespace:
      group: SEATA_GROUP  # 后续需要自己在nacos 里面新建，不想新建 SEATA_GROUP 就写 DEFAULT_GROUP
      username: nacos
      password: nacos
      context-path:
      data-id: seataServer.properties
  registry:
    type: nacos
    # preferred-networks: 30.240.*
    nacos:
      application: seata-server
      server-addr: 192.168.255.100:8848
      group: SEATA_GROUP # 后续需要自己在nacos 里面新建，不想新建 SEATA_GROUP 就写 DEFAULT_GROUP
      namespace:
      cluster: default
      username: nacos
      password: nacos
      context-path:
  store:
    mode: db
    db:
      datasource: druid
      db-type: mysql
      # driver-class-name: com.mysql.jdbc.Driver
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://192.168.255.101:3306/seata?rewriteBatchedStatements=true
      user: root
      password: 88888888
      min-conn: 10
      max-conn: 100
      global-table: global_table
      branch-table: branch_table
      lock-table: lock_table
      distributed-lock-table: distributed_lock
      query-limit: 1000
      max-wait: 5000

  security:
    secretKey: SeataSecretKey0c382ef121d778043159209298fd40bf3850a017
    tokenValidityInMilliseconds: 1800000
    ignore:
      urls: /,/**/*.css,/**/*.js,/**/*.html,/**/*.map,/**/*.svg,/**/*.png,/**/*.jpeg,/**/*.ico,/api/v1/auth/login,/metadata/v1/**

```

# 5、启动`Seata`

> 依赖`nacos`

```shell
cd /opt/installs/seata/bin

sh seata-server.sh
```

&emsp;&emsp;查看启动日志

```shell
cd /root/logs/seata
```

![查看启动日志](./pictures/Centos7-seata/img_4.png)

&emsp;&emsp;查看`nacos`的服务列表：

![查看`nacos`的服务列表](./pictures/Centos7-seata/img_5.png)

&emsp;&emsp;访问`Seata`的控制台：

![`Seata`的控制台](./pictures/Centos7-seata/img_6.png)

# 6、`Seata`的部署指南

![`Seata`的部署指南](./pictures/Centos7-seata/img_n.png)

&emsp;&emsp;[`seata`部署指南](https://seata.apache.org/zh-cn/docs/ops/deploy-server)

