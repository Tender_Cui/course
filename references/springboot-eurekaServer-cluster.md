# 参考文献

* [springboot整合eureka](https://blog.csdn.net/qq_36151389/article/details/133354498)
* [Eureka服务集群DS Replicas为空](https://blog.csdn.net/ywanju/article/details/119686024)

# 1、pom.xml 配置

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-eureka-server</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
        </dependency>
    </dependencies>

</project>
```

2、本地hosts 文件配置

&emsp;&emsp;本地搭建 eureka 集群的话，如果`eureka.instance.hostname=locaohost` 则页面中的 `DS Replicas` 下面为空

```html
127.0.0.1   eureka.server1
127.0.0.1   eureka.server2
127.0.0.1   eureka.server3
```

3、配置文件设置

* application-dev.yaml

```shell
server:
  port: 9101

spring:
  application:
    name: shiro-eureka-server

eureka:
  instance:
    # eureka所在的服务器名
    # 搭建集群的话，不能使用localhost，本地资源紧张的话，可以hosts 文件配置不同的域名来模拟
    #
    # 127.0.0.1   eureka.server1
    # 127.0.0.1   eureka.server2
    # 127.0.0.1   eureka.server3
    hostname: eureka.server1
    # 解决 Eureka 在相互注册（集群）形成高可用时DS Replicas为空
    prefer-ip-address: true
  client:
    # 默认值为 true ，表示是否将自己本节点注册到 Eureka Servet，本应用配置为 false ，因为本身就是 Eureka Server (服务注册中心)
    register-with-eureka: true
    # 默认值为 true，表示是否从Eureka Server获取注册信息，注册中心节点配置为 false
    fetch-registry: true
    # eureka提供服务的地址
    # 注册中心默认需要的端口为 8761，如果不是 8761，则其他节点必须要通过eureka.client.service-url.defaultZone 来指明，不能省略
    service-url:
      # 设置与 eureka server 交互的地址查询服务和注册服务都需要依赖的地址

      # 单机版配置defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka
      # defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka

      # 集群配置
      defaultZone: http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
```

* application-sit.yaml

```shell
server:
  port: 9102

spring:
  application:
    name: shiro-eureka-server

eureka:
  instance:
    # eureka所在的服务器名
    # 搭建集群的话，不能使用localhost，本地资源紧张的话，可以hosts 文件配置不同的域名来模拟
    #
    # 127.0.0.1   eureka.server1
    # 127.0.0.1   eureka.server2
    # 127.0.0.1   eureka.server3
    hostname: eureka.server2
    # 解决 Eureka 在相互注册（集群）形成高可用时DS Replicas为空
    prefer-ip-address: true
  client:
    # 默认值为 true ，表示是否将自己本节点注册到 Eureka Servet，本应用配置为 false ，因为本身就是 Eureka Server (服务注册中心)
    register-with-eureka: true
    # 默认值为 true，表示是否从Eureka Server获取注册信息，注册中心节点配置为 false
    fetch-registry: true
    # eureka提供服务的地址
    # 注册中心默认需要的端口为 8761，如果不是 8761，则其他节点必须要通过eureka.client.service-url.defaultZone 来指明，不能省略
    service-url:
      # 设置与 eureka server 交互的地址查询服务和注册服务都需要依赖的地址

      # 单机版配置defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka
      # defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka

      # 集群配置
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server3:9103/eureka
```

* application-uat.yaml

```shell
server:
  port: 9103

spring:
  application:
    name: shiro-eureka-server

eureka:
  instance:
    # eureka所在的服务器名
    # 搭建集群的话，不能使用localhost，本地资源紧张的话，可以hosts 文件配置不同的域名来模拟
    #
    # 127.0.0.1   eureka.server1
    # 127.0.0.1   eureka.server2
    # 127.0.0.1   eureka.server3
    hostname: eureka.server3
    # 解决 Eureka 在相互注册（集群）形成高可用时DS Replicas为空
    prefer-ip-address: true
  client:
    # 默认值为 true ，表示是否将自己本节点注册到 Eureka Servet，本应用配置为 false ，因为本身就是 Eureka Server (服务注册中心)
    register-with-eureka: true
    # 默认值为 true，表示是否从Eureka Server获取注册信息，注册中心节点配置为 false
    fetch-registry: true
    # eureka提供服务的地址
    # 注册中心默认需要的端口为 8761，如果不是 8761，则其他节点必须要通过eureka.client.service-url.defaultZone 来指明，不能省略
    service-url:
      # 设置与 eureka server 交互的地址查询服务和注册服务都需要依赖的地址

      # 单机版配置defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka
      # defaultZone: http://${eureka.instance.hostname}:${server.port}/eureka

      # 集群配置
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka
```

# 4、启动

&emsp;&emsp;通过指定 `VM` 参数来激活不同的配置文件，以激活 `application-dev.yaml` 配置文件为示例：

![激活指定的配置文件](./pictures/springboot-eurekaServer-cluster/img.png)

# 5、浏览器验证

![浏览器验证](./pictures/springboot-eurekaServer-cluster/img_1.png)
