# 参考文献

* [CentOS7 下 Zookeeper 安装及配置](https://blog.csdn.net/qifu123/article/details/133387353)
* [zk关于命令操作创建节点](https://blog.csdn.net/cx897459376/article/details/114991401)
* 
# 1、Zookeeper 是什么

&emsp;&emsp;[官网](https://www.apache.org/dyn/closer.lua/zookeeper)

* ZooKeeper 是一个开源的分布式协调服务，它提供了一个高性能的、可靠的分布式环境，用于协调和管理分布式应用程序的配置、状态和元数据信息。

* ZooKeeper 旨在解决分布式系统中的一致性和协调问题。它提供了一个简单的文件系统层次结构，类似于标准文件系统，但是用于存储和管理分布式应用程序的数据（注意他的主要作用不是用来存数据）。

* ZooKeeper 主要特性
    * 分布式协调：ZooKeeper 提供了一套原语，如锁、队列、同步和通知机制，用于分布式应用程序之间的协调和同步。
    * 高性能：ZooKeeper 的设计目标之一是提供低延迟和高吞吐量的访问性能，以满足高负载的分布式应用程序的需求。
    * 可靠性：ZooKeeper 使用了一致性协议（ZAB）来确保数据的一致性和可靠性。它采用主从架构，支持自动故障恢复和数据冗余，以提供高可用性和可靠性。
    * 容错性：ZooKeeper 允许在集群中部署多个服务器实例，以提供容错性。即使有部分服务器故障，ZooKeeper 仍然可以继续正常运行，保持服务的可用性。

* ZooKeeper 应用方向：分布式锁、配置管理、集群管理、分布式队列等。

# 2、前置准备

## 2.1、环境准备

| 主机              | id | 名称         | IP             |
|-------------------|:--:|------------|-----------------|
| 第一台Zookeeper主机 | 1  | zookeeper1 | 192.168.255.100 |
| 第二台Zookeeper主机 | 2  | zookeeper2 | 192.168.255.101 |
| 第三台Zookeeper主机 | 3  | zookeeper3 | 192.168.255.102 |

## 2.2、jdk环境准备

&emsp;&emsp;[Centos7 安装 jdk 1.8的参考手册](./Centos7-jdk1.8.md ':included')

&emsp;&emsp;三台机子的java 版本都是 1.8

![jdk 版本](./pictures/Centos7-ZooKeeper/img.png)

# 3、下载 Zookeeper

&emsp;&emsp;下载官方推荐的稳定版本的 [地址](https://dlcdn.apache.org/zookeeper/stable/)。

![下载 Zookeeper](./pictures/Centos7-ZooKeeper/img_1.png)

&emsp;&emsp;下载后，利用工具 FinalShell 传到 `192.168.255.100` 机器，然后再用 scp 命令传到别的机器。

![上传安装包](./pictures/Centos7-ZooKeeper/img_2.png)

&emsp;&emsp;scp 到另外两台机器：

```html
scp apache-zookeeper-3.8.4-bin.tar.gz root@192.168.255.101:/opt/softs/
scp apache-zookeeper-3.8.4-bin.tar.gz root@192.168.255.102:/opt/softs/
```

![scp 到另外两台机器](./pictures/Centos7-ZooKeeper/img_3.png)

# 4、安装 Zookeeper

## 4.1、解压（三台机器都执行）

&emsp;&emsp;解压到目录：

```html
tar -zxvf apache-zookeeper-3.8.4-bin.tar.gz -C ../installs/
```

![解压到目录](./pictures/Centos7-ZooKeeper/img_4.png)

## 4.2、配置 Zookeeper 参数（三台机器都执行）

&emsp;&emsp;复制一份新的配置文件 zoo.cfg

```shell
cp zoo_sample.cfg zoo.cfg
```

&emsp;&emsp;修改`zoo.cfg`的配置项，格式如下：

```shell
server.<ID>=<hostname>:2888:3888

# 具体解释如下如下：
<ID>替换为节点的ID号
<hostname>替换为节点的主机名或IP地址
2888 为ZooKeeper点对点通信的端口号
3888 为leader选举的端口号

# 例如，第一个节点的ID为1，主机名为“zookeeper1”，ip为”192.168.211.136”则在配置文件中增加如下一行:
server.1=zookeeper:2888:3888
# 或者指定 ip
server.1=192.168.255.100:2888:3888

#若有3台服务器，则需要添加三行：
server.1=192.168.255.100:2888:3888
server.2=192.168.255.101:2888:3888
server.3=192.168.255.102:2888:3888
```

&emsp;&emsp;关于<span style="color:red">本次集群搭建的配置</span>如下：

```shell
# zookeeper 数据目录
dataDir=/var/zookeeper/data
# 3台服务器的配置
# 这里可以使用IP，也可以使用主机名，但使用主机名时要配置好hosts文件。
server.1=192.168.255.100:2888:3888
server.2=192.168.255.101:2888:3888
server.3=192.168.255.102:2888:3888
```

&emsp;&emsp;同时拷贝`zoo.cfg` 到另外两个机器

```shell
# pwd 拷贝到相同的目录
scp zoo.cfg root@192.168.255.101:/opt/installs/apache-zookeeper-3.8.4-bin/conf
scp zoo.cfg root@192.168.255.102:/opt/installs/apache-zookeeper-3.8.4-bin/conf
```

&emsp;&emsp; 由于上面的 `zoo.cfg` 指定了zookeeper 数据目录：`/var/zookeeper/data`，因此需要创建对应的目录。

```shell
#（三个机器都要执行）
mkdir -p /var/zookeeper/data
```

## 4.3、创建`myid`文件（三个机器都要执行）

&emsp;&emsp;`myid` 文件一定要在目录：`dataDir=/var/zookeeper/data` 下面。比如：`/var/zookeeper/data/myid`，文件里面填写数字，不需要加引号。

```shell
cd /var/zookeeper/data
touch myid
vim myid
# 或者
echo 1 > myid

# 分别填写数据1
# 分别填写数据2
# 分别填写数据3
# 依此类推
```

# 5、配置 Zookeeper 环境变量（三个机器都要执行）

&emsp;&emsp;

```shell
vi /etc/profile

export ZOOKEEPER_HOME=/opt/installs/apache-zookeeper-3.8.4-bin
export PATH=$PATH:$ZOOKEEPER_HOME/bin

# 刷新配置
source /etc/profile
```

# 6、启动测试（三个机器都要启动）

## 6.1、启动 Zookeeper

```shell
zkServer.sh start

# 前台启动，前台启动可以直接看到日志信息
zkServer.sh start-foreground

# 重新启动 
zkServer.sh restart
```

&emsp;&emsp;如果有防火墙，需要开通 `2888` 和 `3888` 端口。

```shell
# 查看防火墙状态
systemctl status firewalld

firewall-cmd --zone=public --add-port=2181/tcp --permanent 
firewall-cmd --zone=public --add-port=2888/tcp --permanent 
firewall-cmd --zone=public --add-port=3888/tcp --permanent 
# 防火墙重载刷新
firewall-cmd --reload
```

## 6.2、查看 Zookeeper 状态

```shell
zkServer.sh status
```

&emsp;&emsp;通过查询状态可以看到其中一台是leader，另外两台是follower

![192.168.255.100](./pictures/Centos7-ZooKeeper/img_5.png)

![192.168.255.101](./pictures/Centos7-ZooKeeper/img_6.png)

![192.168.255.102](./pictures/Centos7-ZooKeeper/img_7.png)

## 6.3、查看 Zookeeper 版本

```shell
zkServer.sh version
```

## 6.4、停止 Zookeeper

```shell
zkServer.sh stop
```

## 6.5、查看 Zookeeper数据

```shell
zkCli.sh
```

&emsp;&emsp;输入`zkCli.sh`回车后会进入Zookeeper系统，在这可以查看Zookeeper数据等信息，如命令“ ls / ”可以查看根目录下的内容，如下图。

![客户端操作](./pictures/Centos7-ZooKeeper/img_8.png)

# 7、配置文件说明

```shell
# The number of milliseconds of each tick
# 主从服务间心跳时间间隔
tickTime=2000
# The number of ticks that the initial
# synchronization phase can take
# 从节点追随主机点时，主节点对从节点初始延迟等待时间=tickTime * initLimit
initLimit=10
# The number of ticks that can pass between
# sending a request and getting an acknowledgement
# 主节眯对从节点同步数据超时时间=tickTime * syncLimit
syncLimit=5
# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just
# example sakes.
# 数据持久化目录
# dataDir=/tmp/zookeeper
# the port at which the clients will connect
# Zookeeper 服务端口号
clientPort=2181
# the maximum number of client connections.
# increase this if you need to handle more clients
# 最大客户端连接数
#maxClientCnxns=60
#
# Be sure to read the maintenance section of the
# administrator guide before turning on autopurge.
#
# https://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance
#
# The number of snapshots to retain in dataDir
#autopurge.snapRetainCount=3
# Purge task interval in hours
# Set to "0" to disable auto purge feature
#autopurge.purgeInterval=1

## Metrics Providers
#
# https://prometheus.io Metrics Exporter
#metricsProvider.className=org.apache.zookeeper.metrics.prometheus.PrometheusMetricsProvider
#metricsProvider.httpHost=0.0.0.0
#metricsProvider.httpPort=7000
#metricsProvider.exportJvmInfo=true

# Zookeeper 各节点配置信息
# 3台服务器的配置
# 这里可以使用IP，也可以使用主机名，但使用主机名时要配置好hosts文件。
server.1=192.168.255.100:2888:3888
server.2=192.168.255.101:2888:3888
server.3=192.168.255.102:2888:3888
```
