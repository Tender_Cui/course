# 参考文献

* [ingress 的官方使用手册](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration)

# 安装 Ingress

# 一、Ingress 简介

&emsp;&emsp;Ingress：Service 的统一网关入口，底层就是 nginx。   
&emsp;&emsp;[Ingress 官网地址](https://kubernetes.github.io/ingress-nginx/)

## 1.1、下载 Ingress 资源文件

```shell
# -O ingress-deploy.yaml
# -O: 以 ingress-deploy.yaml 文件来存储
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.47.0/deploy/static/provider/baremetal/deploy.yaml -O ingress-deploy.yaml
```

[请详见ingress-deploy.yaml文件内容](./k8s_yaml/ingress_v_0.46.0/ingress-deploy.yaml 'included')

## 1.2、修改 ingress-deploy.yaml

&emsp;&emsp;由于镜像下载太慢，使用 雷神 的镜像。

```shell
# 雷神的镜像
registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/ingress-nginx-controller:v0.46.0
```

```shell
vim ingress-deploy.yaml
```

&emsp;&emsp;修改内容如下：

![修改ingress-deploy.yaml](pictures/ingress_v_0.46.0/img.png)

## 1.3、 应用 ingress-deploy.yaml 文件

```shell
kubectl apply -f ingress-deploy.yaml
```

&emsp;&emsp;<span style="color:red">解决报错</span>

&emsp;&emsp;通过查看 pod，可以看到 还不是 running 状态。

```shell
kubectl get pod -n ingress-nginx

# 查看 ingress-nginx-controller 的状态，可以看到错误提示了。
#  MountVolume.SetUp failed for volume "webhook-cert" : secret "ingress-nginx-admission" not found
kubectl describe pod ingress-nginx-controller-68466b9c78-zv2m9 -n ingress-nginx
```

![修改ingress-deploy.yaml](pictures/ingress_v_0.46.0/img_1.png)

> ingress-nginx-admission-create-ctkn6 和 ingress-nginx-admission-patch-hghzw 状态都是 Completed ，这两个不需要管。

![ingress-nginx-controller对应的pod报错](pictures/ingress_v_0.46.0/img_2.png)

&emsp;&emsp;直接修改已经部署的 deploy

```shell
# 查看 secret 的 name
# 拿到：ingress-nginx-admission-token-k6n45
kubectl get secret -n ingress-nginx

# 查看ingress 的 deploy 名称
# 拿到：ingress-nginx-controller
kubectl get deploy -n ingress-nginx

# 在线修改 （搜索：webhook-cert）
kubectl edit deploy ingress-nginx-controller -n ingress-nginx
```

&emsp;&emsp;找到需要修改的 secret name

![修改ingress-deploy.yaml](pictures/ingress_v_0.46.0/img_3.png)

## 1.4、查看 ingress 相关服务信息

&emsp;&emsp;修改和保存后，查看 pod 的描述，直至正常后再查看 ingress 。

```shell
kubectl get pod,svc -n ingress-nginx
```

## 1.5、浏览器访问

&emsp;&emsp;返回 nginx 说明流程已经跑通，只是转发后找不到后台服务！
![浏览器访问反馈](pictures/ingress_v_0.46.0/img_4.png)

# 二、测试 Ingress 资源

## 2.1、部署 service 服务

&emsp;&emsp;service 资源文件（service-test.yaml）

```shell
kubectl apply -f service-test.yaml
```

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: hello-server
  template:
    metadata:
      labels:
        app: hello-server
    spec:
      containers:
        - name: hello-server
          image: registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/hello-server
          ports:
            - containerPort: 9000
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-demo
  name: nginx-demo
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx-demo
  template:
    metadata:
      labels:
        app: nginx-demo
    spec:
      containers:
        - image: nginx
          name: nginx
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: nginx-demo
  name: nginx-demo
spec:
  selector:
    app: nginx-demo
  ports:
    - port: 8000
      protocol: TCP
      targetPort: 80
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app: hello-server
  name: hello-server
spec:
  selector:
    app: hello-server
  ports:
    - port: 8000   # service 端口是 8000
      protocol: TCP
      targetPort: 9000  # 目标容器端口是 9000
```

## 2.2、部署 ingress 服务

&emsp;&emsp;Ingress 资源文件（ingress-rule.yaml）

```shell
kubectl apply -f ingress-rule.yaml
```

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress  
metadata:
  name: ingress-host-bar
spec:
  ingressClassName: nginx
  rules:
  - host: "hello.atguigu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: hello-server
            port:
              number: 8000 # hello-server （service） 的端口是 8000
  - host: "demo.atguigu.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"  # 把请求会转给下面的服务，下面的服务一定要能处理这个路径，不能处理就是404
        backend:
          service:
            name: nginx-demo  #java，比如使用路径重写，去掉前缀nginx
            port:
              number: 8000
```

&emsp;&emsp;安装 ingress 报错以及问题解决：

![安装 ingress 报错以及问题解决](pictures/ingress_v_0.46.0/img_5.png)

## 2.3、浏览器访问

```shell
# 本机添加hosts 配置，不是虚机添加

192.168.255.101 hello.atguigu.com
192.168.255.101 demo.atguigu.com

# 查看 Ingress svc
 kubectl get svc -n ingress-nginx
```

![查看 Ingress svc](pictures/ingress_v_0.46.0/img_6.png)

&emsp;&emsp;浏览器访问结果如下：

![结果01](pictures/ingress_v_0.46.0/img_7.png)

![结果02](pictures/ingress_v_0.46.0/img_8.png)

# 三、Ingress 的使用

