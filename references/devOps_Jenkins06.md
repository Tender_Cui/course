# 参考文献

* [Jenkins 进阶篇 - 节点配置](https://www.cnblogs.com/liudecai/p/14931120.html)
* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)

# 构建 Jenkins 主从架构的可持续集成平台

# 一、Jenkins 传统的的Master-Slave分布式构建（了解）

## 1.1、什么是Master-Slave分布式构建?

&emsp;&emsp;什么是Master-Slave分布式构建?

![Jenkins的Master-Slave分布式构建](./pictures/devOps_Jenkins06/img.png)

> Jenkins的Master-Slave分布式构建，就是通过将构建过程分配到从属Slave节点上，从而减轻Master节点的压力，而且可以同时构建多个，有点类似负载均衡的概念。

## 1.2、如何实现Master-Slave分布式构建

## 1.2.1、开启代理程序的TCP端口

&emsp;&emsp;Manage Jenkins->Security->Security

![开启代理程序的TCP端口](./pictures/devOps_Jenkins06/img_1.png)

### 1.2.2、新建节点

&emsp;&emsp;Manage Jenkins->Nodes->New Node

![新建节点01](./pictures/devOps_Jenkins06/img_2.png)

![新建节点配置远程工作目录02](./pictures/devOps_Jenkins06/img_3.png)

### 1.2.3、如何启动新建的节点

![点击新建的节点](./pictures/devOps_Jenkins06/img_4.png)

![如何下载和启动从节点](./pictures/devOps_Jenkins06/img_5.png)

```shell
# 下载 agent.jar
curl -sO http://192.168.255.101:8888/jnlpJars/agent.jar
# 启动 从节点
nohup java -jar agent.jar -url http://192.168.255.101:8888/ -secret 6005ca030a714d1c2c92c60536e2cf47cfa39cd7416a15ff2cf3fb0cb7fd9512 -name "jenkins-slave-01" -workDir "/opt/workspaces/jenkins-slave-01-workspace" &
```

&emsp;&emsp;查看启动结果

![查看启动结果日志](./pictures/devOps_Jenkins06/img_6.png)

![查看启动列表页](./pictures/devOps_Jenkins06/img_7.png)

## 1.3、以拉取代码来测试 Jenkins 的主从结构

### 1.3.1、Freestyle 项目

&emsp;&emsp;创建一个自由风格的项目：jenkins_HA_freestyle_course_test

&emsp;&emsp;配置该项目:

![配置该项目](./pictures/devOps_Jenkins06/img_8.png)

> Jenkins 构建测试（192.168.255.102机器需要安装git）

```shell
yum install git -y
```

![自由风格项目构建结果](./pictures/devOps_Jenkins06/img_9.png)

### 1.3.2、Pipeline 项目

&emsp;&emsp;创建一个 Pipeline 的项目：jenkins_HA_pipeline_course_test

&emsp;&emsp;配置该项目:

![Pipeline项目构建结果](./pictures/devOps_Jenkins06/img_10.png)

```groovy
node {
    stage('pull project code 01') {
        checkout scmGit(
            branches: [[name: '*/master']],
            extensions: [],
            userRemoteConfigs: [[
                credentialsId: "848ca7f1-2ba7-4850-b88b-39c72c7fa5df",
                url: 'git@192.168.255.100:base_tutorials/course.git'
            ]]
        )
    }
}
```

&emsp;&emsp;多次构建该项目，可以看到既有在master上构建也有在slave 从节点构建的结果 (TODO)。

# 二、基于 K8S 构建 Jenkins 主从架构的可持续集成平台

&emsp;&emsp;传统 Jenkins 的 Master-Slave 方案的缺陷:

* Master节点发生单点故障时，整个流程都不可用了
* 每个 Slave节点的配置环境不一样，来完成不同语言的编译打包等操作，但是这些差异化的配置导致管理起来非常不方便，维护起来也是比较费劲
* 资源分配不均衡，有的 Slave节点要运行的job出现排队等待，而有的Slave节点处于空闲状态
* 资源浪费，每台 Slave节点可能是实体机或者VM，当Slave节点处于空闲状态时，也不会完全释放掉资源

&emsp;&emsp;以上种种问题，我们可以引入Kubernates来解决！

## 2.1、Kubernetes 简介

&emsp;&emsp;Kubernetes（简称，K8S）是Google开源的容器集群管理系统，在Docker技术的基础上，为容器化的应用提供部署运行、资源调度、服务发现和动态伸缩等一系列完整功能，提高了大规模容器集群管理的便捷性。 其主要功能如下：

* 使用Docker对应用程序包装(package)、实例化(instantiate)、运行(run)。
* 以集群的方式运行、管理跨机器的容器。以集群的方式运行、管理跨机器的容器。
* 解决Docker跨机器容器之间的通讯问题。解决Docker跨机器容器之间的通讯问题。
* Kubernetes的自我修复机制使得容器集群总是运行在用户期望的状态。

## 2.2、Kubernetes+Docker+Jenkins 持续集成架构图

![持续集成架构图01](./pictures/devOps_Jenkins06/img_11.png)

![持续集成架构图02](./pictures/devOps_Jenkins06/img_12.png)

## 2.3、Kubernetes+Docker+Jenkins 持续集成方案好处

* <span style="color:red">**服务高可用</span>**</span>：当 Jenkins Master 出现故障时，Kubernetes 会自动创建一个新的 Jenkins Master 容器，并且将 Volume 分配给新创建的容器，保证数据不丢失，从而达到集群服务高可用。
* <span style="color:red">**动态伸缩，合理使用资源**</span>：每次运行 Job 时，会自动创建一个 Jenkins Slave，Job 完成后，Slave自动注销并删除容器，资源自动释放，而且 Kubernetes 会根据每个资源的使用情况，动态分配 Slave 到空闲的节点上创建，降低出现因某节点资源利用率高，还排队等待在该节点的情况。
* <span style="color:red">**扩展性好**</span>：当 Kubernetes 集群的资源严重不足而导致 Job 排队等待时，可以很容易的添加一个 Kubernetes Node 到集群中，从而实现扩展。

## 2.4、k8s 集群的安装

&emsp;&emsp;[参考手册：kubernetes集群_tutorials.md](./kubernetes_集群安装_tutorials.md 'include:')

## 2.5、k8s 安装 Jenkins

&emsp;&emsp;[参考手册：devOps_k8s_install_Jenkins.md](./kubernetes_install_Jenkins.md 'include:')

> 访问jenkins页面

![验证k8s部署的jenkins](./pictures/devOps_Jenkins06/img_13.png)

## 2.6、k8s 登录

&emsp;&emsp;如何获取登录密码？由于我们的pvc是挂载到 /opt/nfs/jenkins下。因此我们可以从宿主机 192.168.255.101 寻找密码

```shell
cd /opt/nfs/jenkins
cat secrets/initialAdminPassword
```

![查看jenkins 的密码](./pictures/devOps_Jenkins06/img_14.png)

## 2.6、k8s 基本操作

&emsp;&emsp;[jenkins基本操作请参考：devOps_Jenkins.md](./devOps_Jenkins.md ':include')

&emsp;&emsp;为了简单演示，这边只安装如下几个插件

* Localization: Chinese (Simplified)
* Git
* Pipeline
* Kubernetes

&emsp;&emsp;测试Jenkins 通过 Kubernetes 插件来连接k8s集群

```shell
# 下面这个地址只有再同一个网络才可以（就是k8s 安装的 jenkins）
https://kubernetes.default.svc.cluster.local
```

![Kubernetes 插件来连接k8s集群](./pictures/devOps_Jenkins06/img_15.png)

### 2.6.1、freestyle 风格项目的代码拉取测试

1. 配置gitlab 的凭证

![配置gitlab 的凭证](./pictures/devOps_Jenkins06/img_16.png)

2. 拉取代码测试

![创建一个流水线项目&&配置该项目](./pictures/devOps_Jenkins06/img_17.png)

![流水线项目构建结果](./pictures/devOps_Jenkins06/img_18.png)

### 2.6.2、pipeline 风格项目的代码拉取测试













































































