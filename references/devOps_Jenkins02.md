# 参考文献
* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)

# 一、Jenkins项目构建类型：Pipeline流水线项目构建

## 1.1、Pipeline简介

> 1、概念

&emsp;&emsp;Pipeline，简单来说，就是一套运行在 Jenkins 上的工作流框架，将原来独立运行于单个或者多个节点的任务连接起来，实现单个任务难以完成的复杂流程编排和可视化的工作。

> 2、使用 Pipeline 的好处

* 代码：Pipeline以代码的形式实现，通常被纳入源代码控制，使团队能够编辑，审查和迭代其传送流程。
* 持久：无论是计划内的还是计划外的服务器重启，Pipeline都是可恢复的。
* 可停止：Pipeline可接收交互式输入，以确定是否继续执行Pipeline。
* 多功能：Pipeline支持现实世界中复杂的持续交付要求。它支持fork/join、循环执行，并行执行任务的功能。
* 可扩展：Pipeline插件支持其DSL的自定义扩展 ，以及与其他插件集成的多个选项。

> 3、如何创建 Jenkins Pipeline ？

&emsp;&emsp;Pipeline 脚本是由 Groovy 语言实现的，但是我们没必要单独去学习 Groovy。   
&emsp;&emsp;Pipeline 支持两种语法：Declarative(声明式)和 Scripted Pipeline(脚本式)语法。

&emsp;&emsp;Pipeline 也有两种创建方法：
* 直接在 Jenkins 的 Web UI 界面中输入脚本
* 通过创建一个 Jenkinsfile 脚本文件放入项目源码库中(一般我们都推荐在 Jenkins 中直接从源代码控制(SCM)中直接载入 Jenkinsfile Pipeline 这种方法)

## 1.2、搜索&&安装 Pipeline 相关插件

### 1.2.1、搜索&&安装 Pipeline 插件

&emsp;&emsp;Manage Jenkins->Plugins->Available plugins，搜索"Pipeline"

![搜索"Pipeline"](./pictures/devOps_jenkins02/img.png)

### 1.2.2、搜索&&安装 Pipeline Stage View 插件

&emsp;&emsp;Manage Jenkins->Plugins->Available plugins，搜索"Pipeline Stage View"  

&emsp;&emsp;Pipeline Stage View 插件用于**可视化**构建过程当中的不同阶段

![搜索"Pipeline Stage View"](./pictures/devOps_jenkins02/img_1.png)

&emsp;&emsp;安装完毕后，新建item 就可以看到 Pipeline 选项了

![验证pipeline是否安装成功](./pictures/devOps_jenkins02/img_2.png)

## 1.3、Pipeline 语法快速入门

&emsp;&emsp;创建配置测试项目 dev_project_pipeline_test (视各人情况)

![创建配置测试项目](./pictures/devOps_jenkins02/img_3.png)

> 选择HelloWorld模板

![选择HelloWorld模板](./pictures/devOps_jenkins02/img_4.png)

> 生成的内容如下：

```shell
pipeline {
    agent any
    stages {
        stage('Hello') {
            steps {
                echo 'Hello World'
            }
        }
    }
}
```

&emsp;&emsp;简要说明：
* stages：代表整个流水线的所有执行阶段。通常stages只有1个，里面包含多个stage
* stage：代表流水线中的某个阶段，可能出现n个。一般分为拉取代码，编译构建，部署等阶段。
* steps：代表一个阶段内需要执行的逻辑。steps里面是shell脚本，可以是git拉取代码，ssh远程发布等任意内容。

> 编写一个简单声明式(Declarative) Pipeline

```shell
pipeline {
   agent any
   stages {
       stage('拉取代码') {
           steps {
               echo '拉取代码'
           }
       } 
       stage('编译构建') {
           steps {
               echo '编译构建'
           }
       } 
       stage('项目部署') {
           steps {
               echo '项目部署'
           }
       }
   }
}
```

&emsp;&emsp;点击构建，可以看到整个构建过程

![阶段视图验证](./pictures/devOps_jenkins02/img_5.png)

> 编写一个脚本式(Scripted Pipeline) Pipeline

![脚本式(Scripted Pipeline) Pipeline](./pictures/devOps_jenkins02/img_6.png)

## 1.4、流水线语法的尝试

![流水线语法的入口位置](./pictures/devOps_jenkins02/img_7.png)

&emsp;&emsp;<span style="color:red">**下面以片段生成器生成代码作为示例**</span>

### 1.4.1、片段生成器之代码拉取

![片段生成器之拉取代码的尝试](./pictures/devOps_jenkins02/img_8.png)

> 配置git后，最后点击"生成流水线脚本"

![片段生成器之配置git](./pictures/devOps_jenkins02/img_9.png)

> 生成后的脚本代码如下

```shell
checkout scmGit(
branches: [[name: '*/master']], 
extensions: [], 
userRemoteConfigs: [[
  credentialsId: '6a5f7869-6ebf-4c21-a27a-51f80b9b0e13', 
  url: 'git@192.168.255.100:bruce_tender/project_war_test.git'
]])
```

> 将生成的代码粘贴到"流水线"处

```shell
pipeline {
    agent any

    stages {
        stage('拉取代码') {
            steps {
                checkout scmGit(
                branches: [[name: '*/master']], 
                extensions: [], 
                userRemoteConfigs: [[
                  credentialsId: '6a5f7869-6ebf-4c21-a27a-51f80b9b0e13', 
                  url: 'git@192.168.255.100:bruce_tender/project_war_test.git'
                ]])
            }
        }
    }
}
```

> 保存执行&&查看git拉取代码的结果

![保存执行&&查看结果](./pictures/devOps_jenkins02/img_10.png)

### 1.4.2、片段生成器之构建项目

![片段生成器之构建项目](./pictures/devOps_jenkins02/img_11.png)

> 生成后的脚本代码如下

```shell
sh 'mvn clean package'
```

> 将生成的代码追加到"流水线"处

```shell
pipeline {
    agent any

    stages {
        stage('拉取代码') {
            steps {
                checkout scmGit(
                branches: [[name: '*/master']], 
                extensions: [], 
                userRemoteConfigs: [[
                  credentialsId: '6a5f7869-6ebf-4c21-a27a-51f80b9b0e13', 
                  url: 'git@192.168.255.100:bruce_tender/project_war_test.git'
                ]])
            }
        }
        stage('构建项目') {
            steps {
                sh 'mvn clean package'
            }
        }
    }
}
```

> 保存执行&&查看git拉取代码、项目构建的结果

```shell
Started by user root
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/lib/jenkins/workspace/dev_project_pipeline_test
[Pipeline] {
[Pipeline] stage
[Pipeline] { (拉取代码)
[Pipeline] checkout
The recommended git tool is: NONE
using credential 6a5f7869-6ebf-4c21-a27a-51f80b9b0e13
 ... 省略 ...
Commit message: "feat - 第一次提交"
 > git rev-list --no-walk 0e567f1d2a7fbb6d73bca46ce79a984d30d7359e # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (构建项目)
[Pipeline] sh
+ mvn clean package
[INFO] Scanning for projects...
[INFO] 
[INFO] --------------------< com.tender:project_war_test >---------------------
[INFO] Building project_war_test Maven Webapp 1.0-SNAPSHOT
[INFO]   from pom.xml
[INFO] --------------------------------[ war ]---------------------------------
... 省略 ...
[INFO] Assembling webapp [project_war_test] in [/var/lib/jenkins/workspace/dev_project_pipeline_test/target/project_war_test]
[INFO] Processing war project
[INFO] Copying webapp resources [/var/lib/jenkins/workspace/dev_project_pipeline_test/src/main/webapp]
[INFO] Building war: /var/lib/jenkins/workspace/dev_project_pipeline_test/target/project_war_test.war
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.485 s
[INFO] Finished at: 2023-12-28T22:28:01-05:00
Finished: SUCCESS
```

### 1.4.3、片段生成器之发布项目

![片段生成器之发布项目](./pictures/devOps_jenkins02/img_12.png)

---

![远程tomcat的具体配置](./pictures/devOps_jenkins02/img_13.png)

> 生成后的脚本代码如下

```shell
deploy adapters: [
  tomcat8(
    credentialsId: '9fbd4fd5-19a7-45c2-a275-622fa4dfc6d1', 
    path: '', 
    url: 'http://192.168.255.102:8080'
  )
], 
contextPath: null, 
war: 'target/*.war'
```

> 将生成的代码追加到"流水线"处

```shell
pipeline {
    agent any

    stages {
        stage('拉取代码') {
            steps {
                checkout scmGit(
                branches: [[name: '*/master']], 
                extensions: [], 
                userRemoteConfigs: [[
                  credentialsId: '6a5f7869-6ebf-4c21-a27a-51f80b9b0e13', 
                  url: 'git@192.168.255.100:bruce_tender/project_war_test.git'
                ]])
            }
        }
        stage('构建项目') {
            steps {
                sh 'mvn clean package'
            }
        }
        stage('发布项目') {
            steps {
                deploy adapters: [
                  tomcat8(
                    credentialsId: '9fbd4fd5-19a7-45c2-a275-622fa4dfc6d1', 
                    path: '', 
                    url: 'http://192.168.255.102:8080'
                  )
                ], 
                contextPath: null, 
                war: 'target/*.war'
            }
        }
    }
}
```

> 保存执行&&查看git拉取代码、项目构建、项目发布的结果

```shell
Started by user root
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/lib/jenkins/workspace/dev_project_pipeline_test
[Pipeline] {
[Pipeline] stage
[Pipeline] { (拉取代码)
... 省略 ...
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (构建项目)
[Pipeline] sh
+ mvn clean package
... 省略 ...
[INFO] Copying webapp resources [/var/lib/jenkins/workspace/dev_project_pipeline_test/src/main/webapp]
[INFO] Building war: /var/lib/jenkins/workspace/dev_project_pipeline_test/target/project_war_test.war
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  1.378 s
[INFO] Finished at: 2023-12-28T22:38:10-05:00
[INFO] ------------------------------------------------------------------------
... 省略 ...
  Redeploying [/var/lib/jenkins/workspace/dev_project_pipeline_test/target/project_war_test.war]
  Undeploying [/var/lib/jenkins/workspace/dev_project_pipeline_test/target/project_war_test.war]
  Deploying [/var/lib/jenkins/workspace/dev_project_pipeline_test/target/project_war_test.war]
[Pipeline] End of Pipeline
Finished: SUCCESS
```

## 1.5、Pipeline Script from SCM 的应用尝试

&emsp;&emsp;之前我们都是直接在Jenkins的UI界面编写Pipeline代码，这样不方便脚本维护，<span style="color:red">建议把Pipeline脚本放在项目中</span>（一起进行版本控制）

## 1.5.1、项目中维护 Jenkinsfile 文件

&emsp;&emsp;在项目<span style="color:red">根目录建立</span> Jenkinsfile 文件，把Pipeline代码复制到该文件中，提交到Gitlab中。

![项目中维护 Jenkinsfile 文件](./pictures/devOps_jenkins02/img_14.png)

## 1.5.2、Jenkins中引用 Jenkinsfile 文件

![选择SCM](./pictures/devOps_jenkins02/img_15.png)

---

![选择SCM02](./pictures/devOps_jenkins02/img_16.png)

> 查看构建结果

&emsp;&emsp;结果也是ok！
