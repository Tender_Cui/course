# 参考文献

* [`Sentinel`流量守卫](https://www.cnblogs.com/euneirophran/p/18073905)
* [`Jmeter`官网下载](https://jmeter.apache.org/download_jmeter.cgi)

# 示例代码

&emsp;&emsp;[请参考项目：Tender_Cui/shiro](https://gitee.com/Tender_Cui/shiro) 下的模块：`shiro-framework-integration/shiro-sentinel`

# 1、`@SentinelResource`

&emsp;&emsp;`SentinelResource`是一个流量防卫防护组件<span style="color:red">注解</span>，用于指定防护资源，对配置的资源进行流量控制、熔断降级等功能。

## 1.1、按照`rest`地址限流+默认限流返回

&emsp;&emsp;可以理解为`rest`地址限流几乎和`@SentinelResource`没有任何关系。

1. 新增一个`controller`

```java
@RestController
public class RateLimitController2 {

    @GetMapping("/rateLimit/byUrl")
    public String byUrl() {
        return "按rest地址，限流测试ok";
    }
}
```

2. 流控配置

![rest地址限流配置](./pictures/springboot-Sentinel-references-02/img.png)

3. 测试结果

&emsp;&emsp;会返回`Sentinel`自带的限流处理结果，默认。

![测试结果](./pictures/springboot-Sentinel-references-02/img_1.png)

## 1.2、按照`SentinelResource`资源名称限流+默认限流返回

&emsp;&emsp;不想用默认的限流提示(Blocked by Sentinel (flow limiting))，想<span style="color:red">返回自定义的限流提示</span>。

1. `java`代码

```java
package com.tender.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RateLimitController2 {

    // 省略其余代码
    
    @GetMapping("/rateLimit/byResource")
    @SentinelResource(value = "byResource", blockHandler = "byResourceHandler")
    public String byResource() {
        return "按照资源名称 SentinelResource 限流测试：O(∩_∩)O";
    }

    /**
     * 如果有入参，照抄，最后面加上 {@link BlockException}
     * @param blockException
     * @return
     */
    public String byResourceHandler(BlockException blockException) {
        return "服务不可用，触发了@SentinelResource启动：/(ㄒoㄒ)/~~";
    }
}
```

2. 流控配置

![流控配置](./pictures/springboot-Sentinel-references-02/img_2.png)

3. 测试结果

![测试结果](./pictures/springboot-Sentinel-references-02/img_3.png)


## 1.3、按照`SentinelResource`资源名称限流+自定义限流返回+服务降级处理

&emsp;&emsp;按照`SentinelResource`配置，点击超过限流配置返回自定义限流提示+<span style="color:red">程序异常返回`fallback`服务降级</span>。

1. `java`代码

```java
@RestController
public class RateLimitController2 {
    
    // 省略其余java 代码

    @GetMapping("/rateLimit/byFallback/{i}")
    @SentinelResource(value = "byFallback", blockHandler = "byBlockHandler", fallback = "byFallback")
    public String rateLimitByFallback(@PathVariable("i") Integer i) {
        if (i == 0) {
            throw new RuntimeException("i == 0 异常");
        }
        return "使用注解并使用, Fallback, 服务可用：O(∩_∩)O";
    }

    public String byBlockHandler(@PathVariable("i") Integer i, BlockException blockException) {
        System.out.println("配置了自定义限流, " + blockException.getMessage());
        return "服务不可用：(；′⌒`), 这是自定义返回的字符串";
    }

    public String byFallback(@PathVariable("i") Integer i, Throwable throwable) {
        System.out.println("程序逻辑异常, " + throwable.getMessage());
        return "逻辑异常：/(ㄒoㄒ)/~~, 这是自定义返回的字符串";
    }
    
}
```

2. 流控配置

![流控配置](./pictures/springboot-Sentinel-references-02/img_4.png)

3. 测试结果

&emsp;&emsp;配合上述`Sentinel`设定的流控配置，疯狂点击，返回了自定义的限流处理信息，限流发生。

![测试结果01](./pictures/springboot-Sentinel-references-02/img_5.png)

&emsp;&emsp;入参为0，<span style="color:red">异常发生，返回了自定义的服务降级处理</span>。

![测试结果02](./pictures/springboot-Sentinel-references-02/img_6.png)

> 小结

* **byBlockHandler**：主要针对`Sentinel`配置后出现的违规情况处理
* **fallback**：程序异常了`JVM`抛出的异常服务降级处理
* 两者可以同时共存

# 2、热点规则案例配置

## 2.1、何为热点

&emsp;&emsp;[官网对"热点参数限流"的解释](https://github.com/alibaba/Sentinel/wiki/%E7%83%AD%E7%82%B9%E5%8F%82%E6%95%B0%E9%99%90%E6%B5%81)

![官网图示](./pictures/springboot-Sentinel-references-02/img_7.png)

&emsp;&emsp;Sentinel 利用 `LRU` 策略统计最近最常访问的热点参数，结合令牌桶算法来进行参数级别的流控。热点参数限流支持集群模式。

&emsp;&emsp;热点即经常访问的数据，很多时候我们希望统计或者限制某个热点数据中访问频次最高的`TopN`数据，并对其访问进行限流或者其它操作。

## 2.2、新增热点代码

```java
@RestController
public class RateLimitController2 {
    
    // 省略其余代码

    @GetMapping("/testHotKey")
    @SentinelResource(value = "testHotKey", blockHandler = "testHotKeyBlockHandler")
    public String testHotKey(@RequestParam(value = "p1", required = false) String p1,
                             @RequestParam(value = "p2", required = false) String p2) {
        return "testHotKey 正常访问 O(∩_∩)O ...";
    }

    public String testHotKeyBlockHandler(@RequestParam(value = "p1", required = false) String p1,
                                         @RequestParam(value = "p2", required = false) String p2, BlockException blockException) {
        return "testHotKey 被限流了 /(ㄒoㄒ)/~~ ...";
    }
}
```

## 2.3、热点限流配置

### 2.3.1、普通正常限流

> 需求：

&emsp;&emsp;如果第一个参数：`p1`有值的话，`QPS`设为 `1`，注意，参数下边位置从`0`开始。再被限流后，会调用方法：`testHotKeyBlockHandler`。

![热点限流配置](./pictures/springboot-Sentinel-references-02/img_8.png)

![代码与规则对照表](./pictures/springboot-Sentinel-references-02/img_9.png)

> 结果测试：

```html
1. http://localhost:8401/testHotKey            # 随便怎么点，都ok
     
2. http://localhost:8401/testHotKey?p1=1       # QPS 超过1 就限流
     
3. http://localhost:8401/testHotKey?p2=1       # 随便怎么点，都ok

4. http://localhost:8401/testHotKey?p1=1&p2=2  # QPS 超过1 就限流
```

&emsp;&emsp;只要含有参数`p1`，且`QPS`超过 `1`，就会触发`Sentinel`限流操作。

### 2.3.2、例外特殊限流

> 需求：

&emsp;&emsp;我们期望参数：`p1`当它是某个特殊值时，到达某个约定值后`【普通正常限流】`就失效了，它的限流值和平时不一样。

&emsp;&emsp;例如：`p1=5`的时候，`QPS`设置为`200`

![例外特殊限流配置](./pictures/springboot-Sentinel-references-02/img_10.png)

> 结果测试：

```html
1. http://localhost:8401/testHotKey?p1=5&p2=2     # QPS 超过200 就限流

```

# 3、`Sentinel`授权规则

## 3.1、概述

&emsp;&emsp;在某些场景下，需要根据调用接口的来源判断是否允许执行本次请求。此时就可以使用`Sentinel`提供的授权规则来实现，`Sentinel`的授权规则能够根据请求的来源，来判断是否允许本次请求通过。

&emsp;&emsp;在`Sentinel`的授权规则钟，<span style="color:red">提供了白名单和黑名单两种授权类型</span>。

&emsp;&emsp;[官方参看手册](https://github.com/alibaba/Sentinel/wiki/%E9%BB%91%E7%99%BD%E5%90%8D%E5%8D%95%E6%8E%A7%E5%88%B6)

    调用方信息通过 ContextUtil.enter(resourceName, origin) 方法中的 origin 参数传入。

## 3.2、`controller`代码

```java
package com.tender.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class EmpowerController {
    @GetMapping("/empower")
    public String requestSentinel() {
        log.info("授权规则");
        return "授权规则";
    }
}
```

## 3.3、`RequestOriginParser`接口实现

```java
package com.tender.parser;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
// import jakarta.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class CustomRequestOriginParser implements RequestOriginParser {
    @Override
    public String parseOrigin(HttpServletRequest httpServletRequest) {
        return httpServletRequest.getParameter("serverName");
    }
}
```

## 3.4、授权配置

![授权配置](./pictures/springboot-Sentinel-references-02/img_11.png)

## 3.5、测试结果

```html
1. http://localhost:8401/empower?serverName=test   # 结果：Blocked by Sentinel (flow limiting)

2. http://localhost:8401/empower?serverName=test2   # 结果：Blocked by Sentinel (flow limiting)

3. http://localhost:8401/empower?serverName=test3   # 结果：正常通过

```

# 4、`Sentinel`持久化规则

## 4.1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-sentinel</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!-- sentinel 规则持久化到 nacos start -->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-datasource-nacos</artifactId>
        </dependency>
        <!-- sentinel 规则持久化到 nacos end -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- 常用工具 -->
        <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
        </dependency>
    </dependencies>

</project>
```

## 4.2、yaml 配置

```yaml
server:
  port: 8401

spring:
  application:
    name: shiro-sentinel

  cloud:
    sentinel:
      transport:
        dashboard: 192.168.255.100:8080
        port: 8719 # 默认端口，加入端口被占用，会自动从 8719 开始 +1 尝试，直到找到未被占用的端口。
      web-context-unify: false # controller 层的方法对 service 层的调用，不认为是同一个根链路。
      # 本次规则持久化新增 ------ start ------
      datasource:
        ds1:   # 自定义的key，可以配置多个。
          nacos:
            server-addr: ${spring.cloud.nacos.config.server-addr}
            namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39  # 指定哪个命名空间下,默认 public
            data-id: ${spring.application.name}
            group-id: DEFAULT_GROUP
            data-type: json
            rule-type: flow # com.alibaba.cloud.sentinel.datasource.RuleType (flow代表 流控 )
      # 本次规则持久化新增 ------ end ------
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP

```

&emsp;&emsp;枚举类：``

```java
public enum RuleType {

	/**
	 * flow.   流量控制规则
	 */
	FLOW("flow", FlowRule.class),
	/**
	 * degrade.  熔断降级规则
	 */
	DEGRADE("degrade", DegradeRule.class),
	/**
	 * param flow.  热点规则
	 */
	PARAM_FLOW("param-flow", ParamFlowRule.class),
	/**
	 * system.      系统保护规则
	 */
	SYSTEM("system", SystemRule.class),
	/**
	 * authority.   访问控制规则
	 */
	AUTHORITY("authority", AuthorityRule.class),
	/**
	 * gateway flow.
	 */
	GW_FLOW("gw-flow",
			"com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule"),
	/**
	 * api.
	 */
	GW_API_GROUP("gw-api-group",
			"com.alibaba.csp.sentinel.adapter.gateway.common.api.ApiDefinition");

```

## 4.3、nacos 配置

![nacos 配置](./pictures/springboot-Sentinel-references-02/img_12.png)

```json
[
    {
        "resource": "/rateLimit/byUrl",
        "limitApp": "default",
        "grade": 1,
        "count": 1,
        "strategy": 0,
        "controlBehavior": 0,
        "clusterMode": false
    }
]
```
  
> 词汇解释：

* resource：资源名称
* limitApp：来源应用
* grade：阈值类型，0 -> 线程数；1 -> QPS
* count：单机阈值
* strategy：流控模式，0 -> 直接；1 -> 关联；2 -> 链路
* controlBehavior：流控效果，0 -> 快速失败；1 -> Warm Up；2 -> 排队等待
* clusterMode：是否集群










