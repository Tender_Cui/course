# 参考文献

* [Anaconda官方下载地址](https://www.anaconda.com/download/success)

# 一、下载

&emsp;&emsp;省略

# 二、安装

&emsp;&emsp;省略

# 三、环境配置

```shell
D:\A_Tender_workspace\install_4_code\Conda\install
D:\A_Tender_workspace\install_4_code\Conda\install\Library\bin
D:\A_Tender_workspace\install_4_code\Conda\install\Scripts
```

# 四、验证

```shell
# 查看版本
conda --version

# 结果
conda 24.11.3
```

# 五、添加镜像仓库

&emsp;&emsp;由于 `anaconda` 的仓库在国外，这里先添加清华的镜像仓库

&emsp;&emsp;你可以通过开始菜单搜索 <span style="color:red">Anaconda Prompt</span> 并打开它，后续的操作都在这个命令行工具中完成。

```shell
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --set show_channel_urls yes
```

## 5.1、解释

&emsp;&emsp;上述命令的作用分别为：

* `conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/` ：将清华大学的免费软件包镜像源添加到 `conda` 的搜索路径中。   
* `conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/` ：将清华大学的主要软件包镜像源添加到 `conda` 的搜索路径中。
* `conda config --set show_channel_urls yes` ：设置在安装包时显示镜像源的 `URL`，方便确认是否使用了指定的镜像源。

## 5.2、验证配置

&emsp;&emsp;通过以下命令查看当前的配置信息：

```shell
conda config --show channels
```

## 5.3、恢复默认镜像源

&emsp;&emsp;如果你想恢复到默认的镜像源，可以使用以下命令：

```shell
conda config --remove-key channels
```


