# 记录常用的工具类

# 1、加载`META-INF/spring.factories`资源

```java

/**
 * @see org.springframework.core.io.support.SpringFactoriesLoader.loadSpringFactories
 */

try {
    Enumeration<URL> urls = classLoader.getResources("META-INF/spring.factories");
    while (urls.hasMoreElements()) {
        URL url = urls.nextElement();
        UrlResource resource = new UrlResource(url);
        Properties properties = PropertiesLoaderUtils.loadProperties(resource);
        for (Map.Entry<?, ?> entry : properties.entrySet()) {
            String factoryTypeName = ((String) entry.getKey()).trim();
            String[] factoryImplementationNames =
                    StringUtils.commaDelimitedListToStringArray((String) entry.getValue());
            for (String factoryImplementationName : factoryImplementationNames) {
                result.computeIfAbsent(factoryTypeName, key -> new ArrayList<>())
                        .add(factoryImplementationName.trim());
            }
        }
    }

    // Replace all lists with unmodifiable lists containing unique elements
    result.replaceAll((factoryType, implementations) -> implementations.stream().distinct()
            .collect(Collectors.collectingAndThen(Collectors.toList(), Collections::unmodifiableList)));
    cache.put(classLoader, result);
}
catch (IOException ex) {
    throw new IllegalArgumentException("Unable to load factories from location [" +
            FACTORIES_RESOURCE_LOCATION + "]", ex);
}
```

> 关联API 

```java
// 1、方法一：

default void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
    Objects.requireNonNull(function);
    for (Map.Entry<K, V> entry : entrySet()) {
        K k;
        V v;
        try {
            k = entry.getKey();
            v = entry.getValue();
        } catch(IllegalStateException ise) {
            // this usually means the entry is no longer in the map.
            throw new ConcurrentModificationException(ise);
        }

        // ise thrown from function is not a cme.
        v = function.apply(k, v);

        try {
            entry.setValue(v);
        } catch(IllegalStateException ise) {
            // this usually means the entry is no longer in the map.
            throw new ConcurrentModificationException(ise);
        }
    }
}

// 2、方法二：

default V getOrDefault(Object key, V defaultValue) {
    V v;
    return (((v = get(key)) != null) || containsKey(key))
        ? v
        : defaultValue;
}


// 3、方法三：
        
V putIfAbsent(K key, V value);

// 等价于
if (!map.containsKey(key))    
    return map.put(key, value);
else    return map.get(key);

```

# 2、读取`Class`上的注解

&emsp;&emsp;比如如下方式，来获取注解：`@ComponentScan`

```java
ComponentScan componentScan = AnnotationUtils.findAnnotation(Config.class, ComponentScan.class);
```

&emsp;&emsp;是否有某个`注解`、是否有`派生注解`的<span style="color:yellow">区别</span>

* annotationMetadata.hasAnnotation(Component.class.getName()): 是否<span style="color:red">直接</span>拥有某个注解
* annotationMetadata.hasMetaAnnotation(Component.class.getName()): 是否<span style="color:red">间接</span>拥有某个注解
* annotationMetadata.isAnnotated(Component.class.getName()): 是上面两个 API 的<span style="color:red">整合</span>。


```java

public class ComponentScanPostProcessor implements BeanDefinitionRegistryPostProcessor {
    /**
     * 调用 context.refresh() 方法时回调
     */
    @Override
    @SneakyThrows
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        ComponentScan componentScan = AnnotationUtils.findAnnotation(Config.class, ComponentScan.class);
        if (componentScan != null) {
            for (String packageName : componentScan.basePackages()) {
                System.out.println(packageName);
                // indi.mofan.bean.a05.component -> classpath*:indi/mofan/bean/a05/component/**/**.class
                String path = "classpath*:" + packageName.replace(".", "/") + "/**/**.class";
                //  Resource[] resources = context.getResources(path);
                Resource[] resources = new PathMatchingResourcePatternResolver().getResources(path);
                CachingMetadataReaderFactory factory = new CachingMetadataReaderFactory();
                AnnotationBeanNameGenerator generator = new AnnotationBeanNameGenerator();
                for (Resource resource : resources) {
                    MetadataReader reader = factory.getMetadataReader(resource);
                    AnnotationMetadata annotationMetadata = reader.getAnnotationMetadata();
//                    System.out.println("类名: " + reader.getClassMetadata().getClassName());
//                    System.out.println("是否加了 @Component: " + annotationMetadata.hasAnnotation(Component.class.getName()));
//                    System.out.println("是否加了 @Component 派生: " + annotationMetadata.hasMetaAnnotation(Component.class.getName()));
                    if (annotationMetadata.hasAnnotation(Component.class.getName())
                            || annotationMetadata.hasMetaAnnotation(Component.class.getName())) {
                        AbstractBeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition(reader.getClassMetadata().getClassName())
                                .getBeanDefinition();
                        String name = generator.generateBeanName(beanDefinition, registry);
                        registry.registerBeanDefinition(name, beanDefinition);
                    }
                }
            }
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
```
