# 参考文献
* [maven教程-常用插件介绍](https://zhuanlan.zhihu.com/p/528387287)
* [mybatis-maven-插件4逆向工程](https://blog.csdn.net/c1523456/article/details/122500177)
* [2023最新国内maven仓库镜像地址](https://blog.csdn.net/qq_38217990/article/details/129257106)

# 一、maven 常用设置

## 1.1、jdk 1.8设置

> 方法一：

```shell
<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
```

> 方法二：

```shell
<build>
    <plugins>
        <!-- 设定jdk版本为1.8 -->
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <!--  <version>3.10.1</version> -->
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## 1.2、maven 依赖多个父工程

```shell

<!-- 依赖声明 -->
<dependencyManagement>
    <dependencies>
        <!-- spring-boot-starter-parent 的依赖配置-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>2.7.17</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

## 1.3、maven 镜像源配置（eg:aliyun、mvnrepository等）

```shell
<mirrors>
    <mirror>
        <id>nexus-aliyun</id>
        <mirrorOf>central</mirrorOf>
        <name>Nexus aliyun</name>
        <url>http://maven.aliyun.com/nexus/content/groups/public</url>
    </mirror>
    <mirror>
		<id>mvnrepository</id>
		<mirrorOf>mvnrepository</mirrorOf>
		<url>http://mvnrepository.com/</url>
	</mirror>
</mirrors>
```

```shell
<mirrors>
  <mirror>
    <id>aliyun</id>
    <url>http://maven.aliyun.com/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>central</id>
    <url>https://repo1.maven.org/maven2/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>netease</id>
    <url>http://maven.netease.com/repository/public/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>huaweicloud</id>
    <url>https://repo.huaweicloud.com/repository/maven/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>tencent</id>
    <url>https://mirrors.cloud.tencent.com/repository/maven/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>ustc</id>
    <url>http://mirrors.ustc.edu.cn/maven/maven2/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>nju</id>
    <url>http://maven.nju.edu.cn/repository/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>tsinghua</id>
    <url>https://repo.maven.apache.org/maven2/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>bit</id>
    <url>http://mirror.bit.edu.cn/maven/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>neusoft</id>
    <url>https://mirrors.neusoft.edu.cn/maven2/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>opencas</id>
    <url>http://maven.opencas.cn/maven/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
  <mirror>
    <id>bjtu</id>
    <url>http://maven.bjtu.edu.cn/maven2/</url>
    <mirrorOf>central</mirrorOf>
  </mirror>
</mirrors>
```

## 1.4、maven 清除无效缓存

> windows中创建一个.bat 文件，具体内容如下：需要修改**REPOSITORY_PATH**

```shell


rem maven仓库路径
set REPOSITORY_PATH=C:\tender_work_space\maven_repository
rem 正在搜索...
for /f "delims=" %%i in ('dir /b /s "%REPOSITORY_PATH%\*lastUpdated*"') do (
    del /s /q %%i
)
rem 搜索完毕
pause 
```

## 1.4、springboot 中的maven 打包插件

```shell
# SpringBoot微服务项目打包必须导入该插件

<plugin>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-maven-plugin</artifactId>
</plugin>

# 有时候会有如下问题！则解决办法如下：
<plugin>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-maven-plugin</artifactId>
	<version>2.3.7.RELEASE</version>
    <configuration>
        <mainClass>主类的路径，如：com.xxx.Application.Application</mainClass>
        <excludes>
            <exclude>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
            </exclude>
        </excludes>
    </configuration>
    <executions>
        <execution>
            <id>repackage</id>
            <goals>
                <goal>repackage</goal>
            </goals>
        </execution>
    </executions>
</plugin>

```

## 1.5、maven 安装包到本地仓库

```shell
# 到 jar 包所在的路径，执行如下命令即可
mvn install:install-file -Dfile=/path/to/jar/your-jar-file.jar -DgroupId=com.example -DartifactId=your-artifact-id -Dversion=1.0 -Dpackaging=jar
```

## 1.6、maven 跳过测试
```shell
-Dmaven.test.skip=true
```

## 1.7、mvn -f 与多模块项目

&emsp;&emsp;在Maven的多模块项目中，每个子模块都有自己的pom.xml文件。如果要执行这些子模块中的某个模块，我们就需要使用 mvn -f 命令来指定当前要编译、打包的子模块。
比如，在一个多模块Maven项目中，有2个子模块：module1和module2。我们想要执行module2的clean操作，可以使用以下命令：

```shell
mvn -f module2/pom.xml clean
```

> 这个时候，我们就将当前的pom.xml文件指定为module2的pom.xml，然后执行了clean操作。

## 1.8、mvn dockerfile:build

&emsp;&emsp;利用dockerfile-maven-plugin插件构建Docker镜像，他可以通过读取 Dockerfile 然后构建出docker镜像

> 参考文献

* [dockerfile插件构建镜像&&发布到Harbor](https://blog.51cto.com/u_16175516/8583332)
* [dockerfile插件极简教程](https://www.cnblogs.com/Naylor/p/13803532.html)

```shell
mvn  clean package dockerfile:build
```

```shell
<!-- https://mvnrepository.com/artifact/com.spotify/dockerfile-maven-plugin -->
<plugin>
    <groupId>com.spotify</groupId>
    <artifactId>dockerfile-maven-plugin</artifactId>
    <version>1.3.6</version>
    <configuration>
        <repository>${project.artifactId}</repository>
        <buildArgs>
            <JAR_FILE>target/${project.build.finalName}.jar</JAR_FILE>
        </buildArgs>
    </configuration>
</plugin>

<!-- pluginGroups 这玩意儿在 maven 的pom.xml 中 -->
<pluginGroups>  
    <pluginGroup>com.spotify</pluginGroup>  
</pluginGroups>
```

&emsp;&emsp;构建镜像

```shell
mvn -f course-test/pom.xml clean package dockerfile:build
```












