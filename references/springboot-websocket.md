# 参考文献

* [SpringBoot 整合 WebSocket](https://blog.csdn.net/H_Sino/article/details/138991758)
* [WebSocket在高并发时候的问题](https://blog.csdn.net/abu935009066/article/details/131218149)
* [RocketMQ广播时候的本地 rocketmq_offset 文件位置的自定义](https://juejin.cn/post/7133511198284185636)

# 示例代码

* [请参考示例代码](https://gitee.com/Tender_Cui/springboot-component/tree/master/component-integration/component-websocket)

# 1、后端`WebSocket`的实现

&emsp;&emsp;采用 `Spring` 封装的 `WebSocket` 实现方案。

## 1.1、`pom.xml`引用

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-websocket</artifactId>
</dependency>
```

## 1.2、业务抽象接口

### 1.2.1、`WebSocketService`接口

```java

/**
 * WebSocket 服务接口
 */
public interface WebSocketService {

    /**
     * 会话开始回调
     *
     * @param session 会话
     */
    void handleOpen(WebSocketSession session);

    /**
     * 会话结束回调
     *
     * @param session 会话
     */
    void handleClose(WebSocketSession session);

    /**
     * 处理消息
     *
     * @param session 会话
     * @param message 接收的消息
     */
    void handleMessage(WebSocketSession session, String message);

    /**
     * 发送消息
     *
     * @param session 当前会话
     * @param message 要发送的消息
     * @throws IOException 发送io异常
     */
    void sendMessage(WebSocketSession session, String message) throws IOException;

    /**
     * 发送消息
     *
     * @param userId  用户id
     * @param message 要发送的消息
     * @throws IOException 发送io异常
     */
    void sendMessage(String userId, TextMessage message) throws IOException;

    /**
     * 发送消息
     *
     * @param userId  用户id
     * @param message 要发送的消息
     * @throws IOException 发送io异常
     */
    void sendMessage(String userId, String message) throws IOException;

    /**
     * 发送消息
     *
     * @param session 当前会话
     * @param message 要发送的消息
     * @throws IOException 发送io异常
     */
    void sendMessage(WebSocketSession session, TextMessage message) throws IOException;

    /**
     * 广播
     *
     * @param message 字符串消息
     * @throws IOException 异常
     */
    void broadCast(String message) throws IOException;

    /**
     * 广播
     *
     * @param message 文本消息
     * @throws IOException 异常
     */
    void broadCast(TextMessage message) throws IOException;

    /**
     * 处理会话异常
     *
     * @param session 会话
     * @param error   异常
     */
    void handleError(WebSocketSession session, Throwable error);

}
```

### 1.2.1、`WebSocketService`接口实现类

```java
package com.tender.service.impl;

import com.tender.service.WebSocketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import com.tender.common.Constants;

import java.io.IOException;
import java.util.Map;
import com.tender.utils.RedisUtils;
import java.util.concurrent.ConcurrentHashMap;
import com.tender.domain.ClientUserInfo;
import org.springframework.web.socket.handler.ConcurrentWebSocketSessionDecorator;

/**
 * 【消息中心】：直接查【消息中心分页接口】，【已读】、【未读】则利用用户消息表来实现（用户已读消息，则user_id 和 msg_id 绑定入库）
 * 【审核通过消息】：则利用 WebSocket 来推送
 *      1、审核通过后，如果用户在线，则直接推送消息给前端
 *      2、如果用户离线，则消息入待发送表，（建立连接的时候，用户状态存入redis中，断开连接的时候，清除用户状态）
 *          2.1、当有新用户连线，就尝试发送消息到该用户，利用 WebSocket 集群特性。
 *          2.2、定时扫描历史信息，来尝试发送给用户。（防止一直没人登录）
 */
@Slf4j
public class WebSocketServiceImpl implements WebSocketService {

    @Autowired
    private RedisUtils redisUtils;

    private final Map<String, WebSocketSession> clients = new ConcurrentHashMap<>();

    @Override
    public void handleOpen(WebSocketSession session) {
        // 这个时候就需要在建立 webSocket 时存储的 用户信息了
        Map<String, Object> attributes = session.getAttributes();
        ClientUserInfo clientUserInfo = (ClientUserInfo) attributes.get(Constants.CLIENT_USER_INFO_KEY);
        // 用户id 和 session 关联起来
        // ConcurrentWebSocketSessionDecorator 线程安全的
        clients.put(clientUserInfo.getUserId(), new ConcurrentWebSocketSessionDecorator(session, 10 * 1000, 64000));
        // 记录用户状态到 redis 中
        redisUtils.set(Constants.USER_WEBSOCKET_CONNECT_KEY_PREFIX + Constants.REDIS_JOIN + clientUserInfo.getUserId(), true, Constants.USER_WEBSOCKET_CNT_EXPIRE);
        log.info("a new connection opened，current online count：{}", clients.size());
    }

    @Override
    public void handleClose(WebSocketSession session) {
        // 这个时候就需要 清除掉该用户 userId 对应的 session 了
        Map<String, Object> attributes = session.getAttributes();
        ClientUserInfo clientUserInfo = (ClientUserInfo) attributes.get(Constants.CLIENT_USER_INFO_KEY);
        clients.remove(clientUserInfo.getUserId());
        redisUtils.delete(Constants.USER_WEBSOCKET_CONNECT_KEY_PREFIX + Constants.REDIS_JOIN + clientUserInfo.getUserId());
        log.info("a new connection closed，current online count：{}", clients.size());
    }

    @Override
    public void handleMessage(WebSocketSession session, String message) {
        // 处理前端传来的文本消息
        Map<String, Object> attributes = session.getAttributes();
        ClientUserInfo clientUserInfo = (ClientUserInfo) attributes.get(Constants.CLIENT_USER_INFO_KEY);
        // redis 中用户状态续命
        redisUtils.set(Constants.USER_WEBSOCKET_CONNECT_KEY_PREFIX + Constants.REDIS_JOIN + clientUserInfo.getUserId(), true, Constants.USER_WEBSOCKET_CNT_EXPIRE);
        // TODO 简单打印
        log.info("received a message：{}", message);
    }

    @Override
    public void sendMessage(WebSocketSession session, String message) throws IOException {
        this.sendMessage(session, new TextMessage(message));
    }

    @Override
    public void sendMessage(String userId, TextMessage message) throws IOException {
        WebSocketSession webSocketSession = clients.get(userId);
        // 考虑 WebSocketSession 集群情况
        if (null != webSocketSession) {
            if (webSocketSession.isOpen()) {
                webSocketSession.sendMessage(message);
            }
        }
    }

    @Override
    public void sendMessage(String userId, String message) throws IOException {
        this.sendMessage(userId, new TextMessage(message));
    }

    @Override
    public void sendMessage(WebSocketSession session, TextMessage message) throws IOException {
        session.sendMessage(message);
    }

    @Override
    public void broadCast(String message) throws IOException {
        clients.values().forEach(session -> {
            if (session.isOpen()) {
                try {
                    session.sendMessage(new TextMessage(message));
                } catch (IOException e) {
                    // TODO 异常自定义
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void broadCast(TextMessage message) throws IOException {
        clients.values().forEach(session -> {
            if (session.isOpen()) {
                try {
                    session.sendMessage(message);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void handleError(WebSocketSession session, Throwable error) {
        // TODO 错误异常自定义处理
        Map<String, Object> attributes = session.getAttributes();
        ClientUserInfo clientUserInfo = (ClientUserInfo) attributes.get(Constants.CLIENT_USER_INFO_KEY);
        try {
            session.close();
        } catch (IOException e) {
            redisUtils.delete(Constants.USER_WEBSOCKET_CONNECT_KEY_PREFIX + Constants.REDIS_JOIN + clientUserInfo.getUserId());
        }
        log.error("websocket error：{}，session id：{}", error.getMessage(), session.getId());
        log.error("", error);
    }
}

```

## 1.3、`WebSocketHandler`接口实现类

```java
package com.tender.websocket.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import com.tender.service.WebSocketService;
import org.springframework.web.socket.TextMessage;

public class DefaultWebSocketHandler implements WebSocketHandler {

    @Autowired
    private WebSocketService webSocketService;

    /**
     * 连接建立后
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        webSocketService.handleOpen(session);
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            webSocketService.handleMessage(session, textMessage.getPayload());
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        webSocketService.handleError(session, exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
        webSocketService.handleClose(session);
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

}
```

## 1.4、`HandshakeInterceptor`拦截接口实现类

```java
package com.tender.websocket.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import com.tender.domain.ClientUserInfo;
import com.tender.utils.JwtUtils;
import com.tender.common.Constants;

import java.util.Map;

@Slf4j
public class WebSocketInterceptor implements HandshakeInterceptor {

    /**
     * 建立请求之前，可以用来做权限判断
     *
     * @param request    the current request
     * @param response   the current response
     * @param wsHandler  the target WebSocket handler
     * @param attributes the attributes from the HTTP handshake to associate with the WebSocket
     *                   session; the provided attributes are copied, the original map is not used.
     * @return
     * @throws Exception
     */
    @Override
    public boolean beforeHandshake(ServerHttpRequest request,
                                   ServerHttpResponse response,
                                   WebSocketHandler wsHandler,
                                   Map<String, Object> attributes) throws Exception {
        if (request instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest servletServerHttpRequest = (ServletServerHttpRequest) request;
            // 模拟用户（通常利用JWT令牌解析用户信息）
            // 获取 ? 后面的参数 token
            String token = servletServerHttpRequest.getServletRequest().getParameter("token");
            try {
                ClientUserInfo clientUserInfo = JwtUtils.getClientUserInfo(token);
                // 设置当前这个session的属性，后续我们在发送消息时，可以通过 session.getAttributes().get("clientUserInfo")可以取出 clientUserInfo参数
                attributes.put(Constants.CLIENT_USER_INFO_KEY, clientUserInfo);
            } catch (Exception e) {
                log.error("webSocket 请求 token:{} ", token);
                log.error("webSocket 认证失败 ", e);
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * 建立请求之后
     *
     * @param request   the current request
     * @param response  the current response
     * @param wsHandler the target WebSocket handler
     * @param exception an exception raised during the handshake, or {@code null} if none
     */
    @Override
    public void afterHandshake(ServerHttpRequest request,
                               ServerHttpResponse response,
                               WebSocketHandler wsHandler,
                               Exception exception) {

    }

}
```

# 2、前端`WebSocket`的实现

```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Examples</title>
  <meta name='viewport' content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no' />
  <meta name="description" content="">
  <meta name="keywords" content="">
  <link href="" rel="stylesheet">
  <style>
    .wrap {
      display: flex;
      margin: 10px;
    }

    .text {
      flex: 1
    }
  </style>
</head>

<body>
  <div onclick="clickme()">点击我</div>
  <script>
    const clickme = () => {
      console.log("====");
      <!-- 通过参数传递 token -->
      const ws = new WebSocket("ws://10.220.5.226:8086/ws/msg?token=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIwNzYyZjhlYS1jNDE4LTQ2ZGUtOGZkNi1kMjVkYTk1Mzk2NmYiLCJ1c2VyX25hbWUiOiJsdWN5IiwiaWF0IjoxNzI0OTAxMTUxLCJhZ2UiOjEwLCJqdGkiOiI4ODg4ODg4OCJ9.sGv6rglWbNsaAuZvcqsD7yy_OANVgYe7ZNGOPQ_j4Uk");
      ws.onopen = () => {
        console.log("onopen");
      }
      ws.onmessage = (data) => {
        console.log(data.data);
      }
      ws.onerror = () => {
        console.log("onerror");
      }
    }

  </script>
</body>

</html>
```

# 3、`WebSocket`进阶

&emsp;&emsp;`WebSocket`肯定是需要集群部署的，在这种情况下，和用户关联的`WebSocketSession`不可能都在一个`JVM`里面，
这种情况下，如何给指定用户推送消息？

&emsp;&emsp;如果某个用户的审批流走完了，如何给该用户推送前端数据，得要区分用户是否在线，如果在线，就给指定用户推送数据，
用户如果不在线，需要把数据落库，待该用户上线后，重新推送消息

&emsp;&emsp;如何统一维护用户的登录状态

## 3.1、解决方案

* 集群化部署`WebSocket`可以考虑接入`Nacos`
* 给指定用户推送数据，可以借助于`RocketMQ`的广播消息
* 统一维护用户的登录状态，可以借助于`Redis`

