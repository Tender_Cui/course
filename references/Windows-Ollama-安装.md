# 参考文献

* [ollama的官网](https://ollama.com/) 
* [ollama的官网下载地址](https://ollama.com/download) 

* [Chatbox AI客户端](https://chatboxai.app/zh)

# 一、下载

&emsp;&emsp;根据 `OS` 来选择下载 `ollama`

# 二、安装

&emsp;&emsp; windows 默认安装路径为：`C:\Users\bruce\AppData\Local\Programs\Ollama`

# 三、测试

```shell
ollama --verison
```

&emsp;&emsp;结果：

```shell
ollama version is 0.5.12
```

&emsp;&emsp;浏览器访问

```shell
http://localhost:11434/
```

# 四、<span style="color:red">运行</span>或者<span style="color:green">下载</span>模型

&emsp;&emsp;先去 [ollama上搜索](https://ollama.com/search) 模型，比如：`deepseek-r1` 的 `1.5b` 模型。

![搜索模型](./pictures/Windows-ollama-安装/img.png)

> `b` 就是 `billion` 【10亿】级别的参数

```shell
ollama run deepseek-r1:1.5b
```

# 五、发送消息

&emsp;&emsp;本地模型下载并启动后，或者启动后，可以发送消息来交互了。

```shell
>>> 1 加 1 等于几
<think>

</think>

“1 加 1 等于几？”这是一个非常简单的数学问题。在这道题目中，我们通常会将其视为“2”。这是因为根据基本的算术原理，
1 + 1 = 2。

然而，这个答案可能并不是用户真正想要的答案，或者他们对这个问题的理解可能有其他角度。如果需要更深层次的解释或不同的理
解方式，请提供更多背景信息，以便我能够更好地帮助您解答。
```

<hr>

# 六、交互体验问题

&emsp;&emsp;选择 `ChatBox`

*[Chatbox AI客户端](https://chatboxai.app/zh)

&emsp;&emsp;[请参考：chatbox的安裝](./Windows-chatbox-安装.md 'included')