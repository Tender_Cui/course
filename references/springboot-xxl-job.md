# 参考文献

* [xxl-官方文档](https://www.xuxueli.com/xxl-job/)
* [【分布式任务调度】（一）XXL-JOB调度中心集群部署配置](https://blog.csdn.net/qq_38249409/article/details/127425643)
* [【分布式任务调度】（二）XXL-JOB执行器配置及定时任务的创建](https://blog.csdn.net/qq_38249409/article/details/127456979)

# springboot 集成可参考 github 上的demo

&emsp;&emsp;[xxl-job 在github上的项目地址](https://github.com/xuxueli/xxl-job)

&emsp;&emsp;部署安装的时候，我们必须下载的是稳定发布版本，而不能是 `master` 分支上的（因为上面有可能提交了新的特性，但是还不稳定）

![稳定发布版本入口](./pictures/springboot-xxl-job/img.png)

# 1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-xxl-job</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>com.xuxueli</groupId>
            <artifactId>xxl-job-core</artifactId>
            <!-- <version>2.4.0</version> -->
        </dependency>
    </dependencies>
</project>

```

# 2、XxlJobConfig 配置类

```java
package com.tender.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class XxlJobConfig {
    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.accessToken}")
    private String accessToken;

    @Value("${xxl.job.executor.appname}")
    private String appname;

    @Value("${xxl.job.executor.address}")
    private String address;

    @Value("${xxl.job.executor.ip}")
    private String ip;

    @Value("${xxl.job.executor.port}")
    private int port;

    @Value("${xxl.job.executor.logpath}")
    private String logPath;

    @Value("${xxl.job.executor.logretentiondays}")
    private int logRetentionDays;

    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        log.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(adminAddresses);
        xxlJobSpringExecutor.setAppname(appname);
        xxlJobSpringExecutor.setIp(ip);
        xxlJobSpringExecutor.setPort(port);
        xxlJobSpringExecutor.setAccessToken(accessToken);
        xxlJobSpringExecutor.setLogPath(logPath);
        xxlJobSpringExecutor.setLogRetentionDays(logRetentionDays);
        return xxlJobSpringExecutor;
    }
}

```

# 3、配置文件：application.yaml

```yaml
server:
  port: 9099

spring:
  application:
    name: shiro-xxl-job

# Xxl-Job分布式定时任务调度中心
xxl:
  job:
    admin:
      # 调度中心部署跟地址 [选填]：如调度中心集群部署存在多个地址则用逗号分隔。(我本地直接启动的xxl的源码)
      addresses: http://127.0.0.1:8080/xxl-job-admin
    # 执行器通讯TOKEN [选填]：非空时启用 系统默认 default_token
    accessToken: default_token
    executor:
      # 执行器的应用名称
      appname: shiro-xxl-job
      # 执行器注册 [选填]：优先使用该配置作为注册地址
      address: ""
      # 执行器IP [选填]：默认为空表示自动获取sIP
      ip: ""
      # 执行器端口号 [选填]：小于等于0则自动获取；默认端口为9999
      port: 9999
      # 执行器运行日志文件存储磁盘路径 [选填] ：需要对该路径拥有读写权限；为空则使用默认路径；
      logpath: /opt/logs/xxl-job/
      # 执行器日志文件保存天数 [选填] ： 过期日志自动清理, 限制值大于等于3时生效; 否则, 如-1, 关闭自动清理功能；
      logretentiondays: 7

# Logger Config
logging:
  level:
    com.tender: debug
```

# 4、demo任务编写

```java
package com.tender.handlers;

import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestJob {
    @XxlJob("testJob")
    public ReturnT<String> testJob(String date) {
        log.info("---------testJob定时任务执行成功--------");
        XxlJobHelper.log("---------testJob定时任务执行成功--------");
        log.info("=========> param = {}", date);
        // *****************************************
        // 通过  XxlJobHelper.getJobParam() 来获取参数
        // *****************************************
        String jobParam = XxlJobHelper.getJobParam();
        log.info("=========> param = {}", date);
        return ReturnT.SUCCESS;
    }
}
```

# 5、启动类

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ShiroXxlJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroXxlJobApplication.class);
    }
}
```

# 6、xxl-job 后台管理配置任务

&emsp;&emsp;由于我本地是直接根据xxl-job的源码启动的后台管理系统，因此后台访问路径如下；

> http://localhost:8080/xxl-job-admin/


## 6.1、新增执行器

![指定appname](./pictures/springboot-xxl-job/img_1.png)

![指定appname](./pictures/springboot-xxl-job/img_2.png)

## 6.2、新增任务

![新增任务](./pictures/springboot-xxl-job/img_3.png)

# 7、启动任务，启动springboot项目，

![启动任务](./pictures/springboot-xxl-job/img_4.png)

![任务调度成功](./pictures/springboot-xxl-job/img_5.png)


