# 参考文献

* [`seata`中文官网](https://seata.apache.org/zh-cn/)
* [`seata github`源码](https://github.com/apache/incubator-seata)

# `Seata`环境准备

&emsp;&emsp;[请参考部署手册](./Centos7-seata.md 'included')

# 示例代码

&emsp;&emsp;[请参考项目：Tender_Cui/shiro](https://gitee.com/Tender_Cui/shiro) 下的模块：`shiro-framework-integration/shiro-sentinel`

# 1、需求

&emsp;&emsp;这里我们创建三个服务，一个订单服务，一个库存服务，一个账户服务。

&emsp;&emsp;当用户下单时，会在订单服务中创建一个订单，然后通过远程调用库存服务来扣减下单商品的库存， 再通过远程调用账户服务来扣减用户账户里面的余额，
最后在订单服务中修改订单状态为已完成。

&emsp;&emsp;该操作跨越三个数据库，有两次远程调用，很明显会有分布式事务问题。

![需求](./pictures/springboot-seata/img.png)

# 2、sql 准备

&emsp;&emsp;建立三个数据库和对应的业务表

> 1. 订单库

```sql
create database seata_order;

create table t_order
(
    id         bigint auto_increment primary key,
    user_id    bigint      null comment '用户ID',
    product_id bigint      null comment '产品ID',
    count      int         null comment '数量',
    money      decimal(11) null comment '金额',
    status     int         null comment '订单状态: 0:创建中, 1:已完结'
);

```

> 2. 库存库

```sql
create database seata_storage;

create table t_storage
(
    id         bigint auto_increment
        primary key,
    product_id bigint      null comment '用户ID',
    total      decimal(11) null comment '总库存',
    used       decimal(11) null comment '已用库存',
    residue    decimal(11) null comment '剩余库存'
);

INSERT INTO seata_storage.t_storage (product_id, total, used, residue) VALUES (11, 100, 0, 100);
```

> 3. 账户库

```sql
create database seata_account;

create table t_account
(
    id      bigint auto_increment
        primary key,
    user_id bigint      null comment '用户ID',
    total   decimal(11) null comment '总额度',
    used    decimal(11) null comment '已用账户余额',
    residue decimal(11) null comment '余额'
);

INSERT INTO seata_account.t_account (user_id, total, used, residue) VALUES (1000, 1000, 0, 1000);
```


&emsp;&emsp;准备`undo_log`表的脚本（<span style="color:red">AT模式专用，别的模式不需要这个</span>）

> [`undo_log`表的脚本的官方出处](https://github.com/apache/incubator-seata/blob/2.x/script/client/at/db/mysql.sql)

```sql
CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT ='AT transaction mode undo table';

ALTER TABLE `undo_log` ADD INDEX `ix_log_created` (`log_created`);
```

&emsp;&emsp;在三个库：`seata_order`、`seata_account`、`seata_storage` 分别创建表：`undo_log`。 

![三个库建表](./pictures/springboot-seata/img_1.png)

# 3、三个微服务

![三个微服务](./pictures/springboot-seata/img_2.png)

> 示例代码

&emsp;&emsp;[请参考项目：Tender_Cui/shiro](https://gitee.com/Tender_Cui/shiro) 下的模块：   
`shiro-framework-integration/shiro-nacos-seata-order`   
`shiro-framework-integration/shiro-nacos-seata-storage`   
`shiro-framework-integration/shiro-nacos-seata-account`   
`shiro-framework-integration/shiro-nacos-seata-facade-api`   

























