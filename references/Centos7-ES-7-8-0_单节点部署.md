# 参考文献
* [ES学习笔记](https://blog.csdn.net/u011863024/article/details/115721328)

* [官网下载地址](https://www.elastic.co/downloads/)
* [ES官网下载地址](https://www.elastic.co/downloads/elasticsearch)
* [ES历史版本官网下载地址](https://www.elastic.co/downloads/past-releases#elasticsearch)


> `Elasticsearch 7.8.0`     
> 为了和课程保持一致，也是采用 `Elasticsearch 7.8.0` 版本来进行安装部署

* [`Elasticsearch 7.8.0`下载地址](https://www.elastic.co/downloads/past-releases/elasticsearch-7-8-0)

# 一、下载`ES-7.8.0`

* [下载地址](https://www.elastic.co/cn/downloads/past-releases/elasticsearch-7-8-0)

> 下载：`elasticsearch-7.8.0-linux-x86_64.tar.gz`

# 二、上传与解压

## 2.1、上传
&emsp;&emsp;上传到下面这个路径：

```shell
  /opt/softs
```

&emsp;&emsp;查看上传后的结果：
```shell
# 省略
-rw-r--r--. 1 root root 305M 2月  19 10:07 elasticsearch-7.8.0-linux-x86_64.tar.gz
# 省略
```

## 2.2、解压

&emsp;&emsp;解压命令

```shell
# -C 解压到某个目录中
  tar -zxvf elasticsearch-7.8.0-linux-x86_64.tar.gz -C ../install/
```

## 2.3、查看解压结果

```shell
cd /opt/install
ll
```

&emsp;&emsp;结果如下；

```shell
drwxr-xr-x.  9 root  root   155 6月  14 2020 elasticsearch-7.8.0
drwxr-xr-x.  7 10143 10143  245 3月  12 2020 jdk1.8.0_251
drwxrwxr-x. 12 mysql mysql  166 12月 13 09:35 mysql5.7
drwxr-xr-x.  7 root  root    96 12月 14 08:07 nacos
drwxrwxr-x.  8 root  root  4096 4月  27 2022 redis-7.0.0

```

## 2.4、重命名

```shell
mv elasticsearch-7.8.0 es-7.8.0
```

# 三、创建用户

&emsp;&emsp;因为安全问题，`ES` 不允许 `root` 用户直接运行，所以要创建新用户，在 `root` 用户中创建`新用户`。

```shell
useradd es #新增 es 用户
passwd es #为 es 用户设置密码
chown -R es:es /opt/install/es-7.8.0 # ES 解压后的文件夹 所有者，修改为 ES

# --------------
userdel -r es #如果错了，可以删除再加
```

# 四、修改配置文件

&emsp;&emsp;修改 `/opt/install/es-7.8.0/config/elasticsearch.yml` 文件。

```shell
# 加入如下配置
cluster.name: elasticsearch
node.name: node-1
network.host: 0.0.0.0
http.port: 9200
cluster.initial_master_nodes: ["node-1"]

```

<hr>

&emsp;&emsp; 修改 `/etc/security/limits.conf` 文件

```shell
# 在文件末尾中增加下面内容
# 每个进程可以打开的文件数的限制
es soft nofile 65536
es hard nofile 65536

```

<hr>

&emsp;&emsp; 修改 `/etc/security/limits.d/20-nproc.conf` 

```shell
# 在文件末尾中增加下面内容
# 每个进程可以打开的文件数的限制
es soft nofile 65536
es hard nofile 65536
# 操作系统级别对每个用户创建的进程数的限制
* hard nproc 4096
# 注： * 带表 Linux 所有用户名称

```

<hr>

&emsp;&emsp; 修改 `/etc/sysctl.conf` 

```shell
# 在文件中增加下面内容
# 一个进程可以拥有的 VMA(虚拟内存区域)的数量,默认值为 65536
vm.max_map_count=655360

```

<hr>

&emsp;&emsp;<span style="color:red">重新加载</span>

```shell
sysctl -p

```

# 五、启动`ES`

&emsp;&emsp;使用 <span style="color:red">ES 用户启动</span>

```shell
cd /opt/install/es-7.8.0
# 启动
bin/elasticsearch
# 后台启动
bin/elasticsearch -d  

```

&emsp;&emsp;如果报如下错误，则重新执行下 `chown -R es:es /opt/install/es-7.8.0` 即可【因为有些文件在 `es-7.8.0` 目录下，是重新生成的】。

&emsp;&emsp;注意：需要 <span style="color:red">root 用户来修改目录所有者！<span>

![访问拒绝错误](./pictures/Centos7-ES-7-8-0_单节点部署/img.png)

# 六、访问测试

## 6.1、如果有防火墙

```shell
# 检查 firewalld 状态
systemctl status firewalld

sudo systemctl start firewalld  # 启动
sudo systemctl enable firewalld # 开机就启动

firewall-cmd --zone=public --add-port=9200/tcp --permanent # 添加放行端口

# 重新加载防火墙配置
firewall-cmd --reload

# 验证端口是否已添加
firewall-cmd --zone=public --query-port=8080/tcp

# 列出所有开放的端口
firewall-cmd --zone=public --list-ports

# 移除端口
firewall-cmd --zone=public --remove-port=8080/tcp --permanent
firewall-cmd --reload
```

