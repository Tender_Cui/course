# 参考文献

* AI 助手直接提问："win10 查看wifi密码的命令"

```shell
netsh wlan show profile name="your_network_name" key=clear
```

> 结果：

```shell
接口 WLAN 上的配置文件 TPLINK-15383:
=======================================================================

已应用: 所有用户配置文件

配置文件信息
-------------------
    版本                   : 1
    类型                   : 无线局域网
    名称                   : TPLINK-15383
    控制选项               :
        连接模式           : 自动连接
        网络广播           : 只在网络广播时连接
        AutoSwitch         : 请勿切换到其他网络
        MAC 随机化: 禁用

连接设置
---------------------
    SSID 数目              : 1
    SSID 名称              :“TPLINK-15383”
    网络类型               : 结构
    无线电类型             : [ 任何无线电类型 ]
    供应商扩展名           : 不存在

安全设置
-----------------
    身份验证         : WPA2 - 个人
    密码                 : CCMP
    身份验证         : WPA2 - 个人
    密码                 : GCMP
    安全密钥               : 存在
    关键内容            : xxxxxxx

费用设置
-------------
    费用                   : 无限制
    阻塞                : 否
    接近流量上限        : 否
    超出流量上限        : 否
    漫游                : 否
    费用来源            : 默认
```