# 安装 SQL Developer

* [SQL Developer官网](https://www.oracle.com/database/sqldeveloper/)
* [SQL Developer官网下载地址](https://www.oracle.com/database/sqldeveloper/technologies/download/)

# 1、下载安装包

&emsp;&emsp;`sqldeveloper-23.1.1.345.2114-x64.zip`

![下载安装包](./pictures/Windows-SQL-Developer/img.png)

> Oracle账号

```html
账号：1789936303@qq.com
密码：Oracle123456

账号：amador.sun@foxmail.com
密码：1211WaN!

账号：amador.sun@qq.com
密码：1211WaN!

账号：liwei@xiaostudy.com
密码：OracleTest1234
```

# 2、解压

![解压](./pictures/Windows-SQL-Developer/img_1.png)

# 3、连接

![连接](./pictures/Windows-SQL-Developer/img_2.png)


