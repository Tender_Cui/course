# 参考文献

* [`Sentinel`流量守卫](https://www.cnblogs.com/euneirophran/p/18073905)
* [`Jmeter`官网下载](https://jmeter.apache.org/download_jmeter.cgi)

# 示例代码

&emsp;&emsp;[请参考项目：Tender_Cui/shiro](https://gitee.com/Tender_Cui/shiro) 下的模块：`shiro-framework-integration/shiro-sentinel`

# 1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-sentinel</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- 常用工具 -->
        <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
        </dependency>
    </dependencies>

</project>
```

# 2、`yaml`配置 

```yaml
server:
  port: 8401

spring:
  application:
    name: shiro-sentinel

  cloud:
    sentinel:
      transport:
        dashboard: 192.168.255.100:8080  # Sentinel 部署虚机的 ip 和启动端口
        port: 8719 # 监听端口（默认端口：8719），加入端口被占用，会自动从 8719 开始 +1 尝试，直到找到未被占用的端口。
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
```

# 3、启动类

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class ShiroSentinelApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroSentinelApplication.class);
    }
}
```

# 4、编写`Controller`类

```java
package com.tender.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Sentinel 流控服务对应的 Controller
 */
@RestController
public class RateLimitController {

    @GetMapping("/testA")
    public String testA() {
        return "==========> testA";
    }

    @GetMapping("/testB")
    public String testB() {
        return "==========> testB";
    }
}
```

# 5、浏览器启动并访问

&emsp;&emsp;方法A

![testA](./pictures/springboot-Sentinel-references/img.png)

&emsp;&emsp;方法B

![testB](./pictures/springboot-Sentinel-references/img_1.png)

&emsp;&emsp;查看`Sentinel`页面

![查看`Sentinel`页面](./pictures/springboot-Sentinel-references/img_2.png)

# 6、<span style="color:red">流控规则</span>

&emsp;&emsp;三种流控模式：<span style="color:yellow">直接、关联、链路</span>

## 6.1、直接

&emsp;&emsp;"直接"是默认的流控模式，当接口达到限流条件时，直接开启限流功能。

1. 流控配置

&emsp;&emsp;以访问`http://localhost:8401/testA` 为示例，进行流控配置

![直接模式](./pictures/springboot-Sentinel-references/img_3.png)

2. 测试

&emsp;&emsp;由于`QPS`只有2，因此点击快了，就会有流控提示：

![流控提示](./pictures/springboot-Sentinel-references/img_4.png)

## 6.2、关联

&emsp;&emsp;当关联的资源达到阈值时，就限流自己。

&emsp;&emsp;比如：当和`A`关联的资源`B`达到阈值时，就限流`A`自己。（<span style="color:red">B惹事，A挂了</span>）

1. 流控配置

![关联配置](./pictures/springboot-Sentinel-references/img_5.png)

&emsp;&emsp;利用`Jmeter`来压测调用`/testB`

![Jmeter压测配置](./pictures/springboot-Sentinel-references/img_6.png)

2. 测试

&emsp;&emsp;启动压测，4s 内访问`/testA`，发现被流控，4s后，访问`/testA`正常。

![4s内被流控](./pictures/springboot-Sentinel-references/img_7.png)

![4s后接口正常](./pictures/springboot-Sentinel-references/img_8.png)

## 6.3、链路

&emsp;&emsp;来自不同链路的请求对同一个目标访问时，实施针对性的不同限流措施。

&emsp;&emsp;比如：C请求来访问就限流，D请求来访问<span style="color:green">就是OK</span>。

1. `java`类

&emsp;&emsp;新增`service`类：`FlowLimitService`

```java
package com.tender.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class FlowLimitService {
    @SentinelResource("common")
    public void common() {
        log.info("======> ... common ...");
    }
}
```

&emsp;&emsp;改造`controller`: `RateLimitController`

```java
package com.tender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tender.service.FlowLimitService;

/**
 * Sentinel 流控服务对应的 Controller
 */
@RestController
public class RateLimitController {

    @Autowired
    private FlowLimitService flowLimitService;

    @GetMapping("/testA")
    public String testA() {
        return "==========> testA";
    }

    @GetMapping("/testB")
    public String testB() {
        return "==========> testB";
    }

    // 新增的。
    @GetMapping("/testC")
    public String testC() {
        flowLimitService.common();
        return "==========> testC";
    }

    // 新增的。
    @GetMapping("/testD")
    public String testD() {
        flowLimitService.common();
        return "==========> testD";
    }
}
```

2. `yaml`配置

```yaml
server:
  port: 8401

spring:
  application:
    name: shiro-sentinel

  cloud:
    sentinel:
      transport:
        dashboard: 192.168.255.100:8080
        port: 8719 # 默认端口，加入端口被占用，会自动从 8719 开始 +1 尝试，直到找到未被占用的端口。
      web-context-unify: false # controller 层的方法对service层的调用，不认为是同一个根链路。

    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP

```

3. 流控配置

![流控配置](./pictures/springboot-Sentinel-references/img_9.png)

4. 测试结果

&emsp;&emsp;当每秒访问速度超过1次时候，报错：

![测试结果](./pictures/springboot-Sentinel-references/img_10.png)

# 7、流控效果

&emsp;&emsp;快速失败（默认）、Warm up、排队等待

## 7.1、快速失败

&emsp;&emsp;这是默认的流控效果，会直接失败，抛出异常【Blocked by Sentinel (flow limiting)】

## 7.2、Warm up

&emsp;&emsp;`Warm up`(冷启动、预热)。

&emsp;&emsp;当流量突然增大的时候，我们常常会希望系统从空闲状态到繁忙状态的切换的时间<span style="color:green">长一些</span>。
即如果系统在此之前长期处于空闲的状态，我们希望处理请求的数量是缓步的增多，经过预期的时间以后，到达系统处理请求个数的最大值。Warm Up（冷启动，预热）模式就是为了实现这个目的的。

&emsp;&emsp;这个场景主要用于启动需要额外开销的场景，例如建立数据库连接等。

&emsp;&emsp;<span style="color:red">公式</span>：阈值除以冷却因子`codeFactor`（默认值为3），经过预热时长后才会达到阈值。

&emsp;&emsp;如下是`WarmUpController`对应的源码：

```java
public class WarmUpController implements TrafficShapingController {
    protected double count;
    private int coldFactor;
    protected int warningToken = 0;
    private int maxToken;
    protected double slope;
    protected AtomicLong storedTokens = new AtomicLong(0L);
    protected AtomicLong lastFilledTime = new AtomicLong(0L);

    public WarmUpController(double count, int warmUpPeriodInSec, int coldFactor) {
        this.construct(count, warmUpPeriodInSec, coldFactor);
    }

    public WarmUpController(double count, int warmUpPeriodInSec) {
        this.construct(count, warmUpPeriodInSec, 3);
    }
    // 省略其它的代码......
}

```

1. 流控规则配置

![流控规则配置-warm-up](./pictures/springboot-Sentinel-references/img_11.png)

> 参数解释：
> 
> 系统初始化的阈值为 10/3 约等于 3，即单机阈值刚开始为 3（我们人工设定单机阈值是10，`sentinel`计算后`QPS`判定为从 3 开始）;
> 
> 然后过了 5s 后，阈值才慢慢升高、恢复到设置的单机阈值 10，也就是说 5s 内`QPS`为 3，过了保护期 5s 后`QPS`为 10。

2. 测试结果

&emsp;&emsp;大概过了 5s 后，就不再被限流了。

![warm-up测试](./pictures/springboot-Sentinel-references/img_12.png)

## 7.3、排队等待

![排队等待-官网截图](./pictures/springboot-Sentinel-references/img_13.png)

![排队等待-时间轴](./pictures/springboot-Sentinel-references/img_14.png)

1. 修改`java`类：`RateLimitController`

```java
/**
 * Sentinel 流控服务对应的 Controller
 */
@RestController
public class RateLimitController {

    // 省略别的代码，新增如下代码
    
    /**
     * 流控效果---排队等待
     */
    @GetMapping("/testE")
    public String testE() {
        System.out.println(System.currentTimeMillis() + "      testE，流控效果---排队等待---");
        return "==========> testE";
    }
}
```

2. `Jmeter`配置

![线程组配置](./pictures/springboot-Sentinel-references/img_15.png)

![待访问接口配置](./pictures/springboot-Sentinel-references/img_16.png)

3. 流控规则配置

![流控规则配置](./pictures/springboot-Sentinel-references/img_17.png)

4. 测试结果

![测试结果](./pictures/springboot-Sentinel-references/img_18.png)


# 8、阈值类型

## 8.1、QPS

## 8.2、并发线程数

&emsp;&emsp;了解即可，实际中工作中，主推 `QPS`

# 9、熔断

&emsp;&emsp;`Sentinel`熔断降级会在调用链路中某个资源出现不稳定状态时（例如调用超时或异常比例升高），对这个资源的调用进行限制，
让请求快速失败，避免影响到其它的资源而导致级联错误。当资源被降级后，在接下来的降级时间窗口之内，对该资源的调用都自动熔断（默认行为是抛出`DegradeException`)。

>熔断策略

--- 

&emsp;&emsp;[官方文档](https://github.com/alibaba/Sentinel/wiki/%E7%86%94%E6%96%AD%E9%99%8D%E7%BA%A7)

&emsp;&emsp;Sentinel 提供以下几种熔断策略：

* **慢调用比例 (`SLOW_REQUEST_RATIO`)**：选择以慢调用比例作为阈值，需要设置允许的慢调用 RT（即最大的响应时间），请求的响应时间大于该值则统计为慢调用。当单位统计时长（statIntervalMs）内请求数目大于设置的最小请求数目，并且慢调用的比例大于阈值，则接下来的熔断时长内请求会自动被熔断。经过熔断时长后熔断器会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求响应时间小于设置的慢调用 RT 则结束熔断，若大于设置的慢调用 RT 则会再次被熔断。
* **异常比例 (`ERROR_RATIO`)**：当单位统计时长（statIntervalMs）内请求数目大于设置的最小请求数目，并且异常的比例大于阈值，则接下来的熔断时长内请求会自动被熔断。经过熔断时长后熔断器会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求成功完成（没有错误）则结束熔断，否则会再次被熔断。异常比率的阈值范围是 [0.0, 1.0]，代表 0% - 100%。
* **异常数 (`ERROR_COUNT`)**：当单位统计时长内的异常数目超过阈值之后会自动进行熔断。经过熔断时长后熔断器会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求成功完成（没有错误）则结束熔断，否则会再次被熔断。

## 9.1、慢调用比例

&emsp;&emsp;名词解释：

&emsp;&emsp;进入熔断状态判断依据：在统计时长内，实际请求数目>设定的最小请求数&&实际慢调用比例＞比例阈值，则进入熔断状态。

![慢调用比例配置](./pictures/springboot-Sentinel-references/img_19.png)

1. **调用**：一个请求发送到服务器，服务器给与响应，一个响应就是一个调用。   
2. **最大RT**：即最大的响应时间，指系统对请求作出响应的业务处理时间。   
3. **慢调用**：处理业务逻辑的实际时间>设置的最大RT时间，这个调用叫做慢调用。   
4. **慢调用比例**：在所有的调用中，慢调用占实际的比例＝慢调用次数/总调用次数。
5. **比例阈值**：自己设定的。比例阈值＝慢调用次数/总调用次数。
6. 统计时长：时间的判断依据。
7. 最小请求数：设置的调用最小请求数，上图比如1秒钟打进来10个线程（大于我们配置的5个了）调用被触发。

---

&emsp;&emsp;进入熔断状态后：

1. **熔断状态（保险丝跳闸断电，不可访问）**：在接下来的熔断时长内请求会自动被熔断。
2. **探测恢复状态（探路先锋）**：熔断时长结束后进入探测恢复状态
3. **结束熔断（保险丝闭合恢复，可以访问）**：在探测恢复状态，如果接下来的一个请求响应时间小于设置的慢调用RT，则结束熔断，否则继续熔断。

### 9.1.1、熔断配置

![熔断配置](./pictures/springboot-Sentinel-references/img_20.png)

### 9.1.2、`Jmeter`配置

![`Jmeter`配置](./pictures/springboot-Sentinel-references/img_21.png)

![`Jmeter`配置02](./pictures/springboot-Sentinel-references/img_22.png)

### 9.1.3、`java`代码

```java
/**
 * Sentinel 流控服务对应的 Controller
 */
@RestController
public class RateLimitController {

    // 省略其余代码
    
    /**
     * 新增熔断规则-慢调用比率
     *
     * 后台服务暂停了1s，都不满足200ms 接口就满足的RT 要求，所以每一次访问都是慢请求。
     * 通过 Jmeter 1s 内发送10个线程，这会导致立马进入 熔断状态。
     */
    @GetMapping("/testF")
    public String testF() {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("------测试：新增熔断规则-慢调用比率");
        return "==========> testF 新增熔断规则-慢调用比率";
    }
}
```

### 9.1.4、测试结果


![测试结果](./pictures/springboot-Sentinel-references/img_23.png)

&emsp;&emsp;按照上述配置，熔断触发：

&emsp;&emsp;多次循环，一秒钟打进来10个线程（大于5个了）调用`/testF`，我们希望 `200ms`（理想状态，实际后台服务休眠`1s`）处理完一次调用。

&emsp;&emsp;假如在统计时长内，实际请求数目>最小请求数&&慢调用比例>比例阈值，断路器打开（保险丝跳闸）微服务不可用`（Blocked by Sentinel (flow limiting)`，进入熔断状态 5 秒。

&emsp;&emsp;后续停止`Jmeter`，没有这么大的访问量了，单独用浏览器访问`rest`地址，断路器关闭（保险丝恢复，合上闸口）,微服务恢复OK


## 9.2、异常比例

### 9.2.1、`java`代码

```java

/**
 * Sentinel 流控服务对应的 Controller
 */
@RestController
public class RateLimitController {
    
    // 省略其余代码

    @GetMapping("/testG")
    public String testG() {
        System.out.println("------测试：新增熔断规则-异常比率");
        throw new RuntimeException("模拟异常");
    }
}
```

### 9.2.2、熔断规则配置

![异常熔断配置](./pictures/springboot-Sentinel-references/img_24.png)

### 9.2.3、`Jmeter`配置

![`Jmeter`配置](./pictures/springboot-Sentinel-references/img_25.png)

![`Jmeter`配置02](./pictures/springboot-Sentinel-references/img_26.png)

### 9.2.4、结果

1. 不配置`Sentinel`的情况

```html
Whitelabel Error Page
This application has no explicit mapping for /error, so you are seeing this as a fallback.

Thu May 02 17:23:03 CST 2024
There was an unexpected error (type=Internal Server Error, status=500).
```

2. 配置`Sentinel`的情况

&emsp;&emsp;如果触发熔断规则，则报：【Blocked by Sentinel (flow limiting)】

![流控结果](./pictures/springboot-Sentinel-references/img_27.png)


## 9.3、异常数

&emsp;&emsp;很简单，和前面的 `9.2、异常比例` 类似，就不介绍具体的细节了，只贴上配置。

![异常数配置](./pictures/springboot-Sentinel-references/img_28.png)















