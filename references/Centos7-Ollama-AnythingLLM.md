# 参考文献
* [linux 安装部署Ollama](https://zhuanlan.zhihu.com/p/688811216)
* [github 关于 Ollama 安装到 linux](https://github.com/ollama/ollama/blob/main/docs/linux.md)
* [在CentOS7虚拟机上使用Ollama本地部署Llama3大模型中文版+Open WebUI](https://blog.csdn.net/Inno__cence/article/details/139525782)

---
* [AnythingLLM 官网安装指南](https://useanything.com/download)
* [AnythingLLM 官网 Linux 安装指南](https://docs.useanything.com/installation/desktop/linux#install-using-the-installer-script)


# 1、安装 `Ollama`

## 1.1、自动安装，比较卡，需要魔法解决

```shell
curl -fsSL https://ollama.com/install.sh | sh
```

## 1.2、手动安装

&emsp;&emsp;[请参考官方安装手册](https://github.com/ollama/ollama/blob/main/docs/linux.md)

### 1.2.1、Download the ollama binary

```shell
sudo curl -L https://ollama.com/download/ollama-linux-amd64 -o /usr/bin/ollama
sudo chmod +x /usr/bin/ollama
```

### 1.2.2、Adding Ollama as a startup service (recommended)

&emsp;&emsp;给 Ollama 创建用户

```shell
sudo useradd -r -s /bin/false -m -d /usr/share/ollama ollama
```

### 1.2.3、创建 service 文件

&emsp;&emsp;Create a service file in /etc/systemd/system/ollama.service:

```shell
[Unit]
Description=Ollama Service
After=network-online.target

[Service]
ExecStart=/usr/bin/ollama serve
User=ollama
Group=ollama
Restart=always
RestartSec=3

[Install]
WantedBy=default.target
```

### 1.2.4、开启 Ollama 服务

```shell
sudo systemctl daemon-reload
sudo systemctl enable ollama
```

## 1.3、常用命令

```shell
# 大模型列表
ollama list

# 启动模型
ollama run llama3
```

&emsp;&emsp;启动 `Ollama`

```shell
ollama run llama3
```

# 2、安装 `AnythingLLM`

## 2.1、命令直接安装

```shell
curl -fsSL https://s3.us-west-1.amazonaws.com/public.useanything.com/latest/installer.sh | sh
```

## 2.2、启动

```shell
./AnythingLLMDesktop/start
```
