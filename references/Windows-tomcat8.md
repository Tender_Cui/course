# 参考文献

* [Java 配置Tomcat环境变量并使用(在windows中)](https://blog.csdn.net/weixin_44953227/article/details/111575409)

# 一、简介
&emsp;&emsp;Tomcat是Apache软件基金会的一个核心项目, 也是一个开源免费的一个轻量级web服务器.支持servlet/JSP 少量的JavaEE规范,也是我们学习JavaWeb中常用的服务器.
Tomcat 也被称为Web容器,Servlet容器.Servlet需要依赖Tomcat才能运行

&emsp;&emsp;Tomcat的官网如下：
```shell
https://tomcat.apache.org/
```

# 二、下载

> 官网的下载地址

![官网的下载地址](./pictures/tomcat8_install_4_win/img.png)

# 三、解压

> 已下载文件

```shell
apache-tomcat-8.5.83-windows-x64.zip
```

> 解压后的目录列表

![tomcat目录列表](./pictures/tomcat8_install_4_win/img_1.png)

&emsp;&emsp;简单介绍一下这些目录

* bin: 里面有一些可执行的程序/脚本, 启动Tomcat就是在这里
* conf: 用.xml 的格式表示服务器的配置
* lib: 表示Tomcat 依赖的 一些库
* longs: 这个是日志,Tomcat的运行日志就在这里.如果程序出问题了,可以通过日志来排查问题
* temp: 这个目录用户存放tomcat在运行过程中产生的临时文件
* webapps: webapp就是网站,一个Tomcat可以部署多个网站,因此叫 “webapps”
* work:tomcat有一个work目录，里面存放了页面的缓存，访问的jsp都会编译，编译后的文件都会存储在work目录下

# 四、配置环境变量

&emsp;&emsp;变量名：CATALINA_HOME    
&emsp;&emsp;变量值：E:\install_code\tomcat_8\apache-tomcat-8.5.83 （视各人情况)

> 我的电脑->属性->高级系统设置->环境变量(N)

![环境变量](./pictures/tomcat8_install_4_win/img_2.png)

![新增环境变量 CATALINA_HOME](./pictures/tomcat8_install_4_win/img_3.png)

![path 新加两条](./pictures/tomcat8_install_4_win/img_4.png)

# 五、启动 Tomcat8

> Tomcat8 依赖jdk8，如果没有，请提前安装，这里就不再演示了。

```text
# 快捷键 win + R，打开cmd 窗口
# 输入 startup.bat
```

![startup.bat 命令启动](./pictures/tomcat8_install_4_win/img_5.png)

![tomcat 启动成功](./pictures/tomcat8_install_4_win/img_6.png)

> 浏览器访问 

![浏览器访问](./pictures/tomcat8_install_4_win/img_7.png)

# 六、修改Tomcat 的默认端口号

```text
# 到路径 E:\install_code\tomcat_8\apache-tomcat-8.5.83\conf 

# 修改 server.xml
```

![修改Tomcat 的默认端口号](./pictures/tomcat8_install_4_win/img_8.png)

