# 参考文献

* [centos7.6安装redis](https://blog.csdn.net/cxy2687365281/article/details/129931785)

# 1、简介

&emsp;&emsp;[redis官网](https://redis.io/)   
&emsp;&emsp;[redis历史发布版本列表](https://download.redis.io/releases/)

# 2、环境准备

> 虚机

```html
192.168.255.100  虚机  单机部署
```

&emsp;&emsp;由于 redis 是用 C 语言开发，需要安装Redis编译所需的工具。在终端中执行以下命令：

```shell
# yum install gcc* tcl tcl-devel make -y
yum install gcc -y
```

&emsp;&emsp;将安装GCC编译器、TCL和相关的开发工具。

# 3、下载、上传、解压

## 3.1、下载

&emsp;&emsp;下载 redis 安装包：redis-6.2.4.tar.gz

![下载 redis 安装包: redis-6.2.4.tar.gz](./pictures/Centos7-redis-standalone/img.png)

## 3.2、上传

![上传: redis-6.2.4.tar.gz](./pictures/Centos7-redis-standalone/img_1.png)

## 3.3、解压

&emsp;&emsp;解压到目录： /opt/installs

```shell
tar -zxvf redis-6.2.4.tar.gz -C ./../installs/
```

![查看解压目录](./pictures/Centos7-redis-standalone/img_2.png)

![查看解压目录详情](./pictures/Centos7-redis-standalone/img_3.png)

# 4、编译和安装

```shell
# 进入对应的解压目录
cd /opt/installs/redis-6.2.4

# 编译源码
make

# 编译 并 安装到指定目录下
make PREFIX=/usr/local/redis install
```

![编译成功](./pictures/Centos7-redis-standalone/img_4.png)

&emsp;&emsp;安装成功后的 /bin 目录：

![安装成功](./pictures/Centos7-redis-standalone/img_5.png)

&emsp;&emsp;拷贝 redis.conf 配置文件

```shell
# 接入解压目录，拷贝 redis.conf 文件
cd /opt/installs/redis-6.2.4/

cp redis.conf /usr/local/redis/bin/
```

&emsp;&emsp;vim redis.conf 配置文件

```shell
cd /usr/local/redis/bin/

vim redis.conf

# 具体修改内容如下：
protected-mode no # 允许远程链接
port 6379         # 默认端口是 6379
daemonize yes     # 允许后台启动
pidfile /usr/local/redis/config/redis_6379.pid  # pid 文件
requirepass 88888888 #设置访问密码
logfile /usr/local/redis/config/logs/redis.log
```

&emsp;&emsp;创建 config 文件夹

```shell
cd /usr/local/redis
mkdir config
cd config
touch redis_6379.pid

mkdir logs
cd logs
touch redis.log
```

# 5、启动redis并连接测试

## 5.1、启动 redis-server

```shell
cd /usr/local/redis/bin

# 启动
redis-server redis.conf
```

## 5.2、查看日志

```shell
cd /usr/local/redis/config/logs

# 查看日志
cat redis.log
```

```text
91771:C 02 Apr 2024 01:08:11.571 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
91771:C 02 Apr 2024 01:08:11.571 # Redis version=6.2.4, bits=64, commit=00000000, modified=0, pid=91771, just started
91771:C 02 Apr 2024 01:08:11.571 # Configuration loaded
91771:M 02 Apr 2024 01:08:11.572 * Increased maximum number of open files to 10032 (it was originally set to 1024).
91771:M 02 Apr 2024 01:08:11.572 * monotonic clock: POSIX clock_gettime
91771:M 02 Apr 2024 01:08:11.572 * Running mode=standalone, port=6379.
91771:M 02 Apr 2024 01:08:11.573 # Server initialized
91771:M 02 Apr 2024 01:08:11.573 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
91771:M 02 Apr 2024 01:08:11.573 * Loading RDB produced by version 6.2.4
91771:M 02 Apr 2024 01:08:11.573 * RDB age 722 seconds
91771:M 02 Apr 2024 01:08:11.573 * RDB memory usage when created 0.77 Mb
91771:M 02 Apr 2024 01:08:11.573 * DB loaded from disk: 0.000 seconds
91771:M 02 Apr 2024 01:08:11.573 * Ready to accept connections
```

## 5.3、客户端连接测试

```shell
redis-cli 

# 输入密码
auth 88888888
```

```html
127.0.0.1:6379> auth 88888888
OK
127.0.0.1:6379> set name tender
OK

127.0.0.1:6379> get name
"tender"
127.0.0.1:6379>
```


