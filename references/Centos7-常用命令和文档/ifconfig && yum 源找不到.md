# 最小安装 `Centos7` 的时候，查看 `ip` 提示没有 `ifconfig`

# 1、安装命令

```shell
sudo apt-get install net-tools  # 对于Debian/Ubuntu
sudo yum install net-tools      # 对于CentOS/RHEL
```

&emsp;&emsp;如果提示 `yum` 源的问题，可进行如下操作：

## 1.1、在开始配置之前，最好先备份当前的 `yum` 配置，以防万一。

```shell
sudo cp /etc/yum.repos.d /etc/yum.repos.d.backup -r
```

## 1.2、编辑 `CentOS-Base.repo` 文件
&emsp;&emsp;编辑仓库配置文件：进入 `/etc/yum.repos.d/` 目录，找到你的 `CentOS-Base.repo` 文件：
```shell
cd /etc/yum.repos.d/
sudo vi CentOS-Base.repo  # 利用 finalShell 直接编辑
```

## 1.3、修改仓库源的 `baseurl` 地址：

&emsp;&emsp;找到 `[base], [updates], [extras]` 等配置部分，将 baseurl 的值修改为 CentOS Vault 的地址。例如，修改为以下内容：

```shell
[base]
name=CentOS-$releasever - Base
baseurl=http://mirrors.aliyun.com/centos/7/os/x86_64/
gpgcheck=1
enabled=1
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

[updates]
name=CentOS-$releasever - Updates
baseurl=http://mirrors.aliyun.com/centos/7/updates/x86_64/
gpgcheck=1
enabled=1
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

[extras]
name=CentOS-$releasever - Extras
baseurl=http://mirrors.aliyun.com/centos/7/extras/x86_64/
gpgcheck=1
enabled=1
gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7
```

## 1.4、清除 `yum` 缓存

&emsp;&emsp;修改完源后，清除 `yum` 缓存并更新仓库数据：

```shell
sudo yum clean all
sudo yum makecache
```

## 1.5、尝试安装或更新

&emsp;&emsp;在修改并清除缓存后，再次尝试安装软件包或者进行系统更新：

```shell
sudo yum update
```

# 2、如果 `yum install net-tools` 遇到如下问题 

> 问题：net-tools-2.0-0.25.20131004git.el7.x86_64.rpm 的公钥尚未安装

> <span style="color:red">解决办法:</span>

## 2.1、导入 `GPG` 公钥

&emsp;&emsp;在 `CentOS` 中，`net-tools` 软件包的 `GPG` 公钥可以通过以下命令导入：

```shell
sudo rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
```

&emsp;&emsp;如果 `/etc/pki/rpm-gpg/` 中没有这个公钥文件，可以从 `CentOS` 官方网站下载公钥：

* 下载 `CentOS 7` 的 `GPG` 公钥：

```shell
sudo wget -O /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 https://www.centos.org/keys/RPM-GPG-KEY-CentOS-7
```

## 2.2、安装软件包

&emsp;&emsp;导入公钥后，尝试重新安装软件包：

```shell
sudo yum install net-tools
```





