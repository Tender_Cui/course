# 参考文献

* [Chatbox AI客户端](https://chatboxai.app/zh)
* [硅基流动(siliconflow)官网](https://cloud.siliconflow.cn/models)

# 一、下载

&emsp;&emsp;根据我们的 `os` 来选择下载的客户端

# 二、设置

![本地设置](./pictures/Windows-chatbox-安装/img.png)

# 三、对话使用

![对话使用](./pictures/Windows-chatbox-安装/img_1.png)

# 四、与硅基流动的整合

&emsp;&emsp;如果没有注册，可以注册！

> 开启新的对话

![开启新的对话](./pictures/Windows-chatbox-安装/img_2.png)

> 用户管理 -> API 密钥 -> 新建 API 密钥

![新建密钥](./pictures/Windows-chatbox-安装/img_3.png)

# 五、模型联网

&emsp;&emsp;使用 `Page Assist` 插件





