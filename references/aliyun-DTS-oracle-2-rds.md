# 参考文献

* [自建Oracle迁移至RDS MySQL](https://help.aliyun.com/zh/dts/user-guide/migrate-data-from-a-self-managed-oracle-database-to-an-apsaradb-rds-for-mysql-instance-1?spm=a2c4g.11174283.0.0.3077656aSz1bYE#section-les-736-q3v)

# 1、DTS 简介

&emsp;&emsp;数据传输服务DTS（`Data Transmission Service`），将**自建** `Oracle 数据`迁移至`RDS MySQL`实例。

> DTS支持
* 结构迁移
* 全量数据迁移
* 增量数据迁移   

&emsp;&emsp;同时使用这三种迁移类型可以实现在本地应用<span style="color:red">不停服</span>的情况下，平滑地完成Oracle数据库的数据迁移。


# 2、前提条件

* 已创建源数据库自建Oracle（<span style="color:green">**✔**</span>）
* 创建用于采集Oracle 数据的账号并授权。（<span style="color:yellow">**?**</span>）


| 数据库          | 结构迁移 | 全量迁移 | 增量数据迁移     |
|--------------|:--:|----|------------|
| 自建Oracle数据库  | Schema的Owner权限  | Schema的Owner权限 | 需要精细化授权    | 
| RDS MySQL实例  | 待迁入数据库的写权限  | 待迁入数据库的写权限  | 待迁入数据库的写权限 | 

> 数据库账号创建及授权方法：
> 1. 自建Oracle数据库请参见[数据库账号准备](https://help.aliyun.com/zh/dts/user-guide/configure-the-oracle-database-and-create-an-account?spm=a2c4g.11186623.0.i12#546a5ab1643wq)、[CREATE USER](https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_8003.htm)和[GRANT](https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_9013.htm)。
> 2. RDS MySQL实例请参见[创建账号](https://help.aliyun.com/zh/rds/apsaradb-rds-for-mysql/create-an-account-on-an-apsaradb-rds-for-mysql-instance?spm=a2c4g.11186623.0.i106#concept-kxw-k1p-ydb)和[修改账号权限](https://help.aliyun.com/zh/rds/apsaradb-rds-for-mysql/modify-the-permissions-of-a-standard-account-on-an-apsaradb-rds-for-mysql-instance?spm=a2c4g.11186623.0.i106#concept-ys2-4bp-ydb)。

* 已创建目标`RDS MySQL`实例，创建方式，请参见[快速创建RDS MySQL实例](https://help.aliyun.com/zh/rds/apsaradb-rds-for-mysql/create-an-apsaradb-rds-for-mysql-instance?spm=a2c4g.11186623.0.i9#concept-wzp-ncf-vdb)。（<span style="color:yellow">**?**</span>）
* 专网打通（<span style="color:red">**×**</span>）
* `自建Oracle`数据库已开启`ARCHIVELOG`（归档模式），设置合理的`归档日志保持周期`且`归档日志能够被访问`，详情请参见[ARCHIVELOG](https://docs.oracle.com/database/121/ADMIN/archredo.htm#ADMIN008)（<span style="color:yellow">**?**</span>）
* `自建Oracle`数据库已开启`Supplemental Logging`，且已开启`supplemental_log_data_pk`，`supplemental_log_data_ui`，详情请参见[Supplemental Logging](https://docs.oracle.com/database/121/SUTIL/GUID-D857AF96-AC24-4CA1-B620-8EA3DF30D72E.htm#SUTIL1582)（<span style="color:yellow">**?**</span>）。
* 建议在执行数据迁移前了解源库为`Oracle`时`DTS`支持的能力和限制条件，并使用`ADAM（Advanced Database & Application Migration）`进行数据库评估，以便您平滑地迁移上云。（<span style="color:green">**✔**</span>）

> 更多信息，请参见:

* [Oracle数据库的限制和准备工作](https://help.aliyun.com/zh/dts/user-guide/configure-the-oracle-database-and-create-an-account?spm=a2c4g.11186623.0.i12)
* [数据库评估概览](https://help.aliyun.com/zh/dts/user-guide/overview?spm=a2c4g.11186623.0.i12)

# 3、注意事项

> 说明：

1. 在库表结构迁移过程中，`DTS`会将<span style="color:pink">源数据库</span>中的<span style="color:pink">外键</span>迁移到目标数据库。
2. 在全量迁移和增量迁移过程中，DTS会以Session级别暂时禁用约束检查以及外键级联操作。若任务运行时源库存在级联更新、删除操作，可能会导致数据不一致。<span style="color:yellow">（可以在业务低谷期操作）</span>

> 具体注意事项：

## 3.1、 源库限制

1. **宽带要求**：源库所属的服务器，带宽尽量大，否则将影响数据迁移速率
2. 源库通过专线的方式接入，那么需要将其中<span style="color:pink">任意1个VIP</span>配置到连接信息中，实现`Oracle RAC`通过专线接入迁移任务
3. 如自建`Oracle`为<span style="color:pink">RAC架构</span>，且用专线/VPN网关/智能接入网关、数据库网关DG、云企业网CEN和ECS的接入方式，则<span style="color:pink">不支持配置ScanIP</span>，仅支持将其中任意1个VIP配置到连接信息中，该方式配置后<span style="color:pink">不支持RAC的节点切换</span>。
4. 如果<span style="color:pink">源库存在varchar2类型的**空**字符串</span>（Oracle会将其处理为null）且对应的<span style="color:pink">目标库字段有非空约束</span>，将会导致迁移任务失败。
5. 迁移对象要求：   
> 1. 待迁移的表需具备主键或唯一约束，且字段具有唯一性，否则可能会导致目标数据库中出现重复数据。
> 2. 如果您的自建Oracle版本为<span style="color:pink">12c(烟台港是：oracle 19c )</span>及以上，待迁移表的名称长度需不超过30个字节。
> 3. 如迁移对象为表级别，且需进行编辑（如表列名映射），则单次迁移任务仅支持迁移至多1000张表。当超出数量限制，任务提交后会显示请求报错，此时建议您拆分待迁移的表，分批配置多个任务，或者配置整库的迁移任务。（<span style="color:green">**目前表少，我们不需要考虑**</span>）
6. 需进行增量迁移相关：
> 1. 开启: `Redo Log`、`Archive Log`
> 2. DTS要求源数据库的`Redo Log`、`Archive Log`保存24小时以上
> 3. 如果为全量迁移和增量迁移任务：DTS要求源数据库的`Redo Log`、`Archive Log`至少保留7天以上（您可在全量迁移完成后将`Redo Log`、`Archive Log`保存时间设置为24小时以上），否则DTS可能因无法获取`Redo Log`、`Archive Log`而导致任务失败，极端情况下甚至可能会导致数据不一致或丢失。
7. 源库的操作限制：
> 1. 在库表结构迁移和全量迁移**阶段**，<span style="color:pink">请勿执行库或表结构变更的`DDL`操作</span>，否则数据迁移任务失败
> 2. 如<span style="color:pink">仅执行全量数据迁移</span>，请勿向源实例中写入新的数据，否则会导致源和目标数据不一致。为实时保持数据一致性，建议选择结构迁移、全量数据迁移和增量数据迁移。（<span style="color:green">**我们考虑：全量数据迁移和增量数据迁移**</span>）

## 3.2、 其他限制

1. 执行数据迁移前需评估源库和目标库的性能，同时建议业务低峰期执行数据迁移。否则全量数据迁移时DTS占用源和目标库一定读写资源，可能会导致数据库的负载上升。
2. `DTS`会尝试恢复七天之内迁移失败任务。因此业务切换至目标实例前，<span style="color:pink">请务必结束或释放该任务，或者将DTS访问目标实例账号的写权限用revoke命令回收掉</span>。避免该任务被自动恢复后，源端数据覆盖目标实例的数据。
3. 若目标库的DDL写入失败，DTS任务会继续运行，您需要在任务日志中查看执行失败的DDL。查看任务日志的方法，请参见[查询任务日志](https://help.aliyun.com/zh/dts/user-guide/view-task-logs-1?spm=a2c4g.11186623.0.i12)。
4. 需确保源库和目标库的<span style="color:pink">字符集兼容</span>，否则可能会导致数据不一致或任务失败。
5. <span style="color:pink">建议使用DTS的库表结构迁移功能</span>，否则可能会因为数据类型不兼容而导致任务失败。
6. 源库和目标库的<span style="color:pink">时区必须保持一致</span>。
7. 若您将列名仅大小写不同的字段写入到目标MySQL数据库的同一个表中，<span style="color:pink">可能会因为MySQL数据库列名大小写不敏感</span>，导致迁移结果不符合预期。
8. 在数据迁移完成后，建议使用analyze table <表名>命令检查数据是否已写入目标表。例如，在MySQL触发HA切换机制后，可能会导致数据只写到了内存，从而造成数据丢失。

## 3.3、 特殊情况

1. RDS MySQL实例对表名的英文大小写不敏感，如果使用大写英文建表，RDS MySQL会先把表名转为小写再执行建表操作。 

> 如果源Oracle数据库中存在表名相同仅大小写不同的表，可能会导致迁移对象重名并在结构迁移中提示“对象已经存在”。如果出现这种情况，请在配置迁移对象的时候，使用DTS提供的对象名映射功能对重名的对象进行重命名，将表名转为大写，详情请参见[库表列映射](https://help.aliyun.com/zh/dts/user-guide/object-name-mapping?spm=a2c4g.11186623.0.i12#concept-610481)。

2. DTS会自动在RDS MySQL中创建数据库，如果待迁移的数据库名称不符合RDS MySQL的定义规范，您需要在配置迁移任务之前在RDS MySQL中创建数据库。相关操作，请参见[管理数据库](https://help.aliyun.com/zh/rds/apsaradb-rds-for-mysql/create-a-database-for-an-apsaradb-rds-for-mysql-instance?spm=a2c4g.11186623.0.i12#concept-cg3-ljq-wdb)。


# 4、迁移类型

&emsp;&emsp;全量数据迁移 + 增量数据迁移

## 4.1、支持增量迁移的SQL操作

1. DML：INSERT、UPDATE、DELETE
2. DDL
> 1. CREATE TABLE (表内定义不能包含函数。)
> 2. ALTER TABLE、ADD COLUMN、DROP COLUMN、RENAME COLUMN、ADD INDEX
> 3. DROP TABLE
> 4. RENAME TABLE、TRUNCATE TABLE、CREATE INDEX (仅支持迁移当前数据库账号下的CREATE INDEX操作。)

# 5、<span style="color:red">数据类型映射关系</span>

&emsp;&emsp;详情请参见[异构数据库间的数据类型映射关系](https://help.aliyun.com/zh/dts/user-guide/data-type-mappings-between-heterogeneous-databases?spm=a2c4g.11186623.0.i12#concept-1813831)。

## 5.1、待迁移的表



# 6、迁移步骤

&emsp;&emsp;请参考[具体的迁移指引](https://help.aliyun.com/zh/dts/user-guide/migrate-data-from-a-self-managed-oracle-database-to-an-apsaradb-rds-for-mysql-instance-1?spm=a2c4g.11174283.0.0.3077656aSz1bYE#section-les-736-q3v)
