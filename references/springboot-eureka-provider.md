# 参考文献

# 1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-eureka-provider</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>

</project>
```

# 2、yaml配置文件

&emsp;&emsp;想要启动多个服务提供者，可以通过拷贝 yaml 文件，通过指定不同的 VM 参数：`-Dspring.profiles.active=dev1`来激活不同的 yaml 文件

1. application-dev1.yaml

```yaml
server:
  port: 9201

spring:
  application:
    # 此实例注册到 eureka 服务端的name
    name: shiro-eureka-provider

eureka:
  client:
    serviceUrl:
      # eureka服务端提供的注册地址 参考服务端配置的这个路径
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID (不同的微服务，需要修改)
    instance-id: shiro-eureka-provider-1
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户需要多长时间发送心跳给eureka服务器，表明它仍然活着,默认为30 秒 (与下面配置的单位都是秒)
    leaseRenewalIntervalInSeconds: 10
    # eureka服务器在接收到实例的最后一次发出的心跳后，需要等待多久才可以将此实例删除，默认为90秒
    leaseExpirationDurationInSeconds: 30

```

2. application-dev2.yaml

```yaml
server:
  port: 9202

spring:
  application:
    # 此实例注册到 eureka 服务端的name
    name: shiro-eureka-provider

eureka:
  client:
    serviceUrl:
      # eureka服务端提供的注册地址 参考服务端配置的这个路径
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID
    instance-id: shiro-eureka-provider-2
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户需要多长时间发送心跳给eureka服务器，表明它仍然活着,默认为30 秒 (与下面配置的单位都是秒)
    leaseRenewalIntervalInSeconds: 10
    # eureka服务器在接收到实例的最后一次发出的心跳后，需要等待多久才可以将此实例删除，默认为90秒
    leaseExpirationDurationInSeconds: 30

```

# 3、启动类配置

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
public class ShiroEurekaProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroEurekaProviderApplication.class);
    }
}
```

# 4、启动并查看注册中心

&emsp;&emsp;启动两个服务提供者

![启动两个服务提供者](./pictures/springboot-eureka-provider/img.png)

&emsp;&emsp;查看eureka 注册中心

![查看eureka 注册中心](./pictures/springboot-eureka-provider/img_1.png)