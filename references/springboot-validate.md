# 参考文献

* [SpringBoot的参数校验之Spring Validation](https://segmentfault.com/a/1190000041836423)

# 1、pom.xml

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-validation</artifactId>
</dependency>
```

&emsp;&emsp;通过上面的方式，实际引入如下的 `gav`

```xml
<dependency>  
    <groupId>org.hibernate</groupId>  
    <artifactId>hibernate-validator</artifactId>  
    <version>6.2.3.Final</version>  
</dependency>
```

# 2、使用方式

## 2.1、声明`DTO`，并加入限制注解

```java
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class UserDTO {
  private Long userId;

  @NotNull
  @Length(min = 2,max = 10)
  private String userName;

  @NotNull
  @Length(min = 6,max = 20)
  private String account;

  @NotNull
  @Length(min = 6,max = 20)
  private String password;

  public Long getUserId() {
      return userId;
  }

  public void setUserId(Long userId) {
      this.userId = userId;
  }

  public String getUserName() {
      return userName;
  }

  public void setUserName(String userName) {
      this.userName = userName;
  }

  public String getAccount() {
      return account;
  }

  public void setAccount(String account) {
      this.account = account;
  }

  public String getPassword() {
      return password;
  }

  public void setPassword(String password) {
      this.password = password;
  }
}
```

## 2.2、在方法参数上声明校验注解

&emsp;&emsp;这种情况下，使用`@Valid`和`@Validated`都可以。

```java
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {

  @PostMapping("/save")
  public Map<String,Object> saveUser(@RequestBody @Validated UserDTO userDTO){
      Map<String,Object> res = new HashMap();
      res.put("code","200");
      res.put("message","ok");
      return res;
  }
}
```

# 3、异常处理

## 3.1、统一异常处理

&emsp;&emsp;校验失败会抛出`MethodArgumentNotValidException`或者`ConstraintViolationException`异常，我们需要通过统一异常处理来返回一个更友好的报错。

```java
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class CommonExceptionHandler {

  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseStatus(HttpStatus.OK)
  public Map<String,Object> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
      BindingResult bindingResult = ex.getBindingResult();
      StringBuilder sb = new StringBuilder("校验失败:");
      for (FieldError fieldError : bindingResult.getFieldErrors()) {
          sb.append(fieldError.getField()).append("：").append(fieldError.getDefaultMessage()).append(", ");
      }
      String msg = sb.toString();
      Map<String,Object> err = new HashMap<>();
      err.put("code","-200");
      err.put("message",msg);
      return err;
  }

  @ExceptionHandler({ConstraintViolationException.class})
  @ResponseStatus(HttpStatus.OK)
  public Map<String,Object> handleConstraintViolationException(ConstraintViolationException ex) {
      Map<String,Object> err = new HashMap<>();
      err.put("code","-200");
      err.put("message",ex.getMessage());
      return err;
  }

}
```

## 3.2、利用方法入参`BindingResult`来处理

```java
    @PostMapping("/auth")
    public Result<UserInfo> eauth(@Parameter(description = "表单实体") @RequestBody
            @Valid AuthenticationForm param, BindingResult result) throws Exception {
        if (result.hasErrors()) {
            String code = result.getAllErrors().get(0).getDefaultMessage();
            throw new XxxException(ErrorCodeEnum.getEnum(Integer.parseInt(code)));
        }

        return Result.success(authenticationService.enterpriseEmployeesAuth(param));
    }
```

# 4、进阶使用

## 4.1、分组校验

&emsp;&emsp;在实际项目中，可能多个方法需要使用同一个`DTO`类来接收参数，而不同方法的校验规则很可能是不一样的。这个时候，
简单地在`DTO`类的字段上加约束注解无法解决这个问题。因此，`spring-validation`支持了分组校验的功能，专门用来解决这类问题。还是上面的例子，
比如保存`User`的时候，`UserId`是可空的，但是更新`User`的时候，`UserId`的值必须`>=10000000000000000L`；
其它字段的校验规则在两种情况下一样。这个时候使用分组校验的代码示例如下：

&emsp;&emsp;约束注解上声明适用的分组信息`groups`

```java
public class UserDTO {  

  @Min(value = 10000000000000000L, groups = Update.class)  
  private Long userId;  

  @NotNull(groups = {Save.class, Update.class})  
  @Length(min = 2, max = 10, groups = {Save.class, Update.class})  
  private String userName;  

  @NotNull(groups = {Save.class, Update.class})  
  @Length(min = 6, max = 20, groups = {Save.class, Update.class})  
  private String account;  

  @NotNull(groups = {Save.class, Update.class})  
  @Length(min = 6, max = 20, groups = {Save.class, Update.class})  
  private String password;  

  public interface Save {  
  }  

  public interface Update {  
  }  
}
```

&emsp;&emsp;`@Validated`注解上指定校验分组

```java
@PostMapping("/save")  
public Map<String,Object> saveUser(@RequestBody @Validated(UserDTO.Save.class) UserDTO userDTO) {  
        Map<String,Object> res = new HashMap();
      res.put("code","200");
      res.put("message","ok");
      return res; 
}  

@PostMapping("/update")  
public Map<String,Object> updateUser(@RequestBody @Validated(UserDTO.Update.class) UserDTO userDTO) {  
       Map<String,Object> res = new HashMap();
      res.put("code","200");
      res.put("message","ok");
      return res;
}
```

# 5、常用正则表达式

* 1、二代身份证正则表达式校验

```shell
^[1-9]\d{5}(18|19|20)\d{2}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])\d{3}[\dxX]$
```
> * ^[1-9]：以非零开头
> * \d{5}：后面跟着5个数字（地区码）
> * (18|19|20)\d{2}：接下来是年份，可以是18、19或20**开头**的4位数字
> * (0[1-9]|1[0-2])：月份，01到12
> * (0[1-9]|[1-2][0-9]|3[0-1])：日期，01到31
> * \d{3}：顺序码，一般是三位数字
> * [\dxX]：校验码，可以是数字0-9或者字母X（大写或小写）

* 2、二代身份证正则表达式校验

```shell
^1[3-9]\d{9}$
```
> * ^ 表示字符串的开始
 > * 1 表示手机号码以1开头
 > * [3-9] 表示第二位可以是3到9之间的任意数字
 > * \d{9} 表示后面跟着9个数字
 > * $ 表示字符串的结尾
 


