# 参考文献
* [Springboot集成Junit4](https://blog.csdn.net/weixin_45433031/article/details/128658764)
* [junit官网](https://junit.org/junit5/)


# 一、什么是单元测试
## 1.1、单元测试环节
&emsp;&emsp;测试过程按照阶段划分分为：单元测试、集成测试、系统测试、验收测试等。相关含义如下：
1. 单元测试： 针对计算机程序模块进行输出正确性检验工作。
2. 集成测试： 在单元测试基础上，整合各个模块组成子系统，进行集成测试。
3. 系统测试： 将整个交付所涉及的协作内容都纳入其中考虑，包含计算机硬件、软件、接口、操作等等一系列作为一个整体，检验是否满足软件或需求说明。
4. 验收测试： 在交付或者发布之前对所做的工作进行测试检验。

&emsp;&emsp;单元测试是阶段性测试的首要环节，也是白盒测试的一种，该内容的编写与实践可以前置在研发完成，
研发在编写业务代码的时候就需要生成对应代码的单元测试。单元测试的发起人是程序设计者，
受益人也是编写程序的人，所以对于程序员，非常有必要形成自我约束力，完成基本的<span style="color:red">单元测试用例</span>编写。

## 1.2、单元测试环节单元测试特征
&emsp;&emsp;由上可知，单元测试其实是针对软件中最小的测试单元来进行验证的。
这里的单元就是指相关的功能子集，比如一个方法、一个类等。值得注意的是作为最低级别的测试活动，单元测试验证的对象仅限于当前测试内容，
与程序其它部分内容相隔离，总结起来单元测试有以下特征：
1. 主要功能是证明编写的代码内容与期望输出一致。
2. 最小最低级的测试内容，由程序员自身发起，保证程序基本组件正常。
3. 单元测试尽量不要区分类与方法，主张以过程性的方法为测试单位，简单实用高效为目标。
4. 不要偏离主题，专注于测试一小块的代码，保证基础功能。
5. 剥离与外部接口、存储之间的依赖，使单元测试可控。
6. 任何时间任何顺序执行单元测试都需要是成功的。

# 二、为什么要单元测试
## 2.1、单元测试意义
&emsp;&emsp;程序代码都是由基本单元不断组合成复杂的系统，底层基本单元都无法保证输入输出正确性，
层级递增时，问题就会不断放大，直到整个系统崩溃无法使用。所以单元测试的意义就在于保证基本功能是正常可用且稳定的。
而对于**接口**、**数据源**等原因造成的不稳定因素，是**外在原因**，不在单元测试考虑范围之内。

## 2.2、使用 main 方法进行测试(费时费力)

&emsp;&emsp;假如要对下面的 Controller 进行测试
```shell
    @PostMapping(value="/save")
    public Map<String,Object> save(@RequestBody Student stu) {
        studentService.save(stu);
        Map<String,Object> params = new HashMap<>();
        params.put("code",200);
        params.put("message","保存成功");
        return params;
    }
```
&emsp;&emsp;可以编写如下的代码示例，使用 main 方法进行测试的时候，先启动整个工程应用，然后编写 main 方法如下进行访问，在单步调试代码。
```shell
    public static void main(String[] args) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String json = "{"name":"张三","className":"三年级一班","age":"20","sex":"男"}";
        HttpEntity<String> httpEntity = new HttpEntity<>(json, headers);
        String url = "http://localhost:9092/student/save";
        MainMethodTest test = new MainMethodTest();
        ResponseEntity<Map> responseEntity = test.getRestTemplate().postForEntity(url, httpEntity, Map.class);
        System.out.println(responseEntity.getBody());
    }
```

## 2.3、使用 main 方法进行测试的缺点

1. 通过编写大量的 main 方法针对每个内容做打印输出到控制台枯燥繁琐，不具备优雅性。
2. 测试方法不能一起运行，结果需要程序员自己判断正确性。
3. 统一且重复性工作应该交给工具去完成。

# 三、单元测试框架 - JUnit

## 3.1、JUnit 简介

&emsp;&emsp;[JUnit 官网](https://junit.org/junit5/)。JUnit 是一个用于编写可重复测试的简单框架。它是用于单元测试框架的 xUnit 体系结构的一个实例。

> JUnit 的特点：

1. 针对于 Java 语言特定设计的单元测试框架，使用非常广泛。
2. 特定领域的标准测试框架。
3. 能够在多种 IDE 开发平台使用，包含 Idea、Eclipse 中进行集成。
4. 能够方便由 Maven 引入使用。
5. 可以方便的编写单元测试代码，查看测试结果等。

> JUnit 的重要概念

| 名称         | 功能作用                     |
|:------------|:-----------------------------|
| Assert      | 断言方法集合                   |
| TestCase    | 表示一个测试案例                |
| TestSuite   | 包含一组 TestCase，构成一组测试  |
| TestResult  | 收集测试结果                   |

> JUnit 的一些注意事项及规范

1. 测试<span style="color:red">**方法**</span>必须使用 <span style="color:red">@Test</span> 修饰
2. 测试方法必须使用 **public void** 进行修饰，不能带参数
3. 测试代码的包**应该**和被测试代码包结构保持一致
4. 测试单元中的每个方法必须可以独立测试，方法间不能有任何依赖
5. 测试**类**一般使用 Test 作为类名的**后缀**
6. 测试**方法**使一般用 test 作为方法名的**前缀**

> JUnit 失败结果说明

1. Failure：测试结果和预期**结果不一致**导致，表示测试不通过
2. error：由**异常代码**引起，它可以产生于测试代码本身的错误，也可以是被测代码的 Bug

## 3.2、JUnit 内容

### 3.2.1、断言 API

| 方法                                                                          | 描述                              |
|:-----------------------------------------------------------------------------|:----------------------------------|
| assertNull(String message, Object object)                                    | 检查对象是否为空，不为空报错           |
| assertNotNull(String message, Object object)	                            | 检查对象是否不为空，为空报错            |
| assertEquals(String message, Object expected, Object actual)	                | 检查对象值是否相等，不相等报错          |
| assertTrue(String message, boolean condition)	                            | 检查条件是否为真，不为真报错            |
| assertFalse(String message, boolean condition)		                        | 检查条件是否为假，为真报错              |
| assertSame(String message, Object expected, Object actual)			        | 检查对象引用是否相等，不相等报错         |
| assertNotSame(String message, Object unexpected, Object actual)				| 检查对象引用是否不等，相等报错           |
| assertArrayEquals(String message, Object[] expecteds, Object[] actuals)	    | 检查数组值是否相等，遍历比较，不相等报错   |
| assertArrayEquals(String message, Object[] expecteds, Object[] actuals)	    | 检查数组值是否相等，遍历比较，不相等报错   |
| assertThat(String reason, T actual, Matcher<? super T> matcher)	            | 检查对象是否满足给定规则，不满足报错      | 


### 3.2.2、 JUnit 常用注解

1. @Test: 定义一个测试方法 @Test (excepted=xx.class): xx.class 表示异常类，表示测试的方法抛出此异常时，认为是正常的测试通过的 @Test (timeout = 毫秒数) : 测试方法执行时间是否符合预期。
2. @BeforeClass： 在所有的方法执行前被执行，static 方法全局只会执行一次，而且第一个运行。
3. @AfterClass：在所有的方法执行之后进行执行，static 方法全局只会执行一次，最后一个运行。
4. @Before：在**每一个测试方法**被运行**前**执行一次。
5. @After：在**每一个测试方法**运行**后**被执行一次。
6. @Ignore：所修饰的测试方法会被测试运行器忽略。
7. @RunWith：可以更改测试执行器使用 junit 测试执行器。

## 3.3、JUnit的 使用

### 3.3.1、Controller 层单元测试

> 引入jar包

```shell
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
</dependency>

<!-- spring-boot-starter-test的版本号被springboot 父工程管理了 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
</dependency>
```

> 在2.2章节中的main测试方法可以被如下方法替换

```java
@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlayWayControllerTest {

    // 注入Spring容器
    @Autowired
    private WebApplicationContext applicationContext;

    // 模拟Http请求
    private MockMvc mockMvc;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Before
    public void setupMockMvc() {
        // 初始化MockMvc对象
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

    @Before
    public void setupMockMvc() {
        // 初始化MockMvc对象
        mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).build();
    }

    /**
     * 查询单个测试
     * @throws Exception
     */
    @Test
    public void selectOneTest() throws Exception {
        String json = "{\"id\": 2}";
        mockMvc.perform(MockMvcRequestBuilders.post("/selectOne")    //构造一个post请求
                        // 发送端和接收端数据格式
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .accept(MediaType.APPLICATION_JSON_UTF8)
                        .content(json.getBytes()))
                // 断言校验返回的code编码
                .andExpect(MockMvcResultMatchers.status().isOk())
                // 添加处理器打印返回结果
                .andDo(MockMvcResultHandlers.print());
    }
}
```

![controller测试结果](./pictures/junit/img.png)

> 本案例中构造 mockMVC 对象时，也可以使用如下方式：

```shell
    @Before
    public void setupMockMvc() {
        // 初始化MockMvc对象
        mockMvc = MockMvcBuilders.standaloneSetup(user4FlayWayController).build();
    }
```

> 其中 MockMVC 是 **Spring** 测试框架提供的用于 **REST** 请求的工具，是对 Http 请求的模拟，
> 无需启动整个模块就可以对 Controller 层进行调用，速度快且**不依赖**网络环境。

> 使用 MockMVC 的基本步骤如下：
1. mockMvc.perform 执行请求
2. MockMvcRequestBuilders.post 或 get 构造请求
3. MockHttpServletRequestBuilder.param 或 content 添加请求参数
4. MockMvcRequestBuilders.contentType 添加请求类型
5. MockMvcRequestBuilders.accept 添加响应类型
6. ResultActions.andExpect 添加结果断言
7. ResultActions.andDo 添加返回结果后置处理
8. ResultActions.andReturn 执行完成后返回相应结果

### 3.3.2、Service 层单元测试
> Service 层selectOne方法进行单测

```java
@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlayWayServiceTest {

    @Autowired
    private User4FlayWayService user4FlayWayService;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Test
    public void selectOneTest() {
        User4FlaywayDOCustomParam param = new User4FlaywayDOCustomParam();
        param.setId(2L);
        User4FlayWayModel user4FlayWayModel = user4FlayWayService.selectOne(param);
        System.out.println(JSON.toJSONString(user4FlayWayModel));
    }
}
```

### 3.3.3、Dao 层单元测试

> 对 Dao 层方法进行单测

```java
@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class User4FlaywayDOMapperTest {

    @Autowired
    private User4FlaywayDOMapper user4FlaywayDOMapper;

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Test
    public void testSelectOne() {
        User4FlaywayDOExample example = new User4FlaywayDOExample();
        User4FlaywayDOExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(1L);
        List<User4FlaywayDO> user4FlaywayDOS = user4FlaywayDOMapper.selectByExample(example);
        System.out.println(user4FlaywayDOS);
    }
}
```

> 如何保证数据新增/修改后，数据能够被还原
```shell

    @Test
    @Transactional
    @Rollback(value = true)
    public void testAddOne() {
        List<User4FlaywayDO> user4FlaywayDOS = user4FlaywayDOMapper.selectByExample(new User4FlaywayDOExample());
        System.out.println("*************************************");
        System.out.println("原始数据个数：" + user4FlaywayDOS.size());
        System.out.println("*************************************");

        User4FlaywayDO record = new User4FlaywayDO();
        record.setId(5L);
        record.setName("tom");
        int size = user4FlaywayDOMapper.insertSelective(record);
        System.out.println("*************************************");
        System.out.println("影响的行数：" + size);
        System.out.println("*************************************");
    }
```

### 3.3.4、异常测试

```shell
    @Test(expected = ArithmeticException.class)
    public void exceptionTest() {
        int i = 0;
        int j = 9;
        int result = j / i;
        System.out.println(result);
    }
```

### 3.3.5、<span style="color:red">测试套件测多个类</span>

1. 新建一个**空的**单元测试类
2. 利用注解 @RunWith (Suite.class) 和 @SuiteClasses 标明要一起单元测试的类

```java
@RunWith(Suite.class)
@Suite.SuiteClasses({
        User4FlayWayBizServiceTest.class,
        User4FlayWayControllerTest.class,
        User4FlaywayDOMapperTest.class,
        User4FlayWayServiceTest.class
})
public class User4FlayWaySuit {

}
```

### 3.3.6、idea 中查看单元测试覆盖率
&emsp;&emsp;测试覆盖率是衡量测试过程工作本身的有效性，提升测试效率和减少程序 bug，提升产品可靠性与稳定性的指标。

> 统计单元测试覆盖率的意义：

1. 可以洞察整个代码中的基础组件功能的所有盲点，发现相关问题。
2. 提高代码质量，通常覆盖率低表示代码质量也不会太高，因为单测不通过本来就映射出考虑到各种情况不够充分。
3. 从覆盖率的达标上可以提高代码的设计能力。

> idea 中如何查看单元测试覆盖率
* 在单元测试方法或类上右键 Run 'xxx' with Coverage
![右击查看](./pictures/junit/img_1.png)

* 点击debug右侧的按钮
![debug右侧按钮](./pictures/junit/img_2.png)

> 执行结果是一个表格，列出了类、方法、行数、分支覆盖情况。

![覆盖率结果](./pictures/junit/img_3.png)

> 导出单侧结果

![导出单侧结果](./pictures/junit/img_4.png)

# 四、单元测试工具 - Mockito

## 4.1、简介
&emsp;&emsp;在单元测试过程中主张不要依赖特定的接口与数据来源，此时就涉及到对相关数据的模拟，比如 Http 和 JDBC 的返回结果等，
可以使用虚拟对象即 Mock 对象进行模拟，使得单元测试不在耦合。

> Mock 过程的使用前提:

1. 实际对象**很难**被构造出来
2. 实际对象的特定行为**很难**被触发
3. 实际对象可能当前还不存在，比如依赖的接口还没有开发完成等等。

&emsp;&emsp;Mockito 官网：https://site.mockito.org 。Mockito 和 JUnit 一样是专门针对 Java 语言的 mock 数据框架，
它与同类的 EasyMock 和 jMock 功能非常相似，但是该工具更加简单易用。

> Mockito 的特点：

1. 可以模拟类不仅仅是接口
2. 通过注解方式简单易懂
3. 支持顺序验证
4. 具备参数匹配器

## 4.2、Mockito 使用

> maven 引入 spring-boot-starter-test 会自动将 mockito 引入到工程中

### 4.2.1、简单案例
* 在之前的代码中在定义一个 BookService 接口（订购书本的服务,**无实现**）

```java
public interface BookService {

    /**
     * 根据书名来预定书本
     * @param bookName
     * @return
     */
    BookModel orderBook(String bookName);
}

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class MockThirdInterfaceTest {

    /**
     * 模拟jvm 参数
     */
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @MockBean
    private BookService bookService;

    @Test
    public void orderBookTest() {
        BookModel bookModel = new BookModel();
        bookModel.setBookName("西游记");
        bookModel.setId("001");
        bookModel.setLocation("图书馆1好楼-四大名著-15排");

        Mockito.when(bookService.orderBook(anyString())).thenReturn(bookModel);
        BookModel result = bookService.orderBook("");
        System.out.println("测试结果：" + JSON.toJSONString(result));

        Assert.assertTrue("预定数据不符合", result.equals(bookModel));
    }
}
```

> 注意: Mock、Spy、@MockBean、@SpyBean 之间的区别

> mockito 定义的注解主要有三个

* @Mock
* @Spy
* @InjectMocks

```java

@SpringBootTest(classes = {CourseApplication.class})
@RunWith(SpringRunner.class)
public class MockInjectInterfaceTest {

    // 模拟jvm 参数
    static {
        // -Djasypt.encryptor.password=wb-csd876509
        System.getProperties().setProperty("jasypt.encryptor.password", "wb-csd876509");
    }

    @Mock
    private BookService bookService;

    @InjectMocks // 注意不许是一个类，不能是接口
    private StudentServiceImpl studentService;

    @Test
    public void orderBookTest() {
        BookModel bookModel = new BookModel();
        bookModel.setBookName("西游记");
        bookModel.setId("001");
        bookModel.setLocation("图书馆1好楼-四大名著-15排");

        Mockito.when(bookService.orderBook(anyString())).thenReturn(bookModel);
        BookModel result = studentService.orderBook("");
        System.out.println("测试结果：" + JSON.toJSONString(result));

        Assert.assertTrue("预定数据不符合", result.equals(bookModel));
    }
}
```
> **@Spy** 和 **@Mock** 生成的对象**不受**spring管理, 也**不会替换**Spring对应类型的bean

> @InjectMocks 用于自动注入 **@Spy**和**@Mock**标注的对象

&emsp;&emsp;<span style="color:red">**Mock和Spy异同**</span>

| 注解    | 描述                                                                                         |
|:------|:-------------------------------------------------------------------------------------------|
| @Mock | Mock是将目标对象整个模拟 ，所有方法默认都返回null，并且**原方法**中的代码逻辑**不会**执行，被Mock出来的对象，想用哪个方法，哪个方法就需要打桩，否则返回null |
| @Spy  | Spy可实现对目标对象部分方法、特定入参条件时的打桩，没有被打桩的方法，将会**真实调用**                                             |

> Spring 定义的注解主要有两个

* @MockBean
* @SpyBean

> 代码如： 4.2.1、简单案例

@MockBean 和 @SpyBean 生成的对象受spring管理，相当于<span style="color:red">**自动替换对应类型bean**</span>的注入

### 4.2.2、相关语法

> 常用api

&emsp;&emsp;上述案例中用到了 mockito 的 when、any、theWhen 等语法。接下来介绍下都有哪些常用的 API：

| 注解               | 描述                                    |
|:-----------------|:--------------------------------------|
| mock             | 模拟一个需要的对象                             |
| when             | 一般配合 thenXXX 一起使用，表示当执行什么操作之后怎样       |
| anyString        | 返回一个特定对象的缺省值，上例中标识可以填写任何 String 类型的数据 |
| theReturn        | 在执行特定操作后返回指定结果                        |
| spy              | 创造一个监控对象                              |
| verify           | 验证特定的行为                               |
| doReturn         | 返回结果                                  |
| doThrow          | 抛出特定异常                                |
| doAnswer         | 做一个自定义响应                              |
| times            | 操作执行次数                                |
| atLeastOnce      | 操作至少要执行一次                             |
| atLeast          | 操作至少执行指定的次数                           |
| atMost           | 操作至多执行指定的次数                           |
| atMostOnce       | 操作至多执行一次                              |
| doNothing        | 不做任何的处理                               |
| doReturn         | 返回一个结果                                |
| doThrow          | 抛出一个指定异常                              |
| doAnswer         | 指定一个特定操作                              |
| doCallRealMethod | 用于监控对象返回一个真实结果                        |

