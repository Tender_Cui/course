# 参考文献

* [尚硅谷2024最新SpringCloud教程，springcloud从入门到大牛](https://www.bilibili.com/video/BV1gW421P7RD/)
* [官网文档：spring-cloud-gateway(3.1.9)](https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-starter)

# 示例代码

&emsp;&emsp;[请参考项目：Tender_Cui/shiro](https://gitee.com/Tender_Cui/shiro) 下的模块：`shiro-framework-integration/shiro-gateway`

# 1、简单的后端微服务接口

&emsp;&emsp;后端微服务接口测试，<span style="color:red">证明ok</span>。

&emsp;&emsp;[请参考项目：Tender_Cui/shiro](https://gitee.com/Tender_Cui/shiro) 下的模块：`shiro-framework-integration/shiro-gateway-backend-server`

```shell
curl "http://localhost:9099/backend/service"
```

&emsp;&emsp;接口ok

![后端微服务接口测试](./pictures/springboot-gateway/img.png)

# 2、pom.xml 

&emsp;&emsp;`spring cloud gateway` 整合网关。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-gateway</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!--
        从 Spring Cloud 2020.0.0 版本开始，Ribbon 被废弃，Spring Cloud LoadBalancer 成为了推荐的负载均衡方案。
        在这个版本变动中，为了提供更大的灵活性，spring-cloud-starter-loadbalancer 被标记为了可选依赖，不再默认包含在 Spring Cloud Gateway 中。
        因此，在使用 3.1.4 版本的 Spring Cloud Gateway 并需要服务发现和负载均衡功能时，如果没有显式包含这个依赖，就会导致无法处理 lb://URI，从而返回503错误。
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-gateway</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
    </dependencies>
</project>
```

# 2、bootstrap.yaml

&emsp;&emsp;工程最终`yaml`示例配置，文章后面`【4、网关配置逐一讲解】`会逐一实践、讲解每一行配置，并附上结果截图。

```yaml
server:
  port: 9093

spring:
  application:
    name: shiro-gateway

  cloud:
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP

    gateway:
      routes:
        - id: backend_service_id                   # 路由ID, 要求唯一
          # uri: http://localhost:9099             # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
          uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
                                                   # 注意，需要引入 <<spring-cloud-starter-loadbalancer>>

          # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
          predicates:
#            - Path=/backend/service/**              # 断言，路径匹配后进行路由
            - Path=/tender/{segment}/**              # 断言，路径匹配后进行路由
#            - Path=/service/**              # 断言，路径匹配后进行路由（配合 PrefixPath 使用）
#            - After=2024-04-24T16:42:55.921+08:00[Asia/Shanghai]   # 断言，某个时间后，才能访问后端服务
#            - Before=2024-03-19T21:26:00.000+08:00[Asia/Shanghai]  # 断言，某个时间前，才能访问后端服务
#            - Between=2024-03-19T21:29:00.000+08:00[Asia/Shanghai],2024-03-19T21:30:00.000+08:00[Asia/Shanghai]  # 两个时间之间才能访问
            - Header=X-Request-Id, \d+    # 断言，请求头中的 key=X-Request-Id，值要满足正则表达式 \d+
            # 请求命令示例：curl "http://localhost:9093/backend/service?userLevel=gold" --cookie "username=tender" -H "X-Request-Id:123"
            - Cookie=username, tender        # 断言，带着cookie，cookie的名称为username，值为 tender
            # 1. 自定义 predicate 的 Shortcut Configuration 风格配置格式
#            - Custom=gold                 # 断言，自定义断言，值为gold
            # 或者如下这种配置方式：
            # 2. 自定义 predicate 的 Fully Expanded Arguments 风格配置格式
            # 官网配置 Fully Expanded Arguments 参考文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#fully-expanded-arguments
            - name: Custom
              args:
                userLevel: gold
          # 官网 filter 配置相关地址：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gatewayfilter-factories
          filters:
            - CustomSingle=tender
#            - RedirectTo=302, https://baidu.com
            - SetPath=/backend/{segment}      # 本来请求路径是 http://localhost:9093/tender/service
                                              # 通过 SetPath=/backend/{segment} 把 uri 替换成了/backend/service，{segment} 这个代表占位符，传什么，就接受什么
            # 请求路径变为 curl "http://localhost:9093/service?userLevel=gold&tender=bruce" --cookie "username=tender" -H "X-Request-Id:123"
            # 这样最终的路径为：http://localhost:9093/backend/service
            # 请注意，修改前面的  - Path=/service/**
#            - PrefixPath=/backend     # 路径拼接过滤器 PrefixPath + Path 组成最终的 uri
#            - AddRequestHeader=X-Request-Wong,Wong
#            - RemoveRequestHeader=sec-fetch-site
#            - AddResponseHeader=test, tender

```

# 3、启动类

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <a href="https://spring.io/projects/spring-cloud-gateway">spring-cloud-gateway官网</a>
 * <a href="https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/">spring-cloud-gateway官网参考手册</a>
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class ShiroGateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShiroGateWayApplication.class);
    }
}
```

# 4、网关配置之断言`Predicate`

## 4.1、入门级配置：`Path`断言

1. `yaml`配置

```yaml
spring:
    # 省略其余配置
    gateway:
      routes:
        - id: backend_service_id                   # 路由ID, 要求唯一
          uri: http://localhost:9099             # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)

          # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
          predicates:
            - Path=/backend/service/**              # 断言，路径匹配后进行路由
```

![入门级配置](./pictures/springboot-gateway/img_1.png)

2. 测试结果：

```shell
curl "http://localhost:9093/backend/service"
```
![入门级配置对应的测试结果](./pictures/springboot-gateway/img_2.png)


## 4.2、入门级配置，问题优化

1. `yaml`配置优化

&emsp;&emsp;将原先`uri` = `http://localhost:9099` 方式，修改为`lb://shiro-gateway-backend-server`。

```yaml
spring:
    # 省略其余配置
    gateway:
      routes:
        - id: backend_service_id                   # 路由ID, 要求唯一
#          uri: http://localhost:9099             # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
          uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
          # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
          # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
          predicates:
            - Path=/backend/service/**              # 断言，路径匹配后进行路由
```

&emsp;&emsp;引入`loadbalancer`负载均衡器

```xml
<!--
从 Spring Cloud 2020.0.0 版本开始，Ribbon 被废弃，Spring Cloud LoadBalancer 成为了推荐的负载均衡方案。
在这个版本变动中，为了提供更大的灵活性，spring-cloud-starter-loadbalancer 被标记为了可选依赖，不再默认包含在 Spring Cloud Gateway 中。
因此，在使用 3.1.4 版本的 Spring Cloud Gateway 并需要服务发现和负载均衡功能时，如果没有显式包含这个依赖，就会导致无法处理 lb://URI，从而返回503错误。
-->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-loadbalancer</artifactId>
</dependency>
```

2. 测试结果：

```shell
curl "http://localhost:9093/backend/service"
```

![入门级配置对应的测试结果2](./pictures/springboot-gateway/img_2.png)

## 4.3、断言 `After`

1. 生成`ZonedDateTime`时间

```java
package com.tender.util;

import java.time.ZonedDateTime;

public class ZonedDateTimeUtils {

    public static void main(String[] args) {
        ZonedDateTime time = ZonedDateTime.now();
        ZonedDateTime afterAddTime = time.plusMinutes(1);
        System.out.println("原始时间：" + time);
        System.out.println("原始时间 + 1 min 钟后：" + afterAddTime);
    }
}
```

2. `yaml`配置

```yaml
spring:
    # 省略其余配置
    gateway:
      routes:
        - id: backend_service_id                   # 路由ID, 要求唯一
#          uri: http://localhost:9099             # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
          uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
          # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
          # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
          predicates:
            - Path=/backend/service/**              # 断言，路径匹配后进行路由
            - After=2024-04-27T15:41:44.472+08:00[Asia/Shanghai]   # 断言，某个时间后，才能访问后端服务

```

3. 测试结果

```shell
curl "http://localhost:9093/backend/service"
```

![断言 `After`的测试结果](./pictures/springboot-gateway/img_3.png)

## 4.4、断言 `Header`

1. `yaml`配置

```yaml
spring:
    # 省略其余配置
    gateway:
      routes:
        - id: backend_service_id                   # 路由ID, 要求唯一
#          uri: http://localhost:9099             # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
          uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
          # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
          # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
          predicates:
            - Path=/backend/service/**              # 断言，路径匹配后进行路由
            - After=2024-04-27T15:41:44.472+08:00[Asia/Shanghai]   # 断言，某个时间后，才能访问后端服务
            - Header=X-Request-Id, \d+    # 断言，请求头中的 key=X-Request-Id，值要满足正则表达式 \d+

```

2. 测试结果

```shell
curl "http://localhost:9093/backend/service" -H "X-Request-Id:123"
```

![断言 `Header`的测试结果](./pictures/springboot-gateway/img_4.png)

## 4.5、断言 `Cookie`

1. `yaml`配置

```yaml
spring:
  # 省略其余配置
  gateway:
    routes:
      - id: backend_service_id                   # 路由ID, 要求唯一
        #uri: http://localhost:9099              # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
        uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
        # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
        # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
        predicates:
          - Path=/backend/service/**       # 断言，路径匹配后进行路由
          - After=2024-04-27T15:41:44.472+08:00[Asia/Shanghai]   # 断言，某个时间后，才能访问后端服务
          - Header=X-Request-Id, \d+       # 断言，请求头中的 key=X-Request-Id，值要满足正则表达式 \d+
          - Cookie=username, tender        # 断言，带着cookie，cookie的名称为username，值为 tender

```

2. 测试结果

```shell
curl "http://localhost:9093/backend/service" --cookie "username=tender" -H "X-Request-Id:123"
```

![断言 `Cookie`的测试结果](./pictures/springboot-gateway/img_5.png)

## 4.6、自定义断言

&emsp;&emsp;自定义了一个断言`Custom`，名字可以自定义。

1. `java`代码

```java
package com.tender.predicate;

import lombok.Getter;
import lombok.Setter;
import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.cloud.gateway.handler.predicate.BeforeRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.server.ServerWebExchange;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

/**
 * @see BeforeRoutePredicateFactory 可以依据这个，依葫芦画瓢
 * 自定义会员等级
 */
@Component
public class CustomRoutePredicateFactory extends AbstractRoutePredicateFactory<CustomRoutePredicateFactory.Config> {

    public CustomRoutePredicateFactory() {
        super(Config.class);
    }

    public List<String> shortcutFieldOrder() {
        return Collections.singletonList("userLevel");
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return serverWebExchange -> {
            // 请求参数中要带有 userLevel 参数
            // http://localhost:9093/backend/service?userLevel=gold
            String userLevel = serverWebExchange.getRequest().getQueryParams().getFirst("userLevel");
            if (userLevel == null) {
                return false;
            }
            return userLevel.equalsIgnoreCase(config.getUserLevel());
        };
    }

    @Getter
    @Setter
    @Validated
    public static class Config {
        /**
         * yaml 中配置的 Custom=gold， gold 会赋值给 `userLevel` 字段
         */
        @NotNull
        private String userLevel;
    }
}
```

2. `yaml`配置

> 第一种配置方式：（Shortcut Configuration 风格配置格式）

```yaml
spring:
  # 省略其余配置
  gateway:
    routes:
      - id: backend_service_id                   # 路由ID, 要求唯一
        #uri: http://localhost:9099              # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
        uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
        # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
        # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
        predicates:
          - Path=/backend/service/**       # 断言，路径匹配后进行路由
          - After=2024-04-27T15:41:44.472+08:00[Asia/Shanghai]   # 断言，某个时间后，才能访问后端服务
          - Header=X-Request-Id, \d+       # 断言，请求头中的 key=X-Request-Id，值要满足正则表达式 \d+
          - Cookie=username, tender        # 断言，带着cookie，cookie的名称为username，值为 tender
          - Custom=gold                    # 断言，自定义断言，值为gold

```

> 第二种配置方式：（Fully Expanded Arguments 风格配置格式）

```yaml
spring:
  # 省略其余配置
  gateway:
    routes:
      - id: backend_service_id                   # 路由ID, 要求唯一
        #uri: http://localhost:9099              # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
        uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
        # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
        # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
        predicates:
          - Path=/backend/service/**       # 断言，路径匹配后进行路由
          - After=2024-04-27T15:41:44.472+08:00[Asia/Shanghai]   # 断言，某个时间后，才能访问后端服务
          - Header=X-Request-Id, \d+       # 断言，请求头中的 key=X-Request-Id，值要满足正则表达式 \d+
          - Cookie=username, tender        # 断言，带着cookie，cookie的名称为username，值为 tender
#          - Custom=gold                    # 断言，自定义断言，值为gold
          - name: Custom
            args:
              userLevel: gold
```

&emsp;&emsp;[Fully Expanded Arguments 官方参考文档](https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#fully-expanded-arguments)

3. 测试结果

```shell
curl "http://localhost:9093/backend/service?userLevel=gold" --cookie "username=tender" -H "X-Request-Id:123"
```

![自定义断言的测试结果](./pictures/springboot-gateway/img_6.png)

# 5、网关配置之过滤器`Filter`

## 5.1、过滤器`PrefixPath`

1. `yaml`配置

```yaml
spring:
  # 省略其余配置
  gateway:
    routes:
      - id: backend_service_id                   # 路由ID, 要求唯一
        #uri: http://localhost:9099              # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
        uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
        # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
        # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
        predicates:
          - Path=/service/**              # 断言，路径匹配后进行路由（配合 PrefixPath 使用）
        filters:
          - PrefixPath=/backend           # 路径拼接过滤器 PrefixPath + Path 组成最终的 uri
```

2. 测试结果

```shell
curl "http://localhost:9093/service"
```

![过滤器`PrefixPath`的测试结果](./pictures/springboot-gateway/img_7.png)

## 5.2、过滤器`SetPath`

1. `yaml`配置

```yaml
spring:
  # 省略其余配置
  gateway:
    routes:
      - id: backend_service_id                   # 路由ID, 要求唯一
        #uri: http://localhost:9099              # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
        uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
        # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
        # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
        predicates:
          - Path=/tender/{segment}/**              # 断言，路径匹配后进行路由（配合 SetPath 使用）
        filters:
          # 请注意，修改前面的  - Path=/tender/{segment}/**
          - SetPath=/backend/{segment}  # 本来请求路径是 http://localhost:9093/tender/service
                                        # 通过 SetPath=/backend/{segment} 把 uri 替换成了/backend/service，{segment} 这个代表占位符，传什么，就接受什么
```

2. 测试结果

```shell
curl "http://localhost:9093/tender/service"
```

![过滤器`SetPath`的测试结果](./pictures/springboot-gateway/img_8.png)

## 5.3、自定义过滤器

1. `java`代码

```java
package com.tender.filter;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * 需求：请求参数中还有 tender 就放行
 */
@Slf4j
@Component
public class CustomSingleGatewayFilterFactory extends AbstractGatewayFilterFactory<CustomSingleGatewayFilterFactory.Config> {

    public CustomSingleGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            log.info("进入单一的过滤器 status: {}", config.getStatus());
            ServerHttpRequest request = exchange.getRequest();
            // 请求参数中还有 yaml 中配置的 tender 就放行
            // 比如：curl "http://localhost:9093/backend/service?userLevel=gold&tender=bruce" --cookie "username=tender" -H "X-Request-Id:123"
            if (request.getQueryParams().containsKey(config.getStatus())) {
                return chain.filter(exchange);
            }
            // 如果没有，就返回状态码为：400
            exchange.getResponse().setStatusCode(HttpStatus.BAD_REQUEST);
            return exchange.getResponse().setComplete();
        };
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Lists.newArrayList("status");
    }

    @Getter
    @Setter
    public static class Config {
        private String status;
    }
}
```

2. `yaml`配置

```yaml
spring:
  # 省略其余配置
  gateway:
    routes:
      - id: backend_service_id                   # 路由ID, 要求唯一
        #uri: http://localhost:9099              # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
        uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
        # 注意，需要引入`loadbalancer`负载均衡器 <<spring-cloud-starter-loadbalancer>>
        # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
        predicates:
          - Path=/backend/service/**              # 断言，路径匹配后进行路由
        filters:
          - CustomSingle=tender                  # 自定义过滤器，要求请求参数中必须要有 ?tender=xxxx，值随意，但是必须要有tender
```

3. 测试结果

```shell
curl "http://localhost:9093/backend/service?tender=bruce"
```

![自定义过滤器的测试结果](./pictures/springboot-gateway/img_9.png)


# 5、网关全局过滤器

1. `java`代码

```java
package com.tender.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class CustomGlobalFilter implements GlobalFilter, Ordered {

    public static final String START_VISIT_TIME = "startVisitTime";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        exchange.getAttributes().put(START_VISIT_TIME, System.currentTimeMillis());
        return chain.filter(exchange).then(Mono.fromRunnable(() -> {
            Long startVisitTime = exchange.getAttribute(START_VISIT_TIME);
            if (startVisitTime != null) {
                long endVisitTime = System.currentTimeMillis();
                log.info("=================================================");
                log.info("访问接口主机: {}", exchange.getRequest().getURI().getHost());
                log.info("访问接口端口: {}", exchange.getRequest().getURI().getPort());
                log.info("访问接口URL: {}", exchange.getRequest().getURI().getPath());
                log.info("访问接口参数: {}", exchange.getRequest().getURI().getRawQuery());
                log.info("访问接口耗时: {}ms", (endVisitTime - startVisitTime));
                log.info("=================================================");
            }
        }));
    }

    // 数字越小优先级越高
    @Override
    public int getOrder() {
        return -1;
    }
}

```

2. 测试结果

```html
2024-04-27 16:38:18.228  INFO 9456 --- [ctor-http-nio-1] c.t.f.CustomSingleGatewayFilterFactory   : 进入单一的过滤器 status: tender
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : =================================================
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : 访问接口主机: localhost
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : 访问接口端口: 9093
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : 访问接口URL: /backend/service
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : 访问接口参数: tender=bruce
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : 访问接口耗时: 19ms
2024-04-27 16:38:18.247  INFO 9456 --- [ctor-http-nio-1] com.tender.filter.CustomGlobalFilter     : =================================================
```



























