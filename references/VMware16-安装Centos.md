# 参考文献
* [VMware虚拟机安装（非常详细）](https://blog.csdn.net/leah126/article/details/131450225)

# 一、资源下载

> Centos（系统镜像下载后，把后缀改成iso即可正常使用）

    https://www.alipan.com/s/VMKiiUQzfpN

# 二、安装步骤

## 2.1、 创建新的虚拟机

> 相当于买电脑，先把电脑配置整好。什么cpu啊内存条 啊硬盘啊什么乱七八糟的，先不着急装系统。

![创建虚机00](./pictures/VMware_install_Centos/img.png)

> 下一步

> 可以自定义一些cpu、memory、disk啊等等

![自定义高级](./pictures/VMware_install_Centos/img_1.png)

> 下一步

![直接下一步](./pictures/VMware_install_Centos/img_2.png)

> 下一步

![稍后安装操作系统](./pictures/VMware_install_Centos/img_3.png)

> 下一步

![选择linux_Centos7](./pictures/VMware_install_Centos/img_4.png)

> 下一步

![给虚机取个别名](./pictures/VMware_install_Centos/img_5.png)

> 下一步

> win10 查看cpu核数

```shell
cmd
# 进入命令行
wmic
cpu get * 

# 直接查看cpu 核数
cpu get NumberOfCores
```
![查看cpu核数](./pictures/VMware_install_Centos/img_6.png)

![直接查看cpu核数](./pictures/VMware_install_Centos/img_7.png)

![直接查看cpu核数](./pictures/VMware_install_Centos/img_8.png)

> 下一步

> 内存给了4G，一般都够了

![内存4G](./pictures/VMware_install_Centos/img_9.png)

> 下一步

![网络默认NET](./pictures/VMware_install_Centos/img_10.png)

> 下一步

![默认LSI](./pictures/VMware_install_Centos/img_11.png)

> 下一步

![磁盘默认类型](./pictures/VMware_install_Centos/img_12.png)

> 下一步

![创建新虚拟磁盘](./pictures/VMware_install_Centos/img_13.png)

> 下一步

![最大磁盘大小](./pictures/VMware_install_Centos/img_14.png)

> 下一步

![指定磁盘文件](./pictures/VMware_install_Centos/img_15.png)

> 下一步

![已准备创建虚机](./pictures/VMware_install_Centos/img_16.png)


## 2.2、 安装linux操作系统

> 温馨提示：安装操作系统的过程中如果想从虚拟机切换到正常桌面可以按 <span style="color:red">**Ctrl+Alt**</span>

![编辑虚拟机设置](./pictures/VMware_install_Centos/img_17.png)

> 下一步

![使用ISO映像文件](./pictures/VMware_install_Centos/img_18.png)

> 下一步

![开启此虚拟机](./pictures/VMware_install_Centos/img_19.png)

> 下一步

![开始安装](./pictures/VMware_install_Centos/img_20.png)

> 下一步

> 选择 English -> United States

![选择英文](./pictures/VMware_install_Centos/img_21.png)

> 下一步

![最小安装01](./pictures/VMware_install_Centos/img_22.png)
![最小安装02](./pictures/VMware_install_Centos/img_23.png)

> 下一步

> 不启用 KDUMP，启用会占用内存

![不启用 KDUMP01](./pictures/VMware_install_Centos/img_24.png)
![不启用 KDUMP02](./pictures/VMware_install_Centos/img_25.png)

> 下一步

> 安装位置这一项，点进去，然后直接点完成出来即可。不这么操作不让你安装。

![安装位置](./pictures/VMware_install_Centos/img_26.png)

> 下一步

> 网络连接，不开启上不了网

![网络连接01](./pictures/VMware_install_Centos/img_27.png)
![网络连接02](./pictures/VMware_install_Centos/img_28.png)

> 开始安装

![开始安装](./pictures/VMware_install_Centos/img_29.png)

> 设置root密码

> 88888888

![设置root密码01](./pictures/VMware_install_Centos/img_30.png)
![设置root密码02](./pictures/VMware_install_Centos/img_31.png)

> 安装完后**重启**

![安装完后重启](./pictures/VMware_install_Centos/img_32.png)

> 重启后登录

![重启后登录](./pictures/VMware_install_Centos/img_33.png)