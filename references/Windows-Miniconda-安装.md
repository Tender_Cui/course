# 参考文献

* [清华镜像下载地址](https://mirrors.tuna.tsinghua.edu.cn/)
* [清华Miniconda镜像下载地址](https://mirrors.tuna.tsinghua.edu.cn/anaconda/miniconda/)

# 一、下载

&emsp;&emsp;省略

# 二、安装

&emsp;&emsp;省略

# 三、环境配置

```shell
D:\A_Tender_workspace\install_4_code\mini_conda\install
D:\A_Tender_workspace\install_4_code\mini_conda\install\Library\bin
D:\A_Tender_workspace\install_4_code\mini_conda\install\Scripts
```

# 四、验证

```shell
# 查看版本
conda --version

# 结果
conda 25.1.1
```

# 五、添加镜像仓库

&emsp;&emsp;由于 `anaconda` 的仓库在国外，这里先添加清华的镜像仓库

```shell
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
conda config --set show_channel_urls false
```

