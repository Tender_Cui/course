# 参考文献

# 问题

## 1.1、问题一：页面摆平，页面不展示

&emsp;&emsp;使用如下地址的时候，作为 `iframe` 的 `src` 来引入别的模块的页面，发现页面<span style="color:red">一直白屏，不显示</span>。

```html
<iframe data-v-8ee87168="" id="ExternalLink" name="ExternalLink" width="100%" height="100%" frameborder="0" scrolling="yes" src="https://new.ytedi.com/wlview?token=bearer eyJ0eXAiOiJKxxx...">
    
</iframe>
```

&emsp;&emsp;问题解决：

```html
<iframe data-v-8ee87168="" id="ExternalLink" name="ExternalLink" width="100%" height="100%" frameborder="0" scrolling="yes" src="https://new.ytedi.com/wlview/?token=bearer eyJ0eXAiOiJKxxx...">
    
</iframe>
```

&emsp;&emsp; 在 `/wlview` 后面添加 斜线 `/wlview/`。