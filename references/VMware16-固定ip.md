# 参考文献
* [虚拟机（VMware ）如何固定系统IP](https://blog.csdn.net/qq_45156021/article/details/133364706)
* [在 VMware Workstation 中配置固定 IP 地址](https://blog.csdn.net/qq_51870334/article/details/129061220)

# 一、我们为什么要固定ip

&emsp;&emsp;虚拟机中的 Linux 操作系统，其 IP 地址是通过 DHCP 服务 获取的。

> DHCP ：动态获取 IP 地址，即每次重启设备后都会获取一次 IP 地址，所以 IP 地址也会频繁变更。

* 原因1：办公电脑 IP 地址变化无所谓，但是我们要远程连接到 Linux 系统，如果 IP 地址经常变化，我们就要频繁修改适配，这就很麻烦。
* 原因2：远程终端配置了虚拟机 IP 地址和主机名映射，如果 IP 频繁更改，也需要频繁更新映射关系。 

&emsp;&emsp;综上所述，为了避免不必要的麻烦，我们需要把 IP 地址固定下来，不再变化。

# 二、配置步骤

# 2.1、查看VMnet8 的 IPv4 地址

![网络适配器](./pictures/WMware16_ip_fixed/img.png)

> 下一步

![详细信息](./pictures/WMware16_ip_fixed/img_1.png)

> 下一步

![详细信息2](./pictures/WMware16_ip_fixed/img_2.png)

# 2.2、编辑 VM Workstation 

![VWwarem_编辑](./pictures/WMware16_ip_fixed/img_3.png)

> 下一步

![网络设置](./pictures/WMware16_ip_fixed/img_4.png)

> 将这里的子网 IP 前 3 位，修改成在 Windows 终端查看的 VMnet 8 IPv4 地址的前 3 位 。
> 子网 IP 的第4位，可修改为 0~254 之间的任何一个数。这是一个网段，表示我们 IP 地址的范围 192.168.255.0 ~ 192.168.255.254 之间 。

> 子网掩码不需要修改。

&emsp;&emsp;上图序号 ① 和序号 ② 设置好之后，点击序号 ③，记住下图网关 IP 地址。

![网关设置](./pictures/WMware16_ip_fixed/img_5.png)

# 2.3、Centos 修改固定 IP

> Linux 网卡的配置文件目录：/etc/sysconfig/network-scripts

```shell
vim /etc/sysconfig/network-scripts/ifcfg-ens33

TYPE="Ethernet"          # 网卡类型 以太网 
PROXY_METHOD="none" 
BROWSER_ONLY="no" 
BOOTPROTO="static"       # 固定ip 必须选择static
DEFROUTE="yes" 
IPV4_FAILURE_FATAL="no" 
IPV6INIT="yes" 
IPV6_AUTOCONF="yes" 
IPV6_DEFROUTE="yes" 
IPV6_FAILURE_FATAL="no" 
IPV6_ADDR_GEN_MODE="stable-privacy" 
NAME="ens33"             # 网卡名称 
UUID="74c3b442-480d-4885-9ffd-e9f0087c9cf7" 
DEVICE="ens33"           # 本机网卡的名称
ONBOOT="yes"             # 是否开机启动网卡服务，yes -> 开启自启
IPV6_PRIVACY="no"
IPADDR="192.168.255.100" # IP地址，根据个人电脑查看 ifconfig 查看 
PREFIX="24"              # 子网掩码，等效: NETMASK=255.255.255.0 
GATEWAY="192.168.255.2"  # 网关服务，填写上面配置的网关ip 
DNS1="8.8.8.8"           # 网关DNS解析 
DOMAIN="114.114.114.114" # 公网DNS解析  国内DNS -> 114.114.114.114 ; 谷歌 -> 8.8.8.8 

```

> IP 地址这里修改为自己想要固定的 IP 地址，但不要超过 192.168.255.0 ~ 192.168.255.254 这个范围 

# 2.4、Centos 重启网络

> 重启网络，ifconfig 查看 IP 地址。

```shell
# 重启网络
systemctl  restart  network
```
![重启网络](./pictures/WMware16_ip_fixed/img_6.png)

> 查看重启后的ip

![重启后的ip](./pictures/WMware16_ip_fixed/img_7.png)


