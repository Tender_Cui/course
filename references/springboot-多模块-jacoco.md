# 参考文献

[springboot多模块工程单元测试jacoco统计代码覆盖率总结](https://blog.csdn.net/boweiqiang/article/details/128559724)
[jacoco插件配置生成单元测试覆盖率报告](https://blog.csdn.net/weixin_38384296/article/details/122655287)

# 一、概述

&emsp;&emsp;jacoco 统计代码覆盖率，比较简单，只需要在 pom.xml 中引用两个插件：

* maven-surefire-plugin
* jacoco-maven-plugin

> 针对：jacoco-maven-plugin

&emsp;&emsp;jacoco-maven-plugin 插件有两个重要的 execution 配置：

```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>0.8.5</version>
    <executions>
        <execution>
            <id>my-prepare-agent</id>
            <goals>
                <!-- 即准备agent，实现为代码插桩 -->
                <goal>prepare-agent</goal>
            </goals>
        </execution>
        <execution>
            <id>my-report</id>
            <phase>test</phase>
            <goals>
                <!-- 生成覆盖率报告 -->
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

# 二、单模块/简单工程

&emsp;&emsp;简单工程配置实例，就一个pom.xml 文件

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.18.1</version>
        <configuration>
            <skipTests>false</skipTests>
            <testFailureIgnore>true</testFailureIgnore>
        </configuration>
    </plugin>
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.5</version>
        <executions>
            <execution>
                <id>my-prepare-agent</id>
                <goals>
                    <goal>prepare-agent</goal>
                </goals>
            </execution>
            <execution>
                <id>my-report</id>
                <phase>test</phase>
                <goals>
                    <goal>report</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
```

# 三、多模块工程

![多模块工程](./pictures/jacoco/img.png)

&emsp;&emsp;如上图所示：每个模块都这么配置的话，生成的报告是各自独立的？那么怎么把各个模块的代码覆盖率统计在一起，生成一个聚合的报告呢？

&emsp;&emsp;jacoco 提供了报告聚合的能力！我们需要提供一个如上图所示的：course-jacoco 模块，在该模块中配置 jacoco 的 report-aggregate，如下所示：

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <version>0.8.5</version>
            <executions>
                <execution>
                    <id>report-aggregate</id>
                    <phase>test</phase>
                    <goals>
                        <goal>report-aggregate</goal>
                    </goals>
                    <!-- jacoco 的报告路径 -->
<!--                        <configuration>-->
<!--                            <outputDirectory>target/site/jacoco</outputDirectory>-->
<!--                        </configuration>-->
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

&emsp;&emsp;同时这个模块需要引用所有的其它的模块  

```xml
<dependencies>
    <!-- 子模块相关 start -->
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-bootstrap</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-web</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-biz</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-service-integration</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-core</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-common-util</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-dal</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-test</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-mybatis-generator</artifactId>
    </dependency>
    <!-- 子模块相关 end -->
</dependencies>
```

## 3.1、操作步骤

1. 在项目根目录下的父 pom.xml 中添加 jacoco-maven-plugin，并配置 goal 是 prepare-agent（这么做的好处是不用在每个module的pom中都重复添加同样的内容）

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.18.1</version>
        <configuration>
            <skipTests>false</skipTests>
            <testFailureIgnore>true</testFailureIgnore>
            <argLine>${argLine}</argLine>
        </configuration>
    </plugin>
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.5</version>
        <executions>
            <execution>
                <id>my-prepare-agent</id>
                <goals>
                    <goal>prepare-agent</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
```

> jacoco在prepare-agent阶段会生成一个属性，这个属性指向jacoco的runtime agent，默认这个属性叫argLine，
> 我们需要在maven-surefire-plugin的配置中，引用这个属性，这样在surefire执行测试时，才会能够找到并挂载jacoco的agent，
> 从而实现代码覆盖率的统计（当然我们也可以自定义这个属性的名字）

&emsp;&emsp;在 prepare-agent 阶段自定义 jacoco 输出的属性名，那么你的根 pom.xml 文件，需要进行如下配置

```xml
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <version>2.18.1</version>
        <configuration>
            <skipTests>false</skipTests>
            <testFailureIgnore>true</testFailureIgnore>
            <argLine>${jacocoArgLine}</argLine>
        </configuration>
    </plugin>
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.5</version>
        <executions>
            <execution>
                <id>my-prepare-agent</id>
                <goals>
                    <goal>prepare-agent</goal>
                </goals>
                <configuration>
                  <propertyName>jacocoArgLine</propertyName>
                </configuration>
            </execution>
        </executions>
    </plugin>
</plugins>
```

2. 在聚合模块中配置jacoco聚合报告：

```xml
<plugins>
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.5</version>
        <executions>
            <execution>
                <id>my-report</id>
                <phase>test</phase>
                <goals>
                    <goal>report-aggregate</goal>
                </goals>
            </execution>
        </executions>
    </plugin>
</plugins>
```

3. 在聚合模块中引用其他模块（非常重要，只有引用的模块的覆盖率才会被聚合到报告中，未引用的模块的覆盖率不会被收集和聚合）：

```xml
<dependencies>
    <!-- 子模块相关 start -->
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-bootstrap</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-web</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-biz</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-service-integration</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-core</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-common-util</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-dal</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-test</artifactId>
    </dependency>
    <dependency>
        <groupId>com.tender</groupId>
        <artifactId>course-mybatis-generator</artifactId>
    </dependency>
    <!-- 子模块相关 end -->
</dependencies>
```