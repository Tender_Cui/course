# redis的紧凑列表ziplist、quicklist、listpack
- https://blog.csdn.net/qq_32139981/article/details/141677488

# Redis zset 底层结构
- https://www.cnblogs.com/hld123/p/18074778

# redis的zset底层数据结构，你真的懂了吗？
- https://blog.csdn.net/qq_32139981/article/details/141671579

# 什么时候使用压缩链表，什么时候使用跳表呢 ？

> 这主要取决于两个配置参数：zset-max-ziplist-entries （默认值为128 单位：个） 和 zset-max-ziplist-value （默认值为64，单位：字节）。
>
> 使用压缩列表：当 zset 存储的元素数量小于 zset-max-ziplist-entries 的值，且所有元素的最大长度小于 zset-max-ziplist-value 的值时，Redis会选择使用压缩列表作为底层实现。压缩列表占用的内存较少，但是在需要修改数据时，可能需要对整个压缩列表进行重写，性能较低。
>
> 使用跳跃表：当 zset 存储的元素数量超过 zset-max-ziplist-entries 的值，或者任何元素的长度超过 zset-max-ziplist-value 的值时，Redis 会将底层结构从压缩列表转换为跳跃表。跳跃表的查找和修改数据的性能较高，但是占用的内存也较多。
>
> 这两个参数都可以在 Redis 的配置文件中进行设置。通过调整这两个参数，就可以根据自己的应用特性，选择更倾向于节省内存，还是更倾向于提高性能。

* [★★★★★ `redis`数据淘汰策略](https://zhuanlan.zhihu.com/p/105587132)
> 过期策略：定期删除 + 惰性删除 + 内存淘汰策略（补充删除）
> [如何配置Redis淘汰策略](https://blog.csdn.net/qq_30999361/article/details/124488406)
> 1. noeviction：当内存使用超过配置的时候会返回错误，不会驱逐任何键
> 2. allkeys-lru：加入键的时候，如果过限，首先通过LRU算法驱逐最久没有使用的键
> 3. volatile-lru：加入键的时候如果过限，首先从设置了过期时间的键集合中驱逐最久没有使用的键
> 4. allkeys-random：加入键的时候如果过限，从所有key随机删除
> 5. volatile-random：加入键的时候如果过限，从过期键的集合中随机驱逐
> 6. volatile-ttl：从配置了过期时间的键中驱逐马上就要过期的键
> 7. volatile-lfu：从所有配置了过期时间的键中驱逐使用频率最少的键
> 8. allkeys-lfu：从所有键中驱逐使用频率最少的键

* [★★★★★ `Redis`数据持久化策略]()
> [参考链接1](https://blog.csdn.net/weixin_45433817/article/details/135678315)
> [参考链接2](https://blog.csdn.net/2301_78418531/article/details/131515432)












