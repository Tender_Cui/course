# 1、java 培训机构架构课程

&emsp;&emsp;[详见：培训机构架构课程](./培训机构架构课程.md 'included')

# 2、常见面试问题

> 问题轻重程度：   
> ★ 随意   
> ★★ 了解   
> ★★★ 掌握   
> ★★★★ 强烈推荐   
> ★★★★★ 必看 

* [★★★★★ 互联网 java工程师进阶知识完全扫盲](https://doocs.github.io/advanced-java/#/)

* [★★★★★ 彤哥读源码](https://www.cnblogs.com/tong-yuan/p/all.html)

* [★★线程阻塞后会让出`cpu`嘛](https://blog.csdn.net/weixin_43935927/article/details/108586032)
> 1. 阻塞不占用`CPU`时间，当某个线程阻塞时，该会触发`CPU`调度，即让新的线程在该`CPU`上运行   
> 2. `linux`命令： `vmstat` 查看`cs` （`context switch`）

* [★★★★★ 为什么非公平模式效率比较高？](https://doocs.github.io/advanced-java/#/)
> 因为`非公平模式`会在一开始就尝试<span style="color:pink">两次</span>获取锁，如果当时正好state的值为0，它就会成功获取到锁，少了排队导致的阻塞/唤醒过程，并且减少了线程上下文频繁的切换带来的性能损耗。

* [★★★ JUC中的工具类：`Phaser`的原理，如何模拟`CountDownLatch`和`CyclicBarrier`](https://blog.csdn.net/qq_31865983/article/details/105643935)

* [★★★★★ 两个线程，交替打印奇偶数](https://blog.csdn.net/educast/article/details/103007004)

* [★★★★★ notify、wait 和 signal 、await的用法, JUC条件锁的原理](https://www.cnblogs.com/tong-yuan/p/ReentrantLock-Condition.html)
> [demo](https://www.likecs.com/show-204872056.html) 

* [★★★★★ `ThreadPoolExecutor`有哪几个构造函数，有哪些入参，线程池中，普通线程和核心线程有什么区别，当线程池闲置后，超过核心线程数的线程会回收吗，核心线程会被回收吗，如何让核心线程被回收，不是阻塞队列可以吗，使用线程池踩过那些坑没？](https://blog.csdn.net/fen_fen/article/details/122909142)
> 参考：   
> [线程池中`ThreadPoolExecutor`构造器参数介绍](https://www.jianshu.com/p/22e9a9666e2e)

* [★★★ `Executors`工具类创建线程池的几种方式](https://www.jb51.net/article/215163.htm)
> 1. 单线程线程池： Executors.newSingleThreadExecutor()  核心线程和最大线程都为1 ，任务顺序执行, blockingQueue 无界
> 2. 缓存线程池：Executors.newCachedThreadPool()         核心线程数为0， 最大线程数为Integer.MAX_VALUE，blockingQueue 无缓冲阻塞队列。 
     SynchronousQueue 是一个没有容量的阻塞队列，每个插入操作必须等待一个相应的删除操作，反之亦然，它主要用于在线程之间进行直接、同步的数据交换
> 3. 固定线程数线程池：Executors.newFixedThreadPool()     核心线程和最大线程都为传递进来得入参数目，blockingQueue 无界
> 4. 固定线程数线程池：Executors.newScheduledThreadPool() 创建一个线程池，可以调度命令在给定的延迟之后运行，或定期执行, 支持执行定时性或周期性任务。

* [★★★★★ `@Transcational`注解在什么场景下会不生效或者不会事务回滚，spring 事务的传播模式有哪些？](https://blog.csdn.net/D578332749/article/details/90750698)
> [Checked异常和Unchecked异常](https://blog.csdn.net/qq_53508036/article/details/125253645)
```text
1、函数必须是public的，否则事务不生效
2、添加有@Transcational注解的函数不应该在该类的内部调用，因为Spring AOP机制不会拦截内部函数间的调用，所以会导致事务不生效
3、数据库引擎要支持事务，如果是Mysql，注意表要使用支持事务的引擎，比如innodb，如果是myisam，事务不生效
4、因为Spring的事务功能基于AOP，所以该类必须托管于Spring容器内，且必须与调用者处于同一容器内，否则事务不生效
5、需要注意是否打开了使用注解进行事务控制，Spring Boot项目默认打开，非Spring Boot项目需要在配置文件中进行如下配置，否则基于@Transcational注解的事务可能不会生效：
    <tx:annotation-driven transaction-manager="transactionManager" proxy-target-class="true"/>
6、Spring的事务回滚默认只对unchecked异常回滚，如果出现的异常是checked，则不会回滚，如果希望对所有的异常都进行回滚，可以在@Transcational注解中配置属性，如下：
    @Transactional(rollbackFor=Exception.class)
类似的还有norollbackFor属性，用于配置不需要回滚的异常

7、异常在catch后必须抛出，否则不会进行回滚
```

* [★★★★★ mysql 索引失效的场景](https://www.jb51.net/article/176633.htm)
> 总结：
> 1. `where`条件后尽量不要使用`or`，可以使用`unionAll` 或者 `in` 替代
> 2. `where`语句中索引列使用了负向查询，可能会导致索引失效
> 3. 索引字段可以为null，使用is null或is not null时，可能会导致索引失效
> 4. 在索引列上使用内置函数，一定会导致索引失效
> 5. 隐式类型转换导致的索引失效
> 6. 隐式字符编码转换导致的索引失效
> 7. 对索引列进行运算，一定会导致索引失效
> 8. 联合索引中，where中索引列违背最左匹配原则，一定会导致索引失效
> 9. 不要使用反向查询，比如not like 等，

* [★★★ ArrayList 相关问题]()
> 1. ArrayList 的扩容过程 [参考链接](https://www.cnblogs.com/jelly12345/p/14920211.html)
> 2. 每次增长多少容量
> 3. removeIf() 方法的使用
> 4. 求交集retainAll、求差集removeAll
> 5. 利用迭代器对ArrayList 进行遍历的时候，能连续两次进行iterator.remove() 操作吗，为什么？(不可以，查看源码可以看到`lastRet`在第一次删除的时候已经被设置为-1了，第二次再删除的时候，-1 < 0 ,会直接抛异常)

&emsp;&emsp;`问题5`的答案：
```html
 public void remove() {
    if (lastRet < 0) // 第二次删除，直接抛异常
        throw new IllegalStateException();
    checkForComodification();

    try {
        ArrayList.this.remove(lastRet);
        cursor = lastRet;
        lastRet = -1;
        expectedModCount = modCount;
    } catch (IndexOutOfBoundsException ex) {
        throw new ConcurrentModificationException();
    }
}
```

* [★★★ CopyOnWriteArrayList 相关问题](https://blog.csdn.net/feiying0canglang/article/details/118463826)
> 1. 如何实现读写分离的
> 2. 其性能与ArrayList 在新增，删除等操作上，哪个性能更优？
> 3. CopyOnWriteArrayList 为什么使用可重入锁ReentrantLock作为其内部锁？

* [★★★★★ HashMap 相关问题]()
> 1. 如果发生了hash 碰撞，会在什么情况下进行红黑树化？
> 2. HashMap 的内存结构
> 3. HashMap.comparableClassFor(Object x)方法解读 [参考链接，了解](https://blog.csdn.net/qpzkobe/article/details/79533237)

* [★★★★★ 算法结构之排序](https://blog.csdn.net/qq_28063811/article/details/93615797)
> 1. 桶排序  [参考链接](https://blog.csdn.net/qq_28063811/article/details/93615797)

* [★★★★★ 浏览器中敲入一个URL，会发生什么？](https://blog.csdn.net/qq_28063811/article/details/93615797)
> 1. [参考链接1](https://blog.csdn.net/ccss__ddnn/article/details/115345078)
> 2. [参考链接2](https://blog.csdn.net/Richardjgp/article/details/125159922)

* [★★★★★ Redisson中的WatchDog原理](https://blog.csdn.net/qq_37436172/article/details/130656960)

* [★★★ Java数据结构---Trie（字典树/前缀树）](https://blog.csdn.net/qq_37436172/article/details/130656960)
> 1. [参考链接1](https://blog.csdn.net/weixin_43972154/article/details/119635995)
> 2. [参考链接2](https://blog.csdn.net/weixin_44174596/article/details/121524379)

* [★★★★★ Synchronized 锁升级的过程](https://blog.csdn.net/qq_37436172/article/details/130656960)
> 1. [马士兵亲授：多线程与高并发——锁机制、锁升级-哔哩哔哩] https://b23.tv/i7Lr35r
> 升级过程如下：
> 1. 无锁 --> 偏向锁（只有一个线程的时候，把线程指针贴到markword中）
> 2. 偏向锁 --> 轻量级锁（当发生多线程竞争锁的时候, 利用cas 竞争锁）
> 3. 轻量级锁 --> 重量级锁（cas 自循次数达到10次，或者调用了wait 等操作，或者竞争的线程数达到核数的一半）

* [★volatile 内存屏障]()
> 四条指令:
> 1. loadload
> 2. loadstore
> 3. storestore
> 4. storeload

* [★★★★★ jdk动态代理和cglib的区别](https://blog.csdn.net/yujiubo2008/article/details/123137340)
> 总结：
> 1. JDK动态代理是实现了被代理对象的接口，Cglib是继承了被代理对象。
> 2. JDK和Cglib都是在运行期间生成字节码，JDK是直接写Class字节码，Cglib使用ASM框架写Class字节码，Cglib代理实现更复杂，生成代理类比JDK效率低。
> 3. JDK调用代理方法，是通过反射机制调用，Cglib是通过FastClass机制直接调用方法，Cglib执行效率更高。
> 4. Cglib比较适合单例模式。另外由于CGLIB的大部分类是直接对Java字节码进行操作，这样生成的类会在Java的永久堆中。如果动态代理操作过多，容易造成永久堆满，触发OutOfMemory异常。spring默认使用jdk动态代理，如果类没有接口，则使用cglib。

* [★★★★★ `springboot` 的加载原理](https://baijiahao.baidu.com/s?id=1724450055792851489&wfr=spider&for=pc)

* [★★★★★ `springboot` 整合 `mybatis` 的原理](https://blog.csdn.net/weixin_34452850/article/details/90544868)


* [★★★★★ 如何自定义 starter 的步骤](https://www.cnblogs.com/hujunwei/p/16382252.html)

* [★★★数据库隔离级别](https://blog.csdn.net/weixin_51981189/article/details/127465007)
> MySQL数据库默认的事务隔离级别是可重复读`（REPEATABLE READ）`
```shell
# 查看当前会话的隔离级别：
SELECT @@SESSION.TX_ISOLATION;
# 查看全局的默认隔离级别：
SELECT @@GLOBAL.TX_ISOLATION;
# 设置全局默认的隔离级别（需要具有相应的权限）：
SET GLOBAL TRANSACTION ISOLATION LEVEL REPEATABLE READ;
# 设置当前会话的隔离级别：
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
```

* [★★★★★ jvm内存空间哪几部分](https://www.elecfans.com/d/2331607.html)
> 1. 堆
> 2. 方法区
> 3. 虚拟机栈
> 4. 本地方法栈
> 5. 程序计数器


* [★★★★★ `Redis`大键的排查与删除]()
> [参考链接1](https://blog.csdn.net/weixin_38405253/article/details/125630832)
> [参考链接2](https://blog.51cto.com/kusorz/4775615)
```html
一、如何排查大键
1.1、redis-cli --bigkeys  
    以 scan 延迟计算的方式扫描所有 key，因此执行过程中不会阻塞 redis，但实例存在大量的 keys 时，命令执行的时间会很长，这种情况建议在 slave 上扫描。某种类型如果存在较多的大key (>10kb)，只会统计 top1 的那个 key
    使用 memory 命令查看 key 的大小（仅支持 Redis 4.0 以后的版本）
    eg: memory usage keyname;
1.2、使用 Rdbtools 工具包

二、大键的删除
    ==> https://blog.51cto.com/u_15490474/5025230
2.1、非字符串的bigkey，不要使用 del 删除，使用 hscan、sscan、zscan 方式渐进式删除
    （1）key改名，相当于逻辑上把这个key删除了，任何redis命令都访问不到这个key了
    （2）小步多批次的删除
2.2、Redis 4.0 推出了一个重要命令 UNLINK，用来拯救 del 删大key的困境。
    UNLINK 工作思路：
    （1）在所有命名空间中把 key 删掉，立即返回，不阻塞。
    （2）后台线程执行真正的释放空间的操作。
    UNLINK 基本可以替代 del，但个别场景还是需要 del 的，例如在空间占用积累速度特别快的时候就不适合使用 UNLINK，因为 UNLINK 不是立即释放空间。
```

* [★★★★★ `redis` 缓存穿透,击穿,雪崩](https://doocs.github.io/advanced-java/#/docs/high-concurrency/redis-caching-avalanche-and-caching-penetration)
> [对于缓存击穿问题，可以使用布隆过滤器(原理比较简单)](https://www.jb51.net/program/285139yok.htm)

* [★★★★★ `redis` 热点key的解决方案](https://www.php.cn/redis/482412.html)
```html
一、如何查找热key，用redis自带命令
　　　1、monitor命令，也有现成的分析工具可以给你使用，比如redis-faina。但是该命令在高并发的条件下，有内存增暴增的隐患，还会降低redis的性能。
　　　2、redis 4.0.3提供了redis-cli的热点key发现功能，执行redis-cli时加上–hotkeys选项即可（尽量slave上执行）
　　　
二、、热点Key的解决方案：
　　　1、利用二级缓存
　　　2、读写分离
```

* [★★★ `redis` 中得string 类型底层是怎么实现得，为什么不用原生得得字符串类型](https://blog.csdn.net/wangshuai6707/article/details/131101404)
```html
使用SDS 得好处有哪些：
    1、常数复杂度获取字符长度
    2、杜绝缓冲区溢出
    3、减少修改字符串时带来的内存重分配次数
    4、可以保留文本或二进制数据
```
* [★★★ `Redis` 主从模式的搭建及其原理](https://blog.csdn.net/CYK_byte/article/details/132191535)

* [★★★★★ `Redis Cluster`集群的搭建与原理](https://blog.csdn.net/weixin_46120888/article/details/123804731)

* [★★★★★ `Redis`集群（读写分离、哨兵机制、Cluster集群）](https://blog.csdn.net/Dean_xiu/article/details/118661742)

* [★★★★★ `Mysql`数据库锁都有哪几种](https://www.modb.pro/db/609950)
> [参考另一个文章（虽然有些笔误）](https://www.cnblogs.com/dream-ze/p/17995447)

* [★★★★★ B＋tree  b tree的区别](https://blog.csdn.net/luzhensmart/article/details/85247123)

* [★ raft 协议算法有了解吗](https://blog.csdn.net/yangmengjiao_/article/details/1201913143)

* [★★★ spring中用到的九种设计模式](https://blog.csdn.net/weixin_30740295/article/details/97861991)

* [★★★★★ 系统卡顿，cpu 爆满，如何排查，如何查找出死循环等操作](https://blog.csdn.net/chenaini119/article/details/80000949)
> [其它参考链接](https://blog.csdn.net/jwentao01/article/details/123982129)
```shell
# 查看某个进程下所有线程的cpu 执行时间等信息
ps -mp pid -o THREAD,tid,time  

# 全部排查命令如下
1. top               # 查询pid
2. ps aux | grep PID # 确定是 java 进程出现了问题
3. ps -mp pid -o THREAD,tid,time # 定位到具体线程或者代码

# 假设： 找到了耗时最高的线程 28802
4. printf "%x\n" 28802  # 将需要的线程ID转换为16进制格式
5. jstack pid | grep tid -A 30  # grep 第四步的16进制格式 tid
```

* [★★★★★ Java死锁检测的三种方法](https://blog.csdn.net/fengsheng5210/article/details/123576559)
```shell
jps # 该命令可以列举出 java pid (进程id)
jstack pid # jstack 命令用于打印出给定Java进程的"线程堆栈"信息，通过分析线程堆栈信息可以判断是否发生了死锁。

# 查看关键字：deadlock
# 可以看到一些特别的信息
# waiting to lock <0x6666666666666666666>
# locked <0x999999999999999999999>
```

* [★★★★★ TCP 三次握手，四次挥手过程 （分别对应的Socket程序的什么时候）](https://www.cnblogs.com/tdtdttd/p/11088324.html)

* [★★★ redis 事务和乐观锁](https://blog.csdn.net/Jjs_Object/article/details/120181998)

* [★★★★★ redis 如何实现延迟队列？](https://www.cnblogs.com/vnone/articles/16447108.html)
> 通过 `ZSet` 的存储于查询来实现，它的核心思想是在程序中开启一个一直循环的延迟任务的检测器，用于检测和调用延迟任务的执行

* [★★★★★ `redission` 分布式锁实现原理](https://cloud.tencent.com/developer/article/2325411)

* [★★★★★ `mysql` 乐观锁的实现](https://blog.csdn.net/lonely_baby/article/details/129151019)

* [★★★★★ 如何保证mq 的高可用](https://doocs.github.io/advanced-java/#/docs/high-concurrency/how-to-ensure-high-availability-of-message-queues)

* [★★★★★ 如何保证消息的可靠性传输](https://doocs.github.io/advanced-java/#/docs/high-concurrency/how-to-ensure-the-reliable-transmission-of-messages)
> [Kafka的partions和replication-factor参数的理解](https://www.cnblogs.com/lgjlife/p/10569187.html)

* [★★★★★ 如何保证消息消费的顺序性？](https://doocs.github.io/advanced-java/#/docs/high-concurrency/how-to-ensure-the-order-of-messages)

* [★★★★★ 分布式事物的实现方式](https://blog.csdn.net/ityqing/article/details/102655827)

* [★★★★★ 服务降级和服务熔断的区别](https://blog.csdn.net/qq_42554719/article/details/113576868)
> 1. 服务降级：当服务调用出现响应时间过长或者运行出错或者宕机，就会调用服务降级方法快速响应。
> 2. 服务熔断，只有在`一定时间内`服务调用失败（报错、超时、宕机）达到一定次数，才会启动服务熔断，进而调用服务降级方法快速响应。
> 3. 服务降级每次都会先调用原服务方法，调用失败才会执行服务降级方法；服务熔断状态会直接调用服务降级方法。

* [★★★★★ `volatile` 是如何保证多线程之间可见的](https://blog.csdn.net/weixin_44512162/article/details/135797795)

* [★★★ Java BIO 和 NIO 使用，有什么区别](https://blog.csdn.net/u013749113/article/details/130700647)

* [★★★★★ `mysql` order by 的原理，mvcc 的原理]()
> [order by 的原理](https://www.cnblogs.com/luckcs/articles/2619871.html)
> [mvcc 的原理](https://blog.csdn.net/weixin_64940494/article/details/127983307)

* [★★★★★ `mysql` explain 的用法，要重点关注的字段有哪些](https://www.jianshu.com/p/be1c86303c80)
> 1. select_type
> 2. type: system>const>eq_ref>ref>range>index>all

面试题实战一
1、Filter、Interceptor、Aop的执行顺序与各自得区别
2、怎么从spring容器方面判断容器启动完成
3、springboot starter 启动流程
4、spring 如何解决循环依赖的
5、volatile 是如何保证多线程之间可见的
6、ThreadLocal，InheritableThreadLocal，TransmittableThreadLocal的原理、使用场景与各自的区别，InheritableThreadLocal 是如何实现父子线程之间的值传递的


程序员安全知识一览
1、HTTPS、SSL、TLS三者之间的联系和区别
==> https://blog.csdn.net/enweitech/article/details/81781405
==> https://blog.csdn.net/alinyua/article/details/79476365
==> https://jeanboy.blog.csdn.net/article/details/76423436
1.购物车是怎么做的
缓存查询，mq更新db
2.购物车在并发情况，缓存为null，怎么保证同步更新db
3.提交订单 是否加锁(扣减库存，你说要不要加锁，或者乐观锁来控制)
4.redis锁续期
redisson watch dog 加锁30秒每隔10秒检查是否持有锁 业务未结束会延迟锁时间
5.es倒排索引:根据v找k
6.商品查询怎么做的:同步es索引库
7.db最新数据同步es有用过吗？
8.怎么保证mq消费可靠



==> 360 集团
一面：
1、简述下git rebase 和 git merge 的区别
2、单个jvm ，统计某个Controller 中的Get 方法的调用次数
3、线上cpu 100%了，如何排查解决，如果java 代码中有死循环如何定位，如果代码中有死锁，如果定位
4、redis 的常用数据类型有哪些，bitmap  怎么实现的？如何利用 redis 实现延迟队列？
5、Phaser 这个东西和CountdownLatch 和 CyclicBarrier 有什么区别，原理是啥？
6、mysql order by 排序的原理，mvcc 的原理是啥
7、项目中你印象深刻的一个闪光点是啥？
8、mysql中的 间隙锁，next-key，行锁之间的区别，next-key 什么时候退化成 行锁？

二面：
1、map的常用实现，安全和非安全实现有哪些，安全怎么实现的？
非安全： HashMap,   安全实现： HashTable, ConcurrentHashMap  1.7 和 1.8 的区别
2、关键字 volatile 和 synchronized 的作用和区别
3、项目中排查的一个最困难的问题 ： 线程池队列没有设置大小，导致频繁的fullGC 最后系统宕机
4、项目中一个闪光点 ：
5、redis 集群，HA ： 哨兵模式 / redis cluster 模式
6、jvm gc ，cpu 太高，full gc 之类
7、阿里开源的 arthas  的使用 watch 命令等
8、分布式锁的实现方式有哪些，redis + lua 、zk 临时顺序 节点（注意惊群方式的实现，不好）、 mysql 锁表方式
9、elk 问了些，告警的实现

==> 华泰证券 一面
1、项目亮点，介绍下曾经的项目
2、mysql sql 的优化， explain 和 show engine innodb status:G\
3、项目中redis 的架构，哨兵集群 + 主从复制
4、ELK 搭建过程中踩过的坑
5、线上cpu 过高怎么排查
6、线程池的原理，和参数，创建线程池的几种方式
7、有死锁，jstack 拿到死锁后，有哪些具体的关键字可以用来排查
8、线程的状态有哪些
9、有哪些BlockingQueue, 你用了哪些
10、有哪些可以保证代码的原子性
11、分布式锁的实现，锁续命问题
12、redo log 什么时候产生，事物提交的时候，什么时候落到文件中，保证了事物中的哪一个
13、有哪几种方法可以使线程阻塞。Object.wait(), 添加锁的.await(), unsafe.park()
14、项目中有没有使用 cas
15、融资额度的扣减，你们怎么保证的，分布式锁 + 乐观锁
16、还款管理怎么做的，幂等性+ 分布式锁
17、jdk 1.7 和 1.8 在hashMap中的内存结构有什么变化，红黑树的特性和算法，左旋右旋再平衡之类的
18、MQ kakfa 如何保证不丢失，以及对应的具体配置。

==> 华泰证券 二面
1、自我介绍
2、项目中的最大的亮点，有挑战的方案或者解决方案
3、cas 的底层原理，汇编：lock + cmpchxg 指令必须要提一提的。
4、mybatis 框架中，什么时候跟数据库tcp 链接
5、两个服务tcp 通信，其中一个服务突然断开了，如果再次立马链接，会有 Address in use 这种报错，为什么？
6、看spring 、 mybatis 这些框架源码的出发点是什么，
7、像融资额度这种消息，不能丢失，这种你有什么设计思路呢
8、融资额度的，扣减，分布式事物方面有没有涉及？怎么实现的
9、分布式锁的原理，锁续命


==> 论之语
一面：机试算法题：
1、二分查找，给定数组已经排好序，寻找某个值是否存在，存在返回对应数组下标，不存在，则返回 -1
2、三个线程分别打印A，B，C，循环打印10次
```java
package com.tender;

import java.util.concurrent.Phaser;
import java.util.concurrent.TimeUnit;

/**
 * 三个线程交替打印 A、B、C
 */
public class ThreadTasks {
    /**
     * 0 -> A  解释：status = 0 的时候打印 A
     * 2 -> B  解释：status = 2 的时候打印 B
     * 4 -> C  解释：status = 4 的时候打印 C
     */

    private static volatile int status = 0;

    private static final int NUM = 100;
    public static void main(String[] args) throws Exception {
        Phaser phaser = new Phaser(3);

        new Thread(() -> printA(phaser)).start();
        new Thread(() -> printB(phaser)).start();
        new Thread(() -> printC(phaser)).start();

        phaser.awaitAdvanceInterruptibly(phaser.getPhase(), 10, TimeUnit.MINUTES);
    }

    private static void printA(Phaser phaser) {
        try {
            for (int i = 0; i < NUM ; i++) {
                while (0 != status) {
                    // Thread.yield(); // 让出cpu 会导致上线文的切换，不是很好
                }
                System.out.println("A");
                status = 2;
            }
        } finally {
            phaser.arrive();
        }
    }

    private static void printB(Phaser phaser) {
        try {
            for (int i = 0; i < NUM ; i++) {
                while (2 != status) {
                    // Thread.yield();
                }
                System.out.println("B");
                status = 4;
            }
        } finally {
            phaser.arrive();
        }
    }

    private static void printC(Phaser phaser) {
        try {
            for (int i = 0; i < NUM ; i++) {
                while (4 != status) {
                    // Thread.yield();
                }
                System.out.println("C");
                status = 0;
            }
        } finally {
            phaser.arrive();
        }
    }

}

```

二面：
1、nio、nio与 Bio 的区别，nio的应用
2、redis 架构，跳表，基本数据类型
3、cas及其应用
4、数据库 explain 遇到全表扫描怎么优化
5、线程池的参数与原理
6、职业规划

