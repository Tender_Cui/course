# 参考文献

* [SpringBoot整合Mybatis-Plus多数据源](https://blog.csdn.net/qq_52423918/article/details/131053188)


# 工程代码

&emsp;&emsp;[请参考shiro工程下的模块：shiro-framework-integration/shiro-dynamic-datasource](https://gitee.com/Tender_Cui/shiro)

# 1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-dynamic-datasource</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>dynamic-datasource-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid-spring-boot-starter</artifactId>
        </dependency>
        <!--mysql 驱动-->
        <dependency>
            <groupId>com.mysql</groupId>
            <artifactId>mysql-connector-j</artifactId>
            <scope>runtime</scope>
        </dependency>
        <!-- mybatis-plus -->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
        <!-- 数据库密码加密相关 start -->
        <dependency>
            <groupId>com.github.ulisesbocchio</groupId>
            <artifactId>jasypt-spring-boot-starter</artifactId>
        </dependency>
        <!-- 数据库密码加密相关 end -->
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
        </dependency>
    </dependencies>
</project>
```

# 2、application.yaml

```yaml
server:
  port: 9090

spring:
  application:
    name: shiro-dynamic-datasource

  ###### 多数据源配置 ######
  datasource:
    #使用阿里的Druid
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.cj.jdbc.Driver
    dynamic:
      primary: master # 设置默认的数据源或者数据源组,默认值即为master
      strict: false # 严格匹配数据源,默认false. true未匹配到指定数据源时抛异常,false使用默认数据源
      datasource:
        master:
          url: jdbc:mysql://192.168.255.101:3306/dynamic_datasource?serverTimezone=Asia/Shanghai
          # driver-class-name: com.mysql.cj.jdbc.Driver
          username: ENC(QICgHM+XFyrCJOEd2qoeCQ==)
          password: ENC(MgNhKm6EEYeXt/cPzQ01lY8YFhOTCSdy)
        slave_1:
          url: jdbc:mysql://127.0.0.1:3306/dynamic_datasource?serverTimezone=Asia/Shanghai
          # driver-class-name: com.mysql.cj.jdbc.Driver
          username: ENC(QICgHM+XFyrCJOEd2qoeCQ==)
          password: ENC(MgNhKm6EEYeXt/cPzQ01lY8YFhOTCSdy)

#####mybatisplus配置#####
mybatis-plus:
  #加载映射文件
  mapper-locations: classpath:mapper/*.xml
  #设置别名
  type-aliases-package: com.tender.domain
  #开启驼峰命名
  configuration:
    map-underscore-to-camel-case: true
    # 这个配置会将执行的sql打印出来，在开发或测试的时候可以用
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl

#####加密解密的配置#####
jasypt:
  encryptor:
    algorithm: PBEWithMD5AndDES
    iv-generator-classname: org.jasypt.iv.NoIvGenerator
    # password: 这个需要通过-D参数来指定 eg: -Djasypt.encryptor.password=xxxxx
```

# 3、sql脚本

## 3.1、建表语句

```roomsql
CREATE TABLE `config_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `content` longtext COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info';
```

## 3.2、初始化语句

```roomsql
-- 一个机器执行这个脚本
INSERT INTO `config_info`(`data_id`, `group_id`, `content`)
VALUES ('101-101-101', '101-101-101', 'content from 101 machine');

-- 另一个机器执行这个脚本
INSERT INTO `config_info`(`data_id`, `group_id`, `content`)
VALUES ('127-0-0-1', '127-0-0-1', 'content from 127.0.0.1 machine');
```

# 4、代码相关

## 4.1、启动类

```java
package com.tender;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableEncryptableProperties
@MapperScan("com.tender.mapper")
public class ShiroDynamicDataSourceApplication {

    public static void main(String[] args) {
        System.setProperty("jasypt.encryptor.password", "wb-csd876509");
        SpringApplication.run(ShiroDynamicDataSourceApplication.class);
    }
}
```

## 4.2、service 接口

```java
package com.tender.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.tender.domain.ConfigInfo;
import com.tender.domain.ConfigInfoCondition;

import java.util.List;

/**
 * @DS("slave_1")  指定使用 slave_1 数据源
 * @DS("master")  指定使用 master 数据源，默认就是master 数据源
 *
 * @see DS
 */
//@DS("master")
//@DS("slave_1")
public interface ConfigInfoService {

    List<ConfigInfo> selectList(ConfigInfoCondition param);

    List<ConfigInfo> selectListByManual(ConfigInfoCondition param);
}
```

## 4.2、service 接口实现类

```java
package com.tender.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.tender.domain.ConfigInfo;
import com.tender.domain.ConfigInfoCondition;
import com.tender.domain.ConfigInfoExample;
import com.tender.mapper.ConfigInfoMapper;
import com.tender.service.ConfigInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @DS("slave_1") 指定使用 slave_1 数据源
 * @DS("master") 指定使用 master 数据源，默认就是master 数据源
 * @see DS
 */
//@DS("master")
//@DS("slave_1")
@Slf4j
@Service
public class ConfigInfoServiceImpl implements ConfigInfoService {

    @Autowired
    private ConfigInfoMapper configInfoMapper;

    @Override
    public List<ConfigInfo> selectList(ConfigInfoCondition param) {

        ConfigInfoExample example = new ConfigInfoExample();
        ConfigInfoExample.Criteria criteria = example.createCriteria();
        if (!CollectionUtils.isEmpty(param.getDataIdList())) {
            criteria.andDataIdIn(param.getDataIdList());
        }
        if (!CollectionUtils.isEmpty(param.getGroupIdList())) {
            criteria.andGroupIdIn(param.getGroupIdList());
        }

        List<ConfigInfo> configInfos = configInfoMapper.selectByExample(example);
        System.out.println("==============> query from db info : " + JSON.toJSONString(configInfos));

        return configInfos;
    }

    @Override
    public List<ConfigInfo> selectListByManual(ConfigInfoCondition param) {

        ConfigInfoExample example = new ConfigInfoExample();
        ConfigInfoExample.Criteria criteria = example.createCriteria();
        if (!CollectionUtils.isEmpty(param.getDataIdList())) {
            criteria.andDataIdIn(param.getDataIdList());
        }
        if (!CollectionUtils.isEmpty(param.getGroupIdList())) {
            criteria.andGroupIdIn(param.getGroupIdList());
        }

        /**
         * 1、通过线程上下文来设置当前线程数据源 如非必要不要手动调用，调用后确保最终清除
         * DynamicDataSourceContextHolder.push("slave_1");
         * 2、获得当前线程数据源
         * DynamicDataSourceContextHolder.peek();
         * 3、强制清空本地线程 防止内存泄漏，如手动调用了 push 可调用此方法确保清除
         * DynamicDataSourceContextHolder.poll();
         */
        List<ConfigInfo> configInfos = new ArrayList<>();

        try {
            DynamicDataSourceContextHolder.push("slave_1");
            //查看当前数据源
            log.info(DynamicDataSourceContextHolder.peek());
            configInfos.addAll(configInfoMapper.selectByExample(example));
        } finally {
            DynamicDataSourceContextHolder.poll();
        }

        try {
            DynamicDataSourceContextHolder.push("master");
            //查看当前数据源
            log.info(DynamicDataSourceContextHolder.peek());
            configInfos.addAll(configInfoMapper.selectByExample(example));
        } finally {
            DynamicDataSourceContextHolder.poll();
        }

        System.out.println("==============> query from db info : " + JSON.toJSONString(configInfos));

        return configInfos;
    }
}
```

## 4.3、mapper 接口

```java
package com.tender.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.tender.domain.ConfigInfo;
import com.tender.domain.ConfigInfoExample;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @DS("slave_1")  指定使用 slave_1 数据源
 * @DS("master")  指定使用 master 数据源，默认就是master 数据源
 *
 * @see DS
 */
@Mapper
public interface ConfigInfoMapper {
    int countByExample(ConfigInfoExample example);

    int deleteByExample(ConfigInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ConfigInfo record);

    int insertSelective(ConfigInfo record);

    List<ConfigInfo> selectByExampleWithBLOBs(ConfigInfoExample example);

    List<ConfigInfo> selectByExample(ConfigInfoExample example);

    ConfigInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ConfigInfo record, @Param("example") ConfigInfoExample example);

    int updateByExampleWithBLOBs(@Param("record") ConfigInfo record, @Param("example") ConfigInfoExample example);

    int updateByExample(@Param("record") ConfigInfo record, @Param("example") ConfigInfoExample example);

    int updateByPrimaryKeySelective(ConfigInfo record);

    int updateByPrimaryKeyWithBLOBs(ConfigInfo record);

    int updateByPrimaryKey(ConfigInfo record);
}
```

## 4.4、mapper.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.tender.mapper.ConfigInfoMapper">
    <resultMap id="BaseResultMap" type="com.tender.domain.ConfigInfo">
        <id column="id" property="id" jdbcType="BIGINT"/>
        <result column="data_id" property="dataId" jdbcType="VARCHAR"/>
        <result column="group_id" property="groupId" jdbcType="VARCHAR"/>
        <result column="md5" property="md5" jdbcType="VARCHAR"/>
        <result column="gmt_create" property="gmtCreate" jdbcType="TIMESTAMP"/>
        <result column="gmt_modified" property="gmtModified" jdbcType="TIMESTAMP"/>
    </resultMap>
    <resultMap id="ResultMapWithBLOBs" type="com.tender.domain.ConfigInfo" extends="BaseResultMap">
        <result column="content" property="content" jdbcType="LONGVARCHAR"/>
    </resultMap>
    <sql id="Example_Where_Clause">
        <where>
            <foreach collection="oredCriteria" item="criteria" separator="or">
                <if test="criteria.valid">
                    <trim prefix="(" suffix=")" prefixOverrides="and">
                        <foreach collection="criteria.criteria" item="criterion">
                            <choose>
                                <when test="criterion.noValue">
                                    and ${criterion.condition}
                                </when>
                                <when test="criterion.singleValue">
                                    and ${criterion.condition} #{criterion.value}
                                </when>
                                <when test="criterion.betweenValue">
                                    and ${criterion.condition} #{criterion.value} and #{criterion.secondValue}
                                </when>
                                <when test="criterion.listValue">
                                    and ${criterion.condition}
                                    <foreach collection="criterion.value" item="listItem" open="(" close=")"
                                             separator=",">
                                        #{listItem}
                                    </foreach>
                                </when>
                            </choose>
                        </foreach>
                    </trim>
                </if>
            </foreach>
        </where>
    </sql>
    <sql id="Update_By_Example_Where_Clause">
        <where>
            <foreach collection="example.oredCriteria" item="criteria" separator="or">
                <if test="criteria.valid">
                    <trim prefix="(" suffix=")" prefixOverrides="and">
                        <foreach collection="criteria.criteria" item="criterion">
                            <choose>
                                <when test="criterion.noValue">
                                    and ${criterion.condition}
                                </when>
                                <when test="criterion.singleValue">
                                    and ${criterion.condition} #{criterion.value}
                                </when>
                                <when test="criterion.betweenValue">
                                    and ${criterion.condition} #{criterion.value} and #{criterion.secondValue}
                                </when>
                                <when test="criterion.listValue">
                                    and ${criterion.condition}
                                    <foreach collection="criterion.value" item="listItem" open="(" close=")"
                                             separator=",">
                                        #{listItem}
                                    </foreach>
                                </when>
                            </choose>
                        </foreach>
                    </trim>
                </if>
            </foreach>
        </where>
    </sql>
    <sql id="Base_Column_List">
        id, data_id, group_id, md5, gmt_create, gmt_modified
    </sql>
    <sql id="Blob_Column_List">
        content
    </sql>
    <select id="selectByExampleWithBLOBs" resultMap="ResultMapWithBLOBs"
            parameterType="com.tender.domain.ConfigInfoExample">
        select
        <if test="distinct">
            distinct
        </if>
        <include refid="Base_Column_List"/>
        ,
        <include refid="Blob_Column_List"/>
        from config_info
        <if test="_parameter != null">
            <include refid="Example_Where_Clause"/>
        </if>
        <if test="orderByClause != null">
            order by ${orderByClause}
        </if>
    </select>
    <select id="selectByExample" resultMap="BaseResultMap" parameterType="com.tender.domain.ConfigInfoExample">
        select
        <if test="distinct">
            distinct
        </if>
        <include refid="Base_Column_List"/>
        from config_info
        <if test="_parameter != null">
            <include refid="Example_Where_Clause"/>
        </if>
        <if test="orderByClause != null">
            order by ${orderByClause}
        </if>
    </select>
    <select id="selectByPrimaryKey" resultMap="ResultMapWithBLOBs" parameterType="java.lang.Long">
        select
        <include refid="Base_Column_List"/>
        ,
        <include refid="Blob_Column_List"/>
        from config_info
        where id = #{id,jdbcType=BIGINT}
    </select>
    <delete id="deleteByPrimaryKey" parameterType="java.lang.Long">
        delete from config_info
        where id = #{id,jdbcType=BIGINT}
    </delete>
    <delete id="deleteByExample" parameterType="com.tender.domain.ConfigInfoExample">
        delete from config_info
        <if test="_parameter != null">
            <include refid="Example_Where_Clause"/>
        </if>
    </delete>
    <insert id="insert" parameterType="com.tender.domain.ConfigInfo">
        insert into config_info (id, data_id, group_id,
        md5, gmt_create, gmt_modified,
        content)
        values (#{id,jdbcType=BIGINT}, #{dataId,jdbcType=VARCHAR}, #{groupId,jdbcType=VARCHAR},
        #{md5,jdbcType=VARCHAR}, #{gmtCreate,jdbcType=TIMESTAMP}, #{gmtModified,jdbcType=TIMESTAMP},
        #{content,jdbcType=LONGVARCHAR})
    </insert>
    <insert id="insertSelective" parameterType="com.tender.domain.ConfigInfo">
        insert into config_info
        <trim prefix="(" suffix=")" suffixOverrides=",">
            <if test="id != null">
                id,
            </if>
            <if test="dataId != null">
                data_id,
            </if>
            <if test="groupId != null">
                group_id,
            </if>
            <if test="md5 != null">
                md5,
            </if>
            <if test="gmtCreate != null">
                gmt_create,
            </if>
            <if test="gmtModified != null">
                gmt_modified,
            </if>
            <if test="content != null">
                content,
            </if>
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
            <if test="id != null">
                #{id,jdbcType=BIGINT},
            </if>
            <if test="dataId != null">
                #{dataId,jdbcType=VARCHAR},
            </if>
            <if test="groupId != null">
                #{groupId,jdbcType=VARCHAR},
            </if>
            <if test="md5 != null">
                #{md5,jdbcType=VARCHAR},
            </if>
            <if test="gmtCreate != null">
                #{gmtCreate,jdbcType=TIMESTAMP},
            </if>
            <if test="gmtModified != null">
                #{gmtModified,jdbcType=TIMESTAMP},
            </if>
            <if test="content != null">
                #{content,jdbcType=LONGVARCHAR},
            </if>
        </trim>
    </insert>
    <select id="countByExample" parameterType="com.tender.domain.ConfigInfoExample" resultType="java.lang.Integer">
        select count(*) from config_info
        <if test="_parameter != null">
            <include refid="Example_Where_Clause"/>
        </if>
    </select>
    <update id="updateByExampleSelective" parameterType="map">
        update config_info
        <set>
            <if test="record.id != null">
                id = #{record.id,jdbcType=BIGINT},
            </if>
            <if test="record.dataId != null">
                data_id = #{record.dataId,jdbcType=VARCHAR},
            </if>
            <if test="record.groupId != null">
                group_id = #{record.groupId,jdbcType=VARCHAR},
            </if>
            <if test="record.md5 != null">
                md5 = #{record.md5,jdbcType=VARCHAR},
            </if>
            <if test="record.gmtCreate != null">
                gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP},
            </if>
            <if test="record.gmtModified != null">
                gmt_modified = #{record.gmtModified,jdbcType=TIMESTAMP},
            </if>
            <if test="record.content != null">
                content = #{record.content,jdbcType=LONGVARCHAR},
            </if>
        </set>
        <if test="_parameter != null">
            <include refid="Update_By_Example_Where_Clause"/>
        </if>
    </update>
    <update id="updateByExampleWithBLOBs" parameterType="map">
        update config_info
        set id = #{record.id,jdbcType=BIGINT},
        data_id = #{record.dataId,jdbcType=VARCHAR},
        group_id = #{record.groupId,jdbcType=VARCHAR},
        md5 = #{record.md5,jdbcType=VARCHAR},
        gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP},
        gmt_modified = #{record.gmtModified,jdbcType=TIMESTAMP},
        content = #{record.content,jdbcType=LONGVARCHAR}
        <if test="_parameter != null">
            <include refid="Update_By_Example_Where_Clause"/>
        </if>
    </update>
    <update id="updateByExample" parameterType="map">
        update config_info
        set id = #{record.id,jdbcType=BIGINT},
        data_id = #{record.dataId,jdbcType=VARCHAR},
        group_id = #{record.groupId,jdbcType=VARCHAR},
        md5 = #{record.md5,jdbcType=VARCHAR},
        gmt_create = #{record.gmtCreate,jdbcType=TIMESTAMP},
        gmt_modified = #{record.gmtModified,jdbcType=TIMESTAMP}
        <if test="_parameter != null">
            <include refid="Update_By_Example_Where_Clause"/>
        </if>
    </update>
    <update id="updateByPrimaryKeySelective" parameterType="com.tender.domain.ConfigInfo">
        update config_info
        <set>
            <if test="dataId != null">
                data_id = #{dataId,jdbcType=VARCHAR},
            </if>
            <if test="groupId != null">
                group_id = #{groupId,jdbcType=VARCHAR},
            </if>
            <if test="md5 != null">
                md5 = #{md5,jdbcType=VARCHAR},
            </if>
            <if test="gmtCreate != null">
                gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},
            </if>
            <if test="gmtModified != null">
                gmt_modified = #{gmtModified,jdbcType=TIMESTAMP},
            </if>
            <if test="content != null">
                content = #{content,jdbcType=LONGVARCHAR},
            </if>
        </set>
        where id = #{id,jdbcType=BIGINT}
    </update>
    <update id="updateByPrimaryKeyWithBLOBs" parameterType="com.tender.domain.ConfigInfo">
        update config_info
        set data_id = #{dataId,jdbcType=VARCHAR},
        group_id = #{groupId,jdbcType=VARCHAR},
        md5 = #{md5,jdbcType=VARCHAR},
        gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},
        gmt_modified = #{gmtModified,jdbcType=TIMESTAMP},
        content = #{content,jdbcType=LONGVARCHAR}
        where id = #{id,jdbcType=BIGINT}
    </update>
    <update id="updateByPrimaryKey" parameterType="com.tender.domain.ConfigInfo">
        update config_info
        set data_id = #{dataId,jdbcType=VARCHAR},
        group_id = #{groupId,jdbcType=VARCHAR},
        md5 = #{md5,jdbcType=VARCHAR},
        gmt_create = #{gmtCreate,jdbcType=TIMESTAMP},
        gmt_modified = #{gmtModified,jdbcType=TIMESTAMP}
        where id = #{id,jdbcType=BIGINT}
    </update>
</mapper>
```

# 5、测试

&emsp;&emsp;使用 `@DS` 切换数据源。   

&emsp;&emsp;`@DS`可以注解在<span style="color:red">方法</span>上或<span style="color:red">类</span>上。
同时存在的化，按照就近原则。`方法`上的注解<span style="color:red">优先于</span> `类`上的注解。

&emsp;&emsp;`@DS`可以添加在`service`中的方法上来切换数据源，也可以添加在`Mapper`接口上。

&emsp;&emsp;如果一个`Service`只需要使用一个数据源，或者`多个Service`方法都需要使用相同的数据源，则建议将`@DS`注解添加到Mapper接口或XML文件上；

&emsp;&emsp;如果需要根据不同的业务场景动态切换数据源，则可以选择在Service方法上使用`@DS`注解。

&emsp;&emsp;当然还有一种情况，一个`service`方法操作不同的数据源，表结构一样的话可以使用一个`mapper`，在`service`内部进行切换。我们看到官方注释，<span style="color:red">非必要不要这么使用</span>！

```html
// 设置当前线程数据源 如非必要不要手动调用，调用后确保最终清除
DynamicDataSourceContextHolder.push("slave_1");
// 获得当前线程数据源
DynamicDataSourceContextHolder.peek();
// 强制清空本地线程 防止内存泄漏，如手动调用了push可调用此方法确保清除
DynamicDataSourceContextHolder.poll();
```

&emsp;&emsp;测试日志

```shell

2024-04-16 12:31:40.965  INFO 27920 --- [nio-9090-exec-1] c.t.service.impl.ConfigInfoServiceImpl   : slave_1
Creating a new SqlSession
SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@d503f3] was not registered for synchronization because synchronization is not active
JDBC Connection [com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl@3cdb5] will not be managed by Spring
==>  Preparing: select id, data_id, group_id, md5, gmt_create, gmt_modified from config_info
==> Parameters: 
<==    Columns: id, data_id, group_id, md5, gmt_create, gmt_modified
<==        Row: 7, 127-0-0-1, 127-0-0-1, null, 2024-04-16 11:19:13, 2024-04-16 11:19:13
<==      Total: 1
Closing non transactional SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@d503f3]
2024-04-16 12:31:41.459  INFO 27920 --- [nio-9090-exec-1] c.t.service.impl.ConfigInfoServiceImpl   : master
Creating a new SqlSession
SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@b545f8] was not registered for synchronization because synchronization is not active
JDBC Connection [com.alibaba.druid.proxy.jdbc.ConnectionProxyImpl@167dc2a] will not be managed by Spring
==>  Preparing: select id, data_id, group_id, md5, gmt_create, gmt_modified from config_info
==> Parameters: 
<==    Columns: id, data_id, group_id, md5, gmt_create, gmt_modified
<==        Row: 7, 101-101-101, 101-101-101, null, 2024-04-16 11:19:05, 2024-04-16 11:19:05
<==      Total: 1
Closing non transactional SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@b545f8]
==============> query from db info : [{"dataId":"127-0-0-1","gmtCreate":1713237553000,"gmtModified":1713237553000,"groupId":"127-0-0-1","id":7},{"dataId":"101-101-101","gmtCreate":1713237545000,"gmtModified":1713237545000,"groupId":"101-101-101","id":7}]
```