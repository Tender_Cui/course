# 背景

&emsp;&emsp;在使用 `Jackson` 进行 `反序列化` 时，如果字段包含日期或时间字符串，并且该字符串的格式 `不符合默认的日期时间格式`，
你可以通过 <span style="color:red">自定义反序列化器</span> 来处理该字段。

&emsp;&emsp;以下是如何为时间字符串创建一个自定义反序列化器的步骤。

# 一、步骤

## 1.1、创建自定义的反序列化器

&emsp;&emsp;首先，你需要创建一个继承自 `JsonDeserializer<T>` 的类，并重写 `deserialize` 方法。在该方法中，你将定义如何将 `字符串` 解析为 `Date` 或 `LocalDate` 等类型。

&emsp;&emsp;假设我们有一个字段，它存储日期时间字符串，如 `yyyy-MM-dd HH:mm:ss` 格式。我们将创建一个自定义反序列化器来解析这个时间字符串。

```java
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateDeserializer extends JsonDeserializer<Date> {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String dateStr = p.getText().trim();
        try {
            return dateFormat.parse(dateStr);
        } catch (ParseException e) {
            throw new IOException("Unable to parse date: " + dateStr, e);
        }
    }
}
```

> 建议解析失败的时候有几种推荐的做法：① 抛异常【方便捕获异常】 ② 尝试另一个字符串格式的反序列化


## 1.2、注册反序列化器

&emsp;&emsp;接下来，你可以使用 `@JsonDeserialize` 注解将这个 `反序列化器` 应用到特定的字段。

```java
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;

import java.util.Date;

@Data
public class ReqParam {

    private String name;

    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date eventDate;
}
```

# 二、使用 `Jackson` 进行反序列化

```java
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {
    public static void main(String[] args) throws Exception {
        String json = "{\"name\":\"Sample Event\",\"eventDate\":\"2025-01-03 14:30:00\"}";

        ObjectMapper objectMapper = new ObjectMapper();
        ReqParam param = objectMapper.readValue(json, ReqParam.class);

        System.out.println("Event Name: " + param.getName());
        System.out.println("Event Date: " + param.getEventDate());
    }
}
```

> 解释：

* CustomDateDeserializer 类负责处理自定义日期时间字符串的解析。这里使用了 SimpleDateFormat 来解析字符串，并将其转换为 Date 对象。
* @JsonDeserialize(using = CustomDateDeserializer.class) 注解告诉 Jackson 使用 CustomDateDeserializer 来反序列化 eventDate 字段。
* ObjectMapper 的 readValue 方法用于将 JSON 字符串转换为 Event 对象。