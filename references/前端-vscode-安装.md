# 参考文献

* [vscode 官网](https://code.visualstudio.com/)

# 一、下载

![下载](./pictures/前端-vscode-安装/img.png)

# 二、安装

&emsp;&emsp;具体步骤如下：

![同意协议](./pictures/前端-vscode-安装/img_1.png)

<hr>

![选择安装路径](./pictures/前端-vscode-安装/img_2.png)

<hr>

![默认](./pictures/前端-vscode-安装/img_3.png)

<hr>

![倒数第二个不选择](./pictures/前端-vscode-安装/img_4.png)

<hr>

![安装完成](./pictures/前端-vscode-安装/img_5.png)

<hr>

![简单介绍](./pictures/前端-vscode-安装/img_6.png)








