# 参考文献

* 直接问 `DeepSeek` 

```shell
  win10 安装 datasets 版本号 3.2.0
```

# 一、命令安装 `datasets`
```shell
pip install datasets==3.2.0
```

# 二、测试

```shell
import datasets
print(datasets.__version__)  # 输出应为 3.2.0
```

&emsp;&emsp;结果如下：

```shell
D:\A_Tender_workspace\install_4_code\python\python_3.10.9\python.exe D:\A_Tender_workspace\A_code\A_code_4_python\LLaMA-Factory\envTest\DatasetsTest.py 
3.2.0

```

