# 参考文献

* [`Sentinel`官网下载地址](https://github.com/alibaba/Sentinel/tags)
* [`Sentinel`流量守卫](https://www.cnblogs.com/euneirophran/p/18073905)

# 机器准备


| 机器ip            |    部署资源     | 端口       |
|-----------------|:-----------:|----------|
| 192.168.255.100 | Sentinel    | 8080（默认） |  

# 1、下载地址

&emsp;&emsp;[`Sentinel`官网下载地址](https://github.com/alibaba/Sentinel/tags)

![`Sentinel`官网](./pictures/Centos7-Sentinel/img.png)

&emsp;&emsp;下载`sentinel-dashboard-1.8.6.jar`

![下载指定版本的`Sentinel`](./pictures/Centos7-Sentinel/img_1.png)

# 2、上传&&运行

## 2.1、上传
&emsp;&emsp;上传到虚机`192.168.255.100`

![上传到虚机](./pictures/Centos7-Sentinel/img_2.png)

## 2.2、启动

```shell
java -jar sentinel-dashboard-1.8.6.jar
```

![启动`Sentinel`成功](./pictures/Centos7-Sentinel/img_3.png)

## 2.3、防火墙

```shell
# 查看防火墙状态
systemctl status firewalld
 
firewall-cmd --zone=public --add-port=8080/tcp --permanent
# 刷新一下
firewall-cmd --reload
```

# 3、验证

&emsp;&emsp;访问如下网址

```html
http://192.168.255.100:8080/
```

![访问页面](./pictures/Centos7-Sentinel/img_4.png)

&emsp;&emsp;可以看到首页的版本号是`1.8.6`

![首页的版本号是`1.8.6`](./pictures/Centos7-Sentinel/img_5.png)