# 参考文献
* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)

# 一、Jenkins 常见的构建触发器

&emsp;&emsp;Jenkins内置**4种**构建触发器: 

> 从某个项目进去 -> 配置 -> 构建触发器

* 1、触发远程构建
* 2、其他工程构建后触发（Build after other projects are build）
* 3、定时构建（Build periodically）
* 4、轮询SCM（Poll SCM）

## 1.1、触发远程构建

![触发远程构建](./pictures/devOps_jenkins03/img.png)

---

![浏览器输入url触发远程构建](./pictures/devOps_jenkins03/img_1.png)

## 1.2、其他工程构建后触发

![其他工程构建后触发](./pictures/devOps_jenkins03/img_2.png)

## 1.3、定时构建

![定时构建](./pictures/devOps_jenkins03/img_3.png)

&emsp;&emsp;需要填写cron 表达式：分 时 日 月 周（从左往右）

> 一些定时表达式的例子：

```shell
# 每30分钟构建一次：H代表形参 
H/30 * * * * 10:02 10:32

# 每2个小时构建一次: 
H H/2 * * *

# 每天的8点，12点，22点，一天构建3次： (多个时间点中间用逗号隔开) 
0 8,12,22 * * *

# 每天中午12点定时构建一次 
H 12 * * *

# 每天下午18点定时构建一次 
H 18 * * *

# 在每个小时的前半个小时内的每10分钟 
H(0-29)/10 * * * *

# 每两小时一次，每个工作日上午9点到下午5点(也许是上午10:38，下午12:38，下午2:38，下午4:38) 
H H(9-16)/2 * * 1-5
```

## 1.4、轮询SCM

&emsp;&emsp;轮询SCM，是指定时扫描本地代码仓库的代码是否有变更，如果代码有变更就触发项目构建。

![轮询SCM](./pictures/devOps_jenkins03/img_4.png)

> 注意：Jenkins会定时扫描本地整个项目的代码，增大系统的开销，不建议使用轮询SCM。

# 二、Git hook自动触发构建

&emsp;&emsp;前面的<<1.4、轮询SCM>>在Jenkins的内置构建触发器中，轮询SCM可以实现Gitlab代码更新，项目自动构建，但是该方案的性能不佳。那有没有更好的方案呢？

&emsp;&emsp;还是有的：利用Gitlab的 **webhook** 实现代码push到仓库，立即触发项目自动构建。

![轮询SCM原理与webhook原理的比较](./pictures/devOps_jenkins03/img_5.png)

## 2.1、安装插件

> 需要安装两个插件：

* Generic Webhook Trigger
* GitLab

&emsp;&emsp;安装过程省略! 安装完毕后[重启Jenkins](http://192.168.255.101:8888/restart)

## 2.2、Jenkins 构建触发器的配置

![构建触发器的配置](./pictures/devOps_jenkins03/img_6.png)

## 2.3、Gitlab 的配置

&emsp;&emsp;<span style="color:red">需要以 root 账号来设置</span>

![Gitlab允许发送webhooks请求](./pictures/devOps_jenkins03/img_7.png)

> 保存后，项目配置 webhook

![项目配置 webhook](./pictures/devOps_jenkins03/img_8.png)

> 保存后，测试

![报错](./pictures/devOps_jenkins03/img_9.png)

> 解决方案：

&emsp;&emsp;Manage Jenkins->System

![解决方案](./pictures/devOps_jenkins03/img_10.png)

> 保存后，再次测试

![二次测试](./pictures/devOps_jenkins03/img_11.png)

> 提交代码后，查看jenkins

![提交代码后，查看jenkins](./pictures/devOps_jenkins03/img_12.png)

# 三、Jenkins的参数化构建

&emsp;&emsp;有时在项目构建的过程中，我们需要根据用户的输入<span style="color:red">动态</span>传入一些参数，从而影响整个构建结果，这时我们可以使用参数化构建。

&emsp;&emsp;比如在原来Jenkinsfile中只指定了master分支，那么如何用参数替换分支呢？

> Jenkins支持非常丰富的参数类型:

&emsp;&emsp;某个具体项目->配置->General

![参数类型列表](./pictures/devOps_jenkins03/img_14.png)

&emsp;&emsp;接下来演示通过输入gitlab项目的分支名称来部署不同分支项目

&emsp;&emsp;新建分支：feature-dev，代码稍微改动下，然后提交到gitlab上。这时看到gitlab上有一个两个分支：master 和 feature-dev

## 3.1、创建一个 String 类型的参数

![String 类型的参数](./pictures/devOps_jenkins03/img_15.png)

## 3.2、修改项目根据路径中的 Jenkinsfile 文件

&emsp;&emsp;以读取变量值的方式，来获取具体的分支

![读取branch变量的具体值](./pictures/devOps_jenkins03/img_16.png)

> 新版本的jenkins 还需要修改如下配置：

![额外修改配置4新版本](./pictures/devOps_jenkins03/img_17.png)

> 测试参数化构建

![测试参数化构建](./pictures/devOps_jenkins03/img_18.png)

---

![分支结果](./pictures/devOps_jenkins03/img_19.png)

# 四、配置邮箱服务器&&发送构建结果

## 4.1、配置邮箱服务器

&emsp;&emsp;安装 Email Extension 插件(安装步骤省略)

&emsp;&emsp;Jenkins设置邮箱相关参数：Manage Jenkins->System

TODO 34、35集   
[视频地址](https://www.bilibili.com/video/BV1kJ411p7mV/)






