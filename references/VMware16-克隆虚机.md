# 参考文献

* [Linux高级教程（下）](https://zhuanlan.zhihu.com/p/646236761)

# 一、虚机克隆步骤

&emsp;&emsp;学习环境中，一般使用VMware虚拟机克隆Linux系统，用来进行**集群服务器**的搭建。

&emsp;&emsp;VMware支持两种类型的克隆： 完整克隆、链接克隆

&emsp;&emsp;完整克隆是和原始虚拟机完全独立的一个复制，它不和原始虚拟机共享任何资源。可以脱离原始虚拟机独立使用。 
链接克隆需要和原始虚拟机共享同一虚拟磁盘文件，不能脱离原始虚拟机独立运行。但采用共享磁盘文件却大大缩短了创建克隆虚拟机的时间，
同时还节省了宝贵的物理磁盘空间。通过链接克隆，可以轻松的为不同的任务创建一个独立的虚拟机。

> 本教程使用**完整克隆**的方式

&emsp;&emsp;TIPS：公网DNS解析有哪些  

```shell
# 谷歌DNS
8.8.8.8

# 国内的公共域名解析DNS
114.114.114.114
```

> 克隆之前请[查看如何让Linux固定ip地址](./VMware16-固定ip.md ':include')

> 虚拟机<span style="color:red">克隆的前置条件</span>：虚拟机处于关闭状态。

> 开始克隆

![开始克隆](./pictures/WMware16_clone/img.png)

> 下一页

![下一页](./pictures/WMware16_clone/img_1.png)

> 下一步

![克隆自](./pictures/WMware16_clone/img_2.png)

> 下一步

![完整克隆](./pictures/WMware16_clone/img_3.png)

> 下一步

![新的虚机名字加存储](./pictures/WMware16_clone/img_4.png)

> 正在克隆

![正在克隆](./pictures/WMware16_clone/img_5.png)

> 克隆完毕

![克隆完毕](./pictures/WMware16_clone/img_6.png)

# 二、修改虚拟机的硬件配置

![修改硬件配置](./pictures/WMware16_clone/img_7.png)

> 内存配置

![内存配置](./pictures/WMware16_clone/img_8.png)

> cpu配置

![cpu配置](./pictures/WMware16_clone/img_9.png)

> 网络配置

![网络配置](./pictures/WMware16_clone/img_10.png)

# 三、修改虚拟机的网络配置

> 修改主机名

```shell
vim /etc/hostname

# 修改成如下名字
slave-01
```



```shell
vim /etc/sysconfig/network-scripts/ifcfg-ens33
```
![修改slave-01](./pictures/WMware16_clone/img_11.png)

> 重启网络 && 查看ip

```shell
# 重启网络服务
systemctl restart network
```

> slave-01 查看修改后的ip
![查看修改后的ip-01](./pictures/WMware16_clone/img_12.png)






