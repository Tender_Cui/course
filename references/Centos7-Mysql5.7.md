# 参考文献
* [Centos7安装Mysql5.7（超详细版）](https://blog.csdn.net/liu_chen_yang/article/details/124930789)
* [mysql之my.cnf详解](https://blog.csdn.net/ShyTan/article/details/125997031)
* [找不到/tmp/mysql.sock](https://blog.csdn.net/hjf161105/article/details/78850658)

# 一、前置准备工作

## 1.1、卸载系统自带 mariadb

```shell
rpm -qa | grep mariadb

# 如果有则删除
rpm -e --nodeps mariadb-libs-5.5.56-2.el7.x86_64
# 再次检查
rpm -qa | grep mariadb
```

![查看已有的mariadb](./pictures/mysql5.7/img.png)

## 1.2、检查系统是否安装过 MySQL

```shell
rpm -qa | grep mysql
```

![查看是否安装过mysql](./pictures/mysql5.7/img_1.png)

&emsp;&emsp;如果系统中安装过 MySQL ，查询所有 MySQL 对应的文件夹&&全部删除

```shell
whereis mysql
find / -name mysql
```

&emsp;&emsp;查看 **my.cnf** 配置文件，因为默认的mysql的配置文件就是在/etc/下的；我们可以看到也是没有my.cnf的。
![查看mysql默认配置文件my.cnf](./pictures/mysql5.7/img_2.png)

## 1.3、检查有无 MySQL 用户组

&emsp;&emsp;检查有无 MySQL 用户组，没有则创建

> 检查有无

```shell
cat /etc/group | grep mysql
cat /etc/passwd | grep mysql
```

> 没有则创建

```shell
groupadd mysql
# -r, --system 创建一个系统账户
useradd -r -g mysql mysql
```

# 二、安装 MySQL5.7

## 2.1、下载mysql5.7

&emsp;&emsp;[mysql5.7下载地址](https://dev.mysql.com/downloads/mysql/5.7.html)

![下载mysql5.7](./pictures/mysql5.7/img_3.png)

> 点击下载

![点击下载](./pictures/mysql5.7/img_4.png)

## 2.2、上传到服务器&&解压缩

> 推荐使用远程连接工具 finalShell

&emsp;&emsp;上传到/opt/softs 目录（看个人）

> 解压缩

```shell
# 建立一个文件夹
mkdir mysql5.7_package
# 解压
tar xf mysql-5.7.44-linux-glibc2.12-x86_64.tar -C mysql5.7_package/
```

![解压](./pictures/mysql5.7/img_5.png)

&emsp;&emsp;解压缩<span style="color:red">.tar</span>文件后，得到两个<span style="color:red">.tar.gz</span> 文件。

> 继续解压缩文件：mysql-5.7.44-linux-glibc2.12-x86_64.tar.gz

```shell
tar -zxvf mysql-5.7.44-linux-glibc2.12-x86_64.tar.gz -C ./../../installs/
# 修改解压的文件名
mv mysql-5.7.44-linux-glibc2.12-x86_64/ mysql5.7
```

## 2.3、更改 mysql5.7 目录下所有文件夹所属的用户组、用户以及文件权限

```shell
cd /opt/installs
chown -R mysql:mysql mysql5.7/
chmod -R 775 mysql5.7/
```

## 2.4、把mysql5.7/bin目录下的所有命令创建一个“快捷方式”

```shell
# 把这个追加到配置文件里，路径不同，记得要修改路径
echo "export PATH=$PATH:/opt/installs/mysql5.7/bin" >> /etc/profile

# 生效配置文件
source /etc/profile
```

&emsp;&emsp;生效之后，我们可以打出mysql，按tab补全键两下，就可以看到所有的mysql/bin下的都能出来，这样设置是相当于windows的快捷键，以便我们更好的利用；

![mysql所有快捷命令展示](./pictures/mysql5.7/img_6.png)

## 2.5、初始化mysql，同时生成mysql的临时密码

```shell
# 前面已经把mysql5.7/bin 目录下的命令都放到PATH 路径了。下面这个命令放到哪里都可以运行
mysqld --user=mysql --initialize --datadir=/opt/installs/mysql5.7/data
```

![mysql临时登录明码](./pictures/mysql5.7/img_7.png)

```shell
F(G-gf/<c2Q2
```

&emsp;&emsp;如果执行mysqld --user=mysql --initialize --datadir=/opt/installs/mysql5.7/data 报这个错；根据提示安装如下包即可；

```shell
yum -y install libaio*

#安装完成再次执行就好了
mysqld --user=mysql --initialize --datadir=/opt/installs/mysql5.7/data
```

## 2.6、复制启动文件到 /etc/init.d/ 目录

&emsp;&emsp;如果mysql5.7 默认安装在 /usr/local 目录，则不需要修改 mysql.server 文件，否则需要修改。

```shell
vim /opt/installs/mysql5.7/support-files/mysql.server
```

![修改文件mysql.server](./pictures/mysql5.7/img_8.png)

> 再复制启动文件到 /etc/init.d/ 目录

```shell
# -ar 保留原文件的权限，用户属组等原始属性的复制，并且如果是文件夹，递归的复制文件夹到目标文件夹。
cp -ar /opt/installs/mysql5.7/support-files/mysql.server /etc/init.d/mysqld
```

## 2.7、添加my.cnf配置文件

```shell
cd /etc

# 创建 my.cnf 文件
touch my.cnf
```

&emsp;&emsp;在 notepadd++ 中编写，注意<span style="color:red">编码为UTF-8 以及注意要是Linux 的换行</span>。

&emsp;&emsp;在 /opt/installs/mysql5.7/ 下提前手动创建 log 和 socket 文件夹以及log 文件夹下的 mysql.err 文件（因为我指定到这边了，视各人情况）

```shell
cd /opt/installs/mysql5.7/
mkdir log
mkdir socket

# 创建文件
cd log
touch mysql.err

cd ..

# 指定用户组和用户
chown -R mysql:mysql log/
chown -R mysql:mysql socket/

chmod -R 775 log/
chmod -R 775 socket/
```

---

```shell
[client]
port = 3306
socket =  /opt/installs/mysql5.7/socket/mysql.sock

[mysqld]
datadir=/opt/installs/mysql5.7/data
basedir=/opt/installs/mysql5.7/
log-error =  /opt/installs/mysql5.7/log/mysql.err 
socket =  /opt/installs/mysql5.7/socket/mysql.sock
port = 3306
sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES
symbolic-links=0
max_connections=400
innodb_file_per_table=1
lower_case_table_names=1
```

> 给mysql的配置文件加执行权限；

```shell
chmod -R 775 /etc/my.cnf
```

## 2.8、启动mysql服务

```shell
#启动mysql服务
/etc/init.d/mysqld start
```

![启动成功](./pictures/mysql5.7/img_9.png)

## 2.9、设置开机自启

```shell
# 添加服务
chkconfig --add mysqld

# 显示服务列表
chkconfig --list
```

![开机自启](./pictures/mysql5.7/img_10.png)


## 2.10、登录 mysql &&修改 mysql 密码

&emsp;&emsp;使用临时密码登录mysql

```shell
mysql -uroot -p
```

![登录mysql5.7](./pictures/mysql5.7/img_11.png)

&emsp;&emsp;登录进来之后，我们来修改密码

```shell
# 修改密码为 88888888
set password for root@localhost = password('88888888');
```

![重新登陆](./pictures/mysql5.7/img_12.png)

## 2.11、开放远程登陆&&测试本地客户端连接

```shell
# 登录进来之后，切换到mysql库
use mysql;
# 修改用户权限
update user set user.Host='%' where user.User='root';
# 刷新权限
flush privileges;
```

![开放远程登陆](./pictures/mysql5.7/img_13.png)

> 防火墙放开 3306 端口

```shell
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --reload
```

> 远程连接

![远程连接](./pictures/mysql5.7/img_14.png)