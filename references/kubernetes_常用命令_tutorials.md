# 一、k8s 常用命令手册

## 1.1、查看宿主机相关的命令

```shell
# 查看 k8s 集群 所有节点
kubectl get nodes  

# 查看宿主机node 的描述
kubectl describe node [node name]

# 查看master 节点的污点 Taints
kubectl describe node k8s-master | grep Taints
```

## 1.2、查看命名空间（ns）相关的命令

```shell
# 创建命名空间
kubectl create ns [namespace name]

# 删除命名空间
kubectl delete ns [namespace name]

# 查看某个命名空间部署的所有pod
kubectl get pod -n [namespace name]
kubectl get pods -n [namespace name]
```

## 1.3、根据资源文件（yaml）来创建和删除资源

```shell
# 根据配置文件，给 k8s 集群创建资源
kubectl apply -f xxxx.yaml

# 根据配置文件，来删除 k8s 集群创建的资源
kubectl delete -f xxxx.yaml
```

## 1.4、与pod相关的命令

```shell
# 以指定 [pod name] 来创建一个 Pod （默认命名空间）
kubectl run [pod name] --image=nginx

# 进入到 Pod 里面（和 docker exec -it 一样的）
kubectl exec -it [pod name] -n [namespace name] -- /bin/sh
kubectl exec -it [pod name] -n [namespace name] -- /bin/bash

# 查看 某个命名空间 [namespace name] 下的 [pod name] 的描述
kubectl describe pod [pod name] -n [namespace name]

# 删除某个命名空间 [namespace name] 下的 [pod name]
kubectl delete pod [pod name] -n [namespace name]

# 查看某个命名空间 [namespace name] 下的 [pod name] 对应的日志
kubectl logs [pod name] -n [namespace name]
kubectl logs -f [pod name] -n [namespace name]

# 查看 默认命名空间（default）下部署的pod
kubectl get pods
kubectl get pod

# 查看 某个命名空间 [namespace name] 下部署的pod
kubectl get pods -n [namespace name]
kubectl get pod -n [namespace name]

# 查看某个命名空间 [namespace name] 下pod完整的信息，有 部署的 节点、节点IP 等。
kubectl get pod -A -owide -n [namespace name]

# 打印某个命名空间 [namespace name] 下 pod 的整个状态变化过程
kubectl get pod -A -w
```

## 1.5、与 deployment/deploy 相关的命令

```shell
# 用 Deployment 部署 Pod，deploy 名为 [deploy name]，使用镜像 tomcat:8.5.68
kubectl create deployment [deploy name] --image=tomcat:8.5.68 -n [namespace name]

# 查看某个命名空间 [namespace name] 下 Deployment
kubectl get deploy -n [namespace name]

# 删除某个命名空间 [namespace name] 下 Deployment
kubectl delete deploy [deploy name] -n [namespace name]

# 在某个命名空间 [namespace name] 下，利用deployment 来创建 3 个 pod
kubectl create deployment [deploy name] --image=nginx --replicas=3 -n [namespace name]

# 某个名空间 [namespace name] 下，进行扩容/缩容
kubectl scale deploy/[deploy name] --relicas=5 -n [namespace name]

# 也可以通过 修改 yaml 中的 replicas 来达到 扩缩容
kubectl edit deploy [deploy name]

# 查看某个名空间 [namespace name] 下，deploy 对应的 yaml 配置
kubectl get deploy/my-dep -oyaml -n [namespace name]
kubectl get deploy/my-dep -o yaml -n [namespace name]

# 查看之前的镜像，默认是 default 命名空间
kubectl get deploy/my-nginx -o yaml | grep nginx

# 更换镜像，--record: 保留历史记录，可用于未来的回滚等操作，默认是 default 命名空间
kubectl set image deployment/my-nginx nginx=nginx:1.16.1 --record

# 查看滚动升级的状态，默认是 default 命名空间
kubectl rollout status deployment/my-nginx

# 查看 deploy 的历史记录，默认是 default 命名空间
kubectl rollout history deployment/my-nginx

# 查看 deploy 的某个具体版本的历史记录，默认是 default 命名空间
kubectl rollout history deployment/my-nginx --revision=2

# deploy 回滚到上一个版本，默认是 default 命名空间
kubectl rollout undo deployment/my-nginx

# deploy 回滚到某个版本，默认是 default 命名空间
kubectl rollout undo deployment/my-nginx --to-revision=2
```

## 1.6、与 service（svc） 相关的命令

```shell
# 只能集群内部访问（--type不写 默认 ClusterIP）
kubectl expose deploy [deploy name] --port=8000 --target-port=80 --type=ClusterIP

# 集群外部也可以访问 --type=NodePort
kubectl expose deploy [deploy name] --port=8000 --target-port=80 --type=NodePort

# 域名：默认是 服务.所在命名空间.svc
# 这个域名只能在 pod 内可以访问，如果在外部访问，域名是不行的
[deploy name].[namespace name].svc

# 查看 service，里面有 CLUSTER-IP
kubectl get svc
kubectl get service

# 删除 service
kubectl delete svc [svc name] -n [namespace name]
```

&emsp;&emsp;yaml文件创建 service (默认是 ClusterIP 方式)
```yaml
apiVersion: v1
kind: Service
metadata:
  labels:
    [label key]: [label value]
  name: [service name]
spec:
  selector:
    [label key]: [label value]
  ports:
    - port: [service port]
      protocal: TCP
      targetPort: [pod port]
```

&emsp;&emsp;yaml文件创建 service (指定 NodePort 方式)

```yaml
kind: Service
apiVersion: v1
metadata:
  labels:
    k8s-app: kubernetes-dashboard
  name: kubernetes-dashboard
  namespace: kubernetes-dashboard
spec:
  type: NodePort
  ports:
    - port: 443
      targetPort: 8443
      # nodePort 不指定，则随机生成      
      nodePort: 30001
  selector:
    k8s-app: kubernetes-dashboard
```

## 1.6、与 ingress（ing） 相关的命令

```shell
kubectl get ing -A
kubectl get ingress -A
```

## 1.8、与污点 Taint 相关的命令

```shell
# 查看污点 Taints
kubectl describe node k8s-master | grep Taints

# 删除污点
kubectl taint node k8s-master node-role.kubernetes.io/master:NoSchedule-

# 添加污点
kubectl taint node k8s-master node-role.kubernetes.io/master:NoSchedule
```

