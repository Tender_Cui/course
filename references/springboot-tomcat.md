# 参考文献

* [Java 在IDEA社区版中配置Tomcat并使用](https://cloud.tencent.com/developer/article/2156070)

# 一、概述

&emsp;&emsp;因为在 IDEA 社区版中没有 Tomcat and TomEE Integration 和 Application Servers，所以在 Edit Configurations 的时候找不 Tomcat 这个选项。

&emsp;&emsp;不过社区版可以安装 Smart Tomcat 插件，这样就可以在社区版中集成 Tomcat 了。

# 二、下载 Smart Tomcat 插件

## 2.1、下载

&emsp;&emsp; File > Settings > Plugins 搜索 tomcat，点击 install，安装之后点击 Restart IDE，重启 IDEA即可。

## 2.2、配置

![配置tomcat 安装路径](./pictures/springboot-tomcat/img.png)


# 三、在 IDEA 中配置 Tomcat

![配置tomcat](./pictures/springboot-tomcat/img_1.png)
 
## 3.1、启动tomcat

![启动tomcat](./pictures/springboot-tomcat/img_2.png)

![启动tomcat的日志](./pictures/springboot-tomcat/img_3.png)

## 3.2、浏览器访问

![浏览器访问](./pictures/springboot-tomcat/img_4.png)