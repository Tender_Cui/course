# 参考文献
* [idea创建war工程](https://blog.csdn.net/zhou_xue_java/article/details/84947640)

# 一、创建步骤

## 1.1、创建项目

![创建项目](./pictures/idea_create_war_project/img.png)

## 1.2、maven创建war 工程

![maven创建war 工程](./pictures/idea_create_war_project/img_1.png)

## 1.3、搞定

![搞定](./pictures/idea_create_war_project/img_2.png)