# 参考文献

* [Python 官网下载地址](https://www.python.org/downloads/)

# 一、Python 下载

&emsp;&emsp;[官网下载地址](https://www.python.org/downloads/)   
&emsp;&emsp;[华为镜像下载地址](https://mirrors.huaweicloud.com/python/)   
&emsp;&emsp;[阿里镜像下载地址](https://developer.aliyun.com/mirror/)  &emsp;点击进去，搜索 `Python`

> 推荐`阿里镜像`下载

&emsp;&emsp;具体步骤如下：

1. 访问[阿里镜像地址](https://developer.aliyun.com/mirror/)
2. 镜像仓库搜索：`Python`，然后回车

![镜像仓库搜索](./pictures/Windows-Python-安装/img.png)

3. 点击搜索结果：`Python release`
4. 点击下载地址

![点击下载地址](./pictures/Windows-Python-安装/img_1.png)

5. 选择具体的 `OS`，我选择 `windows`

&emsp;&emsp; 我下载的是 `python-3.10.9-amd64.exe`
> 注意：下载的是<span style="color:red;"> 64位</span>

# 二、安装

&emsp;&emsp; 安装步骤如下：

![自定义安装](./pictures/Windows-Python-安装/img_2.png)

![下一步](./pictures/Windows-Python-安装/img_3.png)

![自定义安装目录](./pictures/Windows-Python-安装/img_4.png)

![安装success](./pictures/Windows-Python-安装/img_5.png)

# 三、配置环境变量

&emsp;&emsp;我的电脑 -> 属性 -> 高级系统设置 -> 环境变量 -> Path

&emsp;&emsp;主要就是两个目录：

```shell
D:\A_Tender_workspace\install_4_code\python\python_3.11\Scripts\
D:\A_Tender_workspace\install_4_code\python\python_3.11\
```

# 四、指定安装源来安装模块

```
pip install agentUniverse -i https://mirrors.aliyun.com/pypi/simple/
pip install magent-ui ruamel.yaml -i https://mirrors.aliyun.com/pypi/simple/
```