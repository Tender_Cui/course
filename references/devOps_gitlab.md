# 参考文献
* [黑马程序员Java教程自动化部署Jenkins从环境配置到项目开发](https://www.bilibili.com/video/BV1kJ411p7mV)
* [gitlab安装及使用教程](https://zhuanlan.zhihu.com/p/534072989)
* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)
* [gitlab搭建后 用户头像不显示问题](https://www.cnblogs.com/ll666/p/17818026.html)
* [用户头像不显示-官方解决方案](https://docs.gitlab.com/ee/administration/libravatar.html)

> Gitlab代码托管服务器的安装

# 一、简介

&emsp;&emsp; [gitlab官网](https://about.gitlab.com/)。

&emsp;&emsp;GitLab 是一个用于仓库管理系统的开源项目，使用Git作为代码管理工具，并在此基础上搭建起来的web服务。

&emsp;&emsp;GitLab和GitHub一样属于第三方基于Git开发的作品，免费且开源（基于MIT协议），与Github类似，可以注册用户，任意提交你的代码，添加SSHKey等等。不同的是，GitLab是可以部署到自己的服务器上，数据库等一切信息都掌握在自己手上，适合团队内部协作开发，你总不可能把团队内部的智慧总放在别人的服务器上吧？简单来说可把GitLab看作个人版的GitHub。

> 环境要求：<span style="color:red">内存不低于2G</span>

&emsp;&emsp;查看Linux系统的版本信息

```shell
cat /proc/version
```

&emsp;&emsp;查看Linux系统发行版信息

```shell
cat /proc/redhat-release
# 或者
cat /etc/centos-release
```

# 二、Gitlab 的安装

## 2.1、相关依赖软件的安装、启动&&自启动

> 安装相关依赖

```shell
yum -y install policycoreutils openssh-server openssh-clients postfix
```

> 启动ssh服务&设置为开机启动

```shell
systemctl enable sshd && systemctl start sshd
```

> 设置postﬁx开机自启，并启动，postﬁx支持gitlab发信功能(比如邮件等)

```shell
systemctl enable postfix && systemctl start postfix
```

> 开放ssh以及http服务，然后重新加载防火墙列表 

```shell
firewall-cmd --add-service=ssh --permanent 
firewall-cmd --add-service=http --permanent 
firewall-cmd --add-service=https --permanent
firewall-cmd --reload
```
> 如果关闭防火墙就不需要做以上配置

## 2.2、下载并安装gitlab 的 rpm包

### 2.2.1、<span style="color:red">通过rpm包进行安装（推荐）</span>

> 不同版本的Linux对应不同的rpm包。
> 若Linux版本为Centos 7，则需要下载el7版本的GitLab。

> 下载el7版本的GitLab，

[下载地址1](https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/)   

[下载地址2](https://packages.gitlab.com/app/gitlab/gitlab-ce/search)

> 下载el7版本rpm

```shell
cd /opt/softs
wget https://mirrors.tuna.tsinghua.edu.cn/gitlab-ce/yum/el7/gitlab-ce-15.0.0-ce.0.el7.x86_64.rpm
# 或者
wget https://packages.gitlab.com/gitlab/gitlab-ce/packages/el/7/gitlab-ce-15.0.0-ce.0.el7.x86_64.rpm/download.rpm
```

> 安装

```shell
rpm -ivh gitlab-ce-12.4.2-ce.0.el7.x86_64.rpm
```

> 若是提示错误：依赖检测失败： policycoreutils-python 被 gitlab-ce-12.4.2-ce.0.el7.x86_64 需要

```shell
# 则使用如下命令：
yum install -y policycoreutils-python
```

> 修复错误后，继续运行安装命令

![gitlab安装成功](./pictures/devOps_gitlab/img.png)

### 2.2.2、通过yum源安装

> TODO

# 三、配置修改

> 修改gitlab配置

```shell
# 安装成功的时候，有提示需要修改这个文件，才能完成我们的定制化要求
vim /etc/gitlab/gitlab.rb
```

> 修改gitlab访问地址和端口，默认为80，我们改为82 

```shell
# 根据个人自己的ip 情况
external_url 'http://192.168.255.100:82'
# 默认80端口，我们用82
nginx['listen_port'] = 82
```

> 重载配置及启动gitlab 

```shell
# 重新加载配置 （很耗时 5-6min）
gitlab-ctl reconfigure  
# 然后再重启gitlab
gitlab-ctl restart
```

> GitLab常用命令

```shell
gitlab-ctl start # 启动所有GitLab组件
gitlab-ctl stop # 停止所有GitLab组件
gitlab-ctl restart # 重启所有GitLab组件
gitlab-ctl status # 查看服务状态
gitlab-ctl reconfigure # 刷新配置
gitlab-rake gitlab:check SANITIZE=true --trace # 检查GitLab
gitlab-ctl tail # 查看日志

# GitLab的守护进程关闭和重启
# 备注：GitLab的守护进程会默认开启nginx
gitlab-ctl stop nginx
gitlab-ctl restart nginx

# 释放资源，关闭GitLab
gitlab-ctl stop

# 服务器重启或者GitLab意外停止，执行命令“gitlab-ctl start”。
# 若结果显示“fail: sidekiq: runsv not running”等，则说明GitLab不能启动。可以使用下面命令来解决这个问题：
systemctl start gitlab-runsvdir
gitlab-ctl restart
```

> 把端口添加到防火墙，因为我们开放的是 82 端口，不是80

```shell
firewall-cmd --zone=public --add-port=82/tcp --permanent 
firewall-cmd --reload
```

> 访问 http://192.168.255.100:82/

![访问gitlab界面](./pictures/devOps_gitlab/img_1.png)

> 启动成功后，看到以下修改管理员root密码的页面，修改密码后，然后登录即可
![修改密码](./pictures/devOps_gitlab/img_2.png)

> 登录账户默认是root

![root登录](./pictures/devOps_gitlab/img_3.png)


# 三、Gitlab 的使用

## 3.1、创建组

&emsp;&emsp;使用管理员 root 创建组，一个组里面可以有多个项目分支，可以将开发添加到组里面进行设置权限，不同的组就是公司不同的开发项目或者服务模块，不同的组添加不同的开发即可实现对开发设置权限的管理

![创建组](./pictures/devOps_gitlab/img_4.png)

> 填写具体的事项

![创建组的选择项](./pictures/devOps_gitlab/img_5.png)

> 查看已经建好的组

![查看已经建好的组](./pictures/devOps_gitlab/img_6.png)

## 3.2、在组下面创建项目

![在组下面创建项目](./pictures/devOps_gitlab/img_7.png)

## 3.3、创建用户

![创建用户](./pictures/devOps_gitlab/img_8.png)

> 下一步

![创建用户02](./pictures/devOps_gitlab/img_9.png)

> 下一步

![创建用户03](./pictures/devOps_gitlab/img_10.png)

> 为用户设置密码：88888888

![为用户设置密码](./pictures/devOps_gitlab/img_11.png)

## 3.4、为组分配成员

![为组分配成员](./pictures/devOps_gitlab/img_12.png)

> 可搜索

![可搜索](./pictures/devOps_gitlab/img_13.png)


> Gitlab用户在组里面有5种不同权限：

* Guest：可以创建issue、发表评论，不能读写版本库
* Reporter：可以克隆代码，不能提交，QA、PM可以赋予这个权限
* Developer：可以克隆代码、开发、提交、push，普通开发可以赋予这个权限
* Maintainer：可以创建项目、添加tag、保护分支、添加项目成员、编辑项目，核心开发可以赋予这个权限
* Owner：可以设置项目访问权限 - Visibility Level、删除项目、迁移项目、管理组成员，开发组组长可以赋予这个权限

> 分配好成员后，可以退出，用成员的账户登录

&emsp;&emsp;第一次登录，需要重新修改密码。

# 四、Gitlab 问题清单

## 4.1、Gitlab 头像不显示

![头像不显示](./pictures/devOps_gitlab/img_14.png)
 
&emsp;&emsp;以下为目前国内可以使用的URL: 
* https://cdn.libravatar.org/
* https://gravatar.loli.net/avatar/

&emsp;&emsp;修改如下配置文件

```shell
vim /etc/gitlab/gitlab.rb
```

> 原始内容

![原始内容](./pictures/devOps_gitlab/img_15.png)

> 内容修改





