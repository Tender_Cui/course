# 参考文献
* 

> 推荐使用<span style="color:red">FinalShell</span>工具远程连接linux服务器

# 一、常用命令

## 1.1、find

> 参考文献：[链接](https://zhuanlan.zhihu.com/p/588270288)

&emsp;&emsp;find 命令的一般语法为：
```shell
find [directory to search] [options] [expression]
```
> 方括号 [ ] 中所有的内容都是可选的，这意味着你可以在没有任何选项和参数的情况下运行 find 命令。
> 不过这只会打印出当前路径下所有的文件和目录，这样没什么用处，对吧？

* **directory to search**: 要搜索的目录，基本上就是我们要开始搜索的位置。默认情况下，搜索是递归的（也会搜索子目录），从当前位置开始。
* **options**: 选项，指定搜索的类型，可以按文件名称、文件类型、修改时间等（进行搜索），这里有50多个选项。
* **expression**: 指定的搜索词。如果是要按文件名查找，那么搜索词就是文件名；如果是要查找名称与表达式匹配的文件，那这里就用表达式。

---
&emsp;&emsp;简单示例:
```shell
find . -type f -name myfile
```
> 该命令将在当前目录及其子目录中进行搜索，查找名为 myfile 的文件（不是目录）。选项 -type f 表示只查找文件，单点 . 表示当前目录。

### 1.1.1、按名称查找文件和目录

```shell
find . -name SEARCH_NAME
```
> 由于没有指定文件类型，所以它会搜索具有给定名称的文件和目录。

### 1.1.2、只查找文件或目录

```shell
find . -type f -name SEARCH_NAME
```
> 如果只想查找文件，那么需要指定文件类型 -f：

> 类型和名称的顺序无关紧要

&emsp;&emsp;如果只搜索目录，那么指定类型 -d：
```shell
find . -type d -name SEARCH_NAME
```

### 1.1.3、执行不区分大小写的搜索

```shell
find . -type f -iname SEARCH_NAME
```
> 默认情况下，find 命令区分大小写。如果我们想要执行不区分大小写的搜索，可以使用 <span style="color:red">-iname</span> 来代替 -name：

### 1.1.4、<span style="color:red">按扩展名搜索</span>

&emsp;&emsp;find 命令最常见的用法之一就是查找指定类型的文件，或者说是按照文件扩展名来进行查找。

&emsp;&emsp;比如，我们要在当前目录下搜索所有的 C++ 文件，而C++文件的扩展名是 .cpp，所以我们可以这样搜索：
```shell
find . -type f -name "*.cpp"
```

> 注意：在使用 find 命令时，需要将<span style="color:red">表达式</span>放在 <span style="color:green">双引号</span> 中。

> 为什么要将表达式放在双引号中呢？因为如果要不这样做的话，shell会扩展通配符，将其替换为当前目录中所有以 .cpp 结尾的文件。

&emsp;&emsp;比如，假如当前目录下有两个文件：file1.cpp, file2.cpp，那么下面的命令：
```shell
find . -type f -name *.cpp
```
&emsp;&emsp;会被shell扩展为：
```shell
find . -type f -name file1.cpp file2.cpp
```

&emsp;&emsp;这种情况下，shell 就会给出提示：
```shell
find: paths must precede expression: 'file1.cpp'
find: possible unquoted pattern after predicate '-name'?
```
&emsp;&emsp;另外，如果当前目录下只有一个扩展名为.cpp的文件，那就可以。

### 1.1.5、搜索多个扩展名（或条件）的多个文件

&emsp;&emsp;上述命令搜索给定扩展名的文件。那如果要同时搜索不同扩展名的文件，该怎么办呢？

> 可以使用逻辑或（OR） -o 来运行 find 命令：

```shell
find . -type f -name "*.cpp" -o -name "*.txt"
```
&emsp;&emsp;这样，就会搜索扩展名为 .cpp 或 .txt 的文件：

```shell
find . -type f -name "*.txt" -o -name "*.cpp"
# 结果如下：
./new.txt
./file.cpp
./new/new.txt
./new/dir2/another.txt
./new/dir1/new.txt
./another.txt
```

### 1.1.6、在指定目录中查找文件

&emsp;&emsp;上面的例子都是在当前目录中搜索，因为我们在命令中指定了 . 作为当前路径。

&emsp;&emsp;<span style="color:red">点 . </span>可以替换为绝对路径或者相对路径。这样我们就可以在不离开当前路径的情况下，在指定的目录中查找文件。

```shell
find ./new -name mystuff 
# 结果如下：
./new/mystuff
```

### 1.1.7、在<span style="color:red">多个目录</span>中搜索文件
```shell
find ./location1 /second/location -type f -name "pattern"
```
> 在 find 命令中指定要搜索的<span style="color:green"> 所有目录路径 </span>即可：


### 1.1.8、查找空文件和目录

&emsp;&emsp;-empty 选项可以让你使用 find 命令查找空文件和目录。比如要查找当前路径下所有的空文件和目录，可使用如下命令：
```shell
find . -empty
```

&emsp;&emsp;也可以指定搜索的文件类型，只查找文件或者目录：
```shell
find . -empty -type f
```
&emsp;&emsp;还可以结合文件名使用：
```shell
find . -empty -type f -name "*.cpp"
```

### 1.1.9、查找大文件或小文件（<span style="color:red">根据文件大小搜索</span>）

&emsp;&emsp;根据文件的大小执行搜索，可以查找大文件或者小文件。但这<span style="color:red">只适用于文件</span>，不适用于目录。
&emsp;&emsp;要根据文件的大小搜索，可以使用 -size 选项，后面跟上一个值N（即文件的大小），**+N 查找大于 N 的文件，-N 查找小于 N 的文件**。

&emsp;&emsp;比如，查找大小正好为 50KB 的文件：
```shell
find . -size 50k
```

&emsp;&emsp;在当前路径下查找大于 1G 的文件：
```shell
find . -size +1G
```

&emsp;&emsp;查找小于 20 bytes 的文件（注意单位是c而不是b）：
```shell
find . -size -20c
```

&emsp;&emsp;要查找大于100 MB 但小于 2 GB 的文件：
```shell
find . -size +100M -size -2G
```

&emsp;&emsp;此外，还可以结合名称搜索。比如，要在根目录中查找以 .log 结尾，大于500MB的文件，可使用如下命令：
```shell
find / -size +500M -name "*.log"
```

> <span style="color:red">关于文件大小值 N 的单位：</span>
* c : bytes（字节）
* k: kilobytes（千字节）
* M: Megabytes（兆字节）
* G: Gigabytes（GB）

### 1.1.10、查找最近修改的文件（根据修改或创建时间搜索）

> 我们先来简单介绍下 mtime, atime 和 ctime 的概念：(以天为单位)

* mtime：文件上次修改的时间
* ctime：文件创建的时间
* atime：文件上次访问的时间

> 再简单介绍下 mmin, amin 和 cmin 的概念

* mmin：文件上次修改的分钟
* cmin：文件创建的分钟
* amin：文件上次访问的分钟

> 我们经常会有查找最近修改过的文件的需求，这时候，可以按修改时间来搜索文件。

&emsp;&emsp;比如，要查找 3天（3 * 24H）**以内**修改过的所有文件：
```shell
find . -type f -mtime -3
```
> 3天以内的用于 - 符号

&emsp;&emsp;再比如，查找 5天以前创建的文件：
```shell
find . -type f -ctime +5
```

&emsp;&emsp;比如，要查找 5分钟以内修改过的文件，可使用如下命令：
```shell
find . -type f -mmin -5
```

&emsp;&emsp;此外，还可以结合名称，指定时间的上限和下限进行搜索。比如，下面的命令将搜索过去20分钟到30分钟内，修改过的所有 .java 文件：
```shell
find . -type -f -mmin +20 -mmin -30 -name "*.java"
```

### 1.1.11、查找具有特定文件权限的文件

&emsp;&emsp;find命令允许您搜索具有特定权限的文件。
```shell
find -perm mode
```

&emsp;&emsp;比如，在当前路径下查找所有权限模式为 777 的文件：
```shell
find . -perm 777
```

&emsp;&emsp;查找具有读写权限的文件（完全匹配，如果文件权限为 读写和执行，则不匹配）：
```shell
find . -perm a=r+w
```

### 1.1.12、根据文件的所属用户查找

> find 还可以根据文件的所有权进行搜索。

&emsp;&emsp;比如，在当前路径下搜索用户 John 所拥有的文件：
```shell
find . -type f -user John
```

### 1.1.13、不递归查找，仅在当前目录中搜索

&emsp;&emsp;默认情况下，find 命令在当前位置的所有子目录中搜索。如果不希望这样，可以将搜索深度指定为 1。这将限制搜索仅限于当前目录，不包括任何子目录。

```shell
find . -maxdepth 1 -type f -name "*.txt"
```
> 通过指定 -maxdepth 1

### 1.1.14、处理 find 命令的输出结果（使用 exec 和 xargs）

&emsp;&emsp;可以通过对 find 命令的结果执行某些操作，来改进它。

&emsp;&emsp;例如，查找与特定名称模式匹配的文件并一次性重命名，或者查找空文件并删除。

&emsp;&emsp;我们知道，管道重定向可以用于将一个命令的输出与另一个命令输入相结合。但这对find命令的输出不起作用，至少不直接起作用。

&emsp;&emsp;如果要对find命令的结果执行操作，有两个选项，即 **exec** 和 **xargs**。

#### 1.1.14.1 使用 exec
TODO

#### 1.1.14.2 使用 xargs
TODO

## 1.2、systemctl

> 以防火墙为例

&emsp;&emsp;查看防火墙状态
```shell
systemctl status firewalld
```
![查看防火墙状态](./pictures/shell/img_6.png)

&emsp;&emsp;关闭防火墙
```shell
systemctl stop firewalld
```

&emsp;&emsp;关闭防火墙开机自启动
```shell
systemctl disable firewalld
```

> TIPS: centos6 和 centos7 启动、关停服务之间的区别

```shell
# centos6:(某些可以在centos7下使用)     
service 服务名 start|stop|status|restart     
chkconfig on|off 服务名

# centos7:  
systemctl start|stop|status|restart 服务名     
systemctl disable|enable 服务名  #开机自启动  关闭自启
```

## 1.3、ssh-keygen

> 免密登录

```shell
# 在 192.168.255.100 上输入如下命令，然后一路回车
# 适用 rsa 加密算法
ssh-keygen -t rsa
# ssh-copy-id 具体ip
ssh-copy-id 192.168.255.101
ssh-copy-id 192.168.255.102
ssh-copy-id 192.168.255.103
```

> 跳转到101、102、103 上，验证免密登录的结果

```shell
ssh 192.168.255.101
exit

ssh 192.168.255.102
exit

ssh 192.168.255.103
exit
```

![免密登录验证](./pictures/shell/img_7.png)

> 要想所有linux之间互相免密登录，则需要所有节点执行相同的操作

## 1.4、修改hosts映射

&emsp;&emsp;分别编辑每台虚拟机的hosts文件，在原有内容的基础上，**追加填写**以下内容:

> 注意：不要修改文件原来的内容，四台虚拟机的配置内容都一样。

```shell
# 编辑 hosts 文件
vim /etc/hosts

192.168.255.100 com.tender.master master
192.168.255.101 com.tender.slave-01 slave-01
192.168.255.102 com.tender.slave-02 slave-02
192.168.255.103 com.tender.slave-03 slave-03
```

## 1.5、scp

> <span style="color:red">远程拷贝、远程文件拷贝(很重要)</span>

&emsp;&emsp;远程文件拷贝命令：scp，scp是 remote file copy program 的缩写。

### 1.5.1、本地拷贝到远程机子

#### 1.5.1.1、文件

```shell
scp local_file remote_username@remote_ip:remote_folder
```
&emsp;&emsp;需求: 将本地 01.txt 文件 复制到 192.168.255.102 机器的 /export目录下
```shell
scp /export/aaa/01.txt root@192.168.255.102:/export
scp /export/aaa/02.txt root@slave-02:/export
# 如果远程的用户，与本机的用户名相同，可以省略目标的用户名
scp /export/aaa/02.txt slave-02:/export
```

#### 1.5.1.2、文件夹

> 文件夹/目录

```shell
scp -r local_folder remote_username@remote_ip:remote_folder
```

&emsp;&emsp;需求: 将本地 aaa 目录 复制到 192.168.255.102 机器的 /export 目录下
```shell
scp -r /export/aaa root@192.168.255.102:/export 
scp -r /export/aaa root@slave-02:/export 
# 如果远程的用户，与本机的用户名相同，可以省略目标的用户名
scp -r /export/aaa slave-02:/export
```

### 1.5.2、远程机子拷贝到本地

#### 1.5.2.1、文件

```shell
scp remote_username@remote_ip:remote_file local_folder
```

&emsp;&emsp;需求: 将 slave-02的 111.txt 文件 复制到 master的 export目录下
```shell
scp root@192.168.255.102:/export/a1/111.txt /export
scp root@slave-02:/export/a1/222.txt /export
scp slave-02:/export/a1/222.txt /export
```

#### 1.5.2.2、文件夹

> 文件夹/目录

```shell
scp -r remote_username@remote_ip:remote_folder local_folder
```

&emsp;&emsp;需求: 将 slave-02的 /export/a1/ 目录 复制到 master的 export目录下
```shell
scp -r root@192.168.255.102:/export/a1/export
scp -r slave-02:/export/a1 /export
```

## 1.6、ln 创建链接

> 语法

```shell
# 源文件 = 已经有的文件/目录
# 目标文件 = 快捷键
ln [选项] 源文件  目标文件
```

&emsp;&emsp;为某一个文件在另外一个位置建立一个同步的链接.当我们需要在不同的目录，用到相同的文件时，我们只要在某个固定的目录，放上该文件，然后在其它的目录下用ln命令链接（link）它就可以，不必重复的占用磁盘空间。

### 1.6.1、软连接

```shell
ln -s a.txt /opt/a_link.txt 
```

### 1.6.2、硬连接

```shell
ln a.txt /opt/a_link.txt 
```

## 1.7、配置域名解析服务器

```shell
vim /etc/resolv.conf
nameserver 114.114.114.114
```

## 1.8、cat

&emsp;&emsp;参考文献；
* [cat > file << EOF 的用法](https://www.cnblogs.com/FengZeng666/p/14174909.html)

```shell
# 1. 覆盖写
# 用来创建文件，在这之后输入的任何东西，都是在文件里的，输入完成之后以EOF结尾代表结束。
cat > 文件名 << EOF

# 2. 追加写
# 和上面一样，只不过是追加到文件，上面那个是覆盖写
cat >> 文件名 << EOF
```


# 二、缺失的命令

## 2.1、ifconfig

> 参考文献：[链接](https://blog.csdn.net/sinat_39619040/article/details/130544166)

> 设置或显示网络接口的程序，可以显示出我们机器的网卡信息

&emsp;&ensp;有些时候最小化安装CentOS等Linux发行版的时候会默认不安装 **ifconfig** 等命令，
这时候我们进入终端，运行**ifconfig**命令就会报错

![ifconfig不存在](./pictures/shell/img.png)

### 2.1.1、环境变量里没有ifconfig命令的<span style="color:red">**路径**</span>

![ifconfig是否在/sbin路径下](./pictures/shell/img_1.png)

> 环境变量里**有/sbin**这个路径，也就是说如果ifconfig命令存在，
> 并且就是位于/sbin目录下的话肯定就是可以运行的，那么就看看/sbin目录下有没有ifconfig命令：

![ifconfig不存在](./pictures/shell/img_2.png)

> 结果表明/sbin目录下并没有ifconfig命令，所以：CentOS里边是没有安装ifconfig

### 2.1.2、<span style="color:red">解决办法：</span>使用yum安装ifconfig：

![yum搜索ifconfig](./pictures/shell/img_3.png)

> 通过yum search 这个命令我们发现ifconfig这个命令是在net-tools.x86_64这个包里，接下来只要安装这个包就行了。

> yum -y install 软件名  (-y表示自动确认，-q表示静默安装)

![yum安装ifconfig](./pictures/shell/img_4.png)

### 2.1.3、检查是否安装成功
![检查是否安装成功](./pictures/shell/img_5.png)

## 2.2、vim

```shell
# 解决方案
yum install -y vim
```

## 2.3、ntpdate

> 时间同步命令，可以和阿里云时间服务器同步时间

```shell
yum install -y ntpdate

# 和阿里云时间服务器同步时间
ntpdate ntp4.aliyun.com
```

## 2.4、wget

```shell
yum install -y wget
```
