# 参考文献

* [vscode 官网](https://code.visualstudio.com/)

# 一、常用插件
* Chinese 汉化

&emsp;&emsp;中文汉化插件

![汉化插件](./pictures/前端-vscode-插件/img.png)

<hr>

* vscode-icons

&emsp;&emsp;不同类型的文件，显示不同的图标。

![图片主题插件](./pictures/前端-vscode-插件/img_1.png)

<hr>

* live-server

&emsp;&emsp;可以将写好的页面，发布到服务器上，可实时刷新

![live-server插件](./pictures/前端-vscode-插件/img_2.png)

<hr>

![live-server插件使用](./pictures/前端-vscode-插件/img_3.png)

> <span style="color:red">注意：</span>

&emsp;&emsp;使用 `live-server` 需要打开文件夹。
&emsp;&emsp;打开的网页，需要有正确、标准的网页格式！否则不会自动刷新！

* IntelliJ IDEA Keybindings

&emsp;&emsp;在 `vs-code` 中使用 `idea` 的快捷键

![idea快捷键插件](./pictures/前端-vscode-插件/img_4.png)

* prettier

&emsp;&emsp;自动美化代码，包括加空格，自动加分号

![prettier插件](./pictures/前端-vscode-插件/img_5.png)





