# 参考文献

* [PowerDesigner 16.5（附带安装包下载）](https://blog.csdn.net/qq_43263647/article/details/105377215)
* [一款超好用的E-R图工具，快速构建出高质量的数据库结构，提高开发效率和代码质量](https://blog.csdn.net/Yaoyao2024/article/details/130462433)
* [PowerDesigner安装教程](https://zhuanlan.zhihu.com/p/179260147)

# 1、下载地址

&emsp;&emsp;[破解版下载地址](https://onlinedown.rbread04.cn/huajunsafe/powerdesigner1029.zip)

> 备用地址：

```html
链接：https://pan.baidu.com/s/1SRSfk6RFeq9W0Y-GnqupUg 
提取码：d8wn
```

# 2、安装

![安装文件说明](./pictures/Window-powerDesigner-16.5-install/img.png)

1. 双击红框中的安装程序，开始安装

![开始安装](./pictures/Window-powerDesigner-16.5-install/img_1.png)

2. 进入安装界面，点击`next`按钮

![点击`next`按钮](./pictures/Window-powerDesigner-16.5-install/img_2.png)

3. 选择试用期`15`天

![选择试用15天](./pictures/Window-powerDesigner-16.5-install/img_3.png)

4. 选择中国

![选择中国](./pictures/Window-powerDesigner-16.5-install/img_4.png)

5. 选择安装路径

![选择安装路径](./pictures/Window-powerDesigner-16.5-install/img_5.png)

6. 继续`next`

![继续`next`](./pictures/Window-powerDesigner-16.5-install/img_6.png)

7. 根据自己需求选择(我这里全选）

![根据自己需求选择(我这里全选）](./pictures/Window-powerDesigner-16.5-install/img_7.png)

8. 继续`next`

![继续`next`](./pictures/Window-powerDesigner-16.5-install/img_8.png)

9. 继续`next`

![继续`next`](./pictures/Window-powerDesigner-16.5-install/img_9.png)

10. 安装进行中

![安装进行中](./pictures/Window-powerDesigner-16.5-install/img_10.png)

11. 完成安装

![完成安装](./pictures/Window-powerDesigner-16.5-install/img_11.png)

12. 破解

![破解](./pictures/Window-powerDesigner-16.5-install/img_12.png)

![替换目标中的文件](./pictures/Window-powerDesigner-16.5-install/img_13.png)

13. 启动

&emsp;&emsp;点击`windows`的开始

![点击`windows`的开始](./pictures/Window-powerDesigner-16.5-install/img_14.png)

14. 创建快捷键

![创建快捷键](./pictures/Window-powerDesigner-16.5-install/img_15.png)

![创建快捷键2](./pictures/Window-powerDesigner-16.5-install/img_16.png)

> 问题：如果快捷键打不开，可以到安装目录找到如下启动入口：
> `pdlegacyshell16.exe`，利用这个打开也可以。

# 3、使用

## 3.1、去除网格线

![展示修改配置](./pictures/Window-powerDesigner-16.5-install/img_17.png)

![去掉这个](./pictures/Window-powerDesigner-16.5-install/img_18.png)

## 3.2、导出概念模型为图片格式

&emsp;&emsp;分为三个步骤：
1. 全选   
2. Edit   
3. Export Image

&emsp;&emsp;选择图片格式进行导出，这里优先推荐`.emf`格式，如果给`mac`看的话，则导出`.png`格式。

