# 参考文献

# 一、常见连接(join)

* inner join：可以省略关键字 `inner`，甚至<span style="color:red">可以 inner join 都不写</span>！
* left outer join（左外连接）: 可以省略关键字 `outer` 
* right outer join（右外连接）: 可以省略关键字 `outer`
* full outer join（全外连接）:  返回两个表中的所有行，无论它们是否有匹配的行。如果一方没有匹配，结果会用 NULL 填充。

# 二、详解

## 2.1、inner join

<hr>

&emsp;&emsp;<span style="color:skyblue">显式内连接（INNER JOIN）</span>


```sql
SELECT 
    A.id, B.name
FROM A
INNER JOIN B 
    ON A.id = B.id;
```

<hr>

&emsp;&emsp;<span style="color:skyblue">隐式内连接（逗号连接）</span>

```sql
SELECT 
    A.id, A.name, B.age 
FROM A, B 
WHERE 
    A.id = B.id;
```

## 2.2、left outer join

* **定义**：`LEFT OUTER JOIN` 返回左表（A）中的所有行，以及 右表（B）中 **匹配** 的行。如果右表中没有匹配的行，结果会显示 NULL。

* **行为**：所有来自左表的数据都会出现在结果中，而右表中的数据如果没有匹配，结果会填充为 NULL。

```sql
SELECT 
    A.id, B.name
FROM A
LEFT OUTER JOIN B 
    ON A.id = B.id;
```

## 2.3、right outer join

* **定义**：`RIGHT OUTER JOIN` 返回右表（B）中的所有行，以及 左表（A）中匹配的行。如果左表中没有匹配的行，结果会显示 NULL。

* **行为**：所有来自右表的数据都会出现在结果中，而左表中的数据如果没有匹配，结果会填充为 NULL。

```sql
SELECT 
    A.id, B.name
FROM A
RIGHT OUTER JOIN B 
    ON A.id = B.id;
```

## 2.4、full outer join

* **定义**：`FULL OUTER JOIN` 返回两个表中的所有行，无论它们是否有匹配的行。如果一方没有匹配，结果会用 NULL 填充。
* **行为**：左表和右表的所有行都会出现在结果中，且未匹配的地方用 NULL 填充。

```sql
SELECT 
    A.id, B.name
FROM A
FULL OUTER JOIN B 
    ON A.id = B.id;
```

> 结论：
> 无论左右表是否匹配，所有的数据都会显示，未匹配的地方用 NULL 填充。




