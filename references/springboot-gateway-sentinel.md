# 参考文献

* [sentinel 官方文档之`网关限流`](https://github.com/alibaba/Sentinel/wiki/%E7%BD%91%E5%85%B3%E9%99%90%E6%B5%81)

# 1、`pom.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-gateway-sentinel-nacos</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <!--
        从 Spring Cloud 2020.0.0 版本开始，Ribbon 被废弃，Spring Cloud LoadBalancer 成为了推荐的负载均衡方案。
        在这个版本变动中，为了提供更大的灵活性，spring-cloud-starter-loadbalancer 被标记为了可选依赖，不再默认包含在 Spring Cloud Gateway 中。
        因此，在使用 3.1.4 版本的 Spring Cloud Gateway 并需要服务发现和负载均衡功能时，如果没有显式包含这个依赖，就会导致无法处理 lb://URI，从而返回503错误。
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-gateway</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>

        <!-- 整合sentinel 需要 start -->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-transport-simple-http</artifactId>
            <version>1.8.6</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-spring-cloud-gateway-adapter</artifactId>
            <version>1.8.6</version>
        </dependency>
        <dependency>
            <groupId>javax.annotation</groupId>
            <artifactId>javax.annotation-api</artifactId>
            <version>1.3.2</version>
            <scope>compile</scope>
        </dependency>
        <!-- 整合sentinel 需要 end -->
    </dependencies>

</project>
```

# 2、`yaml`配置

```yaml
server:
  port: 9093

spring:
  application:
    name: shiro-gateway

  cloud:
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP

    gateway:
      routes:
        - id: backend_service_id                   # 路由ID, 要求唯一
#          uri: http://localhost:9099             # 匹配提供服务的地址(http://ip:port 方式，ip和端口需要写死，不推荐)
          uri: lb://shiro-gateway-backend-server   # 匹配提供服务的服务名（依赖服务提供者注册到nacos中的spring.application.name名称）
                                                   # 注意，需要引入 <<spring-cloud-starter-loadbalancer>>
          # 官方文档：https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/#gateway-request-predicates-factories
          predicates:
            - Path=/backend/service/**              # 断言，路径匹配后进行路由


```

# 3、网关配置类

```java
package com.tender.config;

import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayFlowRule;
import com.alibaba.csp.sentinel.adapter.gateway.common.rule.GatewayRuleManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.SentinelGatewayFilter;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.BlockRequestHandler;
import com.alibaba.csp.sentinel.adapter.gateway.sc.callback.GatewayCallbackManager;
import com.alibaba.csp.sentinel.adapter.gateway.sc.exception.SentinelGatewayBlockExceptionHandler;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.view.ViewResolver;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Configuration
public class GatewayConfiguration {
    private final List<ViewResolver> viewResolvers;
    private final ServerCodecConfigurer serverCodecConfigurer;

    public GatewayConfiguration(List<ViewResolver> viewResolvers, ServerCodecConfigurer serverCodecConfigurer) {
        this.viewResolvers = viewResolvers;
        this.serverCodecConfigurer = serverCodecConfigurer;
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    public SentinelGatewayBlockExceptionHandler sentinelGatewayBlockExceptionHandler() {
        return new SentinelGatewayBlockExceptionHandler(viewResolvers, serverCodecConfigurer);
    }

    @Bean
    @Order(-1)
    public GlobalFilter sentinelGatewayFilter() {
        return new SentinelGatewayFilter();
    }

    @PostConstruct
    public void doInit() {
        initBlockHandler();
    }

    private void initBlockHandler() {
        Set<GatewayFlowRule> rules = new HashSet<>();
        rules.add(
                new GatewayFlowRule("backend_service_id")  // 对应yaml 中的 spring.cloud.gateway.routes.id
                        .setCount(2)  // 限流阈值
                        .setIntervalSec(1)  // 统计时间窗口，单位是秒，默认是 1 秒
        );
        GatewayRuleManager.loadRules(rules);

        BlockRequestHandler handler = (serverWebExchange, throwable) -> {
            HashMap<String, String> map = new HashMap<>();
            map.put("ErrorCode", HttpStatus.TOO_MANY_REQUESTS.getReasonPhrase());
            map.put("ErrorMessage", "请求过于频繁, 触发了sentinel限流 ... ");
            return ServerResponse.status(HttpStatus.TOO_MANY_REQUESTS)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromValue(map));
        };

        GatewayCallbackManager.setBlockHandler(handler);
    }
}

```

# 4、启动类

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <a href="https://spring.io/projects/spring-cloud-gateway">spring-cloud-gateway官网</a>
 * <a href="https://docs.spring.io/spring-cloud-gateway/docs/3.1.9/reference/html/">spring-cloud-gateway官网参考手册</a>
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class ShiroGateWaySentinelApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShiroGateWaySentinelApplication.class);
    }
}
```

# 5、测试结果

![流控结果](./pictures/springboot-gateway-sentinel/img.png)






