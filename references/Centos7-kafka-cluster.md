# 参考文献

* [kafkak集群的安装部署](https://blog.csdn.net/m0_61232019/article/details/127683413)
* [Centos7系统部署搭建Kafka集群](https://blog.csdn.net/qq_35241329/article/details/131046264)
* [kafka命令详解：创建topic、生产消息、消费消息](https://blog.csdn.net/zcs2312852665/article/details/134979882)

# 1、准备工作

## 1.1、版本信息

* kafka: 2.13-3.4.0
* JDK: 1.8+

## 1.2、环境准备

&emsp;&emsp;搭建`kafka`集群至少需要`3`台服务器（或虚拟机也可），准备好`3`台不同`IP`的服务器

| 主机              | id | IP              |
|-----------------|:--:|-----------------|
| 第一台kafka主机      | 0  | 192.168.255.100 |
| 第二台kafka主机      | 1  | 192.168.255.101 |
| 第三台kafka主机      | 2  | 192.168.255.102 |

## 1.3、zookeeper 集群:

&emsp;&emsp;[请参考：zookeeper集群安装参考手册](./Centos7-ZooKeeper-cluster.md 'included')

# 2、下载 kafka

&emsp;&emsp;[官网下载地址](https://kafka.apache.org/downloads)，下载  `kafka_2.13-3.4.0.tgz (asc, sha512)`

![官网下载kafka](./pictures/Centos7-kafka-cluster/img.png)

# 3、安装kafka

## 3.1、上传&&解压

&emsp;&emsp;利用finalShell 工具上传安装包，上传到目录：

![利用finalShell工具上传](./pictures/Centos7-kafka-cluster/img_1.png)

&emsp;&emsp;上传完毕

![上传完毕](./pictures/Centos7-kafka-cluster/img_2.png)

&emsp;&emsp;解压到目录： `/opt/installs`

```shell
tar -zxvf kafka_2.13-3.4.0.tgz -C ../installs/
```

&emsp;&emsp;解压后的目录：

![解压后的目录](./pictures/Centos7-kafka-cluster/img_3.png)

## 3.2、修改配置文件：server.properties

```shell
cd /opt/installs/kafka_2.13-3.4.0/config

vim server.properties

# 要修改和新增的内容如下：

# 这个需要修改
broker.id=0  # 集群内全局唯一标识符，每一个节点上需要设置不同的值（值可以随机但是不能相同）
# 这个需要修改
# The address the socket server listens on. If not configured, the host name will be equal to the value of
# java.net.InetAddress.getCanonicalHostName(), with PLAINTEXT listener name, and port 9092.
listeners=PLAINTEXT://192.168.255.100:9092
# If not set, it uses the value for "listeners".
# advertised.listeners=PLAINTEXT://your.host.name:9092
num.network.threads=3
num.io.threads=8
socket.send.buffer.bytes=102400
socket.receive.buffer.bytes=102400
socket.request.max.bytes=104857600
# 这个需要修改
# 存放kafka消息
log.dirs=/var/kafka/logs-data
num.partitions=1
num.recovery.threads.per.data.dir=1
offsets.topic.replication.factor=1
transaction.state.log.replication.factor=1
transaction.state.log.min.isr=1
log.retention.hours=168
log.retention.check.interval.ms=300000
# 这个需要修改
# 配置zookeeper集群的地址
zookeeper.connect=192.168.255.100:2181,192.168.255.101:2181,192.168.255.102:2181
zookeeper.connection.timeout.ms=18000
group.initial.rebalance.delay.ms=0
```

## 3.3、分发 kafka安装目录

```shell
cd /opt/installs

scp -r kafka_2.13-3.4.0/ root@192.168.255.101:/opt/installs/
scp -r kafka_2.13-3.4.0/ root@192.168.255.102:/opt/installs/
```

## 3.4、修改 101 和 102机器的 server.properties

```shell
# 192.168.255.101 修改如下
 
broker.id=1
listeners=PLAINTEXT://192.168.255.101:9092
 
# 192.168.255.102 修改如下
broker.id=2
listeners=PLAINTEXT://192.168.255.102:9092
```

## 3.5、创建日志目录&&配置环境变量(三台机器都执行)

```shell
mkdir -p /var/kafka/logs-data

vim /etc/profile

export KAFKA_HOME=/opt/installs/kafka_2.13-3.4.0
export PATH=$PATH:$KAFKA_HOME/bin

# 刷新环境变量
source /etc/profile
```

## 3.6、防火墙开放端口（如果启用防火墙的话）

```shell
firewall-cmd --zone=public --add-port=9092/tcp --permanent
firewall-cmd --reload
```

# 4、启动集群并测试

## 4.1、启动

&emsp;&emsp;三台机器都启动kafka
```shell
kafka-server-start.sh /opt/installs/kafka_2.13-3.4.0/config/server.properties
```

## 4.2、查看zookeeper /brokers/ids 节点下三个 kafka 是否已经注册上

```shell
# zkCli.sh -server localhost:2181 # 连接zookeeper服务器
# 连接上 zk
zkCli.sh

# 三个 kafka 是否已经注册上
ls /brokers/ids
```

&emsp;&emsp;结果如下：

```shell
[zk: localhost:2181(CONNECTED) 2] ls /brokers/ids
[0, 1, 2]
```

## 4.3、命令行创建topic、生产、消费测试

### 4.3.1、创建topic

```shell
kafka-topics.sh --bootstrap-server 192.168.255.100:9092,192.168.255.101:9092,192.168.255.102:9092 --create --topic my-topic2 --replication-factor 1 --partitions 3  --if-not-exists

# 通过查看 kafka-topics.sh --help
# --topic my-topic2: 需要创建的主题名称是：my-topic2
# --create: Create a new topic
# --bootstrap-server: The Kafka server to connect to
# --partitions 3: 被创建的主题有3个分区
# --replication-factor 1: 每个分区拥有1个副本
# --if-not-exists: 即使主题存在，也不会抛出重复创建主题的错误
```

### 4.3.2、生产消息

```shell
kafka-console-producer.sh --bootstrap-server 192.168.255.100:9092,192.168.255.101:9092,192.168.255.102:9092 --topic my-topic2

# 输入具体的消息，回车就发送了

```

```java
/**
 * 本地搭建的环境已经创建的topic 有如下：
 * 
 * my-topic1
 * my-topic2
 * topic1
 * topic2
 * tender1
 * tender2
 */
```

![生产消息](./pictures/Centos7-kafka-cluster/img_4.png)

### 4.3.3、消费消息

```shell
 kafka-console-consumer.sh --bootstrap-server 192.168.255.100:9092,192.168.255.101:9092,192.168.255.102:9092 --topic my-topic2 --from-beginning

# --from-beginning: 从头开始消费消息
```

![消费消息](./pictures/Centos7-kafka-cluster/img_5.png)




