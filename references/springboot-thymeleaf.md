# 参考文献
* [SpringBoot整合Thymeleaf](https://blog.csdn.net/qq_58608526/article/details/132186217)
* [Thymeleaf 官网](https://www.thymeleaf.org/)
* [Thymeleaf 在Github 的主页](https://github.com/thymeleaf/thymeleaf)
* [Spring官方文档](https://docs.spring.io/spring-boot/docs/2.1.6.RELEASE/reference/htmlsingle/#using-boot-starterf)

# 一、Thymeleaf 简述

> 什么是Thymeleaf？

![Thymeleaf-logo](./pictures/thymeleaf/img.png)

* Thymeleaf 官网是这么解释的：Thymeleaf is a modern server-side Java template engine for both web and standalone environments.
* 译过来就是：Thymeleaf是适用于Web和独立环境的现代服务器端<span style="color:red">**Java模板引擎**</span>

&emsp;&emsp;<span style="color:red">Thymeleaf</span> 是一个跟 <span style="color:red">Velocity、FreeMarker</span> 类似的模板引擎，
它可以完全替代 <span style="color:red">JSP</span> 。相较与其他的模板引擎，它有如下三个非常吸引人的特点:

* **Thymeleaf** 在有网络和无网络的环境下皆可运行，即它可以让美工在浏览器查看页面的静态效果，也可以让程序员在服务器查看带数据的动态页面效果。这是由于它支持 **html** 原型，然后在 **html** 标签里增加额外的属性来达到模板 + 数据的展示方式。浏览器解释 **html** 时会忽略未定义的标签属性，所以 **thymeleaf** 的模板可以静态地运行；当有数据返回到页面时，**Thymeleaf** 标签会动态地替换掉静态内容，使页面动态显示。
* **Thymeleaf** 开箱即用的特性。它提供标准和 **Spring** 标准两种方言，可以直接套用模板实现 **JSTL**、 **OGNL** 表达式效果，避免每天套模板、改 JSTL、改标签的困扰。同时开发人员也可以扩展和创建自定义的方言。
* **Thymeleaf** 提供 **Spring** 标准方言和一个与 **SpringMVC** 完美集成的可选模块，可以快速的实现表单绑定、属性编辑器、国际化等功能。

## 1.1、什么是模板引擎

> 模板引擎（这里特指用于Web开发的模板引擎）是为了使用户界面与业务数据（内容）分离而产生的，它可以生成特定格式的文档，用于网站的模板引擎就会生成一个标准的html文档。
> 从字面上理解模板引擎，最重要的就是模板二字，这个意思就是做好一个模板后套入对应位置的数据，最终以html的格式展示出来，这就是模板引擎的作用。

&emsp;&emsp;对于模板引擎的理解，可以这样形象的做一个类比：开会！ 相信你在上学初高中时候每次开会都要提前布置场地、拿小板凳、收拾场地。而你上了大学之后每次开会再也不去大操场了,
每次开会都去学校的大会议室，桌子板凳音响主席台齐全，来个人即可，还可复用……。模板引擎的功能就类似我们的会议室开会一样开箱即用，将模板设计好之后直接填充数据即可而不需要重新设计整个页面。提高页面、代码的复用性。

![模板引擎类比图](./pictures/thymeleaf/img_1.png)

&emsp;&emsp;不仅如此，在Java中模板引擎还有很多，模板引擎是动态网页发展进步的产物，在最初并且流传度最广的jsp它就是一个模板引擎。
jsp是官方标准的模板，但是由于jsp的缺点比较多也挺严重的，所以很多人弃用jsp选用第三方的模板引擎，市面上开源的第三方的模板引擎也比较多，有Thymeleaf、FreeMaker、Velocity等模板引擎受众较广。

&emsp;&emsp;听完了模板引擎的介绍，相信你也很容易明白了模板引擎在web领域的主要作用：让网站实现界面和数据分离，这样大大提高了开发效率，让代码重用更加容易。

## 1.2、Thymeleaf介绍

&emsp;&emsp;上面知晓了模板引擎的概念和功能，你也知道Thymeleaf是众多模板引擎的一种，你一定会好奇想深入学习Thymeleaf的方方面面。从官方的介绍来看，Thymeleaf的目标很明确：

* Thymeleaf的主要目标是为您的开发工作流程带来优雅自然的模板-HTML可以在浏览器中正确显示，也可以作为静态原型工作，从而可以在开发团队中加强协作。
* Thymeleaf拥有适用于Spring Framework的模块，与您喜欢的工具的大量集成以及插入您自己的功能的能力，对于现代HTML5 JVM Web开发而言，Thymeleaf是理想的选择——尽管它还有很多工作要做。

> 并且随着市场使用的验证Thymeleaf也达到的它的目标和大家对他的期望，在实际开发有着广泛的应用。Thymeleaf作为被Springboot官方推荐的模板引擎，一定有很多过人和不寻同之处：

* **动静分离**： Thymeleaf选用html作为模板页，这是任何一款其他模板引擎做不到的！Thymeleaf使用html通过一些特定标签语法代表其含义，但并未破坏html结构，即使无网络、不通过后端渲染也能在浏览器成功打开，大大方便界面的测试和修改。
* **开箱即用**： Thymeleaf提供标准和Spring标准两种方言，可以直接套用模板实现JSTL、 OGNL表达式效果，避免每天套模板、改JSTL、改标签的困扰。同时开发人员也可以扩展和创建自定义的方言。
* Springboot官方大力推荐和支持，Springboot官方做了很多默认配置，开发者只需编写对应html即可，大大减轻了上手难度和配置复杂度。

&emsp;&emsp;此外，Thymeleaf在曾经还有一次大的版本升级，从Thymeleaf2.0—>Thymeleaf3.0。在Thymeleaf2.0时代，Thymeleaf基于xml实现，虽然它带来了许多出色强大的功能，但有时会降低性能效率，那个时候Thymeleaf的性能真的太差而被很多人所吐槽带来了很不好的印象。

&emsp;&emsp;但是Thymeleaf3.0对比Thymeleaf2.0有着翻天覆地的变化，几乎是全部重写了整个Thymeleaf引擎，在性能、效率上相比Thymeleaf2有了很大改善，能够满足更多项目的需求，且Thymeleaf3.0不再基于xml所以在html环境下有着更宽松的编程环境。

&emsp;&emsp;此外，Thymelaf3.0在方言、独立于Java Servlet API、重构核心API、片段表达等方面有着巨大提升和改善，具体可以参看[Thymeleaf3十分钟参考指南](https://www.thymeleaf.org/doc/articles/thymeleaf3migration.html)。

## 1.3、学习Thymeleaf必知的知识点

> Springboot

> MVC

&emsp;&emsp;我们使用的Thymeleaf模板引擎在整个web项目中起到的作用为视图展示(view)，
谈到视图就不得不提起模型(model)以及控制器(view),其三者在web项目中分工和职责不同，但又相互有联系。三者组成当今web项目较为流行的MVC架构。

> MVC全名是Model View Controller，是模型(model)－视图(view)－控制器(controller)的缩写，其中：
* Model（模型）表示应用程序核心（用来存储数据供视图层渲染）。
* View（视图）显示数据，而本篇使用的就是Thymeleaf作为视图。
* Controller（控制器）处理输入请求，将模型和视图分离。

![模板引擎类比图](./pictures/thymeleaf/img_2.png)

&emsp;&emsp;使用MVC设计模式程序有很多优点，比如降低程序耦合、增加代码的复用性、降低开发程序和接口的成本，并且通过这样分层结构在部署维护能够提供更大的便捷性。

&emsp;&emsp;在Java web体系最流行的MVC框架无疑就是Springmvc框架了，在项目中经常配合模板引擎使用或者提供Restful接口。在下面案例Thymeleaf同样使用Springmvc作为MVC框架进行控制。

## 1.4、动静分离

&emsp;&emsp;你可能还是不明白什么才是真正的动静分离，其实这个主要是由于Thymeleaf模板基于html，后缀也是.html，所以这样就会产生一些有趣的灵魂。

> 对于传统jsp或者其他模板来说，没有一个模板引擎的后缀为.html，就拿jsp来说jsp的后缀为.jsp,它的本质就是将一个html文件修改后缀为.jsp，然后在这个文件中增加自己的语法、标签然后执行时候通过后台处理这个文件最终返回一个html页面。

&emsp;&emsp;浏览器无法直接识别.jsp文件，需要借助网络(服务端)才能进行访问；而Thymeleaf用html做模板可以直接在浏览器中打开。开发者充分考虑html页面特性，
将Thymeleaf的语法通过html的标签属性来定义完成，这些标签属性不会影响html页面的完整性和显示。如果通过后台服务端访问页面服务端会寻找这些标签将服务端对应的数据替换到相应位置实现动态页面！大体区别可以参照下图：

![模板引擎类比图](./pictures/thymeleaf/img_3.png)

&emsp;&emsp;上图的意思就是如果直接打开这个html那么浏览器会对th等标签忽视而显示原始的内容。如果通过服务端访问那么服务端将先寻找th标签将服务端储存的数据替换到对应位置。

> 动态页面每次修改打开都需要重新启动程序、输入链接，这个过程其实是相对漫长的。如果界面设计人员用这种方式进行页面设计时间成本高并且很麻烦，可通过静态页面设计样式，设计完成通过服务端访问即可达成目标UI的界面和应用，达到动静分离的效果。这个特点和优势是所有模板引擎中Thymeleaf所独有的！

# 二、Springboot 整合Thymeleaf

## 2.1、添加maven依赖

```shell
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>
```

## 2.2、yaml配置

```shell
spring:
  # 配置thymeleaf的相关信息
  thymeleaf:
    # 开启视图解析
    enabled: true
    #编码格式
    encoding: UTF-8
    #前缀配置
    prefix: classpath:/templates/
    # 后缀配置
    suffix: .html
    #是否使用缓存 开发环境时不设置缓存
    cache: false
    # 格式为 HTML 格式
    mode: HTML5
    # 配置类型
    servlet:
      content-type: text/html

```

## 2.3、Controller代码

```java
// 此处不能使用 @RestController
// @RestController
@Controller
public class HomeController {

    @GetMapping("/home")
    public String home(Model model) {
        // 业务逻辑
        model.addAttribute("username", "lucy");

        return "index";
    }

}
```

## 2.4、html代码

> 在项目的resources目录下的templates文件夹下面创建一个叫index.html的文件，在这个html文件中的&lt;html&gt;标签修改为
> &lt;html xmlns:th="http://www.thymeleaf.org">这样在Thymeleaf中就可以使用Thymeleaf的语法和规范啦。

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<h1>这是tender ，hi</h1>
<p th:text="${username}">tom</p>
</body>
</html>
```

&emsp;&emsp;你可能会对&lt;p th:text="${username}">tom &lt;/p>感到陌生，这个标签中的th:text="${username}" 
就是Thymeleaf取值的一个语法，这个值从后台渲染而来(前面controller中在Model中存地值)，如果没网络(直接打开html文件)的时候静态数据为：tom。
而如果通过网络访问那么内容将是前面在controller的Model中储存的 lucy。


![页面渲染后](./pictures/thymeleaf/img_4.png)

# 三、Thymeleaf语法详解

&emsp;&emsp;上面虽然完成了第一个Thymeleaf程序，但是那样远远满足不了我们在项目中使用Thymeleaf，所以我们要对Thymeleaf的语法规则进行更详细的学习。

## 3.1、配置
&emsp;&emsp;虽然Springboot官方对Thymeleaf做了很多默认配置，但咱们引入Thymeleaf的jar包依赖后很可能根据自己特定需求进行更细化的配置，例如页面缓存、字体格式设置等等。

>Springboot官方提供的配置内容有以下：

```properties
# THYMELEAF (ThymeleafAutoConfiguration)
spring.thymeleaf.cache=true # Whether to enable template caching.
spring.thymeleaf.check-template=true # Whether to check that the template exists before rendering it.
spring.thymeleaf.check-template-location=true # Whether to check that the templates location exists.
spring.thymeleaf.enabled=true # Whether to enable Thymeleaf view resolution for Web frameworks.
spring.thymeleaf.enable-spring-el-compiler=false # Enable the SpringEL compiler in SpringEL expressions.
spring.thymeleaf.encoding=UTF-8 # Template files encoding.
spring.thymeleaf.excluded-view-names= # Comma-separated list of view names (patterns allowed) that should be excluded from resolution.
spring.thymeleaf.mode=HTML # Template mode to be applied to templates. See also Thymeleaf's TemplateMode enum.
spring.thymeleaf.prefix=classpath:/templates/ # Prefix that gets prepended to view names when building a URL.
spring.thymeleaf.reactive.chunked-mode-view-names= # Comma-separated list of view names (patterns allowed) that should be the only ones executed in CHUNKED mode when a max chunk size is set.
spring.thymeleaf.reactive.full-mode-view-names= # Comma-separated list of view names (patterns allowed) that should be executed in FULL mode even if a max chunk size is set.
spring.thymeleaf.reactive.max-chunk-size=0 # Maximum size of data buffers used for writing to the response, in bytes.
spring.thymeleaf.reactive.media-types= # Media types supported by the view technology.
spring.thymeleaf.servlet.content-type=text/html # Content-Type value written to HTTP responses.
spring.thymeleaf.suffix=.html # Suffix that gets appended to view names when building a URL.
spring.thymeleaf.template-resolver-order= # Order of the template resolver in the chain.
spring.thymeleaf.view-names= # Comma-separated list of view names (patterns allowed) that can be resolved.
```

&emsp;&emsp;上面的配置有些我们可能不常使用，因为Springboot官方做了默认配置大部分能够满足我们的使用需求，
但如果你的项目有特殊需求也需要妥善使用这些配置。

&emsp;&emsp;比如<span style="color:red">spring.thymeleaf.cache=false</span>是否允许页面缓存的配置，
我们在开发时候要确保页面是最新的所以需要禁用缓存；而在上线运营时可能页面不常改动为了减少服务端压力以及提升客户端响应速度会允许页面缓存的使用。

&emsp;&emsp;再比如在开发虽然我们大部分使用UTF-8多一些，我们可以使用<span style="color:red">spring.thymeleaf.encoding=UTF-8</span>来确定页面的编码，但如果你的项目是GBK编码就需要将它改成GBK。

&emsp;&emsp;另外Springboot默认模板引擎文件是放在templates目录下：
<span style="color:red">spring.thymeleaf.prefix=classpath:/templates/</span>,如果你有需求将模板引擎也可修改配置，将templates改为自己需要的目录。同理其他的配置如果需要自定义化也可参照上面配置进行修改。

## 3.2、常用标签
&emsp;&emsp;上面知道Thymeleaf通过特殊的标签来寻找属于Thymeleaf的部分，并渲染该部分内容，而除了上面展示过的th:text之外还有很多常用标签，并且Thymeleaf也主要通过标签来识别替换对应位置内容，
Thymeleaf标签有很多很多，功能也很丰富，这里列举一些比较常用的标签如下：
