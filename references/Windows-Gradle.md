# 参考文献

* [gradle学习笔记](https://www.yuque.com/youyi-ai1ik/emphm9/kyhenl)

# 版本选择问题

* 与`idea`兼容的`gradle`
> 查看`idea`安装目录下的插件：`IntelliJ IDEA Community Edition 2023.1.5\plugins\gradle\lib`

> ![idea下的gradle版本](./pictures/Windows-Gradle/img.png)

* `springboot`要求的`gradle`   
> [springboot参考文档](https://docs.spring.io/spring-boot/gradle-plugin/index.html)

> ![springboot要求的gradle版本](./pictures/Windows-Gradle/img_1.png)

# 1、下载

* [gradle各版本快速下载地址大全](https://blog.csdn.net/ii950606/article/details/109105402)
* [IDEA如何配置 Gradle（详细版）](https://blog.csdn.net/qq_40859560/article/details/129624046)

# 2、环境变量配置

```text
1、`GRADLE_HOME`：环境变量指向你的 `Gradle` 解压路径 

2、将 `%GRADLE_HOME%\bin` 添加到 `Path` 环境变量中

3、配置gradle本地仓库：`GRADLE_USER_HOME`=`C:\tender_work_space\gradle_repository`  备注：(`GRADLE_USER_HOME` 是写死的)

4、配置环境变量：`M2_HOME` = `D:\install_4_code\apache-maven-3.6.3`
```

# 3、验证安装是否ok

![验证安装结果](./pictures/Windows-Gradle/img_2.png)

# 4、创建`gradle`项目初体验

> 利用`spring initializr`建立一个`gradle`空项目

> https://start.spring.io/

![spring initializer 初始化gradle项目](./pictures/Windows-Gradle/img_3.png)

> 项目工程结构

![项目工程结构](./pictures/Windows-Gradle/img_4.png)

```text
1、① ③ 本地构建开发的时候，可以不需要
2、gradlew与gradlew.bat执行的指定wrapper版本中的gradle指令,不是本地安装的gradle指令
3、文件：build.gradle 是构建脚本，类似于maven中的pom.xml
4、设置文件：settings.gradle 用来定义项目及子项目名称信息，和项目是一一对应关系 
```

## 3.1、`gradle`项目和`maven`项目结构对比

![结构比对](./pictures/Windows-Gradle/img_5.png)

## 3.2、gradle中<span style="color:red">常用命令</span>

![常用命令](./pictures/Windows-Gradle/img_6.png)

&emsp;&emsp;需要注意的是：<span style="color:red">gradle 的指令要在含有build.gradle 的目录执行。</span>

# 4、修改`maven`下载源

&emsp;&emsp;`Gradle` 自带的`Maven` 源地址是国外的，该`Mave`n 源在国内的访问速度是很慢的，除非使用了特别的手段。一般情况下，我们建议使用国内的第三方开放的`Maven` 源或企业内部自建`Maven` 源。

## 4.1、认识`init.d`文件夹

&emsp;&emsp;我们可以在`gradle` 的`init.d` 目录下创建以`.gradle` 结尾的文件，`.gradle` 文件可以实现在`gradle build` 开始之前执行，所以你可以在这个文件配置一些你想预先加载的操作。

&emsp;&emsp;在i`nit.d` 文件夹创建`init.gradle` 文件（文件名随意）

```text
allprojects {
    repositories {
        mavenLocal()
        maven { name "Alibaba" ; url "https://maven.aliyun.com/repository/public" } 
        maven { name "Bstek" ; url "https://nexus.bsdn.org/content/groups/public/" } 
        mavenCentral()
    }
    
    buildscript {
        repositories {
            maven { name "Alibaba" ; url 'https://maven.aliyun.com/repository/public' } 
            maven { name "Bstek" ; url 'https://nexus.bsdn.org/content/groups/public/' } 
            maven { name "M2" ; url 'https://plugins.gradle.org/m2/' }
        }
    }
}
```

### 拓展 1：启用`init.gradle` 文件的方法有：

```text
1. 在命令行指定文件,例如：gradle --init-script yourdir/init.gradle -q taskName。你可以多次输入此命令来指定多个init文件
2. 把init.gradle文件放到 USER_HOME/.gradle/ 目录下
3. 把以.gradle结尾的文件放到 USER_HOME/.gradle/init.d/ 目录下
4. 把以.gradle结尾的文件放到 GRADLE_HOME/init.d/ 目录下

如果存在上面的 4 种方式的 2 种以上，gradle会按上面的1-4序号依次执行这些文件，如果给定目录下存在多个init脚本，会按拼音a-z顺序执行这些脚本，每个init脚本都存在一个对应的gradle实例,你在这个文件中调用的所有方法和属性，都会委托给这个gradle实例，每个init脚本都实现了Script接口。
```

### 拓展 2：仓库地址说明

```text
1. mavenLocal(): 指定使用maven本地仓库，而本地仓库在配置maven时settings文件指定的仓库位置。如E:/repository，
   gradle 查找 jar 包顺序如下：USER_HOME/.m2/settings.xml -> M2_HOME/conf/settings.xml -> USER_HOME/.m2/repository
2. maven { url 地址}，指定maven仓库，一般用私有仓库地址或其它的第三方库【比如阿里镜像仓库地址】。
3. mavenCentral()：这是Maven的中央仓库，无需配置，直接声明就可以使用。
4. jcenter():JCenter中央仓库，实际也是是用的maven搭建的，但相比Maven仓库更友好，通过CDN分发，并且支持https访问,在新版本中已经废弃了，替换为了mavenCentral()。
```

> 总结：

```text
gradle可以通过指定仓库地址为：本地maven仓库地址和远程仓库地址相结合的方式，避免每次都会去远程仓库下载依赖库。
这种方式也有一定的问题，如果本地maven仓库有这个依赖，就会从直接加载本地依赖，如果本地仓库没有该依赖，那么还是会从远程下载。
但是下载的jar不是存储在本地maven仓库中，而是放在自己的缓存目录中，默认在USER_HOME/.gradle/caches目录，
当然如果我们配置过GRADLE_USER_HOME环境变量，则会放在GRADLE_USER_HOME/caches目录，
```

> 那么可不可以将gradle caches指向maven repository

&emsp;&emsp;不行的，caches下载文件不是按照maven仓库中存放的方式。

### 拓展 3：阿里云仓库地址请参考

[阿里云仓库地址请参考](https://developer.aliyun.com/mvn/guide)

# 5、`Wrapper`包装器

&emsp;&emsp;`Gradle Wrapper` 实际上就是对 `Gradle` 的一层包装，用于解决实际开发中可能会遇到的不同的项目需要不同版本的 `Gradle`。

> 问题

&emsp;&emsp;例如：把自己的代码共享给其他人使用，可能出现如下情况:
1. 对方电脑没有安装 `gradle`
2. 对方电脑安装过 `gradle`，但是版本太旧了
   这时候，我们就可以考虑使用 `Gradle Wrapper` 了。这也是官方建议使用 `Gradle Wrapper` 的原因。实际上有了 `Gradle Wrapper` 之后，
   我们本地是可以不配置 `Gradle` 的,下载`Gradle` 项目后，使用 `gradle` 项目自带的`wrapper` 操作也是可以的。

> 那如何使用`Gradle Wrapper` 呢？

&emsp;&emsp;项目中的`gradlew`、`gradlew.bat`脚本用的就是`wrapper`中规定的`gradle`版本。参见`gradlew`脚本源码

&emsp;&emsp;而我们上面提到的`gradle`指令用的是本地`gradle`,所以`gradle`指令和`gradlew`指令所使用的`gradle`版本<span style="color:red">有可能是不一样的</span>。

&emsp;&emsp;`gradlew`、`gradlew.bat`的使用方式与`gradle`使用方式完全一致，只不过把`gradle`指令换成了`gradlew`指令。
当然,我们也可在终端执行 `gradlew` 指令时，指定指定一些参数,来控制 `Wrapper` 的生成，比如依赖的版本等，如下：

![来控制 `Wrapper` 的生成](./pictures/Windows-Gradle/img_7.png)

> 具体操作如下所示 ：

```text
1. gradle wrapper --gradle-version = 4.4 # 升级wrapper版本号,只是修改gradle.properties中wrapper版本，未实际下载
2. gradle wrapper --gradle-version 5.2.1 --distribution-type all # 关联源码用
```

## 5.1、`Gradle Wrapper` 的执行流程

1. 当我们第一次执行 `./gradlew build` 命令的时候，`gradlew` 会读取 `gradle-wrapper.properties` 文件的配置信息
2. 准确的将指定版本的 `gradle` 下载并解压到指定的位置(`GRADLE_USER_HOME`目录下的`wrapper/dists`目录中)
3. 并构建本地缓存(`GRADLE_USER_HOME`目录下的`caches`目录中),下载再使用相同版本的`gradle`就不用下载了
4. 之后执行的 `./gradlew` 所有命令都是使用指定的 `gradle` 版本。如下图所示：

![执行流程](./pictures/Windows-Gradle/img_8.png)

## 5.2、`gradle-wrapper.properties` 文件解读

![文件解读](./pictures/Windows-Gradle/img_9.png)

> 注意

&emsp;&emsp;前面提到的 `GRADLE_USER_HOME` 环境变量用于这里的`Gradle Wrapper` 下载的特定版本的`gradle` 存储目录。如果我们没有配置过`GRADLE_USER_HOME` 环境变量,默认在`当前用户家目录`下的`.gradle` 文件夹中。

> 那什么时候选择使用 gradle wrapper、什么时候选择使用本地gradle?

&emsp;&emsp;下载`别人的项目`或者使用操作以前自己写的`不同版本的gradle`项目时：用`Gradle wrapper`,也即:`gradlew`

> 什么时候使用本地`gradle`?

&emsp;&emsp;新建一个项目时: 使用`gradle`指令即可。




















