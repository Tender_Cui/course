# 项目整合dockerFile 打镜像

# 1、背景

1. 项目是一个多模块嵌套多模块的工程

![嵌套工程](./pictures/docker_mirroring_wth_shell/img.png)

2. 需要利用Jenkins来CI\CD
3. 利用插件 `dockerfile-maven-plugin` 可以在特定的阶段进行镜像的打包操作
4. 如果第3步骤不可以，可以利用 `shell` 脚本来代替

```xml
<!-- docker image 构建插件 start -->
<plugin>
    <groupId>com.spotify</groupId>
    <artifactId>dockerfile-maven-plugin</artifactId>
    <version>1.4.13</version>
    <configuration>
        <repository>${project.artifactId}</repository>
        <!-- 镜像的tag -->
        <tag>${project.version}</tag>
        <!-- 最终的镜像名称 -->
        <finalName>course</finalName>
        <buildArgs>
            <JAR_FILE>target/${project.build.finalName}.jar</JAR_FILE>
        </buildArgs>
    </configuration>
</plugin>
<!-- docker image 构建插件 end -->
```

# 2、具体操作步骤

## 2.1、创建 `Jenkins` 项目 `jnfp-docker-build-project-test`

![创建 `Jenkins` 项目](./pictures/docker_mirroring_wth_shell/img_1.png)

## 2.2、利用 `shell` 脚本实现编译、打包、打镜像操作

```shell
mvn clean package
echo "package success"
cd gyl-visualization/gyl-visualization-bootstrap
echo "进入目录 ============= gyl-visualization/gyl-visualization-bootstrap  "
docker build --build-arg JAR_FILE=./target/gyl-visualization-3.4.7-RELEASE.jar -t jnpf:v1 .
```

# 2.3、`docker file` 所在路径

&emsp;&emsp;`Dockerfile` 和 `打包插件`都放在同一个子模块下：

![`docker file` 所在路径](./pictures/docker_mirroring_wth_shell/img_2.png)

&emsp;&emsp;打包插件如下：

```xml
<build>
    <finalName>gyl-visualization-${project.version}</finalName>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <!-- 指定该Main Class为全局的唯一入口 -->
                <mainClass>jnpf.AntLogisticsVisualizationApplication</mainClass>
                <layout>ZIP</layout>
            </configuration>
            <executions>
                <execution>
                    <goals>
                        <goal>repackage</goal><!--可以把依赖的包都打包到生成的Jar包中-->
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

# 3、`Jenkins` 构建结果

![打镜像成功](./pictures/docker_mirroring_wth_shell/img_3.png)

> 查看镜像

![查看镜像](./pictures/docker_mirroring_wth_shell/img_4.png)




