# 参考文献

* [官网下载地址](https://www.elastic.co/downloads/)
* [ES官网下载地址](https://www.elastic.co/downloads/elasticsearch)
* [ES历史版本官网下载地址](https://www.elastic.co/downloads/past-releases#elasticsearch)


> `Elasticsearch 7.8.0`     
> 为了和课程保持一致，也是采用 `Elasticsearch 7.8.0` 版本来进行安装部署

* [`Elasticsearch 7.8.0`下载地址](https://www.elastic.co/downloads/past-releases/elasticsearch-7-8-0)

# 一、`Windows` 版本

&emsp;&emsp;`elasticsearch-7.8.0-windows-x86_64.zip`

## 1.1、解压

&emsp;&emsp;解压后的 `Elasticsearch` 的目录结构如下 ：

![windows解压后的目录](./pictures/Centos7-Elasticsearch-安装/img.png)

<hr>

&emsp;&emsp;目录解释：

| 目录  |    含义     | 
|:---:|:---------:|
|  bin  |  可执行脚本目录  | 
|  config  |   配置目录    | 
|  jdk  | 内置 JDK 目录 | 
|  lib  |    类库     | 
|  logs  |   日志目录    | 
|  modules  |   模块目录    | 
|  plugins  |   插件目录    | 

## 1.2、启动`Elasticsearch`

&emsp;&emsp;进入 `bin` 文件目录，点击 `elasticsearch.bat` 文件启动 `ES` 服务。

> 注意： `9300` 端口为 `Elasticsearch` 集群间组件的通信端口。   

> `9200` 端口为浏览器访问的 `http` 协议 `RESTful` 端口。

## 1.3、验证

&emsp;&emsp;打开浏览器，输入地址： `http://localhost:9200`，测试返回结果，返回结果如下：

```json
{
  "name": "LAPTOP-R3AEP7DG",
  "cluster_name": "elasticsearch",
  "cluster_uuid": "6FRTEJZhSJuQltRbtXhuWw",
  "version": {
    "number": "7.8.0",
    "build_flavor": "default",
    "build_type": "zip",
    "build_hash": "757314695644ea9a1dc2fecd26d1a43856725e65",
    "build_date": "2020-06-14T19:35:50.234439Z",
    "build_snapshot": false,
    "lucene_version": "8.5.1",
    "minimum_wire_compatibility_version": "6.8.0",
    "minimum_index_compatibility_version": "6.0.0-beta1"
  },
  "tagline": "You Know, for Search"
}
```
