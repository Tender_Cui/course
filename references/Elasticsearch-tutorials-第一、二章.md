# 参考文献

* [Elasticsearch学习笔记](https://blog.csdn.net/u011863024/article/details/115721328)

# 第一章 `Elasticsearch`概述

# 第二章 `Elasticsearch`入门

## 07、入门-倒排序索引

### 2.7.1、<span style="color:red">正排索引（传统）</span>


|  id   |  content  | 
|:-----:|:---------:|
| 1001	 | 好好学习，天天上网 | 
| 1002	 | 认证学习，报效祖国 | 


### 2.7.2、<span style="color:red">倒排索引</span>

|  keyword   |    id     | 
|:-----:|:---------:|
| 学习	 | 1001,1002 | 
| 祖国	 |   1002    | 


&emsp;&emsp;`Elasticsearch` 是面向文档型数据库，一条数据在这里就是一个文档。 
为了方便大家理解，我们将 `Elasticsearch` 里存储文档数据和关系型数据库 `MySQL` 存储数据的概念进行一个类比

![对比](./pictures/Elasticsearch-tutorials/img.png)

> `ES` 里的 `Index` 可以看做一个库，而 `Types` 相当于表， `Documents` 则相当于`表的行`。   

> 这里 `Types` 的概念已经被逐渐弱化， `Elasticsearch 6.X` 中，一个 `index` 下已经只能包含一个 `type`， `Elasticsearch 7.X` 中, `Type` 的概念已经被 **删除** 了。


## 08、入门-HTTP-索引-创建

### 2.8.1、<span style="color:red">创建索引</span>

&emsp;&emsp;对比关系型数据库，创建索引就等同于创建数据库。

&emsp;&emsp;在 `Apifox` 中，向 `ES` 服务器发 `PUT` 请求 ： `http://127.0.0.1:9200/shopping`

> 服务器返回响应：

```json
{
  "acknowledged": true,
  "shards_acknowledged": true,
  "index": "string"
}
```

<hr>

> 后台日志：

```shell
[2025-02-10T23:28:37,760][INFO ][o.e.c.m.MetadataCreateIndexService] [LAPTOP-R3AEP7DG] [shopping] creating index, cause [api], templates [], shards [1]/[1], mappings []
```

> 如果 **重复** 发 `PUT` 请求 ： `http://127.0.0.1:9200/shopping` 添加索引，会返回错误信息 :

```json
{
    "error": {
        "root_cause": [
            {
                "type": "resource_already_exists_exception",
                "reason": "index [shopping/gsvzMc0gS9O37EAd5juJTw] already exists",
                "index_uuid": "gsvzMc0gS9O37EAd5juJTw",
                "index": "shopping"
            }
        ],
        "type": "resource_already_exists_exception",
        "reason": "index [shopping/gsvzMc0gS9O37EAd5juJTw] already exists",
        "index_uuid": "gsvzMc0gS9O37EAd5juJTw",
        "index": "shopping"
    },
    "status": 400
}
```

## 09、入门-HTTP-索引-查询 & 删除

### 2.9.1、<span style="color:red">查看单个索引</span>

&emsp;&emsp;查询刚刚创建的 `shopping` 单个索引，用 `GET` 请求 `http://127.0.0.1:9200/shopping`

```json
{
    "shopping": {
        "aliases": {},
        "mappings": {},
        "settings": {
            "index": {
                "creation_date": "1739201317735",
                "number_of_shards": "1",
                "number_of_replicas": "1",
                "uuid": "gsvzMc0gS9O37EAd5juJTw",
                "version": {
                    "created": "7080099"
                },
                "provided_name": "shopping"
            }
        }
    }
}
```

<hr>

### 2.9.2、<span style="color:red">查看所有索引</span>

&emsp;&emsp;<span style="color:red">查看所有索引</span> `http://127.0.0.1:9200/_cat/indices?v`

> 这里请求路径中的 `_cat` 表示查看的意思， `indices` 表示索引复数，所以整体含义就是查看当前 `ES` 服务器中的所有索引，就好像 `MySQL` 中的 `show tables` 的感觉，服务器响应结果如下 :

&emsp;&emsp;结果如下：

```shell
health status index    uuid                   pri rep docs.count docs.deleted store.size pri.store.size
yellow open   shopping gsvzMc0gS9O37EAd5juJTw   1   1          0            0       208b           208b
```


|      表头       |    含义     | 
|:-------------:|:---------:|
|    health	    | 当前服务器健康状态： green(集群完整) yellow(单点正常、集群不完整) red(单点不正常) | 
|    status	    |   索引打开、关闭状态    | 
|    index	     |   索引名    | 
|     uuid	     |   索引统一编号    | 
|     pri	      |   主分片数量    | 
|     rep	      |   副本数量    | 
|  docs.count	  |   可用文档数量    | 
| docs.deleted	 |   文档删除状态（逻辑删除）    | 
|  store.size   |   主分片和副分片整体占空间大小    | 
|  pri.store.size   |   主分片占空间大小    | 


### 2.9.3、<span style="color:red">删除单个索引</span>

&emsp;&emsp;向 `ES` 服务器发 `DELETE` 请求 ： `http://127.0.0.1:9200/shopping`

> 结果如下：

```json
{
    "acknowledged": true
}
```

&emsp;&emsp;再次查看 `所有索引`，GET `http://127.0.0.1:9200/_cat/indices?v`

> 结果如下：

```shell
health status index uuid pri rep docs.count docs.deleted store.size pri.store.size
```

&emsp;&emsp;成功删除！


## 10、入门-HTTP-文档-创建（Put & Post）

&emsp;&emsp;假设 **索引** 已经创建好了，接下来我们来创建 **文档**，并添加数据。

> `PUT` 请求，要求幂等

### 2.10.1、<span style="color:red">创建文档-POST</span>

> 这里的 <span style="color:red">文档</span> 可以类比为关系型数据库中的 <span style="color:red">表</span> 数据，添加的数据格式为 `JSON` 格式

<hr>

&emsp;&emsp;向 `ES` 服务器发 `POST` 请求 ： `http://127.0.0.1:9200/shopping/_doc`

![创建文档](./pictures/Elasticsearch-tutorials/img_1.png)

&emsp;&emsp;`JSON` 请求内容

```JSON
{
    "title":"小米手机",
    "category":"小米",
    "images":"http://www.gulixueyuan.com/xm.jpg",
    "price":3999.00
}
```

> 此处发送请求的方式 **必须** 为 `POST`，不能是 `PUT`，否则会发生错误 

&emsp;&emsp;返回结果：

```json
{
    "_index": "shopping",           // 索引
    "_type": "_doc",                // 类型-文档
    "_id": "KDOf8pQBZ7a-N0vznYLw",  // 唯一标识，可以类比为 MySQL 中的主键，随机生成
    "_version": 1,                  // 版本
    "result": "created",            // 结果，这里的 create 表示创建成功
    "_shards": {
        "total": 2,                 // 分片 - 总数
        "successful": 1,            
        "failed": 0
    },
    "_seq_no": 0,
    "_primary_term": 2
}
```

> 上面的数据创建后，由于没有指定数据唯一性标识（`ID`），默认情况下， `ES` 服务器会随机生成一个。

### 2.10.2、<span style="color:red">创建文档-PUT/POST(需要指定ID)</span>

&emsp;&emsp;如果想要<span style="color:red">自定义唯一性标识</span>，需要在创建时指定： `http://127.0.0.1:9200/shopping/_doc/1001`

![创建索引-POST](./pictures/Elasticsearch-tutorials/img_2.png)

&emsp;&emsp;结果返回：

```json
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1001",          // ------------------自定义唯一性标识
    "_version": 1,
    "result": "created",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 1,
    "_primary_term": 2
}
```

&emsp;&emsp;
> 1. 如果增加数据时<span style="color:red">明确数据主键</span>，那么请求方式既可以为 <span style="color:red">POST</span>，也可以为 <span style="color:red">PUT</span>。

> 2. 自定义 `id` 的时候，`_doc` 还可以换成 `_create`

```shell
http://127.0.0.1:9200/shopping/_create/1002
```

## 11、入门-HTTP-查询-主键查询 & 全查询

### 2.11.1、<span style="color:red">根据主键查询文档</span>

&emsp;&ensp;查看文档时，需要指明文档的 **唯一性标识**，类似于 `MySQL` 中数据的 **主键** 查询

&emsp;&ensp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_doc/1001` 

&emsp;&emsp;返回结果如下：

```json
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1003",
    "_version": 1,
    "_seq_no": 8,
    "_primary_term": 2,
    "found": true,               // 查找成功
    "_source": {
        "title": "小米手机",
        "category": "小米",
        "images": "http://www.gulixueyuan.com/xm.jpg",
        "price": 3999.00
    }
}
```

&emsp;&emsp;查找不存在的内容，向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_doc/999999`

&emsp;&emsp;返回结果如下：
```json
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "999999",
    "found": false          // 没有查找到
}
```

### 2.11.2、<span style="color:red">查看索引下所有数据</span>

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;结果返回

```json
{
    "took": 120,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 5,
            "relation": "eq"
        },
        "max_score": 1.0,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            },
           ... // 省略中间值
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KTOt8pQBZ7a-N0vz6IL1",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            }
        ]
    }
}

```

## 12、入门-HTTP-全量修改 & 局部修改 & 删除

### 2.12.1、<span style="color:red">全量修改（覆盖）</span>

&emsp;&emsp;和 `新增文档` 一样，输入相同的 `URL` 地址请求，如果请求体变化，会将原有的数据内容覆盖

&emsp;&emsp;向 `ES` 服务器发 `POST` 请求 ： `http://127.0.0.1:9200/shopping/_doc/1001`

&emsp;&emsp;入参

```json
{
    "title":"华为手机",
    "category":"华为",
    "images":"http://www.gulixueyuan.com/hw.jpg",
    "price":1999.00
}
```

&emsp;&emsp;返回结果：

```json
{
  "_index": "shopping",
  "_type": "_doc",
  "_id": "1001",
  "_version": 7,
  "result": "updated",    // 更新结果
  "_shards": {
    "total": 2,
    "successful": 1,
    "failed": 0
  },
  "_seq_no": 10,
  "_primary_term": 2
}
```

> 如果更新的 `id` **不存在**，则会创建一个新的文档。


### 2.12.2、<span style="color:red">局部修改</span>

&emsp;&emsp;修改数据时，也可以只修改某一给条数据的 <span style="color;red">局部</span> 信息

&emsp;&emsp;向 `ES` 服务器发 `POST` 请求 ： `http://127.0.0.1:9200/shopping/_update/1001`

&emsp;&emsp;请求入参

```json
{
	"doc": {
		"title":"小米手机",
		"category":"小米"
	}
}

```

&emsp;&emsp;返回结果：

```json
{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1001111",
    "_version": 4,
    "result": "noop",
    "_shards": {
        "total": 0,
        "successful": 0,
        "failed": 0
    },
    "_seq_no": 14,
    "_primary_term": 2
}
```

<hr>

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_doc/1001111`

```json

{
    "_index": "shopping",
    "_type": "_doc",
    "_id": "1001111",
    "_version": 4,
    "_seq_no": 14,
    "_primary_term": 2,
    "found": true,
    "_source": {
        "title": "小米手机",
        "category": "小米",
        "images": "http://www.gulixueyuan.com/hw.jpg",
        "price": 1999.0
    }
}
```


## 13、入门-HTTP-条件查询 & 分页查询 & 查询排序

### 2.13.1、<span style="color:red">`URL`带参的查询条件</span>

> 这种方式<span style="color:red">了解即可，实际不用</span>

&emsp;&emsp;查找 `category` 为小米的文档，`http://127.0.0.1:9200/shopping/_search?q=category:小米`

&emsp;&emsp;结果返回：

```json
{
    "took": 58,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 5,
            "relation": "eq"
        },
        "max_score": 1.0521863,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0521863,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            },
            ... // 内容省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": 1.0521863,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.0
                }
            }
        ]
    }
}
```

&emsp;&emsp;上述为 `URL` 带参数形式查询，这很容易让不善者心怀恶意，或者参数值出现中文会出现乱码情况。为了避免这些情况，我们可用使用带 `JSON` 请求体请求进行查询。


### 2.13.2、<span style="color:red">请求体带参查询</span>

&emsp;&emsp;以 `JSON` 请求体来查找 `category` 为 `小米` 的文档，向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp; `JSON` 体如下：

````json
{
  "query": {
    "match": {
      "category": "小米"
    }
  }
}
````

&emsp;&emsp; 返回结果：

```json
{
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 5,
            "relation": "eq"
        },
        "max_score": 1.0521863,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0521863,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            },
            // ... 省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": 1.0521863,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.0
                }
            }
        ]
    }
}
```

### 2.13.3、<span style="color:red">请求体查询所有内容</span>

&emsp;&emsp;查找<span style="color:green">所有文档内容</span>，也可以这样，向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp; `JSON` 请求体内容为：

```json
{
    "query": {
        "match_all": {}
    }
}
```

&emsp;&emsp;返回内容如下：

```json
{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": 1.0,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            },
            // ... 内容省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.0
                }
            }
        ]
    }
}

```

### 2.13.4、<span style="color:red">只想查询文档的指定字段</span>

&emsp;&emsp;如果你想查询指定字段，向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体如下:

```json
{
	"query":{
		"match_all":{}  // 查询所有
	},
	"_source":["title"]
}

```

&emsp;&emsp;返回内容如下：

```json
{
    "took": 5,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": 1.0,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机"
                }
            },
            // ... 省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机"
                }
            }
        ]
    }
}

```

### 2.13.5、<span style="color:red">分页查询</span>

&emsp;&emsp;`ES` 分页查询类似于 `Mysql` 的分页查询，向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体内容：

```json
{
    "query": {
        "match_all": {}
    },
    "_source": ["title"],
    "from": 0,
    "size": 2
}

```

&emsp;&emsp;返回内容如下：

```json
{
    "took": 4,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": 1.0,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机"
                }
            },
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1002",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机"
                }
            }
        ]
    }
}
```

### 2.13.6、<span style="color:red">查询排序</span>

&emsp;&emsp;如果你想通过排序查出价格最高的手机，向 `ES` 服务器发 `GET` 请求 ： http://127.0.0.1:9200/shopping/_search

&emsp;&emsp;`JSON` 请求体如下：

```json
{
	"query":{
		"match_all":{}
	},
	"sort":{
		"price":{
			"order":"desc"
		}
	}
}

```

&emsp;&emsp;返回内容如下：

```json
{
    "took": 45,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": null,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": null,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                },
                "sort": [
                    3999.0
                ]
            },
           // ... 省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": null,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.0
                },
                "sort": [
                    1999.0
                ]
            }
        ]
    }
}
```

## 14、入门-HTTP-多条件查询 & 范围查询

### 2.14.1、<span style="color:red">多条件查询&&</span>

&emsp;&emsp;假设想找出小米牌子，价格为 `3999` 元的。（`must`相当于数据库的<span style="color:red">&&</span>）

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： http://127.0.0.1:9200/shopping/_search

&emsp;&emsp;`JSON` 请求体如下：

```json
{
	"query":{
		"bool":{
			"must":[{
				"match":{
					"category":"小米"
				}
			},{
				"match":{
					"price":3999.00
				}
			}]
		}
	}
}

```

&emsp;&emsp;结果如下：

```json
{
  "took": 33,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 4,
      "relation": "eq"
    },
    "max_score": 2.0521863,
    "hits": [
      {
        "_index": "shopping",
        "_type": "_doc",
        "_id": "KDOf8pQBZ7a-N0vznYLw",
        "_score": 2.0521863,
        "_source": {
          "title": "小米手机",
          "category": "小米",
          "images": "http://www.gulixueyuan.com/xm.jpg",
          "price": 3999.00
        }
      },
      // ... 省略
      {
        "_index": "shopping",
        "_type": "_doc",
        "_id": "KTOt8pQBZ7a-N0vz6IL1",
        "_score": 2.0521863,
        "_source": {
          "title": "小米手机",
          "category": "小米",
          "images": "http://www.gulixueyuan.com/xm.jpg",
          "price": 3999.00
        }
      }
    ]
  }
}
```

### 2.14.2、<span style="color:red">多条件查询||</span>

&emsp;&emsp;假设想找出 **小米** 和 **华为** 的牌子。（<span style="color:red">should</span>相当于数据库的<span style="color:red"> ||</span>）

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体如下：

```json
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "category": "小米"
          }
        },
        {
          "match": {
            "category": "华为"
          }
        }
      ]
    }
  }
}
```

&emsp;&emsp;结果如下：

```json
{
  "took": 6,
  "timed_out": false,
  "_shards": {
    "total": 1,
    "successful": 1,
    "skipped": 0,
    "failed": 0
  },
  "hits": {
    "total": {
      "value": 6,
      "relation": "eq"
    },
    "max_score": 1.7876358,
    "hits": [
      {
        "_index": "shopping",
        "_type": "_doc",
        "_id": "1001",
        "_score": 1.7876358,
        "_source": {
          "title": "华为手机",
          "category": "华为",
          "images": "http://www.gulixueyuan.com/hw.jpg",
          "price": 1999.00
        }
      },
      // ... 省略
      {
        "_index": "shopping",
        "_type": "_doc",
        "_id": "1001111",
        "_score": 1.0521863,
        "_source": {
          "title": "小米手机",
          "category": "小米",
          "images": "http://www.gulixueyuan.com/hw.jpg",
          "price": 1999.0
        }
      }
    ]
  }
}
```

### 2.14.3、<span style="color:red">范围查询</span>

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体如下：

```json
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "category": "小米"
          }
        },
        {
          "match": {
            "category": "华为"
          }
        }
      ],
      "filter": {
        "range": {
          "price": {
            "gt": 2000
          }
        }
      }
    }
  }
}

```

&emsp;&emsp;结果如下：

```json
{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 4,
            "relation": "eq"
        },
        "max_score": 1.0521863,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0521863,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            },
            // ... 省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KTOt8pQBZ7a-N0vz6IL1",
                "_score": 1.0521863,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            }
        ]
    }
}
```

## 15、入门-HTTP-全文检索 & 完全匹配 & 高亮查询

### 2.15.1、<span style="color:red">全文检索</span>

&emsp;&emsp;像搜索引擎那样，如品牌输入“小华”，返回结果带回品牌有“小米”和“华为”的。

&emsp;&emsp;`JSON` 请求体如下：

```json
{
    "query": {
        "match": {
            "category": "小华"
        }
    }
}
```

&emsp;&emsp;结果如下：

```json
{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": 0.8938179,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001",
                "_score": 0.8938179,
                "_source": {
                    "title": "华为手机",
                    "category": "华为",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.00
                }
            },
            // ... 省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": 0.5260931,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.0
                }
            }
        ]
    }
}
```

### 2.15.2、<span style="color:red">完全匹配</span>

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体如下：

```json
{
    "query": {
        "match_phrase": {
            "category": "为"
        }
    }
}
```

&emsp;&emsp;结果如下：

```json
{
    "took": 2,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 1,
            "relation": "eq"
        },
        "max_score": 0.8938179,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001",
                "_score": 0.8938179,
                "_source": {
                    "title": "华为手机",
                    "category": "华为",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.00
                }
            }
        ]
    }
}
```

### 2.15.3、<span style="color:red">高亮查询</span>

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体如下：

```json
{
  "query": {
    "match_phrase": {
      "category": "为"
    }
  },
  "highlight": {
    "fields": {
      "category": {}  //<----高亮这字段
    }
  }
}
```

&emsp;&emsp;结果如下：

```json
{
    "took": 61,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 1,
            "relation": "eq"
        },
        "max_score": 0.8938179,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001",
                "_score": 0.8938179,
                "_source": {
                    "title": "华为手机",
                    "category": "华为",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.00
                },
                "highlight": {
                    "category": [
                        "华<em>为</em>"
                    ]
                }
            }
        ]
    }
}
```

## 16、入门-HTTP-聚合查询

&emsp;&emsp;聚合允许使用者对 `ES` 文档进行统计分析，类似与 `关系型数据库` 中的 `group by`，当然还有很多其他的聚合，例如取最大值 `max`、平均值 `avg` 等等。

### 2.16.1、<span style="color:red">分组</span>

&emsp;&emsp;接下来按 `price` 字段进行分组：

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： http://127.0.0.1:9200/shopping/_search

&emsp;&emsp;`JSON` 请求体如下：

```json
{
  "aggs":{//聚合操作
    "price_group":{//名称，随意起名
      "terms":{//分组
        "field":"price"//分组字段
      }
    }
  }
}
```

&emsp;&emsp;结果如下：

```json
{
    "took": 32,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": 1.0,
        "hits": [
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "KDOf8pQBZ7a-N0vznYLw",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/xm.jpg",
                    "price": 3999.00
                }
            },
            // ... 省略
            {
                "_index": "shopping",
                "_type": "_doc",
                "_id": "1001111",
                "_score": 1.0,
                "_source": {
                    "title": "小米手机",
                    "category": "小米",
                    "images": "http://www.gulixueyuan.com/hw.jpg",
                    "price": 1999.0
                }
            }
        ]
    },
    "aggregations": {
        "price_group": {  // 我们自己指定的名字
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": 3999.0,
                    "doc_count": 4
                },
                {
                    "key": 1999.0,
                    "doc_count": 2
                }
            ]
        }
    }
}
```

<hr>

&emsp;&emsp;上面返回结果会附带原始数据的。若不想要不附带原始数据的结果， 向 `ES` 服务器发 `GET` 请求 ： `http://127.0.0.1:9200/shopping/_search`

&emsp;&emsp;`JSON` 请求体如下：

```json
{
  "aggs":{//聚合操作
    "price_group":{//名称，随意起名
      "terms":{//分组
        "field":"price"//分组字段
      }
    }
  },
  "size": 0
}
```

&emsp;&emsp;结果如下：

```json
{
    "took": 9,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": null,
        "hits": []
    },
    "aggregations": {
        "price_group": {
            "doc_count_error_upper_bound": 0,
            "sum_other_doc_count": 0,
            "buckets": [
                {
                    "key": 3999.0,
                    "doc_count": 4
                },
                {
                    "key": 1999.0,
                    "doc_count": 2
                }
            ]
        }
    }
}
```

### 2.16.2、<span style="color:red">求平均值</span>

&emsp;&emsp;若想对所有手机价格求 **平均值**。

&emsp;&emsp;向 `ES` 服务器发 `GET` 请求 ： http://127.0.0.1:9200/shopping/_search

&emsp;&emsp;`JSON` 请求体如下：

```json
{
  "aggs":{//聚合操作
    "price_avg":{//名称，随意起名
      "avg":{    //求均值
        "field":"price"//哪个字段求均值
      }
    }
  },
  "size": 0
}
```

&emsp;&emsp;结果如下：

```json
{
    "took": 3,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 6,
            "relation": "eq"
        },
        "max_score": null,
        "hits": []
    },
    "aggregations": {
        "price_avg": {
            "value": 3332.3333333333335
        }
    }
}
```

## 17、入门-HTTP-映射关系

&emsp;&emsp;如果有了<span style="color:red">索引库</span>，等于有了数据库中的 `database`。

&emsp;&emsp;接下来就需要建索引库(`index`)中的映射了，类似于数据库(`database`)中的表结构(`table`)。

&emsp;&emsp;创建数据库表需要设置字段名称，类型，长度，约束等；`索引库` 也一样，需要知道这个类型下有哪些字段，每个字段有哪些约束信息，这就叫做映射(`mapping`)。

### 2.17.1、<span style="color:red">创建一个索引</span>

```shell
# PUT http://127.0.0.1:9200/user
```

&emsp;&emsp;结果返回：

```json
{
    "acknowledged": true,
    "shards_acknowledged": true,
    "index": "user"
}
```

### 2.17.2、<span style="color:red">创建映射</span>

```shell
# PUT http://127.0.0.1:9200/user/_mapping

{
    "properties": {
        "name":{  // 有 name 字段
        	"type": "text",  // 可以被分词
        	"index": true    // 被索引
        },
        "sex":{
        	"type": "keyword",   // 关键字,不可以被分词
        	"index": true        // 被索引
        },
        "tel":{
        	"type": "keyword",  // 关键字,不可以被分词
        	"index": false      // 不被索引
        }
    }
}

```

&emsp;&emsp;结果返回：

```json
{
    "acknowledged": true
}
```

### 2.17.3、<span style="color:red">查询映射</span>

```shell
#GET http://127.0.0.1:9200/user/_mapping

```

&emsp;&emsp;结果返回：

```json
{
  "user": {
    "mappings": {
      "properties": {
        "name": {
          "type": "text"
        },
        "sex": {
          "type": "keyword"
        },
        "tel": {
          "type": "keyword",
          "index": false
        }
      }
    }
  }
}
```

### 2.17.4、<span style="color:red">增加数据</span>

```shell
#PUT http://127.0.0.1:9200/user/_create/1001
{
	"name":"小米",
	"sex":"男的",
	"tel":"1111"
}

```

&emsp;&emsp;结果返回：

```json
{
    "_index": "user",
    "_type": "_doc",
    "_id": "1001",
    "_version": 1,
    "result": "created",
    "_shards": {
        "total": 2,
        "successful": 1,
        "failed": 0
    },
    "_seq_no": 0,
    "_primary_term": 1
}
```

### 2.17.5、<span style="color:red">查找name含有"小"数据</span>

```shell
#GET http://127.0.0.1:9200/user/_search
{
	"query":{
		"match":{
			"name":"小"
		}
	}
}

```

&emsp;&emsp;结果返回：

```json
{
	"took": 2,
	"timed_out": false,
	"_shards": {
		"total": 1,
		"successful": 1,
		"skipped": 0,
		"failed": 0
	},
	"hits": {
		"total": {
			"value": 1,
			"relation": "eq"
		},
		"max_score": 0.2876821,
		"hits": [
			{
				"_index": "user",
				"_type": "_doc",
				"_id": "1001",
				"_score": 0.2876821,
				"_source": {
					"name": "小米",
					"sex": "男的",
					"tel": "1111"
				}
			}
		]
	}
}
```

### 2.17.6、<span style="color:red">查找sex含有"男"数据</span>

```shell
#GET http://127.0.0.1:9200/user/_search
{
	"query":{
		"match":{
			"sex":"男"
		}
	}
}

```

&emsp;&emsp;结果返回：

```json
{
    "took": 2,
    "timed_out": false,
    "_shards": {
        "total": 1,
        "successful": 1,
        "skipped": 0,
        "failed": 0
    },
    "hits": {
        "total": {
            "value": 0,
            "relation": "eq"
        },
        "max_score": null,
        "hits": []
    }
}
```

<hr>

> 找不想要的结果，只因创建映射时 `sex` 的类型为 `keyword`。

> `sex` 只能完全为 `男的` ，才能得出原数据。

```json
#GET http://127.0.0.1:9200/user/_search
{
	"query":{
		"match":{
			"sex":"男的"
		}
	}
}

```

### 2.17.7、<span style="color:red">查询电话</span>

```shell
# GET http://127.0.0.1:9200/user/_search
{
	"query":{
		"match":{
			"tel":"11"
		}
	}
}

```

&emsp;&emsp;结果返回：

```json
{
    "error": {
        "root_cause": [
            {
                "type": "query_shard_exception",
                "reason": "failed to create query: Cannot search on field [tel] since it is not indexed.",
                "index_uuid": "oIOpPP7WTgabeZZdzLEtQg",
                "index": "user"
            }
        ],
        "type": "search_phase_execution_exception",
        "reason": "all shards failed",
        "phase": "query",
        "grouped": true,
        "failed_shards": [
            {
                "shard": 0,
                "index": "user",
                "node": "Yt664D-oSmetjglB2mca2w",
                "reason": {
                    "type": "query_shard_exception",
                    "reason": "failed to create query: Cannot search on field [tel] since it is not indexed.",
                    "index_uuid": "oIOpPP7WTgabeZZdzLEtQg",
                    "index": "user",
                    "caused_by": {
                        "type": "illegal_argument_exception",
                        "reason": "Cannot search on field [tel] since it is not indexed."
                    }
                }
            }
        ]
    },
    "status": 400
}
```

## 18 ~ 28 `JavaAPI`操作`ES`

&emsp;&emsp;详见 [gitee代码库](https://gitee.com/Tender_Cui/springboot-component/tree/master/middleware-integration/es-integration-api/src/main/java/com/tender)


