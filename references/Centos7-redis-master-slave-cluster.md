# 参考文献

* [Redis一主两从部署架构](https://blog.csdn.net/qq_38313548/article/details/120319064)
* [Redis单机部署手册](./Centos7-redis-standalone.md  'include:')

# 1、环境准备

> 虚机

```html
192.168.255.100  虚机  主
192.168.255.101  虚机  从
192.168.255.103  虚机  从
```

> redis 单机安装部署请参考：[Redis单机部署手册](./Centos7-redis-standalone.md  'include:')

# 2、转发文件
```shell
scp redis-6.2.4.tar.gz root@192.168.255.101:/opt/softs
scp redis-6.2.4.tar.gz root@192.168.255.103:/opt/softs
```