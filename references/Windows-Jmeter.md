# 参考文献

* [`Jmeter`官网下载](https://jmeter.apache.org/download_jmeter.cgi)
* [`Jmeter` 安装和使用教程](https://blog.csdn.net/xhf852963/article/details/103352811)
* [`Jmeter` 教程-入门](https://blog.csdn.net/yaorongke/article/details/82799609)
* [`Apache Jmeter` 教程](https://www.cnblogs.com/sddai/p/9901858.html)

# 

# 1、下载

## 1.1、简介

&emsp;&emsp;`Apache JMeter`是`Apache`组织开发的基于`Java`的压力测试工具。因此<span style="color:red">依赖Java 开发环境</span>。

> JDK 环境的安装，忽略。

&emsp;&emsp;查看 `java 版本`

![`java 版本`](./pictures/Windows-Jmeter/img.png)

&emsp;&emsp;去`Jmeter`[官网](https://jmeter.apache.org/download_jmeter.cgi)下载`apache-jmeter-5.6.3.zip`。

&emsp;&emsp;[`Jmeter`历史版本汇总地址](https://archive.apache.org/dist/jmeter/binaries/)。可以根据自己的需要下载。

![`Jmeter`官网](./pictures/Windows-Jmeter/img_1.png)

## 1.2、`Jmeter`环境变量配置

1. 计算机图标->鼠标右键->属性->高级系统设置->环境变量，进入`系统变量(s)`->找到path

```html
# Jmeter 首页
JMETER_HOME=C:\tender_work_space\install_4_code\apache-jmeter-5.6.3

# Path 路径
%JMETER_HOME%\bin
```

![JMETER_HOME 配置](./pictures/Windows-Jmeter/img_2.png)

![Path 配置](./pictures/Windows-Jmeter/img_3.png)


# 2、启动

&emsp;&emsp;启动`Jmeter`，启动后不要关闭。

![启动](./pictures/Windows-Jmeter/img_4.png)

# 3、使用入门

## 3.1、需求

&emsp;&emsp;现有一个`http`请求接口`http://localhost:8401/testB` 要使用`Jmeter`对其进行压测。

&emsp;&emsp;测试步骤如下:

1. 新建一个`线程组`

![新建一个`线程组`](./pictures/Windows-Jmeter/img_5.png)

2. 设置线程组参数

![设置线程组参数](./pictures/Windows-Jmeter/img_6.png)

3. 新增`http`请求默认值

![新建http请求](./pictures/Windows-Jmeter/img_7.png)

4. 配置具体接口的请求信息

&emsp;&emsp;在上一步创建的线程组上，新增`http`请求默认值，所有的请求都会使用设置的默认值。

&emsp;&emsp;设置协议为`http`，IP为`localhost`，端口为`8401`。

![配置具体接口的请求信息](./pictures/Windows-Jmeter/img_8.png)

5. 启动压测

![启动压测](./pictures/Windows-Jmeter/img_9.png)










