# 参考文献
* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786) 
* [Jenkins安装配置](https://blog.csdn.net/CSDN2497242041/article/details/130914914) 
* [启动Jenkins服务报错](https://blog.csdn.net/weixin_44427181/article/details/125338379) 
* [Jenkins修改插件下载地址与安装中文插件](https://blog.csdn.net/weixin_44825912/article/details/132392197) 
* [Jenkins权限管理，给不同用户分配不同项目的权限](https://blog.csdn.net/qq_40846862/article/details/131992127) 
* [解决jenkins报错：AWT is not properly configured on this server](https://blog.csdn.net/qq_44959735/article/details/104363491) 
* [Jenkins构建时报错：ERROR: Failed to parse POMs](https://blog.51cto.com/meiling/7599368) 

# 一、Jenkins安装和持续集成环境配置

> Jenkins自动化部署实现原理

![Jenkins自动化部署实现原理](./pictures/devOps_jenkins/img.png)

> 环境部署架构

![部署架构图](./pictures/devOps_jenkins/img_1.png)

* 首先，开发人员每天进行代码提交，提交到Git仓库
* 然后，Jenkins作为持续集成工具，使用Git工具到Git仓库拉取代码到集成服务器，再配合JDK， Maven等软件完成代码编译，代码测试与审查，测试，打包等工作，在这个过程中每一步出错，都重新再执行一次整个流程。
* 最后，Jenkins把生成的jar或war包分发到测试服务器或者生产服务器，测试人员或用户就可以访问应用。

> 环境列表（虚拟机统一采用CentOS7。）

|     名称      |      IP地址     |                    安装的软件                     |
|:-------------:|:--------------:|:--------------------------------------------:|
|  代码托管服务器 | 192.168.66.100 |               gitlab-ce-12.4.2               |
|  持续集成服务器 | 192.168.66.101 | jenkins-2.441，JDK11，Maven3.9.6，Git， SonarQube |
|  应用测试服务器 | 192.168.66.102 |               JDK1.8，Tomcat8.5               |

# 二、Jenkins 的安装

## 2.1、安装jdk

&emsp;&emsp;Jenkins需要依赖JDK，所以先安装JDK11

> 详见 Centos7-jdk11.md 文件

[jdk11_Centos7.md](./Centos7-jdk11.md ':include')

## 2.2、安装 Jenkins

### 2.2.1、下载 Jenkins

&emsp;&emsp;最新版本下载地址：[jenkins官网下载地址_最新版本](https://www.jenkins.io/zh/download/)  
&emsp;&emsp;历史版本下载地址：[jenkins官网下载地址_历史版本](https://mirrors.jenkins.io/redhat/)   
&emsp;&emsp;清华源历史版本下载地址：[清华源历史版本下载地址](https://mirrors.tuna.tsinghua.edu.cn/jenkins/)  
&emsp;&emsp;华为源历史版本下载地址：[华为源历史版本下载地址](https://repo.huaweicloud.com/jenkins/)

> 下载 jenkins-2.441-1.1.noarch.rpm

&emsp;&emsp;新版本都需要最低jdk11了。

```shell
cd /opt/softs
wget https://repo.huaweicloud.com/jenkins/redhat-stable/jenkins-2.441-1.1.noarch.rpm
```

### 2.2.2、安装

```shell
rpm -ivh jenkins-2.441-1.1.noarch.rpm
```

### 2.2.3、修改Jenkins配置

```shell
vim /etc/syscoﬁg/jenkins

# 修改内容如下： 
JENKINS_USER="root" 
JENKINS_PORT="8888"
```

### 2.2.4、启动Jenkins

```shell
# 启动会报错
systemctl start jenkins
```

> <span style="color:red">如何解决启动报错</span>

&emsp;&emsp;修改jenkins配置，添加自己的jdk路径（不修改java路径会启动失败）

#### 2.2.4.1、查看下java 的安装位置

```shell
which java

/opt/installs/jdk-11.0.7/bin/java
```

#### 2.2.4.2、修改jenkins 的配置

```shell
vim /etc/init.d/jenkins
```

![添加jdk安装路径](./pictures/devOps_jenkins/img_2.png)

> jenkins 新版本还需修改如下配置
```shell
vim /usr/lib/systemd/system/jenkins.service

# 修改User 和 Group 为： 
User=root
Group=root

# 启动端口我们修改为 8888
Environment="JENKINS_PORT=8888"

# 修改 java home directory
# The Java home directory. When left empty, JENKINS_JAVA_CMD and PATH are consulted.
# 修改为 jdk 的安装路径
Environment="JAVA_HOME=/opt/installs/jdk-11.0.7/"
```
![新版本额外需要修改的配置](./pictures/devOps_jenkins/img_3.png)

![新版本额外需要修改的配置_java_home](./pictures/devOps_jenkins/img_3.1.png)

![新版本额外需要修改的配置_启动端口](./pictures/devOps_jenkins/img_3.2.png)

> 如果如下错误

```shell
AWT is not properly configured on this server. Perhaps you need to run your container with "-Djava.awt.headless=true"? See also: https://www.jenkins.io/redirect/troubleshooting/java.awt.headless

java.lang.NullPointerException
	at java.desktop/sun.awt.FontConfiguration.getVersion(FontConfiguration.java:1262)
	at java.desktop/sun.awt.FontConfiguration.readFontConfigFile(FontConfiguration.java:225)
	at java.desktop/sun.awt.FontConfiguration.init(FontConfiguration.java:107)
	at java.desktop/sun.awt.X11FontManager.createFontConfiguration(X11FontManager.java:719)
	at java.desktop/sun.font.SunFontManager$2.run(SunFontManager.java:367)
	at java.base/java.security.AccessController.doPrivileged(Native Method)
	at java.desktop/sun.font.SunFontManager.<init>(SunFontManager.java:312)
	at java.desktop/sun.awt.FcFontManager.<init>(FcFontManager.java:35)
	at java.desktop/sun.awt.X11FontManager.<init>(X11FontManager.java:56)
Caused: java.lang.reflect.InvocationTargetException
	at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
	at java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
	at java.base/java.lang.reflect.Constructor.newInstance(Constructor.java:490)
	at java.desktop/sun.font.FontManagerFactory$1.run(FontManagerFactory.java:84)
Caused: java.lang.InternalError
	at java.desktop/sun.font.FontManagerFactory$1.run(FontManagerFactory.java:86)
	at java.base/java.security.AccessController.doPrivileged(Native Method)
	at java.desktop/sun.font.FontManagerFactory.getInstance(FontManagerFactory.java:74)
	at java.desktop/java.awt.Font.getFont2D(Font.java:497)
	at java.desktop/java.awt.Font.getFamily(Font.java:1410)
	at java.desktop/java.awt.Font.getFamily_NoClientCode(Font.java:1384)
	at java.desktop/java.awt.Font.getFamily(Font.java:1376)
	at java.desktop/java.awt.Font.toString(Font.java:1869)
	at hudson.util.ChartUtil.<clinit>(ChartUtil.java:270)
	at hudson.WebAppMain.contextInitialized(WebAppMain.java:217)
	at org.eclipse.jetty.server.handler.ContextHandler.callContextInitialized(ContextHandler.java:1049)
	at org.eclipse.jetty.servlet.ServletContextHandler.callContextInitialized(ServletContextHandler.java:624)
	at org.eclipse.jetty.server.handler.ContextHandler.contextInitialized(ContextHandler.java:984)
	at org.eclipse.jetty.servlet.ServletHandler.initialize(ServletHandler.java:740)
	at org.eclipse.jetty.servlet.ServletContextHandler.startContext(ServletContextHandler.java:392)
	at org.eclipse.jetty.webapp.WebAppContext.startContext(WebAppContext.java:1304)
	at org.eclipse.jetty.server.handler.ContextHandler.doStart(ContextHandler.java:901)
	at org.eclipse.jetty.servlet.ServletContextHandler.doStart(ServletContextHandler.java:306)
	at org.eclipse.jetty.webapp.WebAppContext.doStart(WebAppContext.java:532)
	at org.eclipse.jetty.util.component.AbstractLifeCycle.start(AbstractLifeCycle.java:93)
	at org.eclipse.jetty.util.component.ContainerLifeCycle.start(ContainerLifeCycle.java:171)
	at org.eclipse.jetty.server.Server.start(Server.java:470)
	at org.eclipse.jetty.util.component.ContainerLifeCycle.doStart(ContainerLifeCycle.java:114)
	at org.eclipse.jetty.server.handler.AbstractHandler.doStart(AbstractHandler.java:89)
	at org.eclipse.jetty.server.Server.doStart(Server.java:415)
	at org.eclipse.jetty.util.component.AbstractLifeCycle.start(AbstractLifeCycle.java:93)
	at winstone.Launcher.<init>(Launcher.java:205)
	at winstone.Launcher.main(Launcher.java:496)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.base/java.lang.reflect.Method.invoke(Method.java:566)
	at executable.Main.main(Main.java:351)

```

> 报错解决方案

```shell
# 查看 jenkins 所有进程
ps -ef | grep jenkins
# 干掉与 jenkins 相关的所有进程
kill -9 pid
# 安装，解决 jenkins 启动报错
yum install fontconfig
```

#### 2.2.4.3、<span style="color:red">重新加载 jenkins 的配置</span>

```shell
systemctl daemon-reload
```

### 2.2.5、重新启动Jenkins

> 1、以jenkins 提供的命令启动 

```shell
cd /etc/init.d
# 启动 
./jenkins start 
# 重启 
./jenkins restart
# 停止 
./jenkins stop 
# 状态 
./jenkins status
```

> 2、或者通过systemctl 启动

```shell
# 查看服务状态
systemctl status jenkins.service
# 启动服务
systemctl start jenkins.service
# 停止服务
systemctl stop jenkins.service
```

# 三、访问jenkins

&emsp;&emsp;http://192.168.255.101:8888/

> 注意：本服务器把防火墙关闭了，如果开启防火墙，需要在防火墙添加端口

```shell
firewall-cmd --zone=public --add-port=8888/tcp --permanent 
firewall-cmd --reload
```

> 输入初始化密码

![输入初始化密码](./pictures/devOps_jenkins/img_4.png)

> 跳过插件安装

&emsp;&emsp;因为Jenkins插件需要连接默认官网下载，速度非常慢，而且经过会失败，所以我们暂时先跳过插件安装

![跳过插件安装](./pictures/devOps_jenkins/img_5.png)

> 添加一个管理员账户，并进入Jenkins后台

![管理员账户](./pictures/devOps_jenkins/img_6.png)

> 下一步

![确认地址](./pictures/devOps_jenkins/img_7.png)

> 进入到欢迎页

![进入到欢迎页](./pictures/devOps_jenkins/img_8.png)


# 四、Jenkins插件管理/安装

&emsp;&emsp;Jenkins本身不提供很多功能，我们可以通过使用插件来满足我们的使用。例如从Gitlab拉取代码，使用Maven构建项目等功能都需要依靠插件完成。接下来演示如何下载插件。

>  Jenkins的官方插件下载地址速度非常慢，所以可以修改为国内插件地址。

&emsp;&emsp;具体操作为：Manage Jenkins -> Manage Plugins -> Advanced，滑至最底端，把Update Site改为国内插件下载地址：

```shell
https://mirrors.tuna.tsinghua.edu.cn/jenkins/updates/update-center.json
```

![插件管理-修改国内插件地址](./pictures/devOps_jenkins/img_9.png)

> 修改之前，<span style="color:red">先让Jenkins官方的插件列表下载到本地</span>

![插件管理-Advanced-拉到最下面](./pictures/devOps_jenkins/img_10.png)



```shell
# 把如下地址贴进去
https://mirrors.tuna.tsinghua.edu.cn/jenkins/updates/update-center.json
```

![插件管理-Advanced-设置插件地址](./pictures/devOps_jenkins/img_11.png)

> 下一步

&emsp;&emsp;前面已经把 Jenkins官方的插件列表下载到本地，因此我们接着修改该插件列表文件，将google地址替换为百度地址，jenkins下载地址替换为清华源下载地址。

```shell
# 非容器部署
cd /var/lib/jenkins/updates
 
# 容器部署(前提是，已经进入到挂载目录)
cd /updates
 
sed -i 's/http:\/\/updates.jenkins-ci.org\/download/https:\/\/mirrors.tuna.tsinghua.edu.cn\/jenkins/g' default.json && sed -i 's/http:\/\/www.google.com/https:\/\/www.baidu.com/g' default.json
```

> 重启Jenkins

```shell
# 在浏览器输入
http://192.168.255.101:8888/restart
```

## 4.1、安装中文插件

&emsp;&emsp;Manage Jenkins->Manage Plugins，点击Available，搜索"Chinese"

> 搜索中文插件&&安装

![搜索中文插件&&安装](./pictures/devOps_jenkins/img_12.png)

> 重启后能看到部分汉化效果，但可能部分菜单汉化会失败

## 4.2、Jenkins用户权限管理插件安装

### 4.2.1、安装

&emsp;&emsp;我们可以利用Role-based Authorization Strategy 插件来管理Jenkins用户权限

> 搜索Role-based Authorization Strategy插件&&安装

![jenkins权限管理插件](./pictures/devOps_jenkins/img_13.png)

### 4.2.2、开启权限全局安全配置

> TIPS：如果安装完插件没有重启jenkins，则需要重启

&emsp;&emsp;Manage Jenkins->Configure Global Security

![开启授权选项为：Role-Based Strategy](./pictures/devOps_jenkins/img_14.png)

### 4.2.3、创建角色

&emsp;&emsp;Manage Jenkins->Manage and Assign Roles

![创建角色](./pictures/devOps_jenkins/img_15.png)

> 角色分类如下：

* Global roles：全局角色，主要用于配置用户的功能权限
* Item roles：项目角色，主要用于管理项目的权限
* Agent roles：节点角色，主要用于节点相关的权限

> TIPS:

&emsp;&emsp;创建的<span style="color:red">两种类型角色名称要统一</span>，使用同一个名称，要不然全局有这个角色，项目里又没这个角色了，这样子最后到给用户授权的时候就会出现问题。

&emsp;&emsp;比如**Global roles** 下有个 **role_dev**，那么**Item roles** 下也要有个**role_dev**

> Global roles

![Global roles_创建角色](./pictures/devOps_jenkins/img_16.png)

&emsp;&emsp;根据自己的需求创建角色，选择相应的权限! 这里创建了 role_dev 以及 role_test 两个角色，并给他们了Read权限。

> Item roles

![Item roles_创建角色](./pictures/devOps_jenkins/img_17.png)

&emsp;&emsp;同样创建了 role_dev 以及 role_test 两个角色，并给他们了项目 Read权限。

> Agent roles

&emsp;&emsp;节点权限配置，**暂时忽略**

![Agent roles_暂时忽略](./pictures/devOps_jenkins/img_18.png)

### 4.2.4、创建用户

&emsp;&emsp;Manage Jenkins->Manage Users

![创建用户入口](./pictures/devOps_jenkins/img_19.png)

&emsp;&emsp;创建两个用户 user_dev 和 user_test

![创建具体用户](./pictures/devOps_jenkins/img_20.png)

### 4.2.5、给用户分配角色

&emsp;&emsp;Manage Jenkins->Manage and Assign Roles

![给用户分配角色](./pictures/devOps_jenkins/img_21.png)

> Global roles

![Global roles分配角色](./pictures/devOps_jenkins/img_22.png)

> Item roles

![Item roles分配角色](./pictures/devOps_jenkins/img_23.png)

### 4.2.6、创建项目测试权限

> 新建项目

![新建item](./pictures/devOps_jenkins/img_24.png)

> dev_project_01

![dev_project_01](./pictures/devOps_jenkins/img_25.png)

> test_project_01 同理

### 4.2.7、权限认证结果

> root 视角

![root 视角](./pictures/devOps_jenkins/img_26.png)

> user_dev 视角

![user_dev 视角](./pictures/devOps_jenkins/img_27.png)

> user_test 视角

![user_test 视角](./pictures/devOps_jenkins/img_28.png)

## 4.3、Jenkins凭证管理-Credentials Binding插件

> 安装 Credentials Binding 插件

&emsp;&emsp;Manage Jenkins->Plugin Manager

![Credentials Binding搜索](./pictures/devOps_jenkins/img_29.png)

> 安装失败，如果jenkins的版本比较低，因此需要下载历史版本插件

![jenkins版本不匹配提示](./pictures/devOps_jenkins/img_30.png)

> 下载Jenkins的插件的地址：https://plugins.jenkins.io/

```shell
# Credentials Binding 相关的下载地址
https://plugins.jenkins.io/credentials-binding/releases/
```

## 4.3.1、凭证的分类

![凭证的入口](./pictures/devOps_jenkins/img_31.png)

> 下一步

![点击全局](./pictures/devOps_jenkins/img_32.png)

> 下一步

![如何添加一些凭据](./pictures/devOps_jenkins/img_33.png)

> 可以添加的凭证有5种：

* Username with password：用户名和密码
* SSH Username with private key： 使用SSH用户和密钥
* Secret file：需要保密的文本文件，使用时Jenkins会将文件复制到一个临时目录中，再将文件路径设置到一个变量中，等构建结束后，所复制的Secret file就会被删除。
* Secret text：需要保存的一个加密的文本串，如钉钉机器人或Github的api token
* Certificate：通过上传证书文件的方式

> 常用的凭证类型有：

1. Username with password（用户密码）
2. SSH Username with private key（SSH密钥）

&emsp;&emsp;接下来以使用 Git 工具到 Gitlab 拉取项目源码为例，演示 Jenkins 如何管理 Gitlab 的凭证。

## 4.3.2、Jenkins 安装 Git插件
&emsp;&emsp;为了让Jenkins支持从Gitlab拉取源码，需要安装Git插件。

![Jenkins 安装 Git插件](./pictures/devOps_jenkins/img_34.png)

> 验证git 是否在 Jenkins 上安装成功

&emsp;&emsp;新建item 然后看是否可以有git 的选项

![验证git 是否在 Jenkins 上安装成功](./pictures/devOps_jenkins/img_35.png)

## 4.3.3、Centos7 安装 Git

> 安装git

```shell
yum install -y git
```

> 校验安装结果

![校验git安装结果](./pictures/devOps_jenkins/img_36.png)

## 4.3.4、使用 Username with password

&emsp;&emsp;Manage Jenkins->Security->Credentials->全局->Add Credentials

![使用 Username with password](./pictures/devOps_jenkins/img_37.png)

> 编辑项目

![测试gitlab拉取代码的项目](./pictures/devOps_jenkins/img_38.png)

> 项目HTTP地址

![选择course的HTTP克隆地址](./pictures/devOps_jenkins/img_39.png)

> 选择 Username with password 凭证

![选择前面创建的username_password的凭证](./pictures/devOps_jenkins/img_40.png)

> 点击构建（Build Now）

![点击构建](./pictures/devOps_jenkins/img_41.png)

> 查看构建日志

![查看构建日志](./pictures/devOps_jenkins/img_42.png)

## 4.3.4、使用 SSH Username with private key

&emsp;&emsp;使用root用户生成公钥和私钥
```shell
# 192.168.255.101 机器
ssh-keygen -t rsa

# 一路回车
```

> 在/root/.ssh/目录保存了公钥和私钥

![公钥和私钥](./pictures/devOps_jenkins/img_43.png)

* id_rsa：私钥文件
* id_rsa.pub：公钥文件

> 把生成的公钥放在Gitlab中

![gitlab_头像_setting](./pictures/devOps_jenkins/img_44.png)

> 粘贴公钥到gitlab

![gitlab_粘贴公钥内容](./pictures/devOps_jenkins/img_45.png)

> 粘贴私钥到jenkins 凭证管理器

![粘贴私钥到jenkins_凭证管理器](./pictures/devOps_jenkins/img_46.png)

> 创建一个项目 dev_project_4_gitlab_pullTest_SSH

&emsp;&emsp;拷贝SSH 项目克隆地址

![项目SSH克隆地址](./pictures/devOps_jenkins/img_47.png)

> 配置git仓库和SSH

![配置git仓库和SSH](./pictures/devOps_jenkins/img_48.png)

> 构建以及日志查看

![构建以及日志查看](./pictures/devOps_jenkins/img_49.png)

## 4.4、配置 jdk 和 maven

&emsp;&emsp;Manage Jenkins->System Configuration->Tools

![配置 jdk 和 maven的位置](./pictures/devOps_jenkins/img_50.png)

### 4.4.1、配置 jdk

![配置 jdk1.8](./pictures/devOps_jenkins/img_51.png)

### 4.4.2、配置 maven

![配置 maven3.9.6](./pictures/devOps_jenkins/img_52.png)

### 4.4.3、添加Jenkins全局变量

&emsp;&emsp;Manage Jenkins->System->全局属性

![全局属性之环境变量](./pictures/devOps_jenkins/img_53.png)

#### 4.4.3.1、添加Jenkins全局变量

> JAVA_HOME

![JAVA_HOME](./pictures/devOps_jenkins/img_54.png)

> M2_HOME

![M2_HOME](./pictures/devOps_jenkins/img_55.png)

> PATH+EXTRA

&emsp;&emsp;<span style="color:red">PATH+EXTRA名称固定，不能修改！</span>下面的操作类似于我们把环境变量添加到jenkins 的环境变量中。

![PATH+EXTRA](./pictures/devOps_jenkins/img_56.png)

> 修改Maven的settings.xml

```shell
# 创建如下路径 
/opt/installs/mvn_repository

# 修改 settings.xml
<localRepository>/opt/installs/mvn_repository</localRepository>

# 配上阿里的私服
<mirror>
    <id>nexus-aliyun</id>
    <mirrorOf>central</mirrorOf>
    <name>Nexus aliyun</name>
    <url>http://maven.aliyun.com/nexus/content/groups/public</url>
</mirror>
```

> 项目构建测试

&emsp;&emsp;配置->Build Steps

![项目构建测试](./pictures/devOps_jenkins/img_57.png)

```shell
mvn clean package -Dmaven.test.skip=true
```

# 五、安装tomcat8（很少用了）&& 远程发布

## 5.1、下载与安装

> 现在绝大部都是用springboot，都是jar 包启动了

&emsp;&emsp;Tomcat8的下载&安装请查看[Tomcat8的下载&安装](./Centos7-tomcat8.md ':include')

## 5.2、配置Tomcat用户角色权限

&emsp;&emsp;默认情况下Tomcat是<span style="color:red">**没有**配置用户角色权限的</span>。

![ manager webapp入口](./pictures/devOps_jenkins/img_58.png)

> 点进去

![ manager webapp点进去的结果](./pictures/devOps_jenkins/img_59.png)

&emsp;&emsp;但是，后续Jenkins部署项目到Tomcat服务器，需要用到Tomcat的用户，所以修改tomcat以下配置，添加用户及权限

```shell
cd /opt/installs/apache-tomcat-8.5.97/conf

vim tomcat-users.xml

# 配置的内容如下：
<role rolename="tomcat"/>
<role rolename="role1"/>
<role rolename="manager-script"/>
<role rolename="manager-gui"/>
<role rolename="manager-status"/>
<role rolename="admin-gui"/>
<role rolename="admin-script"/>
<user username="tomcat" password="tomcat" roles="manager-gui,manager-script,tomcat,admin-gui,admin-script"/>

```

&emsp;&emsp;注意：<span style="color:red">账户和密码都是tomcat</span>

> 重启tomcat8

```shell
cd /opt/installs/apache-tomcat-8.5.97/bin

# 先停止
./shutdown.sh 
# 再启动
./startup.sh
```

&emsp;&emsp;发现还是不可以远程访问 tomcat。

![发现还是不可以远程访问](./pictures/devOps_jenkins/img_60.png)

> 开启tomcat 远程访问

&emsp;&emsp;tomcat 默认不允许远程访问的。

```shell
cd /opt/installs/apache-tomcat-8.5.97/webapps/manager/META-INF

vim context.xml

# 注释如下内容
<!--
<Valve className="org.apache.catalina.valves.RemoteAddrValve"
allow="127\.\d+\.\d+\.\d+|::1|0:0:0:0:0:0:0:1" />
-->
```

&emsp;&emsp;tomcat 再次关闭重启tomcat

![输入tomcat账户和密码](./pictures/devOps_jenkins/img_61.png)

> 可以远程访问了

![可以远程访问了](./pictures/devOps_jenkins/img_62.png)

## 5.3、项目打war 包&&远程部署到tomcat8 上

### 5.3.1、创建war 项目

&emsp;&emsp;[参考war包工程的创建](./idea_create_war_project.md ':include')

### 5.3.2、jenkins新建item 用于编译、打包和部署

![jenkins新建item](./pictures/devOps_jenkins/img_63.png)

> git 仓库和拉取配置

![git 仓库和拉取配置](./pictures/devOps_jenkins/img_64.png)

> maven 打包shell脚本

![maven 打包shell脚本](./pictures/devOps_jenkins/img_65.png)

> 保存&&构建并查看打包结果

![保存&&构建并查看打包结果](./pictures/devOps_jenkins/img_66.png)

### 5.3.3、jenkins 利用tomcat8 远程部署

&emsp;&emsp;我们可以利用 Deploy to container 插件来把项目部署到远程的Tomcat8里面

> 搜索Deploy to container插件&&安装

&emsp;&emsp;Manage Jenkins->Security->Plugins->Available plugins

![Deploy to container插件的搜索&&安装](./pictures/devOps_jenkins/img_67.png)

> 构建后操作

![构建后操作](./pictures/devOps_jenkins/img_68.png)

> 下一步

![部署war包到一个容器](./pictures/devOps_jenkins/img_69.png)

> war包模糊匹配

![war包模糊匹配](./pictures/devOps_jenkins/img_70.png)

> Tomcat8 Username with password 凭证配置

&emsp;&emsp;前面的 **5.2、配置Tomcat用户角色权限**我们创建了一个用户名和密码都是tomcat的用户及权限，为jenkins远程部署做准备。

![Tomcat8 Username with password 凭证配置01](./pictures/devOps_jenkins/img_71.png)

> 下一步

![Tomcat8 Username with password 凭证配置02](./pictures/devOps_jenkins/img_72.png)

> tomcat8 容器凭证配置和容器地址配置

![war包模糊匹配](./pictures/devOps_jenkins/img_73.png)

> 查看jenkins 控制台日志

![jenkins 控制台日志](./pictures/devOps_jenkins/img_74.png)

> 利用tomcat 访问项目首页

![访问项目首页](./pictures/devOps_jenkins/img_75.png)

# 六、 Jenkins项目构建类型：Maven项目构建

&emsp;&emsp;之前jenkins构建的项目类型都是 Freestyle project。

## 6.1、安装Maven Integration插件

> 搜索Maven Integration插件&&安装

&emsp;&emsp;Manage Jenkins->Security->Plugins->Available plugins

![搜索Maven Integration插件&&安装](./pictures/devOps_jenkins/img_76.png)

## 6.2、创建Maven类型的项目

&emsp;&emsp;新建item->构建一个maven项目

![选择构建一个maven项目](./pictures/devOps_jenkins/img_77.png)

## 6.3、配置项目

&emsp;&emsp;拉取代码和远程部署的过程和自由风格项目一样，只是"构建"部分不同

> 之前是通过shell来指定编译后的行为，现在是maven 命令

![指定mvn命令](./pictures/devOps_jenkins/img_78.png)

> 并存后，构建&&测试结果

&emsp;&emsp;发现报错，具体错误如下：

```shell
/opt/installs/jdk1.8.0_251/bin/java -cp /var/lib/jenkins/plugins/maven-plugin/WEB-INF/lib/maven35-agent-1.14.jar:/opt/installs/apache-maven-3.9.6/boot/plexus-classworlds-2.7.0.jar:/opt/installs/apache-maven-3.9.6/conf/logging jenkins.maven3.agent.Maven35Main /opt/installs/apache-maven-3.9.6 /var/cache/jenkins/war/WEB-INF/lib/remoting-3198.v03a_401881f3e.jar /var/lib/jenkins/plugins/maven-plugin/WEB-INF/lib/maven35-interceptor-1.14.jar /var/lib/jenkins/plugins/maven-plugin/WEB-INF/lib/maven3-interceptor-commons-1.14.jar 34820
Exception in thread "main" java.lang.UnsupportedClassVersionError: hudson/remoting/Launcher has been compiled by a more recent version of the Java Runtime (class file version 55.0), this version of the Java Runtime only recognizes class file versions up to 52.0
	at java.lang.ClassLoader.defineClass1(Native Method)
	at java.lang.ClassLoader.defineClass(ClassLoader.java:756)
	at java.security.SecureClassLoader.defineClass(SecureClassLoader.java:142)
	at java.net.URLClassLoader.defineClass(URLClassLoader.java:468)
	at java.net.URLClassLoader.access$100(URLClassLoader.java:74)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:369)
	at java.net.URLClassLoader$1.run(URLClassLoader.java:363)
	at java.security.AccessController.doPrivileged(Native Method)
	at java.net.URLClassLoader.findClass(URLClassLoader.java:362)
	at org.codehaus.plexus.classworlds.realm.ClassRealm.findClassInternal(ClassRealm.java:313)
	at org.codehaus.plexus.classworlds.realm.ClassRealm.loadClassFromSelf(ClassRealm.java:432)
	at org.codehaus.plexus.classworlds.strategy.SelfFirstStrategy.loadClass(SelfFirstStrategy.java:42)
	at org.codehaus.plexus.classworlds.realm.ClassRealm.unsynchronizedLoadClass(ClassRealm.java:271)
	at org.codehaus.plexus.classworlds.realm.ClassRealm.loadClass(ClassRealm.java:247)
	at org.codehaus.plexus.classworlds.realm.ClassRealm.loadClass(ClassRealm.java:239)
	at jenkins.maven3.agent.Maven35Main.main(Maven35Main.java:136)
	at jenkins.maven3.agent.Maven35Main.main(Maven35Main.java:66)
ERROR: Failed to parse POMs
java.io.EOFException: unexpected stream termination
	at hudson.remoting.ChannelBuilder.negotiate(ChannelBuilder.java:459)
	at hudson.remoting.ChannelBuilder.build(ChannelBuilder.java:404)
	at hudson.slaves.Channels.forProcess(Channels.java:121)
	at hudson.maven.AbstractMavenProcessFactory.newProcess(AbstractMavenProcessFactory.java:298)
	at hudson.maven.ProcessCache.get(ProcessCache.java:237)
	at hudson.maven.MavenModuleSetBuild$MavenModuleSetBuildExecution.doRun(MavenModuleSetBuild.java:802)
	at hudson.model.AbstractBuild$AbstractBuildExecution.run(AbstractBuild.java:526)
	at hudson.model.Run.execute(Run.java:1895)
	at hudson.maven.MavenModuleSetBuild.run(MavenModuleSetBuild.java:543)
	at hudson.model.ResourceController.execute(ResourceController.java:101)
	at hudson.model.Executor.run(Executor.java:442)
Finished: FAILURE
```

> 解决办法

&emsp;&emsp;maven 目录可能没有写入的权限，尝试执行命令进行授权。

```shell
# chmod -R 775 [maven 目录路径]
chmod -R 775 /opt/installs/apache-maven-3.9.6
```

> 解决问题后再次构建，控制台查看结果

![控制台查看结果](./pictures/devOps_jenkins/img_79.png)






