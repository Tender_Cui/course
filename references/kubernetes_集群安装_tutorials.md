# 参考文献

* [Kubernetes架构](https://www.cnblogs.com/sswind/p/14302994.html)
* [KubeAdmin安装k8s](https://blog.csdn.net/weixin_46837396/article/details/119777362)
* [Jenkins-持续集成环境实战](https://blog.csdn.net/hancoder/article/details/118233786)
* [部署k8s的时候kube-flannel.yml下载不下来解决](https://blog.csdn.net/chen_haoren/article/details/108580338)

# 一、Kubeadm 安装 K8s

## 1.1、 K8s 的架构

![K8s 的架构图01](./pictures/kubernetes集群/img.png)

&emsp;&emsp;组件说明：

* API Server：用于暴露Kubernetes API，任何资源的请求的调用操作都是通过kube-apiserver提供的接口进行的；并提供认证、授权、访问控制、API注册和发现等级制；
* Etcd：是Kubernetes提供默认的存储系统，保存所有集群数据，使用时需要为etcd数据提供备份计划。
* Controller-Manager：作为集群内部的管理控制中心，负责集群内的Node、Pod副本、服务端点 （Endpoint）、命名空间（Namespace）、服务账号（ServiceAccount）、资源定额 （ResourceQuota）的管理，当某个Node意外宕机时，Controller Manager会及时发现并执行自动化修复流程，确保集群始终处于预期的工作状态。
* Scheduler：负责资源的调度，按照预定的调度策略将Pod调度到相应的机器上；
* Kubelet：负责维护容器的生命周期，同时负责Volume和网络的管理
* Kube proxy：是Kubernetes的核心组件，部署在每个Node节点上，它是实现Kubernetes Service的通信与负载均衡机制的重要组件。

## 1.2、 安装环境说明

&emsp;&emsp;IP <span style="color:red">建议采用 192 网段</span>，避免与 k8s 内网冲突。
    
|     服务器名称      |       服务器       |                                      安装的软件                                 |
|:-----------------:|:---------------:|:--------------------------------------------------------------------------------:|
|   代码托管服务器   | 192.168.255.100 |                                 Gitlab-ce-12.4.2                                  |
|  Docker仓库服务器  | 192.168.255.102 |                           Docker24.0.7-ce，Harbor2.10.0                           |
|    k8s-master    | 192.168.255.101 | kube-apiserver、kube-controller-manager、kube-scheduler、docker、etcd、calico，NFS  |
|    k8s-node1     | 192.168.255.103 |                        kubelet、kubeproxy、Docker24.0.7-ce                         |
|    k8s-node2     | 192.168.255.104 |                        kubelet、kubeproxy、Docker24.0.7-ce                         |

## 1.3、前置操作

### 1.3.1、关闭防火墙（所有节点）

```shell
systemctl stop firewalld
systemctl disable firewalld

# 查看防火墙的状态
systemctl status firewalld
```

### 1.3.2、关闭 selinux（所有节点）

```shell
# 用于临时关闭，避免重启
setenforce 0
# -------------------
# 用于永久关闭，（永久关闭必须要重启）
vim /etc/selinux/config
# 需要修改的内容
SELINUX=disabled
```

### 1.3.3、关闭 SWAP分区（所有节点）

```shell
# 用于临时关闭，避免重启
swapoff -a
# -------------------
# 用于永久关闭，（永久关闭必须要重启）
vim /etc/fstab

# 将改行注释掉即可
/dev/mapper/centos-swap swap                    swap    defaults        0 0
```

### 1.3.4、设置主机名（所有节点）

```shell
# 192.168.255.101 机器
hostnamectl set-hostname k8s-master

# 192.168.255.103 机器
hostnamectl set-hostname k8s-node01
# 192.168.255.104 机器
hostnamectl set-hostname k8s-node02

# -------------------
hostnamectl set-hostname xxxx 等价于修改 vim /etc/hostname
# 验证，直接输入如下命令：
hostname
```

### 1.3.5、设置hosts（所有节点）

```shell
# 追加到  hosts 文件
cat >> /etc/hosts <<EOF
192.168.255.101 cluster-endpoint
192.168.255.101 k8s-master
192.168.255.103 k8s-node01
192.168.255.104 k8s-node02
EOF
```

### 1.3.6、配置免密登录、分发公钥（master节点）

```shell
ssh-keygen -t rsa
cd /root/.ssh

# 批量脚本
for node_ip in k8s-master k8s-node01 k8s-node02; do 
  ssh-copy-id $node_ip;
done
```

### 1.3.7、同步集群时间(所有节点)

```shell
yum install -y ntpdate

# 和阿里云时间服务器同步时间
ntpdate ntp4.aliyun.com

# -------------------
# 设置操作系统时区
ll /etc/localtime
# 如果不是 Shanghai 则需要执⾏以下命令
ln -svf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

# 查看时间
date
```

### 1.3.8、配置镜像源(所有节点)

```shell
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo
curl -o /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo 

# 验证结果
ls -l /etc/yum.repos.d/
```

### 1.3.9、更新系统(所有节点)

```shell
# 查看 系统 版本
cat /etc/redhat-release 

# 如果内核符合要求的话，排除内核升级
yum update -y --exclude=kernel*
```

### 1.3.10、安装基础常用软件(所有节点)

```shell
yum install wget expect vim net-tools ntp bash-completion ipvsadm ipset jq iptables conntrack sysstat libseccomp -y
```

### 1.3.11、更新系统内核(所有节点)

&emsp;&emsp;docker对系统内核要求比较高，<span style="color:red">最好用4.4+ </span>。

```shell
# 查看系统内核，docker对系统内核要求比较高，最好用4.4+
uname -a

# 这里我们是centos7采用5.4内核，#如果是centos8则不需要升级内核
# 官网：https://elrepo.org/

cd /opt/softs

# 下载内核
wget https://elrepo.org/linux/kernel/el7/x86_64/RPMS/kernel-lt-5.4.265-1.el7.elrepo.x86_64.rpm

wget https://elrepo.org/linux/kernel/el7/x86_64/RPMS/kernel-lt-devel-5.4.265-1.el7.elrepo.x86_64.rpm
```

![内核文件列表](./pictures/kubernetes集群/img_1.png)

```shell
# 本地文件安装
yum localinstall -y kernel-lt*
#调到默认启动
grub2-set-default  0 && grub2-mkconfig -o /etc/grub2.cfg  
# 查看当前默认启动的内核
grubby --default-kernel  
# 重启才能生效
reboot  

# 查看内核版本
uname -r
```

### 1.3.12、设置系统参数(所有节点)

&emsp;&emsp;设置允许路由转发，不对bridge的数据进行处理。

```shell

# 允许 iptables 检查桥接流量 （K8s 官方要求）
cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
br_netfilter
EOF
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

# 立即生效
sysctl --system  
```

### 1.3.13、安装docker(所有节点)

&emsp;&emsp;[详见docker安装手册](./docker_Centos7_快速入门.md ':include')

```shell
# 这个文件中，添加如下配置(如果没有的话)
vim /etc/docker/daemon.json

# 配置
"exec-opts": ["native.cgroupdriver=systemd"]
```

## 1.4、安装Kubelet、Kubeadm、Kubectl(所有节点)

&emsp;&emsp; 配置 Kubernetes 软件源，使用阿里源

```shell
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF
```

```shell
# 清空yum 缓存
yum clean all 
# 重新制作缓存
yum makecache

# 开始安装
# 指定版本安装
yum install -y kubelet-1.20.9 kubeadm-1.20.9 kubectl-1.20.9 --disableexcludes=kubernetes

# 将 kubelet 开机自启，无需启动，因为还未初始化（否则会报错）
systemctl enable kubelet

# 查看版本 
kubectl version 
```

## 1.5、初始化master节点（master节点）

&emsp;&emsp;查看镜像列表，你会发现都是 k8s.gcr.io，由于默认拉取镜像地址k8s.gcr.io国内无法访问，需要指定阿里云镜像仓库地址。

```shell
kubeadm config images list

# 结果如下：
remote version is much newer: v1.29.0; falling back to: stable-1.20
k8s.gcr.io/kube-apiserver:v1.20.15
k8s.gcr.io/kube-controller-manager:v1.20.15
k8s.gcr.io/kube-scheduler:v1.20.15
k8s.gcr.io/kube-proxy:v1.20.15
k8s.gcr.io/pause:3.2
k8s.gcr.io/etcd:3.4.13-0
k8s.gcr.io/coredns:1.7.0
```

&emsp;&emsp;可以提前把镜像先下载下来(用的雷神的镜像地址)

```shell
cd /opt/softs/k8s

# 配置镜像，生成 images.sh
sudo tee ./images.sh <<-'EOF'
#!/bin/bash
images=(
kube-apiserver:v1.20.9
kube-proxy:v1.20.9
kube-controller-manager:v1.20.9
kube-scheduler:v1.20.9
coredns:1.7.0
etcd:3.4.13-0
pause:3.2
)
for imageName in ${images[@]} ; do
docker pull registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images/$imageName
done
EOF

# 拉取镜像
chmod +x ./images.sh && ./images.sh
```

&emsp;&emsp;初始化命令：

```shell
# 先指定镜像地址
kubeadm config images pull --image-repository=registry.cn-hangzhou.aliyuncs.com/google_containers

# ------------------------------
# 主节点初始化 （只在 master 服务器执行， 其他 node 不用）
# --apiserver-advertise-address: master 的 IP
# --control-plane-endpoint: master 的域名
# --service-cidr 和 --pod-network-cidr 是网络范围，雷神 建议不要改。
# 要改的话 2 个cidr 和 vps（172.31.x.x） 的，3 个网络互相不能重叠；还要修改 calico.yaml的 IP（下图有写）。
# 我的修改为： pod-network-cidr=10.244.0.0/16，因此我也会修改  calico.yaml为 10.244.0.0/16 （搜索：value: "192.168.0.0/16"）
#
kubeadm init \
--apiserver-advertise-address=192.168.255.101 \
--control-plane-endpoint=cluster-endpoint \
--image-repository registry.cn-hangzhou.aliyuncs.com/lfy_k8s_images \
--kubernetes-version v1.20.9 \
--service-cidr=10.96.0.0/16 \
--pod-network-cidr=10.244.0.0/16

# 过程中可监控初始化日志，出现successfully即为成功！
tail -f /var/log/messages

# 或者如下命令
cat /var/log/messages | grep successfully
```

&emsp;&emsp;命令说明：

* --apiserver-advertise-address : 集群通告地址，这个必须是 master 机器的ip地址
* --image-repository : 由于默认拉取镜像地址k8s.gcr.io国内无法访问，这里指定阿里云镜像仓库地址
* --kubernetes-version : K8s版本，与安装的一致
* --service-cidr : 集群内部虚拟网络，Pod统一访问入口
* --pod-network-cidr : Pod网络，，与下面部署的CNI网络组件yaml中保持一致

&emsp;&emsp;安装成功，会有如下日志：

```shell
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of control-plane nodes by copying certificate authorities
and service account keys on each node and then running the following as root:

  kubeadm join cluster-endpoint:6443 --token onhirc.yg8j9lzccrblowzn \
    --discovery-token-ca-cert-hash sha256:81bbe9ea6206703a076c5bf7d1c1e332ef61e8069a0be50237c9870ab077be43 \
    --control-plane 

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join cluster-endpoint:6443 --token onhirc.yg8j9lzccrblowzn \
    --discovery-token-ca-cert-hash sha256:81bbe9ea6206703a076c5bf7d1c1e332ef61e8069a0be50237c9870ab077be43 
    
```

![后续步骤以及从节点要想加入的命令提示](./pictures/kubernetes集群/img_2.png)

&emsp;&emsp;从节点要想加入集群，需要执行如下命令：

```shell
kubeadm join cluster-endpoint:6443 --token onhirc.yg8j9lzccrblowzn \
    --discovery-token-ca-cert-hash sha256:81bbe9ea6206703a076c5bf7d1c1e332ef61e8069a0be50237c9870ab077be43 

```

&emsp;&emsp;执行 k8s 的提示命令：

```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

## 1.6、安装集群网络插件（master节点）

```shell
# 下载 calico.yaml
curl https://docs.projectcalico.org/v3.8/manifests/calico.yaml -O

# 看个人修改ip 段，搜索value: "192.168.0.0/16"
# 我修改了
value: "10.244.0.0/16"

# 加载配置
kubectl apply -f calico.yaml
```

&emsp;&emsp;如果修改了 初始化主节点中的  --pod-network-cidr=192.168.0.0/16，则需要将 calico.yaml 的配置， # 去掉，IP 写成改的 IP。

```shell
cat calico.yaml | grep "192.168"
```

![查看calico的配置](./pictures/kubernetes集群/img_3.png)

## 1.7、将从节点加入集群（从节点）

```shell
# 启动 kubelet
systemctl start kubelet
# 开机自启
systemctl enable kubelet

kubeadm join 192.168.255.101:6443 --token 0g9glt.spullt6lbe590dc6 \
    --discovery-token-ca-cert-hash sha256:c47aa178717b40ffde7e27fabb149f829e8743d02269073a8351150fd3d9a783 


# 查看token值命令 
kubeadm token list

# 扩展：生成永久Token（从节点加入的时候会用到）
kubeadm token create --ttl 0 --print-join-command
# 生成临时token
kubeadm token create --print-join-command
```

## 1.8、查看系统 pod 和 集群节点

### 1.8.1、查看系统 pod

```shell
kubectl get pods --all-namespaces
# 或者
kubectl get pods -A
```

![查看所有系统pod](pictures/kubernetes集群/img_4.png)

&emsp;&emsp;如果报错：Unable to connect to the server: x509: certificate signed by unknown authority (possibly because

&emsp;&emsp;执行 kubeadm reset命令后没有删除创建的 .kube目录，重新创建集群就会出现这个问题！

* [报错解决参考文献](https://blog.csdn.net/m0_55691056/article/details/129958641)

```shell
rm -rf .kube/
sudo mkdir ~/.kube
sudo cp /etc/kubernetes/admin.conf ~/.kube/
 
cd ~/.kube
sudo mv admin.conf config
sudo service kubelet restart
```

### 1.8.2、查看集群节点

```shell
kubectl get nodes
```

![查看集群节点](pictures/kubernetes集群/img_5.png)

## 1.9、从节点上面的报错

### 1.9.1、连接 localhost:8080 拒绝错误

&emsp;&emsp;The connection to the server localhost:8080 was refused - did you specify the right host or port?

&emsp;&emsp;出现这个问题的原因是kubectl命令需要使用kubernetes-admin来运行，解决方法如下，将主节点中的【/etc/kubernetes/admin.conf】文件拷贝到从节点相同目录下，然后配置环境变量：

```shell
# 拷贝 master 节点上的 admin.conf 文件
scp /etc/kubernetes/admin.conf root@k8s-node01:/etc/kubernetes/
scp /etc/kubernetes/admin.conf root@k8s-node02:/etc/kubernetes/

echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> ~/.bash_profile
source ~/.bash_profile
```

# 二、安装 k8s 可视化页面(k8s-master节点安装)

## 2.1、Kuboard

```shell
kubectl apply -f https://addons.kuboard.cn/kuboard/kuboard-v3-swr.yaml

# 查看 service
kubectl get svc -n kuboard

# 登录 kuboard 页面
http://192.168.255.101:30080

# 输入初始用户名和密码，并登录
#用户名： admin
#密码： Kuboard123
```

## 2.2、dashboard

&emsp;&emsp;kubernetes 官方提供的可视化界面

### 2.2.1、运行 pod（创建资源）

1. 下载资源文件

```shell
# 根据 在线配置文件 创建资源
wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.3.1/aio/deploy/recommended.yaml -O k8s-dashboard.yaml
```

> 如果下载不了

&emsp;&emsp;[请参考文件：k8s-dashboard.yaml文件](./k8s_yaml/dashboard/k8s-dashboard.yaml ':include')

2. 修改 Service 为 NodePort 类型

&emsp;&emsp;默认Dashboard 只能集群内访问 修改 Service 为 NodePort 类型，暴露到外部

```shell
vim k8s-dashboard.yaml

# Service 下做如下图修改 暴露端⼝与端⼝⾃定义
type: NodePort
nodePort: 30001
```

![修改Service的类型为NodePort](./pictures/kubernetes集群/img_6.png)

> 安装与查看

```shell
kubectl apply -f k8s-dashboard.yaml
kubectl get pods,svc -n kubernetes-dashboard
```
![查看pod和svc](./pictures/kubernetes集群/img_7.png)

3. 浏览器访问页面

```shell
# 任⼀节点 https://IP:Port （三个节点都可访问）
https://192.168.255.101:30001
https://192.168.255.103:30001
https://192.168.255.104:30001

# 访问谷歌浏览器的时候，键盘敲入 "thisisunsafe"
```

![浏览器访问地址验证结果](./pictures/kubernetes集群/img_8.png)

4. 使⽤Token进⾏认证登录

#创建dashboard-adminuser.yaml   
&emsp;&emsp;[请查看文件:dashboard-adminuser.yaml](./k8s_yaml/dashboard-adminuser.yaml ':include')

```shell
&emsp;&emsp;创建登录用户
kubectl apply -f dashboard-adminuser.yaml
```

> 说明：上⾯创建了⼀个叫admin-user的服务账号，并放在kubernetes-dashboard 命名空间
下。并将cluster-admin⻆⾊绑定到admin-user账户，这样admin-user账户就有了管理员的权限。 

>默认情况下，kubeadm创建集群时已经创建了cluster-admin⻆⾊，直接绑定即可。

&emsp;&emsp;查看admin-user账户的token

```shell
kubectl -n kubernetes-dashboard get secret $(kubectl -n kubernetes-dashboard get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
```

> 说明：生成的token 如下：

```shell
eyJhbGciOiJSUzI1NiIsImtpZCI6IjVCUHZ4UnV6RVVUdk1oWkpVTnEwUnRLU3JtUjNWQVUwTHFQM1hKRDVyd2sifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLTd3ZnYyIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJiZWJhZGVlYy1mZjg1LTQ5NzUtYTNiNS0yMWI2MGVlNTlhYzAiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZXJuZXRlcy1kYXNoYm9hcmQ6YWRtaW4tdXNlciJ9.NkIFGP2clqnxGBttnQonI4x0dJSdDKR5ILtD7Zff_6WWxCwof1uAzjDQCygOCfpsOngVPlXzOsp0On7ytWWuj1AMGiIPE6x8s4uGzHbIiF9BQuFa5jERZ9i0rnDFRnLw7Mv5AVYsNSGrfP16DXSyzOlb0tFhedWqcG533sqZEramHmWqS62uk7owxs3SQid9ZicsXlNNEVPEY5cLtAZylHL05EH4cpcPaTY_JUS9JX912vRghQEjRTOrBquGhQJ4jtv493MHXMpjw9Oc_mb22qnL4faRfZUMEH6mJpu61Le_HMiDLhSWIpXYLz5I8SEYtRP3WWqHVX-gkKgD4bY0gg
```
