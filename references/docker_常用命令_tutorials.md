# 一、docker常用命令

```shell
# 将多个镜像导出为一个 jar包
docker save -o image.tar 镜像1的id 镜像2的id

# 通过docker load 载入镜像
docker load < image.tar
```