# 参考文献
* [FastStone Capture注册码](https://www.jianshu.com/p/372687f65066)
* [FastStone 官网下载地址](https://www.faststonecapture.cn/download)

# 一、注册码

## 1.1、企业版序列号

> 亲测好使

```shell
name = bluman
注册码 = VPISCJULXUFGDDXYAUYF
```

> 待测试

```shell
# FastStone Capture 注册码 序列号： 
name/用户名 = TEAM JiOO 
key/注册码 = CPCWXRVCZW30HMKE8KQQUXW 

# ------------------------------ 
 
USER NAME = TEAM_BRAiGHTLiNG_2007 
CODE = XPNMF-ISDYF-LCSED-BPATU 
```

