# 参考文献

* [【JavaWeb】Tomcat的下载及使用](https://blog.csdn.net/m0_63463510/article/details/129710111)

# 一、简介
&emsp;&emsp;Tomcat是Apache软件基金会的一个核心项目, 也是一个开源免费的一个轻量级web服务器.支持servlet/JSP 少量的JavaEE规范,也是我们学习JavaWeb中常用的服务器.
Tomcat 也被称为Web容器,Servlet容器.Servlet需要依赖Tomcat才能运行

&emsp;&emsp;Tomcat的官网如下：
```shell
https://tomcat.apache.org/
```

# 二、下载

> 官网的下载地址

![官网的下载地址](./pictures/tomcat8_install/img.png)

# 三、上传与解压

> 已下载文件

```shell
apache-tomcat-8.5.97.tar.gz
```

> 解压

```shell
tar -zxvf apache-tomcat-8.5.97.tar.gz -C ./../installs/
```

> 解压后的目录列表

![tomcat目录列表](./pictures/tomcat8_install/img_1.png)

&emsp;&emsp;简单介绍一下这些目录

* bin: 里面有一些可执行的程序/脚本, 启动Tomcat就是在这里
* conf: 用.xml 的格式表示服务器的配置
* lib: 表示Tomcat 依赖的 一些库
* longs: 这个是日志,Tomcat的运行日志就在这里.如果程序出问题了,可以通过日志来排查问题
* temp: 这个目录用户存放tomcat在运行过程中产生的临时文件
* webapps: webapp就是网站,一个Tomcat可以部署多个网站,因此叫 “webapps”
* work:tomcat有一个work目录，里面存放了页面的缓存，访问的jsp都会编译，编译后的文件都会存储在work目录下

# 四、启动 Tomcat8

> Tomcat8 依赖jdk8 如果没有请查看[jdk1.8的安装](./Centos7-jdk1.8.md ':include')

```shell
cd /opt/installs/apache-tomcat-8.5.97/bin

# 启动
sh startup.sh 
```

![启动tomcat8](./pictures/tomcat8_install/img_2.png)

> 浏览器访问 

&emsp;&emsp;需要注意防火墙

```shell
# 查看防火墙状态
systemctl status firewalld
 
firewall-cmd --zone=public --add-port=8080/tcp --permanent
# 刷新一下
firewall-cmd --reload
```

&emsp;&emsp;访问该地址：http://192.168.255.102:8080/

![访问浏览器](./pictures/tomcat8_install/img_3.png)

# 五、修改Tomcat 的默认端口号

```shell
cd /opt/installs/apache-tomcat-8.5.97/conf

# 修改 server.xml
vim server.xml
```

![修改Tomcat 的默认端口号](./pictures/tomcat8_install/img_4.png)

