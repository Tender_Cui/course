# 参考文献

* [windows-cuda-安装](https://llamafactory.readthedocs.io/zh-cn/latest/getting_started/installation.html)

# 一、简介

&emsp;&emsp;`CUDA` 是由 `NVIDIA` 创建的一个并行计算平台和编程模型，它让开发者可以使用 `NVIDIA` 的 `GPU` 进行高性能的并行计算。

# 二、安装

## 2.1、查看 `windows` 规格

&emsp;&emsp;右击我的电脑 -> 属性 -> Windows 规格

```properties
版本： Windows 11 家庭中文版
版本号： 24H2
```

## 2.2、下载 `CUDA` 

* [产品地址](https://developer.nvidia.com/cuda-gpus/)   
* [下载地址](https://developer.nvidia.com/cuda-downloads)

![下载](./pictures/Windows-CUDA-安装/img.png)

## 2.3、安装

![安装](./pictures/Windows-CUDA-安装/img_1.png)

&emsp;&emsp;一路都点击 `下一步` 

# 三、安装测试

&emsp;&emsp;测试是否安装成功

```shell
nvcc -V
```

&emsp;&emsp;结果：

```shell
nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2025 NVIDIA Corporation
Built on Wed_Jan_15_19:38:46_Pacific_Standard_Time_2025
Cuda compilation tools, release 12.8, V12.8.61
Build cuda_12.8.r12.8/compiler.35404655_0
```