# 参考文献

* [如何在 Windows 10 环境下安装和配置 MySQL：初学者指南](https://blog.csdn.net/Mr_Zhangyuge/article/details/141069311)

# 一、下载 `Mysql` 安装包

* [官方下载地址](https://dev.mysql.com/downloads/installer/)

&emsp;&emsp;界面示例：

![界面示例](./pictures/Windows-mysql/img.png)

&emsp;&emsp;这里你会看到两种主要的安装包：Web 版和完整版。
1. **Web 版**：体积较小，安装过程中会从互联网下载所需组件。
2. **完整版**：包含所有必要组件，下载后可以离线安装。

> 建议选择 `完整版`，因为它不需要每次都从互联网下载组件，方便离线安装。

![选择离线版本下载](./pictures/Windows-mysql/img_1.png)

# 二、安装 `MySQL`

![只安装服务器](./pictures/Windows-mysql/img_2.png)

&emsp;&emsp;下一步

![下一步](./pictures/Windows-mysql/img_3.png)

&emsp;&emsp;一路 `next`

![设置root密码](./pictures/Windows-mysql/img_4.png)

&emsp;&emsp;安装成功

![安装成功](./pictures/Windows-mysql/img_5.png)