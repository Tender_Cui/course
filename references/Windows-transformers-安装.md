# 参考文献

* 直接问 `DeepSeek` 

```shell
  win10 安装 transformers 版本是 4.45.2
```

# 一、命令安装 `transformers`
```shell
pip install transformers==4.45.2
```

# 二、测试

```shell
import transformers
print(transformers.__version__)  # 输出应为 4.45.2
```

&emsp;&emsp;结果如下：

```shell
D:\A_Tender_workspace\install_4_code\python\python_3.10.9\python.exe D:\A_Tender_workspace\A_code\A_code_4_python\LLaMA-Factory\envTest\TransformersTest.py 
4.48.3

```

