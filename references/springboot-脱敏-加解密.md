# 参考文献

* [gitee上关于AES + RSA加解密的开源项目](https://gitee.com/bright-boy/springboot-rsa-aes)

&emsp;&emsp;个人实现代码demo：
* [请参考出入参加解密项目](https://gitee.com/Tender_Cui/springboot-component)

# 1、设计方案如下

&emsp;&emsp;只在后端生成`RSA`密钥对，没有在前端生成`RSA`密钥对，降低系统复杂对。

![加解密时序图](./pictures/springboot-encrypt-decrypt/img.png)

# 2、核心类

* RequestBodyAdviceAdapter：拦截请求（只拦截POST、PUT请求）
* ResponseBodyAdvice：拦截返回信息













