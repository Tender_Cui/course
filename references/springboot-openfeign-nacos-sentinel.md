# 参考文献

* [Java之SpringCloud Alibaba【四】【微服务 Sentinel服务熔断】](https://blog.csdn.net/qq_44757034/article/details/131673979)

# 1、背景

&emsp;&emsp;服务`消费者`调用服务`提供者`的接口，服务`消费者`需要有`fallback`服务降级的情况，不能持续访问服务提供者，增加服务提供者的负担，
但是通过`OpenFeign`接口调用的方法又各自不同，如果每个方法都增加一个`fallback`配对方法，这会导致代码膨胀不好管理，屎山代码堆积，工程埋雷 /(ㄒoㄒ)/~~。

&emsp;&emsp;服务`提供者`，同时支持`Sentinel`的流控，一旦`QPS`过高，则触发注解`@SentinelResource`里面配置的`blockHandler`方法。

# 2、解决方案

&emsp;&emsp;<span style="color:red">早期的经验</span>；按照`SentinelResource`资源名称限流+自定义限流返回+服务降级处理

![早期的经验](./pictures/springboot-openfeign-nacos-sentinel/img.png)

&emsp;&emsp;由于早期的经验，会导致来一个方法，就需要适配两个方法，会照成`屎山代码`的堆积，给系统埋雷。

---

&emsp;&emsp;因此，我们通过`fallback`属性进行统一配置，`OpenFeign`接口里面定义的全部方法都走统一的服务降级，<span style="color:green">一个即可搞定</span>。

![OpenFeign统一处理降级](./pictures/springboot-openfeign-nacos-sentinel/img_1.png)

# 3、服务提供者

## 3.1、`pom.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-nacos-openfeign-sentinel-provider</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>com.tender</groupId>
            <artifactId>shiro-nacos-openfeign-sentinel-facade-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
        <!-- spring-boot-starter-web 里面包含了 spring-boot-starter  -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>
</project>
```

## 3.2、`yaml`配置

```yaml
#server:
#  port: 8088

##### 数据源配置 #####
spring:
  profiles:
    active: dev
  application:
    name: shiro-nacos-openfeign-sentinel-provider

  cloud:
    sentinel:
      transport:
        dashboard: 192.168.255.100:8080
        port: 8719 # 默认端口，加入端口被占用，会自动从 8719 开始 +1 尝试，直到找到未被占用的端口。
      web-context-unify: false # controller 层的方法对 service 层的调用，不认为是同一个根链路。

    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP

# Logger Config
logging:
  level:
    com.tender: debug
```

## 3.3、启动类配置

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
public class ShiroNacosOpenFeignSentinelProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroNacosOpenFeignSentinelProviderApplication.class);
    }
}
```

## 3.4、业务类

&emsp;&emsp;`Facade`接口类：

```java
package com.tender.facade;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

// name 大写小写都ok
// @FeignClient(name = "SHIRO-NACOS-OPENFEIGN-SENTINEL-PROVIDER")

/**
 * name = "shiro-nacos-openfeign-sentinel-provider" 或者，需要和服务提供者的 spring.application.name 一样
 * name = "SHIRO-NACOS-OPENFEIGN-SENTINEL-PROVIDER" 大小写都可以
 * 注解 @FeignClient 这边不允许添加 @RequestMapping 注解
 * contextId = "providerServer" 对应于提供着的name @RestController("providerServer") 加不加都可以
 * 比如：
 * // @FeignClient(name = "shiro-nacos-openfeign-sentinel-provider",contextId = "providerServer")
 */
@FeignClient(name = "shiro-nacos-openfeign-sentinel-provider", fallback = StockFacadeSentinelFallBack.class)
public interface StockFacadeSentinelApi {

    @GetMapping("/stock/{orderNo}")
    String deduceStockByOrderNo(@PathVariable String orderNo);


    @GetMapping("/check/stock/{stockNo}")
    String checkStockInfoByNo(@PathVariable String stockNo);

}
```

&emsp;&emsp;`Controller`业务实现类：

```java
package com.tender.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.tender.facade.StockFacadeSentinelApi;

/**
 * openFeign 和 sentinel 进行服务降级和流量监控的整合处理demo
 */
@RestController
@RefreshScope // 以 这个bean 为单位，可以动态感知里面的属性的变化
public class StockController implements StockFacadeSentinelApi {

    @Value("${server.port}")
    private String port;

    @Override
    @SentinelResource(value = "deduceStockByOrderNo", blockHandler = "deduceStockByOrderNorBlockHandler")
    public String deduceStockByOrderNo(@PathVariable String orderNo) {
        return "方法：deduceStockByOrderNo " + " 端口：" + port + " 服务正常响应：O(∩_∩)O" + " orderNo = " + orderNo;
    }

    public String deduceStockByOrderNorBlockHandler(String orderNo, BlockException e) {
        return "方法：deduceStockByOrderNo " + " 端口：" + port + " 服务被Sentinel流控 /(ㄒoㄒ)/~~" + " orderNo = " + orderNo + " 异常：" + e.getMessage();
    }

    @Override
    @SentinelResource(value = "checkStockInfoByNo", blockHandler = "checkStockInfoByNoBlockHandler")
    public String checkStockInfoByNo(@PathVariable String stockNo) {
        return "方法：checkStockInfoByNo " + " 端口：" + port + " 服务正常响应：O(∩_∩)O" + " stockNo = " + stockNo;
    }

    public String checkStockInfoByNoBlockHandler(String stockNo, BlockException e) {
        return "方法：checkStockInfoByNo " + " 端口：" + port + " 服务被Sentinel流控 /(ㄒoㄒ)/~~" + " stockNo = " + stockNo + " 异常：" + e.getMessage();
    }
}
```

## 3.5、流控配置

&emsp;&emsp;以`@SentinelResource(value = "deduceStockByOrderNo", blockHandler = "deduceStockByOrderNorBlockHandler")`为示例

![流控配置](./pictures/springboot-openfeign-nacos-sentinel/img_2.png)

## 3.6、`服务端`测试结果

![正常访问](./pictures/springboot-openfeign-nacos-sentinel/img_3.png)

![被限流](./pictures/springboot-openfeign-nacos-sentinel/img_4.png)

# 4、服务消费者

## 4.1、`pom.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-nacos-openfeign-sentinel-consumer</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>com.tender</groupId>
            <artifactId>shiro-nacos-openfeign-sentinel-facade-api</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
        </dependency>
        <!-- 配置 激活Sentinel 对Feign 的支持 -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- 在SpringBoot 2.4.x的版本之后，对于bootstrap.properties/bootstrap.yaml配置文件
            (我们合起来成为Bootstrap配置文件)的支持，需要导入如下的依赖
            参考文献：https://www.jianshu.com/p/1d13e174b893
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bootstrap</artifactId>
        </dependency>
        <!--
            从 Spring Cloud 2020.0.0 版本开始，Ribbon 被废弃，Spring Cloud LoadBalancer 成为了推荐的负载均衡方案。
            在这个版本变动中，为了提供更大的灵活性，spring-cloud-starter-loadbalancer 被标记为了可选依赖，不再默认包含在 Spring Cloud Gateway 中。
            因此，在使用 3.1.4 版本的 Spring Cloud Gateway 并需要服务发现和负载均衡功能时，如果没有显式包含这个依赖，就会导致无法处理 lb://URI，从而返回503错误。
        -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-loadbalancer</artifactId>
        </dependency>
<!--        <dependency>-->
<!--            <groupId>org.springframework.cloud</groupId>-->
<!--            <artifactId>spring-cloud-loadbalancer</artifactId>-->
<!--        </dependency>-->
        <!-- spring-boot-starter-web 里面包含了 spring-boot-starter  -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    </dependencies>

</project>
```

## 4.2、`yaml`配置

```yaml
server:
  port: 8090

##### 数据源配置 #####
spring:
  main:
    allow-bean-definition-overriding: true

  profiles:
    active: dev

  application:
    name: shiro-nacos-openfeign-sentinel-consumer

  cloud:
    nacos:
      username: nacos
      password: nacos
      config:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP
        file-extension: yaml
        # 配置自动刷新
        refresh-enabled: true
        # 启用远程同步配置
        enable-remote-sync-config: true
      discovery:
        server-addr: 192.168.255.100:8848
        # 集群配置
        # server-addr: 192.168.255.100:8848,192.168.255.101:8848,192.168.255.102:8848
        namespace: 2bb28137-8cf3-4150-9587-e44864b3cd39
        group: DEFAULT_GROUP

# Logger Config
logging:
  level:
    com.tender: debug

# 激活 Sentinel 对 OpenFeign 的支持
feign:
  sentinel:
    enable: true
```

> <span style="color:red">注意这个：</span>

```yaml
# 激活 Sentinel 对 OpenFeign 的支持
feign:
  sentinel:
    enable: true
```

## 4.3、启动类配置

&emsp;&emsp;添加注解：`@EnableFeignClients`

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients
public class ShiroNacosOpenFeignSentinelConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroNacosOpenFeignSentinelConsumerApplication.class);
    }
}
```

## 4.4、业务`Controller`

```java
package com.tender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tender.facade.StockFacadeSentinelApi;

import java.util.UUID;

@RestController
@RefreshScope // 以 这个bean 为单位，可以动态感知里面的属性的变化
public class OrderController {

    @Autowired
    private StockFacadeSentinelApi stockFacadeSentinelApi;

    @Value("${server.port}")
    private String port;

    @GetMapping("/prepare/order")
    public String prepareOrder() {
        return stockFacadeSentinelApi.checkStockInfoByNo(UUID.randomUUID().toString());
    }

    @GetMapping("/place/order")
    public String placeOrder() {
        return stockFacadeSentinelApi.deduceStockByOrderNo(UUID.randomUUID().toString());
    }
}
```

# 5、结果测试

&emsp;&emsp;服务`消费者`，调用服务`提供者`

1. 正常调用

![正常调用](./pictures/springboot-openfeign-nacos-sentinel/img_5.png)

2. 被流控

![被流控](./pictures/springboot-openfeign-nacos-sentinel/img_6.png)

3. 服务提供者宕机

![服务提供者宕机](./pictures/springboot-openfeign-nacos-sentinel/img_7.png)

&emsp;&emsp;<span style="color:red">我这个版本不生效</span>

```xml
<dependencyManagement>
    <dependencies>
        <!-- 版本说明：https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E  -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>2021.0.5</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <!-- https://start.aliyun.com/  利用云原生应用脚手架来选择具体的组件和版本  -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-alibaba-dependencies</artifactId>
            <version>2021.0.5.0</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <!-- springboot 3.2.0 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>2.7.17</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```

&emsp;&emsp;<span style="color:red">替换成如下版本</span>

```xml
<dependencyManagement>
    <dependencies>
        <!-- 版本说明：https://github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E  -->
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>2023.0.0</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <!-- https://start.aliyun.com/  利用云原生应用脚手架来选择具体的组件和版本  -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-alibaba-dependencies</artifactId>
            <version>2022.0.0.0</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
        <!-- springboot 3.2.0 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-parent</artifactId>
            <version>3.2.0</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>
    </dependencies>
</dependencyManagement>
```



