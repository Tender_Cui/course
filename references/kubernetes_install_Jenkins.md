# 参考文献

* [jenkins官网：k8s部署文档-推荐](https://www.jenkins.io/doc/book/installing/kubernetes/)
* [k8s 部署jenkins](https://zhuanlan.zhihu.com/p/514205368?utm_id=0)
* [Linux配置NFS文件共享](https://blog.51cto.com/min2000/8973419?articleABtest=0)
* [k8s-部署到master节点【污点（Taint） 与 容忍度（Toleration）】总结](https://blog.csdn.net/liuyij3430448/article/details/129793091)
* [BACK-OFF RESTARTING FAILED CONTAINER 的解决方法](https://blog.csdn.net/yztezhl/article/details/125421316)

# 一、利用 K8s 来安装 Jenkins && NFS 共享文件

## 1.1、安装和配置NFS

### 1.1.1、NFS简介

&emsp;&emsp;NFS（Network File System），它最大的功能就是可以通过网络，让不同的机器、不同的操作系统可以共享彼此的文件。我们可以利用NFS共享Jenkins运行的配置文件、Maven的仓库依赖文件等

### 1.1.2、NFS安装

&emsp;&emsp;我们把NFS服务器安装在 192.168.255.101 机器上

1. 安装NFS服务(k8s 所有节点)

```shell
# 安装nfs-utils软件包
yum -y install nfs-utils		
```

2. 创建共享目录

```shell
cd /opt
mkdir -p nfs/jenkins

# 编写 NFS 的共享配置
vi /etc/exports

# 内容如下
# no_root_squash: 当NFS客户端以root管理员访问时，映射为NFS服务器的root管理员
# *: 对所有ip都开放此目录
# rw: 可以读写
/opt/nfs/jenkins *(rw,no_root_squash)
```

3. 启动服务(<span style="color:red">master 节点</span>)

```shell
systemctl start nfs
systemctl enable nfs

# 上面两条命令等价于 
systemctl enable nfs --now

systemctl enable nfs-server --now

# 配置生效 
exportfs -r

# 查看
exportfs
```

4. 查看NFS共享目录(<span style="color:red">从节点</span>)

```shell
# 192.168.255.102 或者 192.168.255.103 机器查看
showmount -e 192.168.255.101

# 所有从节点进行如下操作：
mkdir -p /opt/nfs/jenkins

# 在 2 个从服务器执行，将远程 和本地的 文件夹 挂载(这是挂载目录，共享目录)
mount -t nfs 192.168.255.101:/opt/nfs/jenkins /opt/nfs/jenkins

# 在 master 服务器，写入一个测试文件
echo "hello nfs server" > /opt/nfs/jenkins/test.txt

# 在 2 个从服务器查看
cd /opt/nfs/jenkins
ls
```

## 1.2、k8s 安装 jenkins

&emsp;&emsp;[请参考Jenkins 官方的yaml配置文件项目](https://github.com/scriptcamp/kubernetes-jenkins/tree/main)

&emsp;&emsp;[dockerhub搜索jenkins镜像](https://hub.docker.com/r/jenkins/jenkins)

&emsp;&emsp;所有yaml 配置文件我已经保存在 /references/k8s_yaml/jenkins 文件夹下，我主要改动 deployment.yaml 和 volume.yaml 文件.

> 针对数据存储，可以选择的方式有如下：

1. 修改存储为 NFS
2. 修改存储为 PVC、PV
3. 对于生产用例，应该为 Jenkins 数据添加特定于云的存储类持久卷
4. 如果您不需要本地存储持久卷，可以将部署中的卷定义替换为主机目录，如下所示

```shell
volumes:
- name: jenkins-data
emptyDir: {}
```

&emsp;&emsp;将yaml 配置文件拷贝到 /opt/softs/k8s 目录（视各人情况）

![拷贝yaml文件](./pictures/devOps_k8s_install_Jenkins/img.png)


&emsp;&emsp;要注意，节点会被随机分配到master节点和从节点，<span style="color:red">建议</span>都在节点宿主机上先拉取好镜像。

```shell
kubectl apply -f ./namespace.yaml
kubectl apply -f ./serviceAccount.yaml
kubectl apply -f ./service.yaml
# 如果使用nfs 就不用执行 volume.yaml
# kubectl apply -f ./volume.yaml
kubectl apply -f ./deployment.yaml
```

&emsp;&emsp; 查看 Jenkins pod

```shell
kubectl get pod -n devops-tools
```

## 1.3、访问验证 jenkins

![访问验证 jenkins](./pictures/devOps_k8s_install_Jenkins/img_1.png)