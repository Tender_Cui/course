# 参考文献

* [centos7.6安装redis](https://blog.csdn.net/cxy2687365281/article/details/129931785)

# 1、简介

&emsp;&emsp;[redis官网](https://redis.io/)   
&emsp;&emsp;[redis历史发布版本列表](https://download.redis.io/releases/)

# 2、环境准备

```html
192.168.18.104  虚机  单机部署
```

# 3、安装依赖包

&emsp;&emsp;`Redis` 是用 `C` 语言编写的，在编译安装 `Redis` 之前，需要先安装一些`编译工具`和`依赖库`。

## 3.1、安装 `gcc` 编译器

* 执行命令：

```shell
yum install gcc -y
```

* 解释：`yum` 是 `CentOS` 系统中的包管理器，`install` 是安装命令，`gcc` 是要安装的软件包名称，`-y` 表示在安装过程中自动回答 `yes`，避免手动确认。

## 3.2、安装其他依赖库

* 执行命令：

```shell
yum install -y jemalloc-devel
```

* 解释：`jemalloc` 是一个内存分配器，`Redis` 可以使用它来更高效地管理内存。安装 `jemalloc-devel` 包是为了在编译 `Redis` 时能够链接到这个内存分配器。

&emsp;&emsp;将安装GCC编译器、TCL和相关的开发工具。

# 4、下载、上传、解压

## 4.1、下载

&emsp;&emsp;下载 redis 安装包：`redis-7.0.0.tar.gz`

&emsp;&emsp;下载 [地址](https://download.redis.io/releases/)

## 4.2、上传

&emsp;&emsp; 上传到目录：`/opt/softs`

&emsp;&emsp; 上传一般到 `/root` 目录

```shell
cd /root 
mv redis-7.0.0.tar.gz /opt/softs/

ll /opt/softs
```

## 4.3、解压

&emsp;&emsp;解压到目录： `/opt/installs`

```shell
tar -zxvf redis-7.0.0.tar.gz -C ./../installs/
```

* 解释：`tar` 是一个用于打包和压缩文件的工具，`-zxvf` 是参数组合，`-z` 表示使用 `gzip` 解压，`-x` 表示解压，`-v` 表示显示详细信息，`-f` 表示指定要解压的文件。
`-C` 它的完整语法是 `--directory=DIR` 用于指定解压内容存放的目录。

# 5、编译和安装 `Redis`

## 5.1、进入解压后的 `Redis` 目录

* 执行命令：`cd /opt/install/redis-7.0.0`
* 解释：切换到 `Redis` 源代码目录，以便进行后续的编译和安装操作。

## 5.2、编译 `Redis`

* 执行命令：`make install`
* 解释：`make` 命令会根据 `Makefile` 文件中的配置来编译 `Redis` 源代码。这个过程可能需要一些时间，取决于服务器的性能。

## 5.3、安装 `Redis`

* 执行命令：

```shell
make install

# 编译 并 安装到指定目录下
make PREFIX=/usr/local/redis install
```

* 解释：`make install` 会将编译好的 `Redis` 二进制文件、配置文件等安装到系统默认的位置。通常，可执行文件会被安装到 <span style="color:red">/usr/local/bin</span> 目录下。

&emsp;&emsp;安装成功后的 <span style="color:red">/usr/local/bin</span> 目录：

```html
-rwxr-xr-x. 1 root root  5197096 12月  9 10:03 redis-benchmark
lrwxrwxrwx. 1 root root       12 12月  9 10:03 redis-check-aof -> redis-server
lrwxrwxrwx. 1 root root       12 12月  9 10:03 redis-check-rdb -> redis-server
-rwxr-xr-x. 1 root root  5409808 12月  9 10:03 redis-cli
lrwxrwxrwx. 1 root root       12 12月  9 10:03 redis-sentinel -> redis-server
-rwxr-xr-x. 1 root root 11345984 12月  9 10:03 redis-server
```

# 6、配置 `Redis`

## 6.1、复制 `Redis` 配置文件到 `/etc` 目录

* 执行命令：`cp /opt/install/redis-7.0.0/redis.conf /etc/redis.conf`
* 解释：`cp` 是复制文件的命令，将 `Redis` 源代码目录中的 `redis.conf` 配置文件复制到 `/etc` 目录下，方便进行系统级别的配置管理。

## 6.2、编辑 `/etc/redis.conf` 配置文件

* 执行命令：`vi /etc/redis.conf`
* 解释：`vi` 是一个文本编辑器，用于编辑 `redis.conf` 文件。在配置文件中，可以设置 `Redis` 的各种参数，如绑定的 IP 地址、端口号、是否开启持久化等。

```shell
cd /etc

vim redis.conf

# 具体修改内容如下：
protected-mode no # 允许远程链接
port 6379         # 默认端口是 6379
daemonize yes     # 允许后台启动
requirepass 88888888 #设置访问密码
```

# 7、启动 `Redis` 服务

## 7.1、前台启动 `Redis`（用于测试）

* 执行命令：

```shell
cd /usr/local/bin

# 启动
redis-server /etc/redis.conf
```

* 解释：`redis-server` 是 `Redis` 的服务器启动命令，指定 `/etc/redis.conf` 配置文件来启动 `Redis` 服务。这种方式启动 `Redis` 后，会在前台显示 `Redis` 的运行日志，适合在测试环境中查看启动情况。

## 7.2、后台启动 `Redis`（<span style="color:red">实际使用</span>）

* 执行命令：

```shell
cd /usr/local/bin

# 启动
redis-server /etc/redis.conf &
```

* 解释：在命令末尾添加 `&` 符号可以让 `Redis` 在后台启动，这样在终端关闭后，`Redis` 服务仍然可以继续运行。

# 8、验证 `Redis` 服务是否正常运行

```shell
redis-cli 

# 输入密码
auth 88888888
```

```html
127.0.0.1:6379> auth 88888888
OK
127.0.0.1:6379> set name tender
OK

127.0.0.1:6379> get name
"tender"
127.0.0.1:6379>
```

# 9、防火墙配置

```shell
firewall-cmd --zone=public --add-port=6379/tcp --permanent

# 刷新一下
firewall-cmd --reload
```