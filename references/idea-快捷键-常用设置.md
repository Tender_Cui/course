# 一、idea 社区免费版本必备插件

1. MybatisX
2. Maven Helper

# 二、常用快捷键

* CTRL + D          : 拷贝一行
* CTRL + Y          : 删除一行
* CTRL + F9         : 编译单个 java 文件
* CTRL + ALT + L    : 格式化
* CTRL + ALT + M    : 抽取方法
* CTRL + ALT + T    : try catch finally、for each、加锁等操作 
* 双击 SHIFT         : 全局搜索
* CTRL + SHIFT + F  : 文件中查找
* ALT + INSERT      : 覆盖方法、构造函数、getter、setter、toString

# 三、常用设置

## 3.1、 自动清除无效 import
> File -> Settings -> Editor -> General -> Auto Import

![自动清除无效 import](./pictures/idea/img.png)

## 3.2、 设置默认换行符

> File -> Settings -> Editor -> Code Style

![设置默认换行符](./pictures/idea/img_1.png)

## 3.3、 打开多个文件显示在多行tab上

![打开多个文件显示在多行tab上](./pictures/idea/img_2.png)


