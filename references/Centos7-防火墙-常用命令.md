```shell
systemctl status firewalld

sudo systemctl start firewalld  # 启动
sudo systemctl enable firewalld # 开机就启动

firewall-cmd --zone=public --add-port=9200/tcp --permanent # 添加放行端口

# 重新加载防火墙配置
firewall-cmd --reload

# 验证端口是否已添加
firewall-cmd --zone=public --query-port=8080/tcp

# 列出所有开放的端口
firewall-cmd --zone=public --list-ports

# 移除端口
firewall-cmd --zone=public --remove-port=8080/tcp --permanent
firewall-cmd --reload
```