# 参考文献

* [Spring boot 父子项目打包时提示"找不到符号"](https://blog.csdn.net/ganaochina/article/details/124314305)
* [springboot项目打包时提示"程序包xxx不存在，找不到符号"](https://www.cnblogs.com/xiadongqing/p/12979764.html)

# 一、常见错误

## 1.1、maven 打包的时候报 "找不到符号"

&emsp;&emsp;maven 在进行 **package** 的时候报错了，提示 "找不到符号"(course-test模块依赖course-bootstrap模块)，这个是由于course-bootstrap模块里面的 **spring-boot-maven-plugin** 插件打包的缘故，
这个需要把该 maven 插件，放到别的模块，比如course-test模块(最好是依赖链路的最底层)。

```shell
<build>
    <!-- 最终打包的名称 -->
    <finalName>course</finalName>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <mainClass>com.tender.CourseApplication</mainClass>
            </configuration>
            <executions>
                <execution>
                    <id>repackage</id>
                    <goals>
                        <goal>repackage</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
    </plugins>
</build>
```

## 1.2、使用 spring-boot-maven-plugin 生成普通的jar包

```shell
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <skip>true</skip>
            </configuration>
        </plugin>
    </plugins>
</build>
```

> 打出来的包就是没有依赖jar的普通包了。 


