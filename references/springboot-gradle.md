# 参考文献

* [gradle学习笔记](https://www.yuque.com/youyi-ai1ik/emphm9/kyhenl)

# 1、idea中创建由Gradle 管理的java项目

## 1.1、创建工程

![工程](./pictures/springboot-gradle/img.png)

## 1.2、指定本机Gradle

> 可以加快下载项目依赖jar 包的速度【配置了私服地址】

![指定本机Gradle](./pictures/springboot-gradle/img_1.png)

> 注意点

&emsp;&emsp;在`Terminal` 中执行以`gradlew` 开头命令和操作图形化的`IDEA` 使用`Gradle` 版本不一定是同一个版本哦。

1. `Terminal`中以`gradlew`开头指令，用的是`Wrapper`规定的`gradle`版本,`wrapper`中规定版本默认和`idea`插件中规定的版本一致。
2. 而图形化的`IDEA`使用`Gradle`是 **本地** 安装的哦。
3. 当我们在 `build.gradle` 文件添加依赖之后，这些依赖会在下载到`GRADLE_USER_HOME/caches/modules-2/files-2.1` 目录下面,所以这里的 `GRADLE_USER_HOME` 相当于 `Gradle` 的本地仓库, 
   当然也可以如下方式找到`jar` 包位置。

> 如下图：

![指定本机Gradle](./pictures/springboot-gradle/img_2.png)

# 2、idea中创建由Gradle 管理的web项目

&emsp;&emsp;在`idea` 新版本的创建项目中,无法自己选择创建项目是普通 `java` 工程还是 `web` 工程了【`IDEA` 旧版本是可以的】，
所以我们如果想创建 `web` 工程，只需要自己在 `src/main/`目录下添加`webapp/WEB-INF/web.xml` 及页面即可。

&emsp;&emsp;接下来在我们对`gradle` 的其它知识点讲解之前我们先提一下在`gradle` 文件中需要用到的`Groovy` 语法。

# 3、Gradle对测试的支持

&emsp;&emsp;测试任务自动检测并执行测试源集中的所有单元测试。测试执行完成后会生成一个报告。支持JUnit 和 TestNG 测试。

## 3.1、默认测试目录及标准输出

![测试目录](./pictures/springboot-gradle/img_3.png)

## 3.2、Junit 使用

```Groovy
dependencies {
    testImplementation group: 'junit' ,name: 'junit', version: '4.12'
}
test {
    useJUnit()
}
```

## 3.3、Gradle 对于Junit5.x 版本支持

```Groovy
dependencies {
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.1' 
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine:5.8.1'
}
test {
    useJUnitPlatform()
}
```

> 注意

&emsp;&emsp;无论是 `Junit4.x` 版本还是`Junit5.x` 版本，我们只需在 `build.gradle` 目录下执行`gradle test` 指令，`gradle` 就会帮我们执行所有的加了`@Test` 注解的测试，并生成测试报告。

> 测试报告在项目build-reports目录下，浏览器打开index.html即可查看

## 3.4、包含和排除特定测试

```Groovy
test {
    enabled true 
    useJUnit() 
    include 'com/**'
    exclude 'com/abc/**'
}
```

&emsp;&emsp;`gradle` 在`junit` 中的批量测试,可以设置包含或者排除某些特定测试。

# 4、Gradle 进阶

> 为了让大家快速的入门gradle，本章将从整体构建脚本的角度介绍:

* 什么是 `setting` 文件,它有什么作用;
* 说明什么是`build` 文件,它又有什么作用
* 我们可以创建多少个 `build`
* `project` 和`task`,他们有什么作用;又是什么关系,如何配置
* 项目的生命周期
* 项目发布
* 使用`Gradle` 创建`SpringBoot` 项目等

## 4.1、项目的生命周期

&emsp;&emsp;`Gradle` 项目的生命周期分为三大阶段: `Initialization` -> `Configuration` -> `Execution`. 每个阶段都有自己的职责,具体如下图所示:

![项目的生命周期](./pictures/springboot-gradle/img_4.png)

* `Initialization` 阶段：主要目的是初始化构建, 它又分为两个子过程,一个是执行 `Init Script`,另一个是执行 `Setting Script`。
  * `init.gradle` 文件会在每个项目 `build` 之前被调用，用于做一些初始化的操作，它主要有如下作用：
    * 配置内部的仓库信息（如公司的 maven  仓库信息）；
    * 配置一些全局属性；
    * 配置用户名及密码信息（如公司仓库的用户名和密码信息）。

  * `Setting Script` 则更重要, 它初始化了一次构建所参与的所有模块。

* `Configuration` 阶段：这个阶段开始加载项目中所有模块的 `Build Script`。所谓 "加载" 就是执行 `build.gradle` 中的语句, 
    根据脚本代码创建对应的 `task`, 最终根据所有 `task` 生成由 `Task` 组成的有向无环图(`Directed Acyclic Graphs`)，如下:

![有向无环图](./pictures/springboot-gradle/img_5.png)

> 从而构成如下<span style="color:red">有向无环树</span>

![有向无环图2](./pictures/springboot-gradle/img_6.png)

* `Execution` 阶段：这个阶段会根据上个阶段构建好的有向无环图，按着顺序执行 `Task`【Action 动作】。

## 4.2、`settings` 文件

&emsp;&emsp;首先对 `settings` 文件的几点说明：

1. 作用：主要是在项目初始化阶段确定一下引入哪些工程需要加入到项目构建中,为构建项目工程树做准备。
2. 工程树：`gradle` 中有工程树的概念，类似于 `maven` 中的`project` 与`module`。

![父子工程](./pictures/springboot-gradle/img_7.png)

3. 内容：里面主要定义了当前 `gradle` 项目及子 `project` 的项目名称
4. 位置：**必须** 放在 **根工程目录** 下。
5. 名字：为`settings.gradle` 文件，不能发生变化
6. 对应实例：与 `org.gradle.api.initialization.Settings` 实例是一一对应的关系。每个项目只有一个`settings` 文件。
7. 关注：作为开发者我们只需要关注该文件中的`include` 方法即可。使用<span style="color:red">相对路径【 : 】引入子工程</span>。
8. 一个子工程只有在`settings` 文件中配置了才会被 `gradle` 识别,这样在构建的时候才会被包含进去。案例如下所示：

```Groovy
//根工程项目名
rootProject.name = 'root'
//包含的子工程名称
include 'subject01' 
include 'subject02' 
include 'subject03'
//包含的子工程下的子工程名称
include 'subject01:subproject011' 
include 'subject01:subproject012'
```

> <span style="color:red">注意</span>：

&emsp;&emsp;项目名称中 ":" 代表项目的分隔符, 类似路径中的 "/". 如果以 ":" 开头则表示 **相对于**  `root project` 。
然后 `Gradle` 会为每个带有 `build.gradle` 脚本文件的工程构建一个与之对应的 `Project` 对象。

## 4.3、`Task`

&emsp;&emsp;项目实质上是 `Task` 对象的集合。一个 `Task` 表示一个逻辑上较为独立的执行过程，比如编译`Java` 源代码，拷贝文件， 打包`Jar` 文件，甚至可以是执行一个系统命令。另外，一个 `Task` 可以读取和设置`Project` 的`Property` 以完成特定的操作。

### 4.3.1、任务入门

&emsp;&emsp;可参考官方文档：https://docs.gradle.org/current/userguide/tutorial_using_tasks.html 

> 让我们来先看一个例子:

```Groovy
task A {
    println "root taskA" 
    doFirst(){
        println "root taskA doFirst"
    }
    doLast(){
        println "root taskA doLast"
    }
}
```

> 执行指定`Task`

![执行指定Task](./pictures/springboot-gradle/img_8.png)

### 4.3.2、任务的行为

&emsp;&emsp;案例如下：`doFirst`、`doLast` 两个方法可以在任务内部定义，也可以在任务外部定义

```Groovy
def map=new HashMap<String,Object>();
//action属性可以设置为闭包，设置task自身的行为
map.put("action",{println "taskD.."})
task(map,"task2"){
    description   'taskA description	'
    group "atguigu"
    //在task内部定义doFirst、doLast行为
    doFirst {
        def name = 'doFirst..'
        println name
    }
    doLast {
        def name = 'doLast..'
        println name
    }
}
//在task外部定义doFirst、doLast行为
task2.doFirst {
    println it.description
}
task2.doLast {
    println it.group
}
```

> 结果

![执行指定Task2](./pictures/springboot-gradle/img_9.png)

### 4.3.3、底层原理分析

&emsp;&emsp;无论是定义任务自身的 `action`,还是添加的`doLast`、`doFirst` 方法，其实底层都被放入到一个`Action` 的`List` 中了，最初这个 `action List` 是空的，
当我们设置了 `action`【任务自身的行为】,它先将`action` 添加到列表中，此时列表中只有一个`action`,后续执行`doFirst` 的时候`doFirst` 在`action` 前面添加，
执行 `doLast` 的时候`doLast` 在`action` 后面添加。`doFirst` 永远添加在`actions List` 的第一位，保证添加的`Action` 在现有的 `action List` 元素的最前面；
`doLast` 永远都是在`action List` 末尾添加，保证其添加的`Action` 在现有的`action List` 元素的最后面。一个往前面添加,一个往后面添加，
最后这个`action List` 就按顺序形成了`doFirst`、`doSelf`、`doLast` 三部分的 `Actions`,就达到 `doFirst`、`doSelf`、`doLast` 三部分的 `Actions` 顺序执行的目的。

### 4.3.4、任务的依赖方式

&emsp;&emsp;`Task` 之间的依赖关系可以在以下几部分设置：

1. 参数依赖
2. 内部依赖
3. 外部依赖
4. 跨项目依赖

#### 4.3.4.1、参数依赖

```Groovy
task A {
    doLast {
        println "TaskA.."
    }
}
task 'B' {
    doLast {
        println "TaskB.."
    }
}
//参数方式依赖别的任务: dependsOn后面用冒号
task 'C'(dependsOn: ['A', 'B']) {
    doLast {
        println "TaskC.."
    }
}}
```

#### 4.3.4.2、内部依赖

```Groovy
//参数方式依赖
task 'C' {
    //内部依赖：dependsOn后面用 = 号
    dependsOn= [A,B] 
    doLast {
        println "TaskC.."
    }
}
```

#### 4.3.4.3、外部依赖

```Grovvy
task 'C' {
    doLast {
        println "TaskC.."
    }
}
//外部依赖:可变参数,引号可加可不加
C.dependsOn(B,'A')
```

#### 4.3.4.4、跨项目依赖

> 在subproject01 工程的 build.gradle 文件中定义:

```Groovy
task A {
    doLast {
        println "TaskA.."
    }
}
```

> 在subproject02 工程的 build.gradle 文件中定义:

```Groovy
task B {
    dependsOn(":subproject01:A") //依赖 根工程下的 subject01中的 任务A ：跨项目依赖。
    doLast {
        println "TaskB.."
    }
}
```

> 测试结果

```Groovy
// Terminal 中执行如下命令
gradle B
```

![子项目任务引用](./pictures/springboot-gradle/img_10.png)

> <span style="color:red">注意点：</span>

* 当一个 `Task` 依赖多个`Task` 的时候，<span style="color:red">被依赖</span>的`Task` 之间如果没有依赖关系，那么它们的执行顺序是随机的,并无影响。
* 重复依赖的任务只会执行一次,比如：

1. A->B、C 
2. B->C
> 任务A 依赖任务 B 和任务 C、任务 B 依赖C 任务。执行任务A 的时候，显然任务C 被重复依赖了，C 只会执行一次。

### 4.3.5、任务执行

&emsp;&emsp;任务执行语法：`gradle [taskName...] [--option-name...]`

> 1. <span style="color:red">常见的任务（*）</span>

* gradle build: 构建项目:编译、测试、打包等操作
* gradle run :运行一个服务,需要application 插件支持，并且指定了主启动类才能运行
* gradle clean: 请求当前项目的 build 目录
* gradle init : 初始化 gradle 项目使用
* gradle wrapper:生成wrapper 文件夹的。
* gradle wrapper 升级wrapper 版本号：gradle wrapper --gradle-version=4.4
* gradle wrapper --gradle-version 5.2.1 --distribution-type all :关联源码用

> 2. <span style="color:red">项目报告相关任务</span>

* gradle projects : 列出所选项目及子项目列表，以层次结构的形式显示
* gradle tasks: 列出所选项目【当前 project,不包含父、子】的已分配给任务组的那些任务。
* gradle tasks --all :列出所选项目的所有任务。
* gradle tasks --group="build setup":列出所选项目中指定分组中的任务。
* gradle help --task someTask :显示某个任务的详细信息
* gradle dependencies :查看整个项目的依赖信息，以依赖树的方式显示
* gradle properties 列出所选项目的属性列表

> 3. 调试相关选项

-h,--help: 查看帮助信息
-v, --version:打印 Gradle、 Groovy、 Ant、 JVM 和操作系统版本信息。
-S, --full-stacktrace:打印出所有异常的完整(非常详细)堆栈跟踪信息。
-s,--stacktrace: 打印出用户异常的堆栈跟踪(例如编译错误)。
-Dorg.gradle.daemon.debug=true: 调试 Gradle  守护进程。
-Dorg.gradle.debug=true:调试 Gradle 客户端(非 daemon)进程。
-Dorg.gradle.debug.port=(port number):指定启用调试时要侦听的端口号。默认值为 5005。


> 4. 性能选项:【<span style="color:red">备注</span>: 在gradle.properties 中指定这些选项中的许多选项，因此不需要命令行标志】

* -build-cache, --no-build-cache： 尝试重用先前版本的输出。默认关闭(off)。
* --max-workers: 设置 Gradle 可以使用的woker 数。默认值是处理器数。
* -parallel, --no-parallel: 并行执行项目。有关此选项的限制，请参阅并行项目执行。默认设置为关闭(off)

> 5. 守护进程选项

* --daemon, --no-daemon:  使用 Gradle 守护进程运行构建。默认是on
* --foreground:在前台进程中启动 Gradle  守护进程。
* -Dorg.gradle.daemon.idletimeout=(number of milliseconds):
* Gradle Daemon 将在这个空闲时间的毫秒数之后停止自己。默认值为 10800000(3 小时)。


> 6. 日志选项

* -Dorg.gradle.logging.level=(quiet,warn,lifecycle,info,debug):
* 通过 Gradle 属性设置日志记录级别。
* -q, --quiet: 只能记录错误信息
* -w, --warn: 设置日志级别为 warn
* -i, --info: 将日志级别设置为 info
* -d, --debug:登录调试模式(包括正常的堆栈跟踪)

> 7. <span style="color:red">其它</span>

* 其它(*)	-x:-x 等价于: --exclude-task : 常见gradle -x test clean build
* --rerun-tasks: 强制执行任务，忽略up-to-date ,常见gradle build --rerun-tasks
* --continue: 忽略前面失败的任务,继续执行,而不是在遇到第一个失败时立即停止执行。每个遇到的故障都将在构建结束时报告，常见：gradle build --continue。
* gradle init --type pom :将maven 项目转换为gradle 项目(根目录执行)
* gradle [taskName] :执行自定义任务

> 更详细请参考[官方文档](https://docs.gradle.org/current/userguide/command_line_interface.html#sec:command_line_executing_tasks) 

> 注意点：

* `gradle` 任务名可以缩写: 任务名支持驼峰式命名风格的任务名缩写，如：`connectTask`  简写为：`cT`,执行任务 `gradle cT`。
* 前面提到的`Gradle` 指令本质:一个个的`task[任务]`, `Gradle` 中所有操作都是基于任务完成的。

![任务依赖图](./pictures/springboot-gradle/img_11.png)

* `gradle` 默认各指令之间相互的依赖关系：

![依赖关系](./pictures/springboot-gradle/img_12.png)

### 4.3.6、任务定义方式

&emsp;&emsp;任务定义方式，总体分为两大类:一种是通过 `Project` 中的`task()`方法,
另一种是通过`tasks` 对象的 `create` 或者`register` 方法。

```Groovy
task('A',{
    //任务名称,闭包都作为参数
    println "taskA..."
})
task('B'){
    //闭包作为最后一个参数可以直接从括号中拿出来
    println "taskB..."
}
task C{
    //groovy语法支持省略方法括号:上面三种本质是一种
    println "taskC..."
}
def map=new HashMap<String,Object>(); map.put("action",{println "taskD.."}) //action属性可以设置为闭包task(map,"D");
tasks.create('E'){
    //使用tasks的create方法
    println "taskE.."
}
tasks.register('f'){ 
    //注：register执行的是延迟创建。也即只有当task被需要使用的时候才会被创建。
    println "taskF	"
}
```

> 当然：我们也可以在定义任务的同时指定任务的属性，具体属性有：

![任务具体属性](./pictures/springboot-gradle/img_13.png)

> <span style="color:red">在定义任务时也可以给任务分配属性</span>

&emsp;&emsp;定义任务的时候可以直接指定任务属性，也可以给已有的任务动态分配属性：

```Groovy
//①.F是任务名，前面通过具名参数给map的属性赋值,以参数方式指定任务的属性信息
task(group: "atguigu",description: "this is task B","F")
//②.H是任务名，定义任务的同时，在内部直接指定属性信息
task("H") {
group("atguigu") description("this is the task H")
}
//③.Y是任务名，给已有的任务 在外部直接指定属性信息
task "y"{}
y.group="atguigu"

clean.group("atguigu") //案例：给已有的clean任务(Gradle 自带任务)重新指定组信息
```

### 4.3.7、任务类型

&emsp;&emsp;前面我们定义的`task` 都是`DefaultTask` 类型的,如果要完成某些具体的操作完全需要我们自己去编写`gradle` 脚本，势必有些麻烦，
那有没有一些现成的任务类型可以使用呢？有的，`Gradle` 官网给出了一些现成的任务类型帮助我们快速完成想要的任务，我们只需要在创建任务的时候，
指定当前任务的类型即可，然后即可使用这种类型中的`属性和API` 方法了。

| 常见任务类型  |                   该类型任务的作用                    | 
|:---:|:---------------------------------------------:|
|  Delete  |                    删除文件或目录                    | 
|  Copy  |        将文件复制到目标目录中。此任务还可以在复制时重命名和筛选文件。        | 
|  CreateStartScripts  |                    创建启动脚本                     | 
|  Exec  |                    执行命令行进程                    | 
|  GenerateMavenPom  |            生成 Maven 模块描述符(POM)文件。             | 
|  GradleBuild  |                 执行 Gradle 构建                  | 
|  Jar  |                  组装 JAR 归档文件                  | 
|  JavaCompile  |                  编译 Java 源文件                  | 
|  Javadoc  |            为 Java 类生成 HTML API 文档             | 
|  PublishToMavenRepository  | 将 MavenPublication 发布到 mavenartifactrepostal。 | 
|  Tar  |                  组装 TAR 存档文件                  | 
|  Test  |    执行 JUnit (3.8.x、4.x 或 5.x)或 TestNG 测试。     | 
|  Upload  |         将 Configuration 的构件上传到一组存储库。          | 
|  War  |                  组装 WAR 档案。                   | 
|  Zip  |          组装 ZIP 归档文件。默认是压缩 ZIP 的内容。           | 


> 提示

&emsp;&emsp;如果想看更详细的`gradle` 自带`Task` 类型，请参考[官方文档](https://docs.gradle.org/current/dsl/index.html)

![官方任务参考手册](./pictures/springboot-gradle/img_14.png)

> 如何自定义 `Task` 类型

```Groovy
// 自定义 Task 类型
class CustomTask extends DefaultTask {
    //@TaskAction表示Task本身要执行的方法
    @TaskAction
    def doSelf(){
        println "Task 自身 在执行的in doSelf"
    }
}

def myTask=task MyDefinitionTask (type: CustomTask) 

myTask.doFirst(){
    println "task 执行之前 执行的 doFirst方法"   
}
myTask.doLast(){
    println "task 执行之后 执行的 doLast方法"
}
```

### 4.3.8、任务的执行顺序

> 在 `Gradle` 中,有三种方式可以指定 `Task` 执行顺序：

1. `dependsOn` 强依赖方式
2. 通过 `Task` 输入输出
3. 通过 `API` 指定执行顺序

&emsp;&emsp;详细请参考[官方文档](https://docs.gradle.org/current/dsl/org.gradle.api.Task.html)

### 4.3.9、动态分配任务

30 讲

