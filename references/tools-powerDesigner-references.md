# 参考文献

* [powerDesigner使用指南](https://blog.csdn.net/Yaoyao2024/article/details/130462433)

# 1、简述
## 1.1、什么是`E-R`图？

&emsp;&emsp;E-R图，也称ERD（`Entity-Relationship diagram`),实体关系图（这里的实体其实就是指数据库中的表`table`）。用于设计数据库结构，来设计系统范围内的实体，以及之间的关系。

## 1.2、强大的powerDesigner工具

&emsp;&emsp;`powerDesigner`虽然是德国的`SAP公司`开发的数据建模、元数据管理工具。但是它的创始人是中国人：`王晓昀`。源于他自己毕业后从事软件开发工作，想找到一个很好的建模工具，于是他自己开发了一款，也就是`PowerDesigner`。

## 1.3、工具介绍

> 百度百科：PowerDesigner是Sybase公司的CASE工具集，使用它可以方便地对管理信息系统进行分析设计，它几乎包括了`数据库模型设计的全过程`。 
> 利用PowerDesigner可以制作`数据流程图`、`概念数据模型`、`物理数据模型`，可以生成多种客户端开发工具的应用程序，还可为数据仓库制作结构模型，也能对团队设备模型进行控制。 它可与许多流行的数据库设计软件，例如：PowerBuilder，Delphi，VB等相配合使用来缩短开发时间和使系统设计更优化。

# 2、`powerDesigner`使用教程

## 2.1、创建模型

&emsp;&emsp;选择 `file -> new model` 来创建模型

![创建模型](./pictures/tools-powerDesigner-references/img.png)

&emsp;&emsp;查看`可选择`的数据模型

![选择数据模型](./pictures/tools-powerDesigner-references/img_1.png)

> 说明：对于数据库模型设计，我们主要用到以上两个模型；
> 1. `Conceptual Data Model`：概念数据模型
> 2. `Physical Data Model`：物理数据模型
> 
> **概念数据模型**，是我们<span style="color:pink">主要使用</span>的模型设计功能。   
> **物理数据模型**，可以由概念数据模型直接转换而来。为什么叫物理数据模型呢？因为它是有实际意义的，对于不同数据库，我们的概念模型几乎是一样的，但是落到实际，不同数据库的模型定义又稍有不同，这个实际的数据模型，就是物理数据模型，它是依附于具体数据库。

## 2.2、选择`Conceptual Data Model`模型

![选择概念数据模型](./pictures/tools-powerDesigner-references/img_2.png)

### 2.2.1、初识画图界面

![初识画图界面](./pictures/tools-powerDesigner-references/img_3.png)

### 2.2.2、了解控制面板

![了解控制面板](./pictures/tools-powerDesigner-references/img_4.png)

> 问题：如果控制面板被关闭了，如何再打开呢？

![打开控制面板](./pictures/tools-powerDesigner-references/img_4.1.png)

&emsp;&emsp;选择`Palette`，来打开面板
![选择`Palette](./pictures/tools-powerDesigner-references/img_4.2.png)

## 2.3、创建一个实体`entity`

![创建一个实体`entity`](./pictures/tools-powerDesigner-references/img_5.png)

## 2.4、双击这个实体，对它进行编辑(`General` Tap页面)

![编辑1](./pictures/tools-powerDesigner-references/img_6.png)

## 2.5、向该实体中添加属性(`Attributes` Tap页面)

![添加属性](./pictures/tools-powerDesigner-references/img_7.png)

&emsp;&emsp;保存后的展示

![保存后的展示](./pictures/tools-powerDesigner-references/img_8.png)

## 2.6、修改主键名(`Identifiers` Tap页面)

![修改主键名](./pictures/tools-powerDesigner-references/img_9.png)

## 2.7、点击`保存`

&emsp;&emsp;点击save，就会生成一个后缀为`.cdm(concenptual data model)`的文件

![点击`保存`](./pictures/tools-powerDesigner-references/img_10.png)

## 2.8、打开保存的`.cdm(concenptual data model)`的文件

![打开保存的`.cdm`文件](./pictures/tools-powerDesigner-references/img_11.png)

## 2.9、一对多的关系

&emsp;&emsp;再创建一个实体：`ship_record`，记录船次信息。

![船次信息](./pictures/tools-powerDesigner-references/img_12.png)

&emsp;&emsp;一条船(`ship_data`)可以对应多个航次信息(`ship_record`)。

### 2.9.1、绘画`relationship`线条

&emsp;&emsp;选择`palette`面板上的`relationship`，<span style="color:red">从`ship_data`指向`ship_record`</span>

![绘画`relationship`线条](./pictures/tools-powerDesigner-references/img_13.png)

### 2.9.2、双击关系，即可进行编辑

1. 给关系修改名称(`General` Tap页面)

![给关系修改名称](./pictures/tools-powerDesigner-references/img_14.png)

2. 设置基数(`Cardinalities` Tap页面)

![设置基数](./pictures/tools-powerDesigner-references/img_15.png)

# 3、生成sql

## 3.1、生成物理模型 PDM(`Physical Data Model`)

&emsp;&emsp;根据`概念数据模型`<span style="color:red">自动生成</span>`物理数据模型`。

![生成物理模型 PDM](./pictures/tools-powerDesigner-references/img_16.png)

&emsp;&emsp;选择对应的数据库和重命名物理数据模型

![选择对应的数据库和重命名物理数据模型](./pictures/tools-powerDesigner-references/img_17.png)

&emsp;&emsp;生成`mysql5.0版本`的物理数据模型

![生成物理模型4`mysql5.0版本`](./pictures/tools-powerDesigner-references/img_18.png)


## 3.2、自动生成SQL语句

&emsp;&emsp;选择`database->generate database`

![generate database](./pictures/tools-powerDesigner-references/img_19.png)

&emsp;&emsp;生成sql的界面选择
![生成sql的界面选择](./pictures/tools-powerDesigner-references/img_20.png)