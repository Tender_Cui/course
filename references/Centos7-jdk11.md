# 参考文献
* [CentOS7安装配置OpenJDK11](https://blog.csdn.net/caowey/article/details/134683293)
# 一、检查系统有没有安装自带的 open-jdk

```shell
# -qa 查询所有的
rpm -qa | grep java
rpm -qa | grep jdk
rpm -qa | grep gcj
```

> 如果没有输入信息表示没有安装。

> 如果安装了，可以使用如下命令来批量卸载所有带有Java的文件 

```shell
# 需要使用root 账户 
# -e 等价于 --erase 删除的意思
rpm -qa | grep java | xargs rpm -e --nodeps
```

> 这句命令的关键字是 **java**，也有可能是 **openjdk**，视个人安装情况。

## 1.1、yum 安装

&emsp;&emsp;先检索包含java的列表
```shell
yum list java*
yum list java-11*  
```

![java-1.8.0列表](./pictures/jdk11/img.png)

&emsp;&emsp;安装 java-11.0-openjdk*的<span style="color:red">所有文件</span>

```shell
# -y 自动确认
yum install -y java-11.0-openjdk* 
```

> 这样安装有一个好处就是不需要对path进行设置，自动就设置好了

## 1.2、<span style="color:red">安装包 安装</span>

[jdk 11 资源下载](https://mirrors.tuna.tsinghua.edu.cn/Adoptium/)

> 根据自己的系统下载对应的 jdk，文件结尾要是 tar.gz

### 1.2.1、解压

```shell
# 目录根据情况，需要提前创建
# -C（大写的C） 指定解压缩的文件夹路径
tar -zxvf jdk-11.0.7_linux-x64_bin.tar.gz -C ./../installs/
```

### 1.2.2、配置环境变量

> 打开配置文件

```shell
vi /etc/profile
```

> 添加环境变量: 

&emsp;&emsp;我的解压目录是： /opt/installs/

&emsp;&emsp;jdk 解压后，**自带**一个 **jdk-11.0.7** 的文件夹

```shell
export JAVA_HOME=/opt/installs/jdk-11.0.7
export JRE_HOME=/opt/installs/jdk-11.0.7/jre
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
export PATH=$JAVA_HOME/bin:$PATH
```

> 刷新环境变量，使配置文件生效

```shell
source /etc/profile
```

> 用于重新加载系统环境变量文件/etc/profile,以便使修改后的环境变量生效

### 1.2.3、验证结果

```shell
 java -version
```
![验证结果](./pictures/jdk11/img_1.png)
