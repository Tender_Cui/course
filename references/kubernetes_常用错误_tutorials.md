# 一、k8s 常见错误列表：

## 1.1、Harbor有镜像，但还是镜像拉取失败

&emsp;&emsp;<span style="color:red">可能的问题</span>：docker Harbor 中明明有对应的镜像，但是pod 拉取镜像还是失败！

* [拉取镜像失败，参考文献](https://blog.csdn.net/qq_34041723/article/details/125897306)

&emsp;&emsp;<span style="color:red">如何解决 pod拉取 Harbor 里的镜像失败</span>。

1. 先拉取 jenkins 镜像
2. 将 jenkins 镜像推送到harbor 仓库
3. 登录harbor，然后将/root/.docker/config.json 登录信息拷贝到 /var/lib/kubelet/config.json

```shell
docker search jenkins
docker tag jenkins:latest 192.168.255.102:5000/course_project/jenkins:v1
docker push 192.168.255.102:5000/course_project/jenkins:v1

# 登录每个宿主机，然后拉取镜像到本地
docker login -u user_course -p Course88888888 192.168.255.102:5000
docker pull 192.168.255.102:5000/course_project/jenkins:v1
cp /root/.docker/config.json /var/lib/kubelet/config.json
systemctl restart kubelet
```

## 1.2、容器启动成功后即退出

&emsp;&emsp;可能的问题：BACK-OFF RESTARTING FAILED CONTAINER

&emsp;&emsp;Back-off restarting failed container的Warning事件，一般是由于通过指定的镜像启动容器后，容器内部没有常驻进程，导致容器启动成功后即退出，从而进行了持续的重启。

```shell
image: xxx:v1
# image 下添加如下
command: ["/bin/bash", "-ce", "tail -f /dev/null"]
```

## 1.3、certificate signed by unknown authority

&emsp;&emsp;k8s提示certificate signed by unknown authority (possibly because of "crypto/rsa: verification error" while trying to verify candidate authority certificate "kubernetes")

&emsp;&emsp;原因：这是在重新创建集群之前，原来集群的rm -rf $HOME/.kube文件没有删除，所以导致了认证失去作用。

```shell
# 1、删除这个路径下的文件
rm -rf $HOME/.kube

# 2、重新执行命令
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```