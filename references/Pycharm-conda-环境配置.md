# 参考文献

# 一、conda 创建新环境

```shell
# 创建环境
conda create -n owl python=3.11

# 激活环境
conda activate owl
```

![环境准备](./pictures/Pycharm-conda-环境配置/img.png)

> `*` 标识该环境被激活

# 二、Pycharm 绑定已有 conda 环境

![环境绑定](./pictures/Pycharm-conda-环境配置/img_1.png)