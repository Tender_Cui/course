# 参考文献
* [JDK17在Windows安装以及环境变量配置（超详细的教程）](https://blog.csdn.net/tiehou/article/details/129575138)

# 一、下载

&emsp;&emsp;[jdk17的下载地址](https://www.oracle.com/java/technologies/downloads/#jdk17-linux)

![jdk17下载选择](./pictures/jdk17_Centos7/img.png)

# 二、安装

&emsp;&emsp;上传 jdk-17_linux-x64_bin.tar.gz 文件到 /opt/softs/ 目录

> 解压缩

```shell
tar -zxvf jdk-17_linux-x64_bin.tar.gz -C ./../installs/
```

> 配置环境变量

```shell
export JAVA_HOME=/opt/installs/jdk-17.0.9
export JRE_HOME=/opt/installs/jdk-17.0.9/jre
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib:$CLASSPATH
export PATH=$JAVA_HOME/bin:$PATH
```

> 刷新环境变量

```shell
source /etc/profile
```

> 验证结果

![jdk17安装结果验证](./pictures/jdk17_Centos7/img_1.png)


