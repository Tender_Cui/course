# 参考文献
* [springcloud系列之eureka集群和ribbon 负载均衡](https://blog.csdn.net/weixin_43610698/article/details/108137596)

# 1、pom.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.tender</groupId>
        <artifactId>shiro-parent</artifactId>
        <version>0.0.1-SNAPSHOT</version>
        <relativePath>../../pom.xml</relativePath>
    </parent>

    <artifactId>shiro-eureka-consumer</artifactId>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>
    </dependencies>

</project>
```

# 2、`yaml`配置文件

```yaml
server:
  port: 9205

spring:
  application:
    # 此实例注册到 eureka 服务端的name
    name: shiro-eureka-consumer

eureka:
  client:
    serviceUrl:
      # eureka服务端提供的注册地址 参考服务端配置的这个路径
      defaultZone: http://eureka.server1:9101/eureka,http://eureka.server2:9102/eureka,http://eureka.server3:9103/eureka
  instance:
    # 此实例注册到eureka服务端的唯一的实例ID
    instance-id: shiro-eureka-provider-1
    # 是否显示IP地址
    prefer-ip-address: true
    # eureka客户需要多长时间发送心跳给eureka服务器，表明它仍然活着,默认为30 秒 (与下面配置的单位都是秒)
    leaseRenewalIntervalInSeconds: 10
    # eureka服务器在接收到实例的最后一次发出的心跳后，需要等待多久才可以将此实例删除，默认为90秒
    leaseExpirationDurationInSeconds: 30

```

# 3、启动类

```java
package com.tender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
public class ShiroEurekaConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShiroEurekaConsumerApplication.class);
    }
}

```

# 4、配置类

```java
package com.tender.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ConfigBean {

    @Bean
    @LoadBalanced  //使用@LoadBalanced注解赋予RestTemplate负载均衡的能力 默认轮询
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}

```

# 5、Controller 类

```java
package com.tender.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class EurekaConsumerController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/test/port")
    public String getPort() {
        return restTemplate.getForObject("http://SHIRO-EUREKA-PROVIDER/port", String.class);
    }
}
```

# 6、启动并测试

&emsp;&emsp;结果如下，轮询地访问

![结果1](./pictures/springboot-eureka-consumer/img.png)

![结果2](./pictures/springboot-eureka-consumer/img_1.png)