# 一、jar 包说明

## 1.1、dockerfile-maven-plugin-1.4.13.jar

```shell
<plugin>
  <groupId>com.spotify</groupId>
  <artifactId>dockerfile-maven-plugin</artifactId>
  <version>1.4.13</version>
  <executions>
    <execution>
      <id>default</id>
      <goals>
        <goal>build</goal>
        <goal>push</goal>
      </goals>
    </execution>
  </executions>
  <configuration>
    <repository>config-server</repository>
    <tag>${project.version}</tag>
    <buildArgs>
      <JAR_FILE>${project.build.finalName}.jar</JAR_FILE>
    </buildArgs>
  </configuration>
</plugin>
```

> 安装到本地

```shell
# 到 jar 包所在的路径，执行如下命令即可
mvn install:install-file -Dfile=dockerfile-maven-plugin-1.4.13.jar -DgroupId=com.spotify -DartifactId=dockerfile-maven-plugin -Dversion=1.4.13 -Dpackaging=jar
```

&emsp;&emsp;这是用来给jar 打镜像用的。   
&emsp;&emsp;[参考文献](https://zhuanlan.zhihu.com/p/90122357)

